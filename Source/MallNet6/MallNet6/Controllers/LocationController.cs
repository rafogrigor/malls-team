using AutoMapper;
using DAL.Common;
using DAL.Interfaces;
using DAL.Models;
using MallNet6.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MallNet6.Controllers;

[Route("api/[controller]")]
[ApiController]
[Authorize]
public class LocationController : ControllerBase
{
    private readonly ILocation locationRepository;
    private readonly IMapper mapper;
    public LocationController(ILocation locationRepository, IMapper mapper)
    {
        this.locationRepository = locationRepository;
        this.mapper = mapper;
    }

    [HttpPost("Country")]
    public async Task<ActionResult<List<CountryAPI>>> Country([FromHeader] string culture)
    {

        if (!Enum.TryParse(culture.ToUpper(), out CultureMode cultureResult) || !ModelState.IsValid)
        {
            return UnprocessableEntity();
        }

        List<DAL.Models.CountryDal> result = await locationRepository.GetCountries(cultureResult);
        if (result != null)
        {
            return Ok(mapper.Map<List<CountryAPI>>(result));
        }

        return NoContent();
    }

    [HttpPost("Region")]
    public async Task<ActionResult<List<RegionDal>>> Region([FromHeader] string culture, CountryID countryID)
    {

        if (!Enum.TryParse(culture.ToUpper(), out CultureMode cultureResult) || !ModelState.IsValid)
        {
            return UnprocessableEntity();
        }

        List<RegionDal> result = await locationRepository.GetRegions(cultureResult, countryID.ID);
        if (result != null)
        {
            return Ok(mapper.Map<List<RegionAPI>>(result));
        }

        return NoContent();
    }

    [HttpPost("City")]
    public async Task<ActionResult<List<CityDal>>> City([FromHeader] string culture, RegionID regionID)
    {

        if (!Enum.TryParse(culture.ToUpper(), out CultureMode cultureResult) || !ModelState.IsValid)
        {
            return UnprocessableEntity();
        }

        List<CityDal> result = await locationRepository.GetCities(cultureResult, regionID.ID);
        if (result != null)
        {
            return Ok(mapper.Map<List<CityAPI>>(result));
        }

        return NoContent();
    }
}