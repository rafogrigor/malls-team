using AutoMapper;
using DAL.Common;
using DAL.Interfaces;
using MallNet6.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MallNet6.Controllers;

[Route("api/[controller]")]
[ApiController]
[Authorize]
public class VehicleController : ControllerBase
{
    private readonly IVehicle vehicleRepository;
    private readonly IMapper mapper;
    public VehicleController(IVehicle vehicleRepository, IMapper mapper)
    {
        this.vehicleRepository = vehicleRepository;
        this.mapper = mapper;
    }
    [HttpGet("Mileage")]
    public async Task<ActionResult<List<MileageAPI>>> Mileage()
    {
        List<MileageAPI> result = mapper.Map<List<MileageAPI>>(await vehicleRepository.GetMileage());
        if (result != null && result.Count != 0)
        {
            return Ok(result);
        }
        return NoContent();
    }
    [HttpGet("EngineSize")]
    public async Task<ActionResult<List<EngineSizeAPI>>> EngineSize()
    {
        List<EngineSizeAPI> result = mapper.Map<List<EngineSizeAPI>>(await vehicleRepository.GetEngineSize());
        if (result != null && result.Count != 0)
        {
            return Ok(result);
        }
        return NoContent();
    }
    [HttpGet("BodyType")]
    public async Task<ActionResult<List<BodyTypeAPI>>> BodyType([FromHeader] string culture)
    {
        CultureMode currentCulture = CultureMode.AM;
        if (culture != null)
        {
            Enum.TryParse(culture.ToUpper(), out currentCulture);
        }
        List<BodyTypeAPI> result = mapper.Map<List<BodyTypeAPI>>(await vehicleRepository.GetBodyType(currentCulture));
        if (result != null && result.Count != 0)
        {
            return Ok(result);
        }
        return NoContent();
    }
    [HttpGet("Color")]
    public async Task<ActionResult<List<MarkAPI>>> Color([FromHeader] string culture)
    {
        CultureMode currentCulture = CultureMode.AM;
        if (culture != null)
        {
            Enum.TryParse(culture.ToUpper(), out currentCulture);
        }
        List<ColorAPI> result = mapper.Map<List<ColorAPI>>(await vehicleRepository.GetColor(currentCulture));
        if (result != null && result.Count != 0)
        {
            return Ok(result);
        }
        return NoContent();
    }
    [HttpGet("DriveType")]
    public async Task<ActionResult<List<MarkAPI>>> DriveType([FromHeader] string culture)
    {
        CultureMode currentCulture = CultureMode.AM;
        if (culture != null)
        {
            Enum.TryParse(culture.ToUpper(), out currentCulture);
        }
        List<DriveTypeAPI> result = mapper.Map<List<DriveTypeAPI>>(await vehicleRepository.GetDriveType(currentCulture));
        if (result != null && result.Count != 0)
        {
            return Ok(result);
        }
        return NoContent();
    }
    [HttpGet("Mark")]
    public async Task<ActionResult<List<MarkAPI>>> Mark()
    {
        List<MarkAPI> result = mapper.Map<List<MarkAPI>>(await vehicleRepository.GetMarks());
        if (result != null && result.Count != 0)
        {
            return Ok(result);
        }
        return NoContent();
    }
    [HttpGet("Model")]
    public async Task<ActionResult<List<ModelAPI>>> Model(int markID = -1)
    {
        List<ModelAPI> result = mapper.Map<List<ModelAPI>>(await vehicleRepository.GetModels(markID));
        if (result != null && result.Count != 0)
        {
            return Ok(result);
        }
        return NoContent();
    }
    [HttpGet("TransmissionType")]
    public async Task<ActionResult<List<ModelAPI>>> TransmissionType([FromHeader] string culture)
    {
        CultureMode currentCulture = CultureMode.AM;
        if (culture != null)
        {
            Enum.TryParse(culture.ToUpper(), out currentCulture);
        }
        List<TransmissionTypeAPI> result = mapper.Map<List<TransmissionTypeAPI>>(await vehicleRepository.GetTransmissionType(currentCulture));
        if (result != null && result.Count != 0)
        {
            return Ok(result);
        }
        return NoContent();
    }
    [HttpGet("EngineType")]
    public async Task<ActionResult<List<ModelAPI>>> EngineType([FromHeader] string culture)
    {
        CultureMode currentCulture = CultureMode.AM;
        if (culture != null)
        {
            Enum.TryParse(culture.ToUpper(), out currentCulture);
        }
        List<EngineTypeAPI> result = mapper.Map<List<EngineTypeAPI>>(await vehicleRepository.GetEngineType(currentCulture));
        if (result != null && result.Count != 0)
        {
            return Ok(result);
        }
        return NoContent();
    }
    [HttpGet("WheelType")]
    public async Task<ActionResult<List<ModelAPI>>> WheelType([FromHeader] string culture)
    {
        CultureMode currentCulture = CultureMode.AM;
        if (culture != null)
        {
            Enum.TryParse(culture.ToUpper(), out currentCulture);
        }
        List<WheelAPI> result = mapper.Map<List<WheelAPI>>(await vehicleRepository.GetWheelType(currentCulture));
        if (result != null && result.Count != 0)
        {
            return Ok(result);
        }
        return NoContent();
    }
}