using System.ComponentModel.DataAnnotations;
using AutoMapper;
using DAL.Common;
using DAL.Models;
using DAL.Repositories;
using MallNet6.Models;
using MallNet6.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace MallNet6.Controllers;

[Route("api/[controller]")]
[ApiController]
// [Authorize]
public class UserController : ControllerBase
{
    private readonly UserManager<UserDal> userManager;
    private readonly IMapper mapper;
    private readonly UserRepository userRepo;
    private readonly GeneralService generalService;
    public UserController(UserManager<UserDal> userManager, IMapper mapper,
                                                                UserRepository userRepo, GeneralService generalService)
    {
        this.mapper = mapper;
        this.userManager = userManager;
        this.userRepo = userRepo;
        this.generalService = generalService;
    }
    [HttpPost("CreateUser")]
    public async Task<ActionResult<object>> CreateUser([FromBody] UserCreate userData)
    {
        if (!ModelState.IsValid)
        {
            return UnprocessableEntity();
        }
        UserResponse temp = mapper.Map<UserResponse>(await userManager.FindByNameAsync(userData.Email));
        if (temp != null && !temp.IsConfirmed)
        {
            ModelState.AddModelError(CommonConstants.SignUp, "User just registered but not confirmed");
            return UnprocessableEntity(ModelState);
        }
        IdentityResult result = await userManager.CreateAsync(mapper.Map<UserDal>(userData), userData.Password);
        if (result != IdentityResult.Success)
        {
            ModelState.AddModelError(CommonConstants.UserRegError, string.Join(' ', result.Errors.SelectMany(i => i.Description)));
            return BadRequest(ModelState);
        }
        temp = mapper.Map<UserResponse>(await userManager.FindByNameAsync(userData.PhoneNumber));
        if (temp == null)
        {
            ModelState.AddModelError(CommonConstants.SignUp, Resources.UserNotCreated);
            return BadRequest(ModelState);
        }
        return Created(CommonConstants.UserByPhoneNumber, new { email = userData.Email });
    }
    [HttpPost("SendConfirmEmail")]
    public async Task<ActionResult> SendConfirmEmail(EmailConfirmationModelAPI emailTo)
    {
        UserDal temp = await userManager.FindByNameAsync(emailTo.Email);
        if (temp == null)
        {
            ModelState.AddModelError(CommonConstants.Authorization, string.Format(Resources.UserNotFound, emailTo.Email));
            return NotFound(ModelState);
        }
        if (temp.IsConfirmed)
        {
            ModelState.AddModelError(CommonConstants.Confirm, string.Format(Resources.UserJustConfirmed, emailTo.Email));
            return BadRequest(ModelState);
        }
        int code = await EmailService.SendMail(new EmailStruct()
        {
            Email = emailTo.Email,
            Subject = CommonConstants.EmailConfirmation,
            Body = Resources.SendConfirmMail
        });
        if (code == -1)
        {
            return BadRequest();
        }
        EmailConfirmationModelAPI emailConfirmationModelAPI = new EmailConfirmationModelAPI
        {
            Email = emailTo.Email,
            ConfirmationCode = code
        };
        if (!await userRepo.AddEmailConfirmationCodeToTable(mapper.Map<EmailConfirmationModelDAL>(emailConfirmationModelAPI)))
        {
            return BadRequest();
        };
        return Ok("Confirmation code was sended");
    }
    [HttpPost("CheckEmailConfirmationCode")]
    public async Task<ActionResult> CheckEmailConfirmationCode([FromBody] EmailConfirmationModelAPI emailConfirmationModel)
    {
        if (!await userRepo.CheckEmailConfirmationCode(mapper.Map<EmailConfirmationModelDAL>(emailConfirmationModel)))
        {
            return BadRequest("Your confirmation code is not valid,or you have a problem with server:");
        }
        UserDal temp = await userManager.FindByNameAsync(emailConfirmationModel.Email);
        if (temp != null && await userRepo.EmailConfirmAsync(emailConfirmationModel.Email) == IdentityResult.Success)
        {
            if (temp.IsConfirmed)
            {
                ModelState.AddModelError(CommonConstants.Login, Resources.UserJustConfirmed);
                return BadRequest(ModelState);
            }
        }
        return Ok("Confirmed");
    }
    [HttpPost("ForgotPassword")]
    public async Task<ActionResult<int>> SendForgotPasswordMail([FromBody] EmailConfirmationModelAPI email)
    {
        if (!ModelState.IsValid)
        {
            ModelState.AddModelError(CommonConstants.Email, Resources.InvalidEmail);
            return UnprocessableEntity(ModelState);
        }
        UserDal temp = await userManager.FindByNameAsync(email.Email);
        if (temp == null)
        {
            ModelState.AddModelError(CommonConstants.Authorization, string.Format(Resources.UserNotFound, email));
            return NotFound(ModelState);
        }
        else if (!temp.IsConfirmed)
        {
            ModelState.AddModelError(CommonConstants.Login, Resources.UnconfirmedUser);
        }
        else if (temp.State == UserState.DeactivatedUser)
        {
            ModelState.AddModelError(CommonConstants.Login, Resources.DeactivatedUser);
        }
        else
        {
            int code = await EmailService.SendMail(new EmailStruct()
            {
                Email = email.Email,
                Subject = CommonConstants.ForgotPassword,
                Body = Resources.ForgotPasswordMail
            });
            if (code == -1)
            {
                return BadRequest();
            }
            EmailConfirmationModelAPI emailConfirmationModelAPI = new EmailConfirmationModelAPI
            {
                Email = email.Email,
                ConfirmationCode = code
            };
            if (!await userRepo.AddEmailConfirmationCodeToTable(mapper.Map<EmailConfirmationModelDAL>(emailConfirmationModelAPI)))
            {
                return BadRequest();
            };
            // return Ok("Confirmation code was sended");
            return Ok(new { confirmCode = code });
        }
        return BadRequest(ModelState);
    }
    [HttpPut("ResetPassword")]
    public async Task<ActionResult> ResetPassword(UserResetPassword user)
    {
        if (!ModelState.IsValid)
        {
            return UnprocessableEntity();
        }
        UserDal temp = await userManager.FindByNameAsync(user.Email);
        if (temp == null)
        {
            ModelState.AddModelError(CommonConstants.Authorization, string.Format(Resources.UserNotFound, user.Email));
            return NotFound(ModelState);
        }
        else if (!temp.IsConfirmed)
        {
            ModelState.AddModelError(CommonConstants.Login, Resources.UnconfirmedUser);
            return BadRequest(ModelState);
        }
        else if (temp.State == UserState.DeactivatedUser)
        {
            ModelState.AddModelError(CommonConstants.Login, Resources.DeactivatedUser);
            return BadRequest(ModelState);
        }
        else
        {
            EmailConfirmationModelAPI emailConfirmationModel = new EmailConfirmationModelAPI
            {
                Email = user.Email,
                ConfirmationCode = user.ConfirmationCode
            };
            if (!await userRepo.CheckEmailConfirmationCode(mapper.Map<EmailConfirmationModelDAL>(emailConfirmationModel)))
            {
                return BadRequest("Your confirmation code is not valid,or you have a problem with server:");
            }
            string passwordHash = userManager.PasswordHasher.HashPassword(temp, user.Password);
            temp.Password = passwordHash;
            if (await userRepo.RestorePassword(temp) == IdentityResult.Success)
            {
                return Ok(new { phoneNumber = temp.PhoneNumber });
            }
        }
        return BadRequest();
    }
    [HttpPut("Confirm")]
    public async Task<ActionResult> ConfirmUser([FromBody] EmailConfirmationModelAPI email)
    {
        if (!ModelState.IsValid)
        {
            ModelState.AddModelError(CommonConstants.Email, Resources.InvalidEmail);
            return UnprocessableEntity(ModelState);
        }
        UserDal temp = await userManager.FindByNameAsync(email.Email);
        if (temp != null && await userRepo.EmailConfirmAsync(email.Email) == IdentityResult.Success)
        {
            if (temp.IsConfirmed)
            {
                ModelState.AddModelError(CommonConstants.Login, Resources.UserJustConfirmed);
                return BadRequest(ModelState);
            }
            return Ok();
        }
        return NotFound();
    }
    [HttpPut("Data")]
    public async Task<ActionResult<object>> UpdateUser([FromBody] UserUpdateData user)
    {
        if (!ModelState.IsValid)
        {
            return UnprocessableEntity(null);
        }
        UserDal temp = await userManager.FindByNameAsync(user.PhoneNumber);
        if (temp == null)
        {
            ModelState.AddModelError(CommonConstants.Authorization, string.Format(Resources.UserNotFound, user.PhoneNumber));
            return NotFound(ModelState);
        }
        else if (!temp.IsConfirmed)
        {
            ModelState.AddModelError(CommonConstants.Login, Resources.UnconfirmedUser);
            return BadRequest(ModelState);
        }
        else if (temp.State == UserState.DeactivatedUser)
        {
            ModelState.AddModelError(CommonConstants.Login, Resources.DeactivatedUser);
            return BadRequest(ModelState);
        }
        else if (await userManager.CheckPasswordAsync(temp, user.Password))
        {
            UserDal tempUser = mapper.Map<UserDal>(user);
            tempUser.PhoneNumber = user.PhoneNumber;
            tempUser.Password = null;
            tempUser.Avatar = (await generalService.GetImageUrlByImageBase64(new List<string>() { tempUser.Avatar }, "Users")).FirstOrDefault();
            IdentityResult result = await userManager.UpdateAsync(tempUser);
            if (result != IdentityResult.Success)
            {
                ModelState.AddModelError(CommonConstants.UserUpdateError, string.Join(' ', result.Errors.SelectMany(i => i.Description)));
                return BadRequest(ModelState);
            }
            return Ok(new { phonenumber = user.PhoneNumber });
        }
        return Unauthorized(new { phonenumber = string.Empty });
    }
    [HttpPut("Password")]
    public async Task<ActionResult<object>> Put(UserUpdatePassword user)
    {
        if (!ModelState.IsValid)
        {
            return UnprocessableEntity();
        }
        UserDal temp = await userManager.FindByNameAsync(user.PhoneNumber);
        if (temp == null)
        {
            ModelState.AddModelError(CommonConstants.Authorization, string.Format(Resources.UserNotFound, user.PhoneNumber));
            return NotFound(ModelState);
        }
        else if (!temp.IsConfirmed)
        {
            ModelState.AddModelError(CommonConstants.Login, Resources.UnconfirmedUser);
            return BadRequest(ModelState);
        }
        else if (temp.State == UserState.DeactivatedUser)
        {
            ModelState.AddModelError(CommonConstants.Login, Resources.DeactivatedUser);
            return BadRequest(ModelState);
        }
        else
        {
            if (await userManager.ChangePasswordAsync(temp, user.OldPassword, user.NewPassword) == IdentityResult.Success)
            {
                return Ok(new { phonenumber = user.PhoneNumber });
            }
        }
        return BadRequest();
    }
    [HttpPut("Rating")]
    public async Task<object> RateUserAsync([FromBody] UserRateAPI userRate)
    {
        if (!ModelState.IsValid)
        {
            return UnprocessableEntity();
        }
        if (!await userRepo.RateUserAsync(mapper.Map<UserRateDal>(userRate)))
        {
            return BadRequest();
        }
        return Ok();
    }
    [HttpPost("AddPrefernce")]
    public async Task<ActionResult<object>> AddPreference([FromBody] PreferenceAPI preference)
    {
        if (!ModelState.IsValid)
        {
            return UnprocessableEntity();
        }
        if (!await userRepo.AddPreferences(mapper.Map<PreferenceDal>(preference)))
        {
            return BadRequest();
        }
        return Ok();
    }
    [HttpDelete]
    public async Task<ActionResult<object>> Delete(UserIdentity user)
    {
        if (!ModelState.IsValid)
        {
            return UnprocessableEntity(false);
        }
        UserDal temp = await userManager.FindByNameAsync(user.UserName);
        if (temp == null)
        {
            ModelState.AddModelError(CommonConstants.Authorization, string.Format(Resources.UserNotFound, user.UserName));
            return NotFound(ModelState);
        }
        else if (await userManager.CheckPasswordAsync(temp, user.Password))
        {
            IdentityResult result = await userManager.DeleteAsync(new UserDal() { PhoneNumber = user.UserName });
            if (result != IdentityResult.Success)
            {
                ModelState.AddModelError(CommonConstants.UserDeleteError, string.Join(' ', result.Errors.SelectMany(i => i.Description)));
                return BadRequest(ModelState);
            }
            return Ok(new { result = true });
        }
        return BadRequest();
    }
    [HttpPost("GetUser")]
    public new async Task<ActionResult<User>> User([FromHeader] string culture, [FromBody] UserInfoGet userInfo)
    {
        CultureMode currentCulture = CultureMode.AM;
        if (culture != null)
        {
            Enum.TryParse(culture.ToUpper(), out currentCulture);
        }
        UserDal temp = await userRepo.GetUserByIDCultured(userInfo.UserID, currentCulture);
        if (temp.Result.Status == 1)
        {
            return Ok(mapper.Map<User>(temp));
        }
        return BadRequest(temp);
    }
    [HttpGet("Support")]
    public async Task<ActionResult<User>> GetMessages([Required] int userID)
    {
        SupportMessage result = await userRepo.GetSupportMessage(userID);
        if (result.Result.Status == CommonConstants.Ok)
        {
            return Ok(result);
        }
        return BadRequest(result);
    }
    [HttpOptions]
    public void Options()
    {
        HttpContext.Response.Headers.Add("Allows", "GET, POST ,PUT ,DELETE ,OPTIONS");
    }
}