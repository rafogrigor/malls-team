using AutoMapper;
using DAL.Common;
using DAL.Interfaces;
using DAL.Models;
using MallNet6.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MallNet6.Controllers;

[Route("api/[controller]")]
[ApiController]
[Authorize]
public class CategoriesController : ControllerBase
{
    private readonly ICategories categories;
    private readonly IMapper mapper;
    public CategoriesController(ICategories categories, IMapper mapper)
    {
        this.mapper = mapper;
        this.categories = categories;
    }

    [HttpGet("MainCategories")]
    public async Task<ActionResult<List<MainCategoryDal>>> GetMainCategories([FromHeader] string culture)
    {
        CultureMode currentCulture = CultureMode.AM;
        if (culture != null)
        {
            _ = Enum.TryParse(culture.ToUpper(), out currentCulture);
        }

        List<MainCategoryDal> result = await categories.GetMainCategoriesAsync(currentCulture);

        if (result.Count != 0)
        {
            return Ok(mapper.Map<List<MainCategory>>(result));
        }

        return NoContent();
    }

    [HttpPost("SubCategories")]
    public async Task<ActionResult<List<SubCategoryDal>>> GetSubCategories([FromHeader] string culture, [FromBody] CategoryId id)
    {
        CultureMode currentCulture = CultureMode.AM;
        if (culture != null)
        {
            _ = Enum.TryParse(culture.ToUpper(), out currentCulture);
        }

        List<SubCategoryDal> result = await categories.GetSubCategoriesAsync(currentCulture, id.Value);

        if (result.Count != 0)
        {
            return Ok(mapper.Map<List<SubCategory>>(result));
        }

        return NoContent();
    }

    [HttpOptions]
    public void Options()
    {
        HttpContext.Response.Headers.Add("Allows", "GET, POST, OPTIONS");
    }
}