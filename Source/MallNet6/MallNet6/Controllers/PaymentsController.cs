using AutoMapper;
using DAL.Common;
using DAL.Interfaces;
using DAL.Models;
using MallNet6.Models;
using Microsoft.AspNetCore.Mvc;

namespace MallNet6.Controllers;

[Route("api/[controller]")]
[ApiController]
public class PaymentsController : ControllerBase
{
    private readonly IPayments paymentsRepo;
    private readonly IMapper mapper;
    public PaymentsController(IPayments payments, IMapper mapper)
    {
        this.mapper = mapper;
        paymentsRepo = payments;
    }
    [HttpGet("TelcellUser")]
    public async Task<ActionResult<TelcellUserAPI>> GetTelcelUser([FromQuery] int userID)
    {
        TelcellUserDAL result = await paymentsRepo.GetUserPaymentModelByID(userID);
        if (result != null && result.ID != 0)
        {
            return Ok(mapper.Map<TelcellUserAPI>(result));
        }
        return NoContent();
    }
    [HttpGet("CheckTelcellPayment")]
    public async Task<ActionResult<bool>> CheckPayment([FromQuery] long receipt)
    {
        bool result = await paymentsRepo.CheckPayment(receipt);
        if (result == true)
        {
            return Ok();
        }
        return BadRequest(ModelState);
    }
    [HttpPost("CreateTelcellPayment")]
    public async Task<ActionResult<int>> CreatePayment([FromBody] TelcellPaymentModelAPI paymentModel)
    {
        if (!ModelState.IsValid)
        {
            return UnprocessableEntity();
        }
        int result = await paymentsRepo.CreatePayment(mapper.Map<TelcellPaymentModelDAL>(paymentModel));
        if (result != CommonConstants.BadID)
        {
            return Created(Resources.GetPaymentWithID, new
            {
                ID = result
            });
        }
        ModelState.AddModelError(Resources.OcuredError, Resources.PaymentCreatingError);
        return BadRequest(ModelState);
    }
}