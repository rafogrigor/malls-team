using AutoMapper;
using DAL.Common;
using DAL.Interfaces;
using DAL.Models;
using MallNet6.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MallNet6.Controllers;

[Route("api/[controller]")]
[ApiController]
[Authorize]
public class TariffController : ControllerBase
{
    private readonly ITariff tariffRepository;
    private readonly IMapper mapper;
    public TariffController(ITariff tariffRepository, IMapper mapper)
    {
        this.mapper = mapper;
        this.tariffRepository = tariffRepository;
    }
    [HttpGet]
    public async Task<ActionResult<List<TariffAPI>>> Tariff([FromHeader] string culture)
    {
        CultureMode currentCulture = CultureMode.AM;
        if (culture != null)
        {
            Enum.TryParse(culture.ToUpper(), out currentCulture);
        }
        List<TariffAPI> tariff = mapper.Map<List<TariffAPI>>(await tariffRepository.GetTariffs(currentCulture));
        if (tariff == null || tariff.Count == 0)
        {
            return NoContent();
        }
        return Ok(tariff);
    }
    [HttpPost("PurchaseLoaderAd")]
    public async Task<ActionResult<Result>> LoaderAd(PurchaseLoaderAdAPI data)
    {
        Result result = await tariffRepository.AddLoaderAd(mapper.Map<PurchaseLoaderAdDAL>(data));
        if (result.Status == CommonConstants.Ok)
        {
            return Ok(result);
        }
        return BadRequest(result);
    }
}