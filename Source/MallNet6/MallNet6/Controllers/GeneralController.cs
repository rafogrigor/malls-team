using AutoMapper;
using DAL.Common;
using DAL.Interfaces;
using DAL.Models;
using MallNet6.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MallNet6.Controllers;

[Route("api/[controller]")]
[ApiController]
[Authorize]
public class GeneralController : ControllerBase
{
    private readonly IMainPage mpRepository;
    private readonly IMapper mapper;

    public GeneralController(IMainPage mpRepository, IMapper mapper)
    {
        this.mpRepository = mpRepository;
        this.mapper = mapper;
    }

    [HttpGet("MainPage")]
    public async Task<ActionResult<MainPageAPI>> Get([FromHeader] int countryID, [FromHeader] string culture, [FromHeader] Currency currency)
    {
        CultureMode currentCulture = CultureMode.AM;
        if (culture != null)
        {
            _ = Enum.TryParse(culture.ToUpper(), out currentCulture);
        }
        MainPageAPI main = mapper.Map<MainPageAPI>(await mpRepository.GetMainPage(countryID, currentCulture, currency));

        if (main != null)
        {
            return Ok(main);
        }

        return NoContent();
    }

    [HttpPost("Prompt")]
    public async Task<ActionResult<Prompts>> Prompt(GetPromptAPI searchPromptBody)
    {
        if (!ModelState.IsValid)
        {
            return UnprocessableEntity();
        }

        if (string.IsNullOrWhiteSpace(searchPromptBody.Symbol))
        {
            return Ok(new Prompts() { Descriptions = new List<Search>() });
        }

        Prompts result = new()
        {
            Descriptions = (await mpRepository.GetPrompts(mapper.Map<GetPromptDal>(searchPromptBody))).Select(i => new Search() { SearchString = i }).ToList()
        };

        if (result.Descriptions.Count != 0)
        {
            return Ok(result);
        }

        return NoContent();
    }


    [HttpPost("Search")]
    public async Task<ActionResult<List<SearchModelAPI>>> Search(SearchGetAPI searchBody)
    {
        if (!ModelState.IsValid)
        {
            return UnprocessableEntity();
        }

        List<SearchModelAPI> result = mapper.Map<List<SearchModelAPI>>(await mpRepository.Search(mapper.Map<SearchGetDal>(searchBody)));

        if (result.Count != 0)
        {
            return Ok(result);
        }

        return NoContent();
    }
}