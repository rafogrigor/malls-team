using AutoMapper;
using DAL.Common;
using DAL.Interfaces;
using DAL.Models;
using MallNet6.Models;
using MallNet6.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MallNet6.Controllers;

[Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AdController : ControllerBase
    {
        private readonly IAds adRepository;
        private readonly IMapper mapper;
        private readonly GeneralService generalService;

        public AdController(IAds adRepository, IMapper mapper,
                                              GeneralService generalService)
        {
            this.adRepository = adRepository;
            this.mapper = mapper;
            this.generalService = generalService;
        }
        #region "AddingAds"

        [HttpPost("RealEstate")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(RealEstateAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1,
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "RealEstate");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<RealEstateDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.RealEstate;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }

        [HttpPost("Appliance")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(ApplianceAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1,
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "Appliance");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<ApplianceDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.Appliances;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }

        [HttpPost("HouseholdGoods")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(HouseholdGoodAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1,
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "HouseholdGoods");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<HouseholdGoodDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.HouseholdGoods;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }

        [HttpPost("ClothesAndShoes")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(ClothesAndShoesAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1,
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "ClothesAndShoes");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<ClothesAndShoesDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.ClothesAndShoes;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));
            return Ok(result);
        }

        [HttpPost("ForChildren")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(ForChildrenAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1,
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "ForChildren");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<ForChildrenDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.ForChildren;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }

        [HttpPost("JewerlyAndAccessories")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(JewerlyAndAccessoriesAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1,
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "JewerlyAndAccessories");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<JewerlyAndAccessoriesDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.JewerlyAndAccessories;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }

        [HttpPost("Construction")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAdd(ConstructionAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1,
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "Construction");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<ConstructionDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.Construction;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }

        [HttpPost("AllForHomeAndGarden")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(AllForHomeAndGardenAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1,
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "AllForHomeAndGarden");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<AllForHomeAndGardenDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.AllForHomeAndGarden;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }

        [HttpPost("Electronics")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(ElectronicsAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1,
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "Electronics");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<ElectronicsDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.Electronics;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }

        [HttpPost("ProductsAndDrinks")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(ProductsAndDrinksAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "ProductsAndDrinks");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<ProductsAndDrinksDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.ProductsAndDrinks;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }

        [HttpPost("Furniture")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(FurnitureAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "Furniture");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<FurnitureDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.Furniture;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }

        [HttpPost("PetsAndPlants")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(PetsAndPlantsAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                result.Result = new Result()
                {
                    Status = CommonConstants.ImageAddingError,
                    Message = CommonConstants.ImageAddingErrorMessage
                };

                return BadRequest(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "PetsAndPlants");
                if (ad.ImagesList.Count == 0)
                {
                    return UnprocessableEntity(Resources.ErrorAddingImages);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<PetsAndPlantsDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.PetsAndPlants;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }

        [HttpPost("Culture")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(CultureAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "Culture");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<CultureDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.Culture;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }

        [HttpPost("Work")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(WorkAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "Work");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<WorkDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.Work;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }

        [HttpPost("Services")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(ServicesAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "Services");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<ServicesDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.Services;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));
            return Ok(result);
        }

        [HttpPost("Acquaintance")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(AcquaintanceAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "Acquaintance");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<AcquaintanceDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.Acquaintance;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }

        [HttpPost("HealfCare")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(HealfCareAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "HealfCare");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<HealfCareDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.HealfCare;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }

        [HttpPost("Sport")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(SportAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "Sport");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<SportDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.Sport;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }

        [HttpPost("Tourism")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(TourismAndRestAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "TourismAndRest");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<TourismAndRestDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.TourismAndRest;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }

        [HttpPost("CigaretteAndAlcohol")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(CigaretteAndAlcoholAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "CigaretteAndAlcohol");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<CigaretteAndAlcoholDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.CigaretteAndAlcohol;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }

        [HttpPost("EverythingElse")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(EverythingElseAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "EverythingElse");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<EverythingElseDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.EverythingElse;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }

        [HttpPost("Vehicle")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(VehicleAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "Vehicle");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<VehicleDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.Vehicle;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }
        [HttpPost("Exchange")]
        public async Task<ActionResult<Models.AdCreateResponse>> CreateAd(ExchangeAPI ad)
        {
            Models.AdCreateResponse result = new()
            {
                ID = -1
            };

            if (!ModelState.IsValid)
            {
                result.Result = new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                };

                return UnprocessableEntity(result);
            }

            if (ad.ImagesList != null && ad.ImagesList.Count > 0)
            {
                ad.ImagesList = await generalService.GetImageUrlByImageBase64(ad.ImagesList, "Exchange");
                if (ad.ImagesList.Count == 0)
                {
                    result.Result = new Result()
                    {
                        Status = CommonConstants.ImageAddingError,
                        Message = CommonConstants.ImageAddingErrorMessage
                    };

                    return BadRequest(result);
                }
            }

            result = mapper.Map<Models.AdCreateResponse>(await adRepository.CreateAd(mapper.Map<ExchangeDal>(ad)));
            if (!result.Result.Status.Equals(CommonConstants.Ok))
            {
                return BadRequest(result);
            }

            UniversalDal temp = mapper.Map<UniversalDal>(ad);
            temp.AdType = AdType.Exchange;
            List<string> tokens = await adRepository.GetNotificationTokens(temp);
            await Task.Run(() => NotificationService.SendNotification(tokens));

            return Ok(result);
        }

        #endregion

        #region "GetAds"
        [HttpPost("GetWork")]
        public async Task<ActionResult<List<WorkAPI>>> Work([FromHeader] string culture, WorkGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<WorkDal> result = await adRepository.GetWork(mapper.Map<WorkGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<WorkAPI>>(result));
            }

            return NoContent();
        }

        [HttpPost("GetSport")]
        public async Task<ActionResult<List<SportAPI>>> Sport([FromHeader] string culture, SportGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<SportDal> result = await adRepository.GetSport(mapper.Map<SportGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<SportAPI>>(result));
            }

            return NoContent();
        }
        [HttpPost("GetSale")]
        public async Task<ActionResult<List<SaleAPI>>> Sale([FromHeader] string culture, SaleGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<SaleDal> result = await adRepository.GetSale(mapper.Map<SaleGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<SaleAPI>>(result));
            }

            return NoContent();
        }
        [HttpPost("GetEverythingElse")]
        public async Task<ActionResult<List<EverythingElseAPI>>> EverythingElse([FromHeader] string culture, EverythingElseGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<EverythingElseDal> result = await adRepository.GetEverythingElse(mapper.Map<EverythingElseGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<EverythingElseAPI>>(result));
            }

            return NoContent();
        }
        [HttpPost("GetPetsAndPlants")]
        public async Task<ActionResult<List<PetsAndPlantsAPI>>> PetsAndPlants([FromHeader] string culture, PetsAndPlantsGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<PetsAndPlantsDal> result = await adRepository.GetPetsAndPlants(mapper.Map<PetsAndPlantsGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<PetsAndPlantsAPI>>(result));
            }

            return NoContent();
        }
        [HttpPost("GetCulture")]
        public async Task<ActionResult<List<CultureAPI>>> Culture([FromHeader] string culture, CultureGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<CultureDal> result = await adRepository.GetCulture(mapper.Map<CultureGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<CultureAPI>>(result));
            }

            return NoContent();
        }
        [HttpPost("GetHealfCare")]
        public async Task<ActionResult<List<HealfCareAPI>>> Culture([FromHeader] string culture, HealfCareGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<HealfCareDal> result = await adRepository.GetHealfCare(mapper.Map<HealfCareGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<HealfCareAPI>>(result));
            }

            return NoContent();
        }
        [HttpPost("GetFurniture")]
        public async Task<ActionResult<List<FurnitureAPI>>> Culture([FromHeader] string culture, FurnitureGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (!string.IsNullOrWhiteSpace(culture))
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<FurnitureDal> result = await adRepository.GetFurniture(mapper.Map<FurnitureGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<FurnitureAPI>>(result));
            }

            return NoContent();
        }
        [HttpPost("GetForHomeAndGarden")]
        public async Task<ActionResult<List<AllForHomeAndGardenAPI>>> Culture([FromHeader] string culture, ForHomeAndGardenGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<AllForHomeAndGardenDal> result = await adRepository.GetForHomeAndGarden(mapper.Map<ForHomeAndGardenGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<AllForHomeAndGardenAPI>>(result));
            }

            return NoContent();
        }
        [HttpPost("GetHouseholdGoods")]
        public async Task<ActionResult<List<HouseholdGoodAPI>>> HouseholdGoods([FromHeader] string culture, HouseholdGoodGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<HouseholdGoodDal> result = await adRepository.GetHouseholdGoods(mapper.Map<HouseholdGoodGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<HouseholdGoodAPI>>(result));
            }

            return NoContent();
        }
        [HttpPost("GetConstruction")]
        public async Task<ActionResult<List<ConstructionAPI>>> Construction([FromHeader] string culture, ConstructionGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<ConstructionDal> result = await adRepository.GetConstructions(mapper.Map<ConstructionGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<ConstructionAPI>>(result));
            }

            return NoContent();
        }
        [HttpPost("GetElectronics")]
        public async Task<ActionResult<List<ElectronicsAPI>>> Electronics([FromHeader] string culture, ElectronicGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<ElectronicsDal> result = await adRepository.GetElectronics(mapper.Map<ElectronicGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<ElectronicsAPI>>(result));
            }

            return NoContent();
        }

        [HttpPost("GetAcquaintance")]
        public async Task<ActionResult<List<AcquaintanceAPI>>> Acquaintance([FromHeader] string culture, AcquaintanceGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<AcquaintanceDal> result = await adRepository.GetAcquaintance(mapper.Map<AcquaintanceGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<AcquaintanceAPI>>(result));
            }

            return NoContent();
        }
        [HttpPost("GetAppliances")]
        public async Task<ActionResult<List<ApplianceAPI>>> Appliances([FromHeader] string culture, ApplianceGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<ApplianceDal> result = await adRepository.GetAppliance(mapper.Map<ApplianceGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<ApplianceAPI>>(result));
            }

            return NoContent();
        }
        [HttpPost("GetCigaretteAndAlcohol")]
        public async Task<ActionResult<List<CigaretteAndAlcoholAPI>>> CigaretteAndAlcohol([FromHeader] string culture, CigaretteAndAlcoholGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<CigaretteAndAlcoholDal> result = await adRepository.GetCigaretteAndAlcohol(mapper.Map<CigaretteAndAlcoholGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<CigaretteAndAlcoholAPI>>(result));
            }

            return NoContent();
        }
        [HttpPost("GetClothesAndShoes")]
        public async Task<ActionResult<List<ClothesAndShoesAPI>>> ClothesAndShoes([FromHeader] string culture, ClothesAndShoesGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<ClothesAndShoesDal> result = await adRepository.GetClothesAndShoes(mapper.Map<ClothesAndShoesGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<ClothesAndShoesAPI>>(result));
            }

            return NoContent();
        }
        [HttpPost("GetForChildren")]
        public async Task<ActionResult<List<ForChildrenAPI>>> Appliances([FromHeader] string culture, ForChildrenGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<ForChildrenDal> result = await adRepository.GetForChildren(mapper.Map<ForChildrenGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<ForChildrenAPI>>(result));
            }

            return NoContent();
        }
        [HttpPost("GetJewerlyAndAccessories")]
        public async Task<ActionResult<List<JewerlyAndAccessoriesAPI>>> JewerlyAndAccessories([FromHeader] string culture, JewerlyAndAccessoriesGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<JewerlyAndAccessoriesDal> result = await adRepository.GetJewerlyAndAccessories(mapper.Map<JewerlyAndAccessoriesGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<JewerlyAndAccessoriesAPI>>(result));
            }

            return NoContent();
        }
        [HttpPost("GetProductsAndDrinks")]
        public async Task<ActionResult<List<ProductsAndDrinksAPI>>> ProductsAndDrinks([FromHeader] string culture, ProductsAndDrinksGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<ProductsAndDrinksDal> result = await adRepository.GetProductsAndDrinks(mapper.Map<ProductsAndDrinksGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<ProductsAndDrinksAPI>>(result));
            }

            return NoContent();
        }
        [HttpPost("GetRealEstate")]
        public async Task<ActionResult<List<RealEstateAPI>>> RealEstate([FromHeader] string culture, RealEstateGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<RealEstateDal> result = await adRepository.GetRealEstate(mapper.Map<RealEstateGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<RealEstateAPI>>(result));
            }

            return NoContent();
        }
        [HttpPost("GetServices")]
        public async Task<ActionResult<List<ServicesAPI>>> Services([FromHeader] string culture, ServicesGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<ServicesDal> result = await adRepository.GetServices(mapper.Map<ServicesGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<ServicesAPI>>(result));
            }

            return NoContent();
        }
        [HttpPost("GetTourismAndRest")]
        public async Task<ActionResult<List<TourismAndRestAPI>>> TourismAndRest([FromHeader] string culture, TourismAndRestGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<TourismAndRestDal> result = await adRepository.GetTourismAndRest(mapper.Map<TourismAndRestGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<TourismAndRestAPI>>(result));
            }

            return NoContent();
        }
        [HttpPost("GetExchange")]
        public async Task<ActionResult<List<ExchangeAPI>>> Exchange([FromHeader] string culture, ExchangeGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<ExchangeDal> result = await adRepository.GetExchange(mapper.Map<ExchangeGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<ExchangeAPI>>(result));
            }

            return NoContent();
        }

        [HttpGet("BestExchanges")]
        public async Task<ActionResult<List<ExchangeAPI>>> Exchange([FromHeader] string culture)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            Models.BestExchangeList result = mapper.Map<Models.BestExchangeList>(await adRepository.GetBestExchanges(currentCulture));
            if (result.Result.Status != CommonConstants.Ok)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }
        [HttpPost("GetVehicle")]
        public async Task<ActionResult<List<VehicleAPI>>> Vehicle([FromHeader] string culture, VehicleGetAPI filter)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            List<VehicleDal> result = await adRepository.GetVehicle(mapper.Map<VehicleGetDAL>(filter), currentCulture);
            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<VehicleAPI>>(result));
            }

            return NoContent();
        }
        #endregion

        #region "Other"
        [HttpDelete]
        public async Task<ActionResult<Result>> DeleteAd(int adID)
        {
            Result result = await adRepository.DeleteAd(adID);
            if (result.Status == CommonConstants.Ok)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }
        #endregion
        [HttpGet("CurrencyList")]
        public async Task<ActionResult<List<CurrencyAPI>>> CurrencyList()
        {
            List<CurrencyDal> result = await adRepository.GetCurrencyList();

            if (result.Count != 0)
            {
                return Ok(mapper.Map<List<CurrencyAPI>>(result));
            }

            return NoContent();
        }

        [HttpPut("Ad")]
        public async Task<ActionResult<bool>> Ad(UniversalAPI ad)
        {
            if (await adRepository.UpdateAd(mapper.Map<UniversalDal>(ad)))
            {
                return Ok(true);
            }

            return BadRequest(false);
        }
        [HttpPut("AdImages")]
        public async Task<ActionResult<bool>> AdImages(ImagesUpdateAPI adImagesModel)
        {

            if (adImagesModel.OldImagesLinks.Count == 0)
            {
                //TODO: Write in Log 

            }
            if (!(generalService.DeleteImagesFromServer(adImagesModel.OldImagesLinks)))
            {
                //TODO: Write in Log 
            };
            if (adImagesModel.NewImagesList.Count == 0)
            {
                return UnprocessableEntity(Resources.ErrorAddingImages);
            }
            adImagesModel.NewImagesList = await generalService.GetImageUrlByImageBase64(adImagesModel.NewImagesList, adImagesModel.SubCategory);
            if (adImagesModel.NewImagesList.Count == 0)
            {
                return UnprocessableEntity(Resources.ErrorAddingImages);
            }

            if (!await adRepository.UpdateAdImages(mapper.Map<ImagesUpdateDAL>(adImagesModel)))
            {
                return BadRequest(false);
            }
            return Ok(true);
        }
        [HttpGet("Favorite")]
        public async Task<ActionResult<Models.FavoriteAds>> FavoriteAds(int userID, string culture)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            Models.FavoriteAds result = mapper.Map<Models.FavoriteAds>(await adRepository.GetUserFavoriteAds(userID, currentCulture));

            if (result.Result.Status != CommonConstants.Ok)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        [HttpGet("Own")]
        public async Task<ActionResult<Models.UserAds>> OwnAds(int userID, string culture)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            Models.UserAds result = mapper.Map<Models.UserAds>(await adRepository.GetUserAds(userID, currentCulture));

            if (result.Result.Status != CommonConstants.Ok)
            {
                BadRequest(result);
            }

            return Ok(result);
        }

        [HttpPost("Favorite")]
        public async Task<ActionResult<Result>> AdFavorite(FavoriteAdModel favoriteAd)
        {
            if (!ModelState.IsValid)
            {
                return UnprocessableEntity(new Result()
                {
                    Status = CommonConstants.InvalidModel,
                    Message = CommonConstants.InvalidModelMessage
                });
            }

            Result result = await adRepository.AddAdToFavorites(favoriteAd);
            if (result.Status == CommonConstants.Ok)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpDelete("Favorite")]
        public async Task<ActionResult<Result>> DeleteFavorite([FromHeader] int userID, [FromHeader] int adID)
        {
            Result result = await adRepository.DeleteAdAtFavorites(new FavoriteAdModel() { UserID = userID, AdID = adID });
            if (result.Status == CommonConstants.Ok)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpGet("LoaderAd")]
        public async Task<ActionResult<LoaderAdAPI>> LoaderAd([FromHeader] string culture)
        {
            CultureMode currentCulture = CultureMode.AM;
            if (culture != null)
            {
                Enum.TryParse(culture.ToUpper(), out currentCulture);
            }

            LoaderAdAPI result = mapper.Map<LoaderAdAPI>(await adRepository.GetLoaderAd(currentCulture));
            if (result.Result.Status == 1)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }
    }