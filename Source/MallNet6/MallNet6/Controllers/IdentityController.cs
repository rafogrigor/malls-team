using System.IdentityModel.Tokens.Jwt;
using System.Text;
using AutoMapper;
using DAL.Common;
using DAL.Interfaces;
using DAL.Models;
using IdentityModel.Client;
using MallNet6.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace MallNet6.Controllers;

[Route("api/[controller]/[action]")]
[ApiController]
public class IdentityController : ControllerBase
{
    private readonly UserManager<UserDal> userManager;
    private readonly IMapper mapper;
    private readonly IConfiguration configuration;
    private readonly IUser userRepo;

    public IdentityController(UserManager<UserDal> userManager, IMapper mapper, IConfiguration configuration, IUser userRepo)
    {
        this.mapper = mapper;
        this.userManager = userManager;
        this.configuration = configuration;
        this.userRepo = userRepo;
    }

    [HttpPost]
    [Authorize]
    public async Task<ActionResult<User>> Login(UserIdentity user)
    {
        if (!ModelState.IsValid)
        {
            return UnprocessableEntity(null);
        }

        UserDal temp = await userManager.FindByNameAsync(user.UserName);

        if (temp != null && await userManager.CheckPasswordAsync(temp, user.Password))
        {
            if (!temp.IsConfirmed)
            {
                ModelState.AddModelError(CommonConstants.Login, Resources.UnconfirmedUser);
            }
            else if (temp.State == UserState.DeactivatedUser)
            {
                ModelState.AddModelError(CommonConstants.Login, Resources.DeactivatedUser);
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(user.Token))
                {
                    await userRepo.CheckExistanceAndSaveToken(temp.ID, user.Token);
                }

                return Ok(mapper.Map<User>(temp));
            }

            return BadRequest(ModelState);
        }

        return Unauthorized();
    }

    [HttpPost]
    public ActionResult<TokenResponse> AuthenticateClientApp(UserApp clientApp)
    {
        if (!ModelState.IsValid)
        {
            return ValidationProblem(ModelState);
        }

        if (clientApp.UserID == "MyMobileListTrade" && clientApp.UserSecret == "+*z16e3#_yhq112y_j#018z(dzyn5+vw(e@sofeiq5rh__u+6=")
        {
            string issuer = configuration["ListTradeAuth:Domain"];
            string audience = configuration["ListTradeAuth:Audience"];
            JwtSecurityToken mbToken = new JwtSecurityToken(issuer, audience, null,
                expires: DateTime.UtcNow.AddMonths(6),
                signingCredentials:
                    new SigningCredentials(
                        new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(configuration["ListTradeAuth:SecurityKey"])),
                    SecurityAlgorithms.HmacSha256));

            return Ok(new { Token = new JwtSecurityTokenHandler().WriteToken(mbToken).ToString() });
        }

        return Unauthorized();
    }

    [Authorize]
    [HttpOptions]
    public void Options()
    {
        HttpContext.Request.Headers.Add("Allows", "POST , POST , OPTIONS");
    }
}