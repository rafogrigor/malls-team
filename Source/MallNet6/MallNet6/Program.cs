using System.Text;
using DAL.Interfaces;
using DAL.Models;
using DAL.Repositories;
using DAL.SqlConnection;
using MallNet6.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllersWithViews();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
    c.AddSecurityDefinition("bearer", new OpenApiSecurityScheme()
    {
        Name = "Authorization",
        Description = "bearer",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey,
        BearerFormat = "JWT",
        Scheme = "Bearer"
    });
    c.OperationFilter<SecurityRequirementsOperationFilter>(true, "bearer");
});
builder.Services.AddCors(options =>
{
    options.AddPolicy("CorsPolicy",
        builder => builder.WithOrigins("*")
            .AllowAnyMethod()
            .AllowAnyHeader());
    //.AllowCredentials());
});
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

builder.Services.AddIdentityCore<UserDal>(
    option =>
    {
        option.Password.RequireNonAlphanumeric = false;
        option.Password.RequireUppercase = false;
        option.Password.RequireDigit = false;
        option.Password.RequireLowercase = false;
    });
TokenValidationParameters TokenParams = new TokenValidationParameters()
{
    ValidateIssuer = true,
    ValidIssuer = builder.Configuration["ListTradeAuth:Domain"],

    ValidateAudience = true,
    ValidAudience = builder.Configuration["ListTradeAuth:Audience"],

    ValidateIssuerSigningKey = true,
    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["ListTradeAuth:SecurityKey"])),
    ValidateLifetime = true,
    ClockSkew = TimeSpan.Zero,

};

builder.Services.AddAuthentication(option =>
{
    option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(option =>
{

    option.RequireHttpsMetadata = false;
    option.MetadataAddress = "https://login.microsoftonline.com/common/v2.0/.well-known/openid-configuration";
    option.TokenValidationParameters = TokenParams;
});

builder.Services.AddScoped<IUserStore<UserDal>, UserRepository>();
builder.Services.AddScoped<ICategories, CategoriesRepository>();
builder.Services.AddScoped<UserRepository>();
builder.Services.AddScoped<IAds, AdsRepository>();
builder.Services.AddScoped<IMainPage, MainPageRepository>();
builder.Services.AddScoped<ILocation, LocationRepository>();
builder.Services.AddScoped<IVehicle, VehicleRepository>();
builder.Services.AddScoped<GeneralService>();
builder.Services.AddScoped<IUser, UserRepository>();
builder.Services.AddScoped<IPayments, PaymentsRepository>();
builder.Services.AddScoped<ITariff, TariffRepository>();
builder.Services.Configure<DBConfig>(builder.Configuration.GetSection("db"));
builder.Services.AddScoped<Manager>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
});

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();

app.UseCors("CorsPolicy");

app.UseAuthentication();
app.UseAuthorization();



app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

app.MapFallbackToFile("index.html");


app.Run();