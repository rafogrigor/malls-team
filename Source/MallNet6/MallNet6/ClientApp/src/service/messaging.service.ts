import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MessagingService {
  firebaseToken: string;
  currentMessage = new BehaviorSubject(null);
  constructor(private angularFireMessaging: AngularFireMessaging) {
    // this.angularFireMessaging.messages.subscribe(
    //   (_messaging: AngularFireMessaging) => {
    //     _messaging.onMessage = _messaging.onMessage.bind(_messaging);
    //     _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
    //   }
    // );
  }
  requestPermission() {
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        // console.log('Permission granted! Save to the server!', token);
        localStorage.setItem('fToken', token);
      },
      (error) => {
        // console.log(error);
        localStorage.setItem('fToken', error);
      }
    );
  }
  receiveMessage() {
    this.angularFireMessaging.messages.subscribe(
      (payload) => {
        this.currentMessage.next(payload);
        this.showCustomNotification(payload);
      },
      (err) => console.log(err)
    );
  }

  showCustomNotification(payload: any) {
    let notify_data = payload['notification'];
    let title = notify_data['title'];
    let link = notify_data['link'];
    console.log(payload['notification']);

    let options = {
      body: notify_data['body'],
      icon: '../assets/images/mall.png',
      badge: '../assets/images/mall.png',
      image: '../assets/images/mall.png',
    };
    let notify: Notification = new Notification(title, options);
    notify.onclick = (event) => {
      event.preventDefault();
      console.log(link);
      // window.location.href = link;
    };
  }
}
