import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TariffService {

constructor(private http: HttpClient) { }
  token = localStorage.getItem('token')
  baseUrl = environment.baseUrl
  language = localStorage.getItem('language');


  getTariffs()
  {
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)

    return this.http.get(`${this.baseUrl}/api/Tariff`, {observe: 'response', headers: headers})
  }

  purchaseLoaderAd(adID: number, userID: number, tariffID: number)
  {
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    let body = {
      adID: adID,
      userID: userID,
      tariffID: tariffID
    }
    return this.http.post(`${this.baseUrl}/api/tariff/PurchaseLoaderAd`, body, {observe: 'response', headers: headers})
  }

  loaderAd()
  {
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.get(`${this.baseUrl}/api/Ad/LoaderAd`, {observe: 'response', headers: headers})
  }
}
