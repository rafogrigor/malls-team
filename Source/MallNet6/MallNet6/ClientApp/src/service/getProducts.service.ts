import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GetProductsService {
  token = localStorage.getItem('token')
  language = localStorage.getItem('language');
  baseUrl = environment.baseUrl

  constructor(private http: HttpClient) { }

  getWork(body){
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}/api/Ad/GetWork`, body, {observe: 'response', headers: headers})
  }
  getSport(body){
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}/api/Ad/GetSport`, body, {observe: 'response', headers: headers})
  }
  getSale(body){
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}/api/Ad/GetSale`, body, {observe: 'response', headers: headers})
  }
  getEverythingElse(body){
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}/api/Ad/GetEverythingElse`, body, {observe: 'response', headers: headers})
  }
  getPetsAndPlants(body){
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}/api/Ad/GetPetsAndPlants`, body, {observe: 'response', headers: headers})
  }
  getCulture(body){
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}​/api/Ad/GetCulture`, body, {observe: 'response', headers: headers})
  }
  getHealfCare(body){
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}​/api/Ad/GetHealfCare`, body, {observe: 'response', headers: headers})
  }
  getFurniture(body){
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}​/api/Ad/GetFurniture`, body, {observe: 'response', headers: headers})
  }
  getForHomeAndGarden(body){
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}​/api/Ad/GetForHomeAndGarden`, body, {observe: 'response', headers: headers})
  }
  getHouseholdGood(body){
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}​/api/Ad/GetHouseholdGoods`, body, {observe: 'response', headers: headers})
  }
  getConstruction(body){
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}​/api/Ad/GetConstruction`, body, {observe: 'response', headers: headers})
  }
  getElectronics(body){
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}​/api/Ad/GetElectronics`, body, {observe: 'response', headers: headers})
  }
  getAcquaintance(body){//Urish Model
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}/api/Ad/GetAcquaintance`, body, {observe: 'response', headers: headers})
  }
  getAppliances(body){
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}/api/Ad/GetAppliances`, body, {observe: 'response', headers: headers})
  }
  getCigaretteAndAlcohol(body){//Urish Model
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}/api/Ad/GetCigaretteAndAlcohol`, body, {observe: 'response', headers: headers})
  }
  getClothesAndShoes(body){//Urish Model
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}/api/Ad/GetClothesAndShoes`, body, {observe: 'response', headers: headers})
  }
  getForChildren(body){//Urish Model
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}/api/Ad/GetForChildren`, body, {observe: 'response', headers: headers})
  }
  getJewerlyAndAccessories(body){//Urish Model
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}/api/Ad/GetJewerlyAndAccessories`, body, {observe: 'response', headers: headers})
  }
  getProductsAndDrinks(body){//Urish Model
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}/api/Ad/GetProductsAndDrinks`, body, {observe: 'response', headers: headers})
  }
  getRealEstate(body){//Urish Model
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}/api/Ad/GetRealEstate`, body, {observe: 'response', headers: headers})
  }
  getServices(body){//Urish Model
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}/api/Ad/GetServices`, body, {observe: 'response', headers: headers})
  }
  getTourismAndRest(body){//Urish Model
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}/api/Ad/GetTourismAndRest`, body, {observe: 'response', headers: headers})
  }
  getVehicle(body){//Urish Model
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}/api/Ad/GetVehicle`, body, {observe: 'response', headers: headers})
  }
  getExchange(body){
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', this.language)
    return this.http.post(`${this.baseUrl}/api/Ad/GetExchange`, body, {observe: 'response', headers: headers})
  }

}
