import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AllForVehicleService {

constructor(private http: HttpClient) { }
  token = localStorage.getItem('token')
  baseUrl = environment.baseUrl
  language = localStorage.getItem('language');


  getMileage()
  {
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.get(`${this.baseUrl}/api/Vehicle/Mileage`, {observe: 'response', headers: headers})
  }

  getEngineSize()
  {
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.get(`${this.baseUrl}/api/Vehicle/EngineSize`, {observe: 'response', headers: headers})
  }

  getMark()
  {
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.get(`${this.baseUrl}/api/Vehicle/Mark`, {observe: 'response', headers: headers})
  }

  getModel(markID)
  {
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.get(`${this.baseUrl}/api/Vehicle/Model?markID=${markID}`, {observe: 'response', headers: headers})
  }

  getBodyType()
  {
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set("culture", `${this.language}`)
    return this.http.get(`${this.baseUrl}/api/Vehicle/BodyType`, {observe: 'response', headers: headers})
  }

  getColor()
  {
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set("culture", `${this.language}`)
    return this.http.get(`${this.baseUrl}/api/Vehicle/Color`, {observe: 'response', headers: headers})
  }

  getDriveType()
  {
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set("culture", `${this.language}`)
    return this.http.get(`${this.baseUrl}/api/Vehicle/DriveType`, {observe: 'response', headers: headers})
  }

  getTransmissionType()
  {
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set("culture", `${this.language}`)
    return this.http.get(`${this.baseUrl}/api/Vehicle/TransmissionType`, {observe: 'response', headers: headers})
  }

  getEngineType()
  {
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set("culture", `${this.language}`)
    return this.http.get(`${this.baseUrl}/api/Vehicle/EngineType`, {observe: 'response', headers: headers})
  }

  getWheelType()
  {
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set("culture", `${this.language}`)
    return this.http.get(`${this.baseUrl}/api/Vehicle/WheelType`, {observe: 'response', headers: headers})
  }
}
