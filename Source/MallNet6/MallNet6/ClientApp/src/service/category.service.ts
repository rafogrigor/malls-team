import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
let language = localStorage.getItem('language');
@Injectable({
  providedIn: 'root'
})

export class CategoryService {

  token = localStorage.getItem('token')
  baseUrl = environment.baseUrl
  constructor(private http: HttpClient) { }

  getAllCategories(){
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', language)
    return this.http.get(`${this.baseUrl}/api/Categories/MainCategories`, {observe: 'response',headers: headers})
  }
  getSubCategory(id:number){
    let body = {value: id}
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('culture', language)
    return this.http.post(`${this.baseUrl}/api/Categories/SubCategories`,body, {observe:'response', headers:headers})
  }
}
