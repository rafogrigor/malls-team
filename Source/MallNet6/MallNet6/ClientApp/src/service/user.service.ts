import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ChangePassword } from 'src/model/changePassword';
import { ConfirmEmail } from 'src/model/confirmEmail';
import { Login } from 'src/model/login';
import { UpdateUser } from 'src/model/updateUser';
import { UserRegister } from 'src/model/userRegister';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  token = localStorage.getItem('token');
  userId = localStorage.getItem('%%a$');
  baseUrl = environment.baseUrl;
  constructor(private http: HttpClient) {}

  registerUser(body: UserRegister) {
    const headers = new HttpHeaders().set(
      'Authorization',
      `Bearer ${this.token}`
    );
    return this.http.post(`${this.baseUrl}/api/User/CreateUser`, body, {
      observe: 'response',
      headers: headers,
    });
  }
  sendConfirmEmail(email: ConfirmEmail) {
    const headers = new HttpHeaders()
      .set('Authorization', `Bearer ${this.token}`)
      .set('Content-Type', 'application/json');
    return this.http.post(`${this.baseUrl}/api/User/SendConfirmEmail`, email, {
      observe: 'response',
      headers: headers,
    });
  }
  confirmEmail(email: ConfirmEmail) {
    const headers = new HttpHeaders().set(
      'Authorization',
      `Bearer ${this.token}`
    );
    return this.http.put(`${this.baseUrl}/api/User/Confirm`, email, {
      observe: 'response',
      headers: headers,
    });
  }
  login(body: Login) {
    let newBodyWithToken = {
      userName: body.userName,
      password: body.password,
      token: body.token,
    };
    const headers = new HttpHeaders().set(
      'Authorization',
      `Bearer ${this.token}`
    );
    return this.http.post(
      `${this.baseUrl}/api/Identity/Login`,
      newBodyWithToken,
      { observe: 'response', headers: headers }
    );
  }
  forgotPassword(body: string) {
    let sendBody = {
      email: body,
    };
    const headers = new HttpHeaders().set(
      'Authorization',
      `Bearer ${this.token}`
    );
    return this.http.post(`${this.baseUrl}/api/User/ForgotPassword`, sendBody, {
      observe: 'response',
      headers: headers,
    });
  }
  resetPasswordFromLoginPage(body) {
    const headers = new HttpHeaders().set(
      'Authorization',
      `Bearer ${this.token}`
    );
    return this.http.put(`${this.baseUrl}/api/User/ResetPassword`, body, {
      observe: 'response',
      headers: headers,
    });
  }
  getUser(ID) {
    let body = {
      userID: parseInt(ID),
    };
    const headers = new HttpHeaders().set(
      'Authorization',
      `Bearer ${this.token}`
    );
    return this.http.post(`${this.baseUrl}​/api/User/GetUser`, body, {
      observe: 'response',
      headers: headers,
    });
  }
  getUserById(id) {
    let body = {
      userID: parseInt(id),
    };
    const headers = new HttpHeaders().set(
      'Authorization',
      `Bearer ${this.token}`
    );
    return this.http.post(`${this.baseUrl}​/api/User/GetUser`, body, {
      observe: 'response',
      headers: headers,
    });
  }
  changePassword(body: ChangePassword) {
    const headers = new HttpHeaders().set(
      'Authorization',
      `Bearer ${this.token}`
    );
    return this.http.put(`${this.baseUrl}/api/User/Password`, body, {
      observe: 'response',
      headers: headers,
    });
  }
  isAuthorizatedWithMyToken() {
    const myToken = `${this.userId}${this.token}`;
    return myToken;
  }
  isAuthorizated() {
    console.log(!!this.userId);
    return !!this.userId;
  }
  addRating(value: boolean, userToID: string, userFromID: string) {
    // let body = {
    //   id: parseInt(userId),
    //   isIncreased: value
    // }

    let body = {
      userToID: parseInt(userToID),
      userFromID: parseInt(userFromID),
      oper: value,
    };
    const headers = new HttpHeaders().set(
      'Authorization',
      `Bearer ${this.token}`
    );
    return this.http.put(`${this.baseUrl}/api/User/Rating`, body, {
      observe: 'response',
      headers: headers,
    });
  }

  updateRating(data: UpdateUser) {
    const headers = new HttpHeaders().set(
      'Authorization',
      `Bearer ${this.token}`
    );
    return this.http.put(`${this.baseUrl}/api/User/Data`, data, {
      observe: 'response',
      headers: headers,
    });
  }
}
