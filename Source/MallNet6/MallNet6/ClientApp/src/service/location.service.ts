import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
let language = localStorage.getItem('language');

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private http: HttpClient) { }
  token = localStorage.getItem('token')
  baseUrl = environment.baseUrl


  getLocation(){
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('Content-Type', 'application/json; charset=utf-8')
    .set('culture', language)
    return this.http.post(`${this.baseUrl}/api/Location/Country`, language, {observe: 'response',headers:headers})
  }
  getRegion(body: number){
    let sendBody = {
      id: body
    }
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('Content-Type', 'application/json; charset=utf-8')
    .set('culture', language)
    return this.http.post(`${this.baseUrl}/api/Location/Region`, sendBody, {observe: 'response',headers:headers})
  }
  getCity(body: number){
    let sendBody = {
      id: body
    }
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    .set('Content-Type', 'application/json; charset=utf-8')
    .set('culture', language)
    return this.http.post(`${this.baseUrl}/api/Location/City`, sendBody, {observe: 'response',headers:headers})
  }

}
