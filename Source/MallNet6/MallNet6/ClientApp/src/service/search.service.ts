import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }
  token = localStorage.getItem('token')
  baseUrl = environment.baseUrl


  search(obj){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)

    let body = {
      countryID: 1,
      textAM: obj.textAM,
      textEN: obj.textEN,
      textRU: obj.textRU
    }
    return this.http.post(`${this.baseUrl}/api/General/Search`, body, {observe: 'response',headers: headers})
  }

  prompt(symbol: string)
  {
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    let body = {
      countryID: 1,
      symbol: symbol
    }
    return this.http.post(`${this.baseUrl}/api/General/Prompt`, body, {observe: 'response', headers: headers})
  }
}
