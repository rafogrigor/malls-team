import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserAdsAndSettingsService {
  token = localStorage.getItem('token')
  userId = localStorage.getItem('%%a$')
  language = localStorage.getItem('language');
  baseUrl = environment.baseUrl


  constructor(private http: HttpClient) { }

  getMyAds()
  {
    let headers = new HttpHeaders().set('Authorization', `Bearer ${this.token}`)
    return this.http.get(`${this.baseUrl}/api/Ad/Own?userID=${this.userId}&culture=${this.language}`, {observe: 'response', headers: headers})
  }
  getUserAds(id)
  {
    let headers = new HttpHeaders().set('Authorization', `Bearer ${this.token}`)
    return this.http.get(`${this.baseUrl}/api/Ad/Own?userID=${id}&culture=${this.language}`, {observe: 'response', headers: headers})
  }
}
