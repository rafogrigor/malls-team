import { Injectable } from '@angular/core';
import { rejects } from 'assert';

@Injectable({
  providedIn: 'root'
})
export class GetCurrentLocationService {

  lat = 40.173969;
  lng = 44.502750;

constructor() { }
  getLocation():Promise<any>
  {
    return new Promise((reslove, reject) =>
    {
      navigator.geolocation.getCurrentPosition(resp =>
        {
          reslove({lng: resp.coords.longitude, lat: resp.coords.latitude});
          reject({lng: this.lng, lat: this.lat})
        })
    })
  }
}
