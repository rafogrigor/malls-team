import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FavoriteRequest } from 'src/model/favoriteRequest';

@Injectable({
  providedIn: 'root',
})
export class FavoriteService {
  token = localStorage.getItem('token');
  userId = localStorage.getItem('%%a$');
  language = localStorage.getItem('language');
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getFavoriteList(useridBody) {
    let headers = new HttpHeaders().set(
      'Authorization',
      `Bearer ${this.token}`
    );
    return this.http.get(
      `${this.baseUrl}/api/Ad/Favorite?userID=${useridBody}&culture=${this.language}`,
      { observe: 'response', headers: headers }
    );
  }
  addFavorite(body) {
    let sendRequest: FavoriteRequest = new FavoriteRequest();
    sendRequest.adID = body;
    sendRequest.userID = parseInt(this.userId);
    let headers = new HttpHeaders().set(
      'Authorization',
      `Bearer ${this.token}`
    );
    return this.http.post(`${this.baseUrl}/api/Ad/Favorite`, sendRequest, {
      observe: 'response',
      headers: headers,
    });
  }
  deleteFavorite(body: FavoriteRequest) {
    let headers = new HttpHeaders()
      .set('Authorization', `Bearer ${this.token}`)
      .set('userID', `${body.userID}`)
      .set('adID', `${body.adID}`);
    return this.http.delete(`${this.baseUrl}/api/Ad/Favorite`, {
      observe: 'response',
      headers: headers,
    });
  }
}
