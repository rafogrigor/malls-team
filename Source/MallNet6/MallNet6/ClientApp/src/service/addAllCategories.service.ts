import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AddAllCategoriesService {

constructor(private http: HttpClient) { }
  token = localStorage.getItem('token');
  baseUrl = environment.baseUrl

  getCurrentList(){
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.get(`${this.baseUrl}/api/Ad/CurrencyList`, {observe: 'response', headers: headers})
  }
}
