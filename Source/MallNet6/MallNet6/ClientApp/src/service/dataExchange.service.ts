import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataExchangeService {

constructor() { }

private loginMessage = new Subject<boolean>();
private count = new Subject<number>();

updateObj(someData) {
    // console.log(someData)
    this.loginMessage.next(someData)
  };

getObj() {
    return this.loginMessage.asObservable()
  };

addFavorite(fav)
{
  this.count.next(fav)
}
getfavcount()
{
  return this.count.asObservable()
}
}
