import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UpdateService {

constructor(private http: HttpClient) { }
  token = localStorage.getItem('token')
  baseUrl = environment.baseUrl
  updateAd(body)
  {
    let headers = new HttpHeaders().set('Authorization', `Bearer ${this.token}`)
    return this.http.put(`${this.baseUrl}/api/Ad`, body, {observe: 'response', headers: headers})
  }
  delete(id: number)
  {
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.delete(`${this.baseUrl}/api/Ad?adID=${id}`, {observe:'response', headers: headers})
  }

  updateImage(body)
  {
    const headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.put(`${this.baseUrl}/api/Ad/AdImages`, body, {observe: 'response', headers:headers})
  }
}
