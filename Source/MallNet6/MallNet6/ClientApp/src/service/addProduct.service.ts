import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AddProductService {
  token = localStorage.getItem('token')
  baseUrl = environment.baseUrl
  constructor(private http: HttpClient) { }

  addRealEstate(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/RealEstate`, body, {headers: headers})
  }
  addAppilance(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/Appliance`, body, {headers: headers})
  }
  addHouseholdGoods(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/HouseholdGoods`, body, {headers: headers})
  }
  addClothesAndShoes(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/ClothesAndShoes`, body, {headers: headers})
  }
  addForChildren(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/ForChildren`, body, {headers: headers})
  }
  addJewerlyAndAccessories(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/JewerlyAndAccessories`, body, {headers: headers})
  }
  addConstruction(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/Construction`, body, {headers: headers})
  }
  addAllForHomeAndGarden(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/AllForHomeAndGarden`, body, {headers: headers})
  }
  addElectronics(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/Electronics`, body, {headers: headers})
  }
  addProductsAndDrinks(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/ProductsAndDrinks`, body, {headers: headers})
  }
  addFurniture(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/Furniture`, body, {headers: headers})
  }
  addPetsAndPlants(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/PetsAndPlants`, body, {headers: headers})
  }
  addCulture(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/Culture`, body, {headers: headers})
  }
  addWork(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/Work`, body, {headers: headers})
  }
  addServices(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/Services`, body, {headers: headers})
  }
  addAcquaintance(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/Acquaintance`, body, {headers: headers})
  }
  addHealfCare(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/HealfCare`, body, {headers: headers})
  }
  addSport(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/Sport`, body, {headers: headers})
  }
  addTourism(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/Tourism`, body, {headers: headers})
  }
  addCigaretteAndAlcohol(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/CigaretteAndAlcohol`, body, {headers: headers})
  }
  addEverythingElse(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/CigaretteAndAlcohol`, body, {headers: headers})
  }
  addVehicle(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/Vehicle`, body, {headers: headers})
  }
  addExchange(body){
    let headers = new HttpHeaders()
    .set('Authorization', `Bearer ${this.token}`)
    return this.http.post(`${this.baseUrl}/api/Ad/Exchange`, body, {headers: headers})
  }

}
