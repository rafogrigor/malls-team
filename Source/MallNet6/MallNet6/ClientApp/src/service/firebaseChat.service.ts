import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class FirebaseChatService {

constructor(private firestore: AngularFirestore) { }


  getCollection(collectionName)
  {
    return this.firestore.collection(collectionName)
    .valueChanges()
  }

  getMessege(id, userId)
  {
    return this.firestore.collection(`${id}`).doc(`${userId}`).valueChanges()
  }
  sendMessage(id, userId, data)
  {
    return this.firestore.collection(`${id}`).doc(`${userId}`).set({chat: [data]}, {merge: true})
  }

}
