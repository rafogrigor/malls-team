import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
let language = localStorage.getItem('language');

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  token = localStorage.getItem('token')
  currency = localStorage.getItem('currency')
  baseUrl = environment.baseUrl


constructor(private http:HttpClient) { }
  getToken(){
    let t = {userId:"MyMobileListTrade", userSecret: "+*z16e3#_yhq112y_j#018z(dzyn5+vw(e@sofeiq5rh__u+6="};
    return this.http.post(`${this.baseUrl}/api/Identity/AuthenticateClientApp`,t,{observe:"response"})
  }
  getMainPageProducts(){
    let headers = new HttpHeaders()
    .set('countryID', '1')
    .set('culture', language)
    .set('currency', `${this.currency}`)
    .set('Authorization', `Bearer ${this.token}`)
    if(this.token)
      return this.http.get(`${this.baseUrl}/api/General/MainPage`, {observe: 'response' ,headers: headers})
    else
      location.reload()
  }
}
