import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { UserService } from 'src/service/user.service';

@Injectable({
  providedIn: 'root'
})
export class LoginpageGuard implements CanActivate {
  constructor(private authService: UserService,private router: Router) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(!!localStorage.getItem('%%a$'))
      {
        this.router.navigateByUrl("/")
        return of(false);
      }
      else{
        return of(true);

      }
  }

}
