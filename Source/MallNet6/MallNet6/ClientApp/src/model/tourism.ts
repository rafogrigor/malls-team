export class Tourism {
  aim: number;
  departureDay: Date;
  returnDay: Date;
  reservedTickets: number;
}
