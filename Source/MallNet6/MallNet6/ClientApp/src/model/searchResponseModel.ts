export class SearchResponseModel {
  id: number;
  userID: number;
  name: string;
  categoryID: string;
  description: string;
  owner: number;
  image: string;
}
