import { Categories } from "./categories";
import { SubCategories } from "./subCategories";

export class GeneralCategory {
  category: Categories;
  subcategory: SubCategories;
}
