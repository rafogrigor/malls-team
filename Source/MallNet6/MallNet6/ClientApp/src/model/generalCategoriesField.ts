export class GeneralCategoriesField {
  id:  number;
  userID:  number;
  subCategoryID:  number;
  currency:  number;
  price:  number;
  country:  number;
  region:  number;
  city:  number;
  state:  number;
  description: string;
  name: string;
  locationLatitude:  number;
  locationLongitude:  number;
  contact: string;
  owner:  number;
  comment: string;
  tags: string;
  imagesList: [
    string
  ];
}
