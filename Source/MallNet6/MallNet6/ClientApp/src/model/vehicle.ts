export class Vehicle {
  id:  number;
  userID:  number;
  subCategoryID:  number;
  currency:  number;
  price:  number;
  country:  number;
  region:  number;
  city:  number;
  state:  number;
  aim: number;
  description: string;
  name: string;
  locationLatitude:  number;
  locationLongitude:  number;
  contact: string;
  owner:  number;
  comment: string;
  tags: string;
  imagesList: [
    string
  ];
  mark: number;
  // sMark: string;
  model: number;
  // sModel: string;
  engineType: number;
  driveType: number;
  productionYear: string;
  customsCleared: boolean;
  mileage: number;
  bodyType: number;
  // SbodyType: string;
  // sEngineType: string;
  engineSize: number;
  transmissionType: number;
  color: number;
  wheel: number
  // sDriveType: string;
  // sTtransmissionType: string;
  // sColor: string;
  // sWheel: string;
}
