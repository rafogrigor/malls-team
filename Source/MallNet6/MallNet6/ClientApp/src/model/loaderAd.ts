export class LoaderAd {
  adID: number;
  userID: number;
  mainCategoryID: number;
  mainCategoryName: string;
  name: string;
  description: string;
  images: [
    string
  ];
  result: {
    status: number;
    message: string;
  }
}
