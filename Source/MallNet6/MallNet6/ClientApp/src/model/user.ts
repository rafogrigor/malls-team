export class User {
  id: number;
  phoneNumber: string;
  firstName: string;
  lastName: string;
  email: string;
  state: number;
  country: number;
  region: number;
  city: number;
  address: string;
  rating: number;
  balance: number;
  avatar: string;
}
