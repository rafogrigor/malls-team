export class Tariff {
  id: number;
  name: string;
  desc: string;
  price: number;
  salePercent: number;
  salePrice: number;
  viewCount: number;
  adCount: number;
  period: number;
}
