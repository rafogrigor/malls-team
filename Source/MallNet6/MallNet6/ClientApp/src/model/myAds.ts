import { Categories } from "./categories";

export class MyAds {
  category: Categories;
  ads: [
    {
      id: number;
      name: string;
      userID: number;
      description: string;
      image: string;
      owner: number;
    }
  ]
}
