export class NewCreateModel {
  userID:  number;
  subCategoryID:  number;
  currency:  number;
  price:  number;
  salePercent: number;
  salePrice: number;
  country:  number;
  region:  number;
  city:  number;
  state:  number;
  description: string;
  name: string;
  locationLatitude:  number;
  locationLongitude:  number;
  contact: string;
  owner:  number;
  isRegional: boolean
  comment: string;
  tags: string;
  imagesList: [
    string
  ];
}
