import { Categories } from "./categories";

export class FavoriteResponse {
  category: Categories;
  ads: [
    {
      id: number;
      name: string;
      state: number;
      userID: number;
      categoryID: number;
      description: string;
      image: string;
      owner: number;
    }
  ]
}

