export class ProductsAndDrinks {
  aim: number;
  isLocal: boolean;
  isFrozen: boolean;
  isNaturalDrink: boolean;
}
