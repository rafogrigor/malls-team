export class UpdateUser {
  id: number;
  phoneNumber: string;
  password: string;
  firstName: string;
  lastName: string;
  country: number;
  region: number;
  city: number;
  address: string;
  avatar: string;
}
