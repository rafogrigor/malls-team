export class ChangePassword {
  phoneNumber: string;
  oldPassword: string;
  newPassword: string;
}
