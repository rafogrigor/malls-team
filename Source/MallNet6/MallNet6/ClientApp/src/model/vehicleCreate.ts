export class VehicleCreate {
  id: number;
  userID: number;
  subCategoryID: number;
  currency: number;
  price: number;
  salePercent: number;
  salePrice: number;
  country: number;
  region: number;
  city: number;
  state: number;
  description: string;
  name: string;
  locationLatitude: number;
  locationLongitude: number;
  contact: string;
  aim: number;
  owner: number;
  comment: string;
  view: number;
  tags: string;
  imagesList: [
    string
  ];
  mark: number;
  model: number;
  productionYear: Date;
  customsCleared: boolean;
  mileage: number;
  bodyType: number;
  engineType: number;
  engineSize: number;
  driveType: number;
  transmissionType: number;
  color: number;
  wheel: number;
}
