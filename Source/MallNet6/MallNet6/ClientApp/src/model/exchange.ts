export class Exchange {
  sourceCurrencyID: number;
  destinationCurrencyID: number;
  saleSummaRetail: number;
  buySummaRetail: number;
  saleSumma: number;
  buySumma: number;
}
