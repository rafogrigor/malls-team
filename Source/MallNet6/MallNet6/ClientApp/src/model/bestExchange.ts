export class BestExchange {
  bestExchange:{
    sale: number;
    buy: number;
    symbol: string;
    userID: number;
  }
}
