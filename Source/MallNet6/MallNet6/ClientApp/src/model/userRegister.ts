export class UserRegister {
  firstName: string;
  email: string;
  password: string;
  phoneNumber: string;
  country: number;
}
