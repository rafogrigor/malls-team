import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Categories } from 'src/model/categories';
import { City } from 'src/model/city';
import { Country } from 'src/model/country';
import { GeneralCategoriesField } from 'src/model/generalCategoriesField';
import { NewCreateModel } from 'src/model/newCreateModel';
import { Region } from 'src/model/region';
import { SubCategories } from 'src/model/subCategories';
import { Tariff } from 'src/model/tariff';
import { User } from 'src/model/user';
import { Vehicle } from 'src/model/vehicle';
import { VehicleBodyType } from 'src/model/vehicleBodyType';
import { VehicleColor } from 'src/model/vehicleColor';
import { VehicleDriverType } from 'src/model/vehicleDriverType';
import { VehicleEngineSize } from 'src/model/vehicleEngineSize';
import { VehicleEngineType } from 'src/model/vehicleEngineType';
import { VehicleMark } from 'src/model/vehicleMark';
import { VehicleModel } from 'src/model/vehicleModel';
import { VehicleTransmissionType } from 'src/model/vehicleTransmissionType';
import { VehicleWheelType } from 'src/model/vehicleWheelType';
import { AddProductService } from 'src/service/addProduct.service';
import { AllForVehicleService } from 'src/service/allForVehicle.service';
import { CategoryService } from 'src/service/category.service';
import { GetCurrentLocationService } from 'src/service/getCurrentLocation.service';
import { LocationService } from 'src/service/location.service';
import { TariffService } from 'src/service/tariff.service';
import { UserService } from 'src/service/user.service';

@Component({
  selector: 'app-create-new-ad',
  templateUrl: './create-new-ad.component.html',
  styleUrls: ['./create-new-ad.component.css'],
})
export class CreateNewAdComponent implements OnInit, OnDestroy {
  categories: Categories[] = [];
  user: User;
  subCategories: SubCategories[] = [];
  countries: Country[] = [];
  regions: Region[] = [];
  cities: City[] = [];
  selectedCategoryObj: Categories = new Categories();
  selectedSubCategoryObj: SubCategories = new SubCategories();
  sendVehicle: Vehicle = new Vehicle();
  vehicleMark: VehicleMark[] = [];
  vehicleModel: VehicleModel[] = [];
  vehicleEngineSize: VehicleEngineSize[] = [];
  vehicleBodyType: VehicleBodyType[] = [];
  vehicleColor: VehicleColor[] = [];
  vehicleDriveType: VehicleDriverType[] = [];
  vehicleEngineType: VehicleEngineType[] = [];
  vehicleTransmissionType: VehicleTransmissionType[] = [];
  vehicleWheelType: VehicleWheelType[] = [];
  yearsArr: number[] = [];
  profilePicBase64: string | ArrayBuffer;
  imagesList: any[] = [];
  imagePreview: any[] = [];
  adCreateForm: FormGroup;
  sendData: GeneralCategoriesField = new GeneralCategoriesField();
  NewCreateModel: NewCreateModel = new NewCreateModel();
  contact: string;
  lat: number = 40.173969;
  lng: number = 44.50275;
  tariffList: Tariff[];

  //strings
  categoryShow = this.translate.instant('createAd.selectCategory');
  subCategoryShow: string = this.translate.instant('createAd.subCategory');
  ownerText: string = this.translate.instant('createAd.owner');
  aimText: string = this.translate.instant('createAd.aim');
  acquaintanceAimText: string = this.translate.instant(
    'createAd.acquaintanceAim'
  );
  regionText: string = this.translate.instant('createAd.region');
  cityText: string = this.translate.instant('createAd.city');
  currency: string = this.translate.instant('createAd.currency');
  floorText: string = this.translate.instant('createAd.floor');
  roomText: string = this.translate.instant('createAd.rooms');
  constructionTypeText: string = this.translate.instant(
    'createAd.constructionType'
  );
  vehicleMarkText: string = this.translate.instant('createAd.vehicleMark');
  vehicleModelText: string = this.translate.instant('createAd.vehicleModel');
  vehicleEngineSizeText: string = this.translate.instant(
    'createAd.vehicleEngineSize'
  );
  vehicleEngineTypeText: string = this.translate.instant('createAd.engineType');
  vehicleBodyTypeText: string = this.translate.instant('createAd.bodyType');
  vehicleDriveTypeText: string = this.translate.instant('createAd.driveType');
  vehicleYearText: string = this.translate.instant('createAd.year');
  vehicleteTransmissionText: string = this.translate.instant(
    'createAd.transmission'
  );
  vehicleColorText: string = this.translate.instant('createAd.color');
  whellText: string = this.translate.instant('createAd.wheel');
  productStateText: string = this.translate.instant('createAd.productState');
  showGenderText: string = this.translate.instant('createAd.gender');

  // Boolean
  showSubCategory: boolean = false;
  showOwner: boolean = false;
  showMain: boolean = false;
  showAim: boolean = false;
  aimForRent: boolean = true;
  showAcquaintanceAim: boolean = false;
  showRealEstate: boolean = false;
  showVehicle: boolean = false;
  showVehicleOnlyAuto: boolean = false;
  showProductStateOnly: boolean = false;
  showClothesAndShoes: boolean = false;
  showGender: boolean = false;
  showForNewBorns: boolean = false;
  showIsNaturalDrink: boolean = false;
  showIsFrozen: boolean = false;
  showIsLocal: boolean = false;
  showProductsAndDrinks: boolean = false;
  showCigaretteAndAlcohol: boolean = false;
  showTourismAndRest: boolean = false;
  showWork: boolean = false;
  showService: boolean = false;
  showButton: boolean = false;
  showPreview: boolean = false;
  showAcquaintance: boolean = false;
  showLoader: boolean = false;
  itemAdded: boolean = false;
  errorMessege: boolean = false;
  showErrorMessage: boolean = false;
  showHealfCareField: boolean = true;
  showRealEstateFields: boolean = true;

  constructor(
    private categoryService: CategoryService,
    private locationService: LocationService,
    private vehicleService: AllForVehicleService,
    private formBuilder: FormBuilder,
    private addProduct: AddProductService,
    private router: Router,
    private currLocation: GetCurrentLocationService,
    private translate: TranslateService,
    private tariffService: TariffService,
    private userService: UserService
  ) {}

  @ViewChild('fileUpload') inputFile: ElementRef;

  ngOnInit(): void {
    this.categoryService.getAllCategories().subscribe((res) => {
      this.categories = res.body as Categories[];
      this.categories.splice(0, 1);
      this.categories.splice(22, 1);
    });
    this.userService
      .getUserById(parseInt(localStorage.getItem('%%a$')))
      .subscribe((res) => {
        this.user = res.body as User;
      });

    this.getCurrLocation();

    //get country
    this.locationService.getLocation().subscribe((res) => {
      this.countries = res.body as Country[];
      //get region
      this.locationService.getRegion(1).subscribe((res) => {
        this.regions = res.body as Region[];
      });
    });
    this.tariffService.getTariffs().subscribe((res) => {
      this.tariffList = res.body as Tariff[];
      this.tariffList.splice(2, 3);
    });
  }

  selectedCategory(category: Categories) {
    // this.showOwner = false;
    this.resetAllData();
    this.subCategoryShow = this.translate.instant('createAd.subCategory');
    this.categoryShow = category.category;
    this.selectedSubCategoryObj = null;
    this.subCategories = [];
    this.selectedCategoryObj.id = category.id;
    this.selectedCategoryObj.category = category.category;
    this.categoryService.getSubCategory(category.id).subscribe((res) => {
      this.subCategories = res.body as SubCategories[];
      this.showSubCategory = true;
    });
  }
  selectSubCateogiry(subCategory: SubCategories) {
    this.resetAllData();
    this.subCategoryShow = subCategory.type;
    this.selectedSubCategoryObj = subCategory;
    this.filterWithCategoryAndSubCategory(
      this.selectedCategoryObj.id,
      subCategory.id
    );
    // this.showOwner = true;
    // this.adCreateForm.get('subCategoryId').setValue(subCategory.id)
    this.NewCreateModel.subCategoryID = subCategory.id;
    this.adCreateForm.get('subCategoryId').setValue(subCategory.id)
    this.adCreateForm.get('id').setValue('selected')


  }
  resetAllData() {
    this.showAim = false;
    this.aimForRent = true;
    this.showAcquaintanceAim = false;
    this.showMain = false;
    this.showRealEstate = false;
    this.showVehicle = false;
    this.showVehicleOnlyAuto = false;
    this.showProductStateOnly = false;
    this.showClothesAndShoes = false;
    this.showForNewBorns = false;
    this.showIsLocal = false;
    this.showIsFrozen = false;
    this.showIsNaturalDrink = false;
    this.showProductsAndDrinks = false;
    this.showCigaretteAndAlcohol = false;
    this.showTourismAndRest = false;
    this.showWork = false;
    this.showService = false;
    this.showButton = false;
    this.showAcquaintance = false;
  }

  filterWithCategoryAndSubCategory(catId, subId) {
    this.buildForm(catId);
    this.adCreateForm.get('state').setValue(0);
    switch (catId) {
      case 1: //Sale
        break;
      case 2: //Tner
        this.showRealEstateFields = true;
        this.showAim = true;
        this.showRealEstate = true;
        if (subId == 4 || subId == 5) {
          this.showRealEstateFields = false;
        }
        break;
      case 3: //Avtomeqenaner
        this.showAim = true;
        this.getvehicle();
        this.years();
        this.showVehicle = true;
        if (subId == 9 || subId == 10) {
          this.showVehicleOnlyAuto = true;
        }
        break;
      case 4: //Electronics
        this.showAim = true;
        this.showProductStateOnly = true;
        break;
      case 5: //Appliances
        this.showAim = true;
        this.showProductStateOnly = true;
        break;
      case 6: //HouseholdGoods
        this.showAim = true;
        this.showProductStateOnly = true;
        break;
      case 7: //ClothesAndShoes
        this.showAim = true;
        this.showProductStateOnly = true;
        this.showClothesAndShoes = true;
        break;
      case 8: //ForChildren
        this.showAim = true;
        this.showGender = true;
        this.showProductStateOnly = true;
        this.showForNewBorns = true;
        break;
      case 9: //JewerlyAndAccessories
        this.showAim = true;
        this.showGender = true;
        this.showProductStateOnly = true;
        break;
      case 10: //Construction
        this.showAim = true;
        this.showProductStateOnly = true;
        break;
      case 11: //AllForHomeAndGArden
        this.showAim = true;
        this.showProductStateOnly = true;
        break;
      case 12: //ProductsAndDrinks
        this.showAim = true;
        this.aimForRent = false;
        this.showIsLocal = true;
        this.showIsFrozen = true;
        this.showIsNaturalDrink = true;
        this.showProductsAndDrinks = true;
        break;
      case 13: //CigaretteAndAlcohol
        this.showAim = true;
        this.showProductStateOnly = true;
        this.showCigaretteAndAlcohol = true;
        break;
      case 14: //Furniture
        this.showAim = true;
        this.showProductStateOnly = true;
        break;
      case 15: //TourismAndRest
        this.showAim = true;
        this.aimForRent = false;
        this.showTourismAndRest = true;
        break;
      case 16: //Sport
        this.showAim = true;
        this.showProductStateOnly = true;
        break;
      case 17: //PetsAndPlants
        this.showAim = true;
        this.showProductStateOnly = true;
        break;
      case 18: //Culture
        this.showAim = true;
        this.showProductStateOnly = true;
        break;
      case 19: //Work
        this.showMain = true;
        this.showWork = true;
        break;
      case 20: //Services
        this.showMain = true;
        this.showService = true;
        break;
      case 21: //HealfCare
        this.showProductStateOnly = true;
        if (subId == 181) {
          this.showProductStateOnly = false;
        }
        this.showAim = true;
        this.aimForRent = false;
        break;
      case 22: //Acquaintance
        this.showAcquaintanceAim = true;
        this.showGender = true;
        this.showAcquaintance = true;
        break;
      case 23: //EverythingElse
        this.showAim = true;
        break;
      case 24: //Exchange
        break;
    }
    this.showButton = true;
  }

  getAim(val: number) {
    this.aimText =
      val == 0
        ? this.translate.instant('createAd.forSale')
        : val == 1
        ? this.translate.instant('createAd.forRent')
        : val == 2
        ? this.translate.instant('createAd.exchange')
        : val == 3
        ? this.translate.instant('createAd.buy')
        : this.translate.instant('createAd.search');
    this.showMain = true;
    // this.adCreateForm.get('aim').setValue(val)
    this.NewCreateModel['aim'] = val;
    this.adCreateForm.get('aim').setValue(val)

  }
  getAcquaintanceAim(val: number) {
    this.acquaintanceAimText =
      val == 0
        ? this.translate.instant('createAd.acquaintanceAimArr.friendship')
        : val == 1
        ? this.translate.instant('createAd.acquaintanceAimArr.marriage')
        : this.translate.instant('createAd.acquaintanceAimArr.entetainment');
    this.showMain = true;
    // this.adCreateForm.get('acquaintanceAim').setValue(val)
    this.NewCreateModel['acquaintanceAim'] = val;
  }
  getRegion(obj: Region) {
    this.regionText = obj.country;
    this.cityText = this.translate.instant('createAd.city');
    // this.adCreateForm.get('region').setValue(obj.id)
    this.NewCreateModel.region = obj.id;
    this.adCreateForm.get('region').setValue(obj.id)


    //get city
    this.locationService.getCity(obj.id).subscribe((res) => {
      this.cities = res.body as City[];
    });
  }
  getCity(obj: City) {
    this.cityText = obj.country;
    // this.adCreateForm.get('city').setValue(obj.id)
    this.NewCreateModel.city = obj.id;
    this.adCreateForm.get('city').setValue(obj.id)


  }
  getOwner(obj) {
    this.ownerText =
      obj == 0
        ? this.translate.instant('createAd.ownerArr.individual')
        : obj == 1
        ? this.translate.instant('createAd.ownerArr.agency')
        : this.translate.instant('createAd.ownerArr.diller');
    // this.adCreateForm.get('owner').setValue(obj)
    this.NewCreateModel.owner = obj;
    this.adCreateForm.get('owner').setValue(obj)


    // this.showLocation = true
  }
  getCurrency(val) {
    this.currency = val == 0 ? 'AMD' : val == 1 ? 'RUR' : 'USD';
    console.log(val);
    this.NewCreateModel.currency = val * 1;
    this.adCreateForm.get('currency').setValue(val)
    console.log(this.NewCreateModel.currency);

  }
  getFloor(val) {
    this.floorText = val;
    // this.adCreateForm.get('floor').setValue(val)
    this.NewCreateModel['floor'] = val;
    this.adCreateForm.get('floor').setValue(val)

  }
  getRooms(val) {
    this.roomText = val;
    this.adCreateForm.get('rooms').setValue(val)
    this.NewCreateModel['rooms'] = val;

  }
  getConstructionType(val) {
    this.constructionTypeText =
      val == 0
        ? this.translate.instant('createAd.stone')
        : val == 1
        ? this.translate.instant('createAd.panels')
        : val == 2
        ? this.translate.instant('createAd.monolith')
        : val == 3
        ? this.translate.instant('createAd.bricks')
        : val == 4
        ? this.translate.instant('createAd.cassete')
        : this.translate.instant('createAd.wooden');
    this.adCreateForm.get('constructionType').setValue(val)
    this.NewCreateModel['constructionType'] = val;
  }
  getPaymentTime(val) {
    // this.paymentTimeText = val == 0 ? 'Ամսավճար' : 'Օրավճար'
    // this.adCreateForm.get('paymentTime').setValue(val)
    // this.NewCreateModel['paymentTime'] = val
  }

  onUploadProfilePicture(event) {
    let file = event.target.files;
    if (file.length > 6) {
      alert('Maximum 6 images');
      file = {};
      return;
    }
    for (let i = 0; i < file.length; i++) {
      if (this.imagesList.length > 5) {
        alert('Maximum 6 images');
        return;
      }
      const reader = new FileReader();
      reader.onload = () => {
        this.profilePicBase64 = reader.result;
        // <string>this.profilePicBase64.()
        let imageText: string = this.profilePicBase64 as string;
        this.imagePreview.push(imageText); //reset Needs
        imageText = imageText.replace(/^data:image\/[a-z]+;base64,/, '');
        // this.imagesList.push(this.profilePicBase64)
        this.imagesList.push(imageText);
        imageText = '';
      };
      reader.readAsDataURL(file[i]);
    }
    this.showPreview = true;
    this.adCreateForm.get('imagesList').setValue('added')

  }
  getMark(obj: VehicleMark) {
    this.sendVehicle.mark = obj.id;
    // this.sendVehicle.sMark = obj.name
    this.vehicleMarkText = obj.name;
    this.vehicleService.getModel(obj.id).subscribe((res) => {
      this.vehicleModel = res.body as VehicleModel[];
    });
  }
  getModel(obj: VehicleModel) {
    this.vehicleModelText = obj.name;
    this.sendVehicle.model = obj.id;
    // this.sendVehicle.sModel = obj.name
  }
  getEngineSize(obj: VehicleEngineSize) {
    this.vehicleEngineSizeText = obj.size.toString();
    this.sendVehicle.engineSize = obj.id;
  }
  getEngineType(obj: VehicleEngineType) {
    this.vehicleEngineTypeText = obj.engineType;
    this.sendVehicle.engineType = obj.id;
  }
  getBodyType(obj: VehicleBodyType) {
    this.vehicleBodyTypeText = obj.name;
    this.sendVehicle.bodyType = obj.id;
    this.sendVehicle.bodyType = obj.id;
  }
  getDriveType(obj: VehicleDriverType) {
    this.vehicleDriveTypeText = obj.name;
    this.sendVehicle.driveType = obj.id;
  }
  getYear(val) {
    this.vehicleYearText = val;
    this.sendVehicle.productionYear = `${val}-01-08T08:54:46.720Z`;
  }
  getTransmissionType(obj: VehicleTransmissionType) {
    this.vehicleteTransmissionText = obj.transmissionType;
    this.sendVehicle.transmissionType = obj.id;
  }
  getColor(obj: VehicleColor) {
    this.vehicleColorText = obj.name;
    this.sendVehicle.color = obj.id;
  }
  getWhell(obj: VehicleWheelType) {
    this.whellText = obj.wheel;
    this.sendVehicle.wheel = obj.id;
  }
  getProductState(val) {
    this.productStateText =
      val == 0
        ? this.translate.instant('createAd.new')
        : this.translate.instant('createAd.used');
    this.adCreateForm.get('productState').setValue(val)
    this.NewCreateModel['productState'] = val;
  }
  getGender(val: boolean) {
    this.showGenderText =
      val == true
        ? this.translate.instant('createAd.femaleoo')
        : this.translate.instant('createAd.male');
    // this.adCreateForm.get('gender').setValue(val)
    this.NewCreateModel['gender'] = val;
  }

  //getVeficleAll
  getvehicle() {
    this.vehicleService.getMark().subscribe((res) => {
      this.vehicleMark = res.body as VehicleMark[];
    });
    this.vehicleService.getEngineSize().subscribe((res) => {
      this.vehicleEngineSize = res.body as VehicleEngineSize[];
    });
    this.vehicleService.getBodyType().subscribe((res) => {
      this.vehicleBodyType = res.body as VehicleBodyType[];
    });
    this.vehicleService.getColor().subscribe((res) => {
      this.vehicleColor = res.body as VehicleColor[];
    });
    this.vehicleService.getDriveType().subscribe((res) => {
      this.vehicleDriveType = res.body as VehicleDriverType[];
    });
    this.vehicleService.getEngineType().subscribe((res) => {
      this.vehicleEngineType = res.body as VehicleEngineType[];
    });
    this.vehicleService.getTransmissionType().subscribe((res) => {
      this.vehicleTransmissionType = res.body as VehicleTransmissionType[];
    });
    this.vehicleService.getWheelType().subscribe((res) => {
      this.vehicleWheelType = res.body as VehicleWheelType[];
    });
  }
  years() {
    var d = new Date();
    var n = d.getFullYear();
    for (let i = 1990; i <= n; i++) {
      this.yearsArr.push(i);
    }
  }
  deletePreview(val) {
    let indexPreview = this.imagePreview.findIndex((a) => a == val);
    this.imagePreview.splice(indexPreview, 1);
    let image = val.replace(/^data:image\/[a-z]+;base64,/, '');
    let index = this.imagesList.findIndex((a) => a == image);
    this.imagesList.splice(index, 1);
    this.inputFile.nativeElement.value == '';
  }

  // MAPS

  getCurrLocation() {
    if (!navigator.geolocation) {
      console.log(this.lng, this.lat);
    } else
      this.currLocation.getLocation().then((resp) => {
        this.lng = resp.lng == undefined ? 44.50275 : resp.lng;
        this.lat = resp.lat == undefined ? 40.173969 : resp.lat;
      });
  }

  chooseLocation(event) {
    this.lng = event.coords.lng;
    this.lat = event.coords.lat;
  }

  ngOnDestroy() {
    // this.inputFile.nativeElement = ""
  }
  // TEST
  // selectSubCateogoryTest(num){
  //   this.filterWithCategoryAndSubCategory(this.selectedCategoryObj.id, num)
  // }

  //Forms for ad
  bulidMainForm() {
    this.adCreateForm = this.formBuilder.group({
      userId: [''],
      id: [''],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: [''],
      country: [''],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      state: [''],
      locationLatitude: [''],
      locationLongitude: [''],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: [''],
      tags: ['', Validators.required],
      imagesList: [''],
      aim: ['', Validators.required],
      productState: ['', Validators.required],
      isRegional: [''],
    });
  }

  buildAcquaintanceForm() {
    this.adCreateForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      state: [''],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      constructionType: ['', Validators.required],
      gender: ['', Validators.required],
      height: ['', Validators.required],
      weight: ['', Validators.required],
      acquaintanceAim: ['', Validators.required],
      age: ['', Validators.required],
      isRegional: ['', Validators.required],
    });
  }

  buildCiggaretteAndAlcoholForm() {
    this.adCreateForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      state: [''],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      productState: ['', Validators.required],
      isLocal: ['', Validators.required],
      alcoholVolume: ['', Validators.required],
      isRegional: ['', Validators.required],
    });
  }

  buildClothesAndShoesForm() {
    this.adCreateForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      state: [''],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      productState: ['', Validators.required],
      clothingSize: ['', Validators.required],
      shoesSize: ['', Validators.required],
      isRegional: ['', Validators.required],
    });
  }

  buildForChildrenForm() {
    this.adCreateForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      state: [''],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      productState: ['', Validators.required],
      gender: ['', Validators.required],
      forNewBorns: ['', Validators.required],
      isRegional: ['', Validators.required],
    });
  }

  buildJewerlyAndAccessoriesForm() {
    this.adCreateForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      state: [''],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      productState: ['', Validators.required],
      gender: ['', Validators.required],
      isRegional: ['', Validators.required],
    });
  }

  buildProductsAndDrinksForm() {
    this.adCreateForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      state: [''],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      isLocal: ['', Validators.required],
      isFrozen: ['', Validators.required],
      isNaturalDrink: ['', Validators.required],
      isRegional: ['', Validators.required],
    });
  }

  buildRealEstateForm() {
    this.adCreateForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      state: [''],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      productState: ['', Validators.required],
      space: ['', Validators.required],
      constructionType: ['', Validators.required],
      // paymentTime: ['', Validators.required],
      rooms: ['', Validators.required],
      floor: ['', Validators.required],
      isRegional: ['', Validators.required],
    });
  }

  buildServicesForm() {
    this.adCreateForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      state: [''],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      // paymentTime: ['', Validators.required],
      transportation: ['', Validators.required],
      isRegional: ['', Validators.required],
    });
  }

  buildTourismAndRestForm() {
    this.adCreateForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      state: [''],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      departureDay: ['', Validators.required],
      returnDay: ['', Validators.required],
      reservedTickets: ['', Validators.required],
      isRegional: ['', Validators.required],
    });
  }

  buildWorkForm() {
    this.adCreateForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      state: [''],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      productState: ['', Validators.required],
      isRegional: ['', Validators.required],
    });
  }

  buildVehicleForm() {
    this.adCreateForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      state: [''],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      mark: ['', Validators.required],
      model: ['', Validators.required],
      productionYear: ['', Validators.required],
      customsCleared: ['', Validators.required],
      mileage: ['', Validators.required],
      bodyType: ['', Validators.required],
      engineSize: ['', Validators.required],
      sEngineType: ['', Validators.required],
      sDriveType: ['', Validators.required],
      sTransmissionType: ['', Validators.required],
      sColor: ['', Validators.required],
      sWheel: ['', Validators.required],
      isRegional: ['', Validators.required],
    });
  }
  //is regional avelacnel

  //build Form
  buildForm(id) {
    switch (id) {
      case 1: //Sale
        this.bulidMainForm();

        break;
      case 2: //Tner
        this.buildRealEstateForm();

        break;
      case 3: //Avtomeqenaner
        this.buildVehicleForm();
        break;
      case 4: //Electronics
        this.bulidMainForm();

        break;
      case 5: //Appliances
        this.bulidMainForm();

        break;
      case 6: //HouseholdGoods
        this.bulidMainForm();

        break;
      case 7: //ClothesAndShoes
        this.buildClothesAndShoesForm();

        break;
      case 8: //ForChildren
        this.buildForChildrenForm();

        break;
      case 9: //JewerlyAndAccessories
        this.buildJewerlyAndAccessoriesForm();

        break;
      case 10: //Construction
        this.bulidMainForm();

        break;
      case 11: //AllForHomeAndGArden
        this.bulidMainForm();

        break;
      case 12: //ProductsAndDrinks
        this.buildProductsAndDrinksForm();
        break;
      case 13: //CigaretteAndAlcohol
        this.buildCiggaretteAndAlcoholForm();

        break;
      case 14: //Furniture
        this.bulidMainForm();

        break;
      case 15: //TourismAndRest
        this.buildTourismAndRestForm();

        break;
      case 16: //Sport
        this.bulidMainForm();

        break;
      case 17: //PetsAndPlants
        this.bulidMainForm();

        break;
      case 18: //Culture
        this.bulidMainForm();

        break;
      case 19: //Work
        this.buildWorkForm();

        break;
      case 20: //Services
        this.buildServicesForm();

        break;
      case 21: //HealfCare
        this.bulidMainForm();

        break;
      case 22: //Acquaintance
        this.buildAcquaintanceForm();

        break;
      case 23: //EverythingElse
        this.bulidMainForm();

        break;
      case 24: //Exchange
        this.bulidMainForm();

        break;
    }
  }

  // create Ad
  createAd() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    // if(this.selectedCategoryObj.id == 2 && this.NewCreateModel['aim'] != 1)
    // {
    //   console.log('vardzov chi')
    //   this.NewCreateModel['paymentTime'] = null
    // }
    this.contact = this.adCreateForm.get('contact').value.toString();
    // this.contact = this.contact[0] == '0' ? this.contact.replace('0', '+374') : `+374${this.contact}`
    this.showLoader = true;
    this.sendData.city = this.adCreateForm.get('city').value;
    // this.NewCreateModel.contact = this.adCreateForm.get('contact').value.toString()
    this.NewCreateModel.contact = this.contact;
    this.sendData.country = this.adCreateForm.get('country').value;
    // this.NewCreateModel.currency = this.adCreateForm.get('currency').value;
    this.NewCreateModel.description =
      this.adCreateForm.get('description').value;
    // this.sendData.imagesList = this.adCreateForm.get('imagesList').value;
    // this.NewCreateModel.locationLatitude = this.adCreateForm.get('locationLatitude').value;
    // this.NewCreateModel.locationLongitude = this.adCreateForm.get('locationLongitude').value;
    this.NewCreateModel.locationLatitude = this.lat;
    this.NewCreateModel.locationLongitude = this.lng;

    this.NewCreateModel.name = this.adCreateForm.get('name').value;
    this.sendData.owner = this.adCreateForm.get('owner').value;
    this.NewCreateModel.price = this.adCreateForm.get('price').value;
    this.sendData.region = this.adCreateForm.get('region').value;
    // this.sendData.state = this.adCreateForm.get('state').value;
    this.sendData.subCategoryID = this.adCreateForm.get('subCategoryId').value;
    this.NewCreateModel.tags = this.adCreateForm.get('tags').value;
    // this.sendData.userID = this.adCreateForm.get('userID').value;
    this.NewCreateModel.userID = parseInt(localStorage.getItem('%%a$'));
    // this.sendData['salePrice'] = this.adCreateForm.get('price').value;
    // this.sendData['aim'] = this.adCreateForm.get('aim').value;
    this.NewCreateModel.salePrice = this.adCreateForm.get('price').value;

    let objToSend = {
      userID: this.NewCreateModel.userID,
      subCategoryID: this.NewCreateModel.subCategoryID,
      currency: this.NewCreateModel.currency,
      price: this.NewCreateModel.price,
      country: 1,
      region: this.NewCreateModel.region,
      city: this.NewCreateModel.city,
      // state: this.NewCreateModel.state,
      state: this.adCreateForm.get('state').value * 1,
      description: this.NewCreateModel.description,
      name: this.NewCreateModel.name,
      locationLatitude: this.NewCreateModel.locationLatitude,
      locationLongitude: this.NewCreateModel.locationLongitude,
      contact: this.NewCreateModel.contact,
      aim: this.NewCreateModel['aim'],
      owner: this.NewCreateModel.owner,
      tags: this.NewCreateModel.tags,
      isRegional:
        this.adCreateForm.get('isRegional').value == ''
          ? false
          : this.adCreateForm.get('isRegional').value,
      imagesList: this.imagesList,
    };
    console.log(
      this.NewCreateModel.locationLatitude,
      this.NewCreateModel.locationLongitude
    );
    switch (this.selectedCategoryObj.id) {
      case 1: //Sale
        break;
      case 2: //Tner
        if (
          this.NewCreateModel.subCategoryID != 4 &&
          this.NewCreateModel.subCategoryID != 5
        ) {
          objToSend['floor'] = this.NewCreateModel['floor'];
          objToSend['rooms'] = this.NewCreateModel['rooms'];
          objToSend['constructionType'] =
            this.NewCreateModel['constructionType'];
        }
        objToSend['space'] = this.adCreateForm.get('space').value;
        this.addProduct.addRealEstate(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.errorMessege = true;
            setTimeout(() => {
              this.showLoader = false;
              this.errorMessege = false;
            }, 2500);
          }
        );

        break;
      case 3: //Avtomeqenaner
        objToSend['mark'] = this.sendVehicle.mark;
        // objToSend['sMark'] = this.sendVehicle.mark
        objToSend['model'] = this.sendVehicle.model;
        // objToSend['sModel'] = this.sendVehicle.sModel
        objToSend['productionYear'] = this.sendVehicle.productionYear;
        objToSend['customsCleared'] =
          this.adCreateForm.get('customsCleared').value == ''
            ? false
            : this.adCreateForm.get('customsCleared').value;
        objToSend['mileage'] = this.adCreateForm.get('mileage').value * 1;
        objToSend['bodyType'] = this.sendVehicle.bodyType;
        // objToSend['SbodyType'] = this.sendVehicle.SbodyType
        objToSend['engineType'] = this.sendVehicle.engineType;
        objToSend['engineSize'] = this.sendVehicle.engineSize;
        objToSend['driveType'] = this.sendVehicle.driveType;
        objToSend['transmissionType'] = this.sendVehicle.transmissionType;
        objToSend['color'] = this.sendVehicle.color;
        objToSend['wheel'] = this.sendVehicle.wheel;
        this.addProduct.addVehicle(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.errorMessege = true;
            setTimeout(() => {
              this.showLoader = false;
              this.errorMessege = false;
            }, 2500);
          }
        );
        break;
      case 4: //Electronics
        objToSend['productState'] = this.NewCreateModel['productState'];
        console.log(objToSend)
        console.log(this.adCreateForm.value)
        console.log(this.adCreateForm.valid)
        // this.addProduct.addElectronics(objToSend).subscribe(
        //   (res) => {
        //     this.itemAdded = true;
        //     setTimeout(() => {
        //       this.showLoader = false;
        //       this.router.navigateByUrl(``);
        //     }, 2500);
        //   },
        //   (err) => {
        //     this.errorMessege = true;
        //     setTimeout(() => {
        //       this.showLoader = false;
        //       this.errorMessege = false;
        //     }, 2500);
        //   }
        // );

        break;
      case 5: //Appliances
        objToSend['productState'] = this.NewCreateModel['productState'];
        this.addProduct.addAppilance(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.showLoader = false;
          }
        );
        break;
      case 6: //HouseholdGoods
        objToSend['productState'] = this.NewCreateModel['productState'];
        this.addProduct.addHouseholdGoods(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.errorMessege = true;
            setTimeout(() => {
              this.showLoader = false;
              this.errorMessege = false;
            }, 2500);
          }
        );
        break;
      case 7: //ClothesAndShoes
        objToSend['productState'] = this.NewCreateModel['productState'];
        objToSend['clothingSize'] = this.adCreateForm.get('clothingSize').value;
        objToSend['shoesSize'] = this.adCreateForm.get('shoesSize').value * 1;
        this.addProduct.addClothesAndShoes(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.errorMessege = true;
            setTimeout(() => {
              this.showLoader = false;
              this.errorMessege = false;
            }, 2500);
          }
        );
        break;
      case 8: //ForChildren
        objToSend['productState'] = this.NewCreateModel['productState'];
        objToSend['gender'] = this.NewCreateModel['gender'];
        objToSend['forNewBorns'] =
          this.adCreateForm.get('forNewBorns').value == ''
            ? false
            : this.adCreateForm.get('forNewBorns').value;
        this.addProduct.addForChildren(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.errorMessege = true;
            setTimeout(() => {
              this.showLoader = false;
              this.errorMessege = false;
            }, 2500);
          }
        );
        break;
      case 9: //JewerlyAndAccessories
        objToSend['productState'] = this.NewCreateModel['productState'];
        objToSend['gender'] = this.NewCreateModel['gender'];
        this.addProduct.addJewerlyAndAccessories(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.errorMessege = true;
            setTimeout(() => {
              this.showLoader = false;
              this.errorMessege = false;
            }, 2500);
          }
        );
        break;
      case 10: //Construction
        objToSend['productState'] = this.NewCreateModel['productState'];
        this.addProduct.addConstruction(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.errorMessege = true;
            setTimeout(() => {
              this.showLoader = false;
              this.errorMessege = false;
            }, 2500);
          }
        );
        break;
      case 11: //AllForHomeAndGArden
        objToSend['productState'] = this.NewCreateModel['productState'];
        this.addProduct.addAllForHomeAndGarden(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.errorMessege = true;
            setTimeout(() => {
              this.showLoader = false;
              this.errorMessege = false;
            }, 2500);
          }
        );

        break;
      case 12: //ProductsAndDrinks
        objToSend['isLocal'] =
          this.adCreateForm.get('isLocal').value == ''
            ? false
            : this.adCreateForm.get('isLocal').value;
        objToSend['isFrozen'] =
          this.adCreateForm.get('isFrozen').value == ''
            ? false
            : this.adCreateForm.get('isFrozen').value;
        objToSend['isNaturalDrink'] =
          this.adCreateForm.get('isNaturalDrink').value == ''
            ? false
            : this.adCreateForm.get('isNaturalDrink').value;
        this.addProduct.addProductsAndDrinks(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.errorMessege = true;
            setTimeout(() => {
              this.showLoader = false;
              this.errorMessege = false;
            }, 2500);
          }
        );
        break;
      case 13: //CigaretteAndAlcohol
        objToSend['productState'] = this.NewCreateModel['productState'];
        objToSend['isLocal'] =
          this.adCreateForm.get('isLocal').value == ''
            ? false
            : this.adCreateForm.get('isLocal').value;
        objToSend['alcoholVolume'] =
          this.adCreateForm.get('alcoholVolume').value * 1;
        this.addProduct.addCigaretteAndAlcohol(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.errorMessege = true;
            setTimeout(() => {
              this.showLoader = false;
              this.errorMessege = false;
            }, 2500);
          }
        );
        break;
      case 14: //Furniture
        objToSend['productState'] = this.NewCreateModel['productState'];
        this.addProduct.addFurniture(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.errorMessege = true;
            setTimeout(() => {
              this.showLoader = false;
              this.errorMessege = false;
            }, 2500);
          }
        );
        break;
      case 15: //TourismAndRest
        objToSend['departureDay'] = this.adCreateForm.get('departureDay').value;
        objToSend['returnDay'] = this.adCreateForm.get('returnDay').value;
        objToSend['reservedTickets'] =
          this.adCreateForm.get('reservedTickets').value;
        this.addProduct.addTourism(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.errorMessege = true;
            setTimeout(() => {
              this.showLoader = false;
              this.errorMessege = false;
            }, 2500);
          }
        );
        break;
      case 16: //Sport
        objToSend['productState'] = this.NewCreateModel['productState'];
        this.addProduct.addSport(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.errorMessege = true;
            setTimeout(() => {
              this.showLoader = false;
              this.errorMessege = false;
            }, 2500);
          }
        );
        break;
      case 17: //PetsAndPlants
        objToSend['productState'] = this.NewCreateModel['productState'];
        this.addProduct.addPetsAndPlants(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.errorMessege = true;
            setTimeout(() => {
              this.showLoader = false;
              this.errorMessege = false;
            }, 2500);
          }
        );
        break;
      case 18: //Culture
        objToSend['productState'] = this.NewCreateModel['productState'];
        this.addProduct.addCulture(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.errorMessege = true;
            setTimeout(() => {
              this.showLoader = false;
              this.errorMessege = false;
            }, 2500);
          }
        );
        break;
      case 19: //Work
        delete objToSend.aim;
        objToSend['paymentTime'] = 1;
        this.addProduct.addWork(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.errorMessege = true;
            setTimeout(() => {
              this.showLoader = false;
              this.errorMessege = false;
            }, 2500);
          }
        );
        break;
      case 20: //Services
        delete objToSend.aim;
        objToSend['paymentTime'] = 1;
        objToSend['transportation'] =
          this.adCreateForm.get('transportation').value == ''
            ? false
            : this.adCreateForm.get('transportation').value;
        this.addProduct.addServices(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.errorMessege = true;
            setTimeout(() => {
              this.showLoader = false;
              this.errorMessege = false;
            }, 2500);
          }
        );
        break;
      case 21: //HealfCare
        if (this.sendData.subCategoryID != 181) {
          objToSend['productState'] = this.NewCreateModel['productState'];
        }
        this.addProduct.addHealfCare(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.errorMessege = true;
            setTimeout(() => {
              this.showLoader = false;
              this.errorMessege = false;
            }, 2500);
          }
        );
        break;
      case 22: //Acquaintance
        delete objToSend.aim;
        objToSend['gender'] = this.NewCreateModel['gender'];
        objToSend['weight'] = this.adCreateForm.get('weight').value * 1;
        objToSend['height'] = this.adCreateForm.get('height').value * 1;
        objToSend['acquaintanceAim'] = this.NewCreateModel['acquaintanceAim'];
        objToSend['age'] = this.adCreateForm.get('age').value * 1;
        this.addProduct.addAcquaintance(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.errorMessege = true;
            setTimeout(() => {
              this.showLoader = false;
              this.errorMessege = false;
            }, 2500);
          }
        );

        break;
      case 23: //EverythingElse
        this.addProduct.addEverythingElse(objToSend).subscribe(
          (res) => {
            this.itemAdded = true;
            setTimeout(() => {
              this.showLoader = false;
              this.router.navigateByUrl(``);
            }, 2500);
          },
          (err) => {
            this.errorMessege = true;
            setTimeout(() => {
              this.showLoader = false;
              this.errorMessege = false;
            }, 2500);
          }
        );

        break;
      case 24: //Exchange
        break;
    }
    console.log(objToSend);
  }

  checkTariffStatus(val) {
    this.showErrorMessage = false;
    let price = this.tariffList.find((tariff) => tariff.id == val * 1).price;
    let salePrice = this.tariffList.find(
      (tariff) => tariff.id == val * 1
    ).salePrice;
    if (
      (salePrice == price && salePrice <= this.user.balance) ||
      (salePrice < price && price <= this.user.balance)
    ) {
      this.showErrorMessage = false;
      this.adCreateForm.get('state').setValue(val * 1);
      return;
    } else {
      this.showErrorMessage = true;
      this.adCreateForm.get('state').setValue(0);
    }
  }
}
