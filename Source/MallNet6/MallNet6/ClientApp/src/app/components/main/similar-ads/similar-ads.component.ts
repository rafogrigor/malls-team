import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { GeneralCategoriesField } from 'src/model/generalCategoriesField';
import { GetProduct } from 'src/model/getProduct';
import { GetProductsService } from 'src/service/getProducts.service';

@Component({
  selector: 'app-similar-ads',
  templateUrl: './similar-ads.component.html',
  styleUrls: ['./similar-ads.component.css'],
})
export class SimilarAdsComponent implements OnInit {
  sendFilters: GetProduct = new GetProduct();
  addedResponseProduct: GeneralCategoriesField[] = [];

  constructor(
    private getProduct: GetProductsService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  @Input() categoryName: string;
  @Input() categoryId: number;

  ngOnInit(): void {
    this.sendFilters.adCount = 7;
    this.sendFilters.pageNumber = 1;
    this.detectCategory(this.categoryId);
    // this.getProduct.getHouseholdGood(this.sendFilters).subscribe(
    //   res =>
    //   {
    //     this.addedResponseProduct = res.body as GeneralCategoriesField[];
    //   }
    // )
  }

  detectCategory(id) {
    switch (id) {
      case 1: //Sale
        this.getProduct.getSale(this.sendFilters).subscribe(
          (res) => {
            if (res.body != null) {
              this.addedResponseProduct = res.body as GeneralCategoriesField[];
              this.addedResponseProduct = this.shuffle(
                this.addedResponseProduct
              );
              if (this.addedResponseProduct.length > 5) {
                this.addedResponseProduct = this.addedResponseProduct.slice(
                  0,
                  5
                );
              }
            }
          },
          (err) => console.log(err)
        );
        break;
      case 2: //Tner
        this.getProduct.getRealEstate(this.sendFilters).subscribe((res) => {
          if (res.body != null) {
            this.addedResponseProduct = res.body as GeneralCategoriesField[];
            this.addedResponseProduct = this.shuffle(this.addedResponseProduct);
            if (this.addedResponseProduct.length > 5) {
              this.addedResponseProduct = this.addedResponseProduct.slice(0, 5);
            }
          }
        });
        break;
      case 3: //Avtomeqenaner
        this.getProduct.getVehicle(this.sendFilters).subscribe((res) => {
          if (res.body != null) {
            this.addedResponseProduct = res.body as GeneralCategoriesField[];
            this.addedResponseProduct = this.shuffle(this.addedResponseProduct);
            if (this.addedResponseProduct.length > 5) {
              this.addedResponseProduct = this.addedResponseProduct.slice(0, 5);
            }
          }
        });
        break;
      case 4: //Electronics
        this.getProduct.getElectronics(this.sendFilters).subscribe((res) => {
          if (res.body != null) {
            this.addedResponseProduct = res.body as GeneralCategoriesField[];
            this.addedResponseProduct = this.shuffle(this.addedResponseProduct);
            if (this.addedResponseProduct.length > 5) {
              this.addedResponseProduct = this.addedResponseProduct.slice(0, 5);
            }
          }
        });
        break;
      case 5: //Appliances
        this.getProduct.getAppliances(this.sendFilters).subscribe((res) => {
          if (res.body != null) {
            this.addedResponseProduct = res.body as GeneralCategoriesField[];
            this.addedResponseProduct = this.shuffle(this.addedResponseProduct);
            if (this.addedResponseProduct.length > 5) {
              this.addedResponseProduct = this.addedResponseProduct.slice(0, 5);
            }
          }
        });
        break;
      case 6: //HouseholdGoods
        this.getProduct.getHouseholdGood(this.sendFilters).subscribe((res) => {
          if (res.body != null) {
            this.addedResponseProduct = res.body as GeneralCategoriesField[];
            this.addedResponseProduct = this.shuffle(this.addedResponseProduct);
            if (this.addedResponseProduct.length > 5) {
              this.addedResponseProduct = this.addedResponseProduct.slice(0, 5);
            }
          }
        });
        break;
      case 7: //ClothesAndShoes
        this.getProduct
          .getClothesAndShoes(this.sendFilters)
          .subscribe((res) => {
            if (res.body != null) {
              this.addedResponseProduct = res.body as GeneralCategoriesField[];
              this.addedResponseProduct = this.shuffle(
                this.addedResponseProduct
              );
              if (this.addedResponseProduct.length > 5) {
                this.addedResponseProduct = this.addedResponseProduct.slice(
                  0,
                  5
                );
              }
            }
          });
        break;
      case 8: //ForChildren
        this.getProduct.getForChildren(this.sendFilters).subscribe((res) => {
          if (res.body != null) {
            this.addedResponseProduct = res.body as GeneralCategoriesField[];
            this.addedResponseProduct = this.shuffle(this.addedResponseProduct);
            if (this.addedResponseProduct.length > 5) {
              this.addedResponseProduct = this.addedResponseProduct.slice(0, 5);
            }
          }
        });
        break;
      case 9: //JewerlyAndAccessories
        this.getProduct
          .getJewerlyAndAccessories(this.sendFilters)
          .subscribe((res) => {
            if (res.body != null) {
              this.addedResponseProduct = res.body as GeneralCategoriesField[];
              this.addedResponseProduct = this.shuffle(
                this.addedResponseProduct
              );
              if (this.addedResponseProduct.length > 5) {
                this.addedResponseProduct = this.addedResponseProduct.slice(
                  0,
                  5
                );
              }
            }
          });
        break;
      case 10: //Construction
        this.getProduct.getConstruction(this.sendFilters).subscribe((res) => {
          if (res.body != null) {
            this.addedResponseProduct = res.body as GeneralCategoriesField[];
            this.addedResponseProduct = this.shuffle(this.addedResponseProduct);
            if (this.addedResponseProduct.length > 5) {
              this.addedResponseProduct = this.addedResponseProduct.slice(0, 5);
            }
          }
        });
        break;
      case 11: //AllForHomeAndGArden
        this.getProduct
          .getForHomeAndGarden(this.sendFilters)
          .subscribe((res) => {
            if (res.body != null) {
              this.addedResponseProduct = res.body as GeneralCategoriesField[];
              this.addedResponseProduct = this.shuffle(
                this.addedResponseProduct
              );
              if (this.addedResponseProduct.length > 5) {
                this.addedResponseProduct = this.addedResponseProduct.slice(
                  0,
                  5
                );
              }
            }
          });
        break;
      case 12: //ProductsAndDrinks
        this.getProduct
          .getProductsAndDrinks(this.sendFilters)
          .subscribe((res) => {
            if (res.body != null) {
              this.addedResponseProduct = res.body as GeneralCategoriesField[];
              this.addedResponseProduct = this.shuffle(
                this.addedResponseProduct
              );
              if (this.addedResponseProduct.length > 5) {
                this.addedResponseProduct = this.addedResponseProduct.slice(
                  0,
                  5
                );
              }
            }
          });
        break;
      case 13: //CigaretteAndAlcohol
        this.getProduct
          .getCigaretteAndAlcohol(this.sendFilters)
          .subscribe((res) => {
            if (res.body != null) {
              this.addedResponseProduct = res.body as GeneralCategoriesField[];
              this.addedResponseProduct = this.shuffle(
                this.addedResponseProduct
              );
              if (this.addedResponseProduct.length > 5) {
                this.addedResponseProduct = this.addedResponseProduct.slice(
                  0,
                  5
                );
              }
            }
          });
        break;
      case 14: //Furniture
        this.getProduct.getFurniture(this.sendFilters).subscribe((res) => {
          if (res.body != null) {
            this.addedResponseProduct = res.body as GeneralCategoriesField[];
            this.addedResponseProduct = this.shuffle(this.addedResponseProduct);
            if (this.addedResponseProduct.length > 5) {
              this.addedResponseProduct = this.addedResponseProduct.slice(0, 5);
            }
          }
        });
        break;
      case 15: //TourismAndRest
        this.getProduct.getTourismAndRest(this.sendFilters).subscribe((res) => {
          if (res.body != null) {
            this.addedResponseProduct = res.body as GeneralCategoriesField[];
            this.addedResponseProduct = this.shuffle(this.addedResponseProduct);
            if (this.addedResponseProduct.length > 5) {
              this.addedResponseProduct = this.addedResponseProduct.slice(0, 5);
            }
          }
        });
        break;
      case 16: //Sport
        this.getProduct.getSport(this.sendFilters).subscribe((res) => {
          if (res.body != null) {
            this.addedResponseProduct = res.body as GeneralCategoriesField[];
            this.addedResponseProduct = this.shuffle(this.addedResponseProduct);
            if (this.addedResponseProduct.length > 5) {
              this.addedResponseProduct = this.addedResponseProduct.slice(0, 5);
            }
          }
        });
        break;
      case 17: //PetsAndPlants
        this.getProduct.getPetsAndPlants(this.sendFilters).subscribe((res) => {
          if (res.body != null) {
            this.addedResponseProduct = res.body as GeneralCategoriesField[];
            this.addedResponseProduct = this.shuffle(this.addedResponseProduct);
            if (this.addedResponseProduct.length > 5) {
              this.addedResponseProduct = this.addedResponseProduct.slice(0, 5);
            }
          }
        });
        break;
      case 18: //Culture
        this.getProduct.getCulture(this.sendFilters).subscribe((res) => {
          if (res.body != null) {
            this.addedResponseProduct = res.body as GeneralCategoriesField[];
            this.addedResponseProduct = this.shuffle(this.addedResponseProduct);
            if (this.addedResponseProduct.length > 5) {
              this.addedResponseProduct = this.addedResponseProduct.slice(0, 5);
            }
          }
        });
        break;
      case 19: //Work
        this.getProduct.getWork(this.sendFilters).subscribe((res) => {
          if (res.body != null) {
            this.addedResponseProduct = res.body as GeneralCategoriesField[];
            this.addedResponseProduct = this.shuffle(this.addedResponseProduct);
            if (this.addedResponseProduct.length > 5) {
              this.addedResponseProduct = this.addedResponseProduct.slice(0, 5);
            }
          }
        });
        break;
      case 20: //Services
        this.getProduct.getServices(this.sendFilters).subscribe((res) => {
          if (res.body != null) {
            this.addedResponseProduct = res.body as GeneralCategoriesField[];
            this.addedResponseProduct = this.shuffle(this.addedResponseProduct);
            if (this.addedResponseProduct.length > 5) {
              this.addedResponseProduct = this.addedResponseProduct.slice(0, 5);
            }
          }
        });
        break;
      case 21: //HealfCare
        this.getProduct.getHealfCare(this.sendFilters).subscribe((res) => {
          if (res.body != null) {
            this.addedResponseProduct = res.body as GeneralCategoriesField[];
            this.addedResponseProduct = this.shuffle(this.addedResponseProduct);
            if (this.addedResponseProduct.length > 5) {
              this.addedResponseProduct = this.addedResponseProduct.slice(0, 5);
            }
          }
        });
        break;
      case 22: //Acquaintance
        this.getProduct.getAcquaintance(this.sendFilters).subscribe((res) => {
          if (res.body != null) {
            this.addedResponseProduct = res.body as GeneralCategoriesField[];
            this.addedResponseProduct = this.shuffle(this.addedResponseProduct);
            if (this.addedResponseProduct.length > 5) {
              this.addedResponseProduct = this.addedResponseProduct.slice(0, 5);
            }
          }
        });
        break;
      case 23: //EverythingElse
        this.getProduct.getEverythingElse(this.sendFilters).subscribe((res) => {
          if (res.body != null) {
            this.addedResponseProduct = res.body as GeneralCategoriesField[];
            this.addedResponseProduct = this.shuffle(this.addedResponseProduct);
            if (this.addedResponseProduct.length > 5) {
              this.addedResponseProduct = this.addedResponseProduct.slice(0, 5);
            }
          }
        });
        break;
      case 24: //Exchange
        this.getProduct.getExchange(this.sendFilters).subscribe((res) => {
          this.addedResponseProduct = this.shuffle(this.addedResponseProduct);
          if (res.body != null) {
            this.addedResponseProduct = res.body as GeneralCategoriesField[];
            if (this.addedResponseProduct.length > 5) {
              this.addedResponseProduct = this.addedResponseProduct.slice(0, 5);
            }
          }
        });
        break;
    }
  }

  shuffle(array) {
    // var currentIndex = array.length,  randomIndex;

    // // While there remain elements to shuffle...
    // while (0 !== currentIndex) {

    //   // Pick a remaining element...
    //   randomIndex = Math.floor(Math.random() * currentIndex);
    //   currentIndex--;

    //   // And swap it with the current element.
    //   [array[currentIndex], array[randomIndex]] = [
    //     array[randomIndex], array[currentIndex]];
    // }
    console.log(array.sort(() => Math.random() - 0.5));
    return array.sort(() => Math.random() - 0.5);

    // return array;
  }

  redirectToPage(obj: GeneralCategoriesField) {
    // console.log(obj)
    // this.router.navigateByUrl(`item/${this.categoryName}/${obj.id}`)
    this.router
      .navigateByUrl(`item/${this.categoryName}/${this.categoryId}/${obj.id}`)
      .then(() => location.reload());
  }
}
