import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Categories } from 'src/model/categories';
import { SubCategories } from 'src/model/subCategories';
import { CategoryService } from 'src/service/category.service';

@Component({
  selector: 'app-category-filter',
  templateUrl: './category-filter.component.html',
  styleUrls: ['./category-filter.component.css']
})
export class CategoryFilterComponent implements OnInit {

  categories: Categories[];
  subCategories: SubCategories[]
  subCategoryName: string;
  categoryName: string;
  selectedCategory: Categories;
  selectedSubCategory: SubCategories;

  showRealEstate: boolean = false;
  showGender: boolean = false;
  showForNewBorns: boolean = false;
  showIsLocal: boolean = false;
  showisFrozen: boolean = false;
  showisNaturalDrink: boolean = false;
  showAlcoholVolume: boolean = false;
  showDepartureDay: boolean = false;
  showReturnDay: boolean = false;
  showReservedTickets: boolean = false;
  showAdPaymentTime: boolean = false;
  showPaymentTime: boolean = false;
  showTransportation: boolean = false;
  showAcquintanceAim: boolean = false;
  showHeight: boolean = false;
  showWeight: boolean = false;
  showAgeFrom: boolean = false;
  showAgeTo: boolean = false;
  showClothingSize: boolean = false;
  showShoesSize: boolean = false;

  showAIMall: boolean = true;
  showAdOwner: boolean = true;
  showCurrency: boolean = true;
  showProductState: boolean = true;

  formIsCreated: boolean = false;
  endOfContent: boolean = false;
  loading: boolean = false;
  empty: boolean = false;

  constructor(private route: ActivatedRoute, private categoryService: CategoryService) { }


  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap(params => params.getAll('name'))).subscribe(data=>
         {
            this.categoryName = data;
            this.categoryService.getAllCategories().subscribe(
              res =>
              {
                this.categories = res.body as Categories[];
                this.selectedCategory = this.categories.find(e => e.category == this.categoryName)
                this.readSubCategoryFromLink();
              }
            )
        }
      );
  }

  readSubCategoryFromLink()
  {
    this.route.paramMap.pipe(
      switchMap(params => params.getAll('subName'))).subscribe(data=>
         {
            this.subCategoryName = data
            this.categoryService.getSubCategory(this.selectedCategory.id).subscribe(
              res =>
              {
                this.subCategories = res.body as SubCategories[];
                this.selectedSubCategory = this.subCategories.find(e => e.type == this.subCategoryName)
                this.readSubCategoryFromLink();
              }
            )
        }
      );
  }

}
