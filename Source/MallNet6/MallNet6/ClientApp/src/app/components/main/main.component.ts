import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FavoriteRequest } from 'src/model/favoriteRequest';
import { MainPageProducts } from 'src/model/mainPageProducts';
import { ProductsMain } from 'src/model/products-main';
import { DataExchangeService } from 'src/service/dataExchange.service';
import { FavoriteService } from 'src/service/favorite.service';
import { LocationService } from 'src/service/location.service';
import { MessagingService } from 'src/service/messaging.service';
import { ProductsService } from 'src/service/products.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
})
export class MainComponent implements OnInit {
  products: ProductsMain[];
  mainProducts: MainPageProducts[];
  loginMessege: boolean = false;
  constructor(
    private prService: ProductsService,
    private location: LocationService,
    private router: Router,
    private favService: FavoriteService,
    private activatedRoute: ActivatedRoute,
    protected dataEx: DataExchangeService,
    private messagingService: MessagingService
  ) {
    activatedRoute.queryParams.subscribe((res) => {
      if (res['accessDenied'] == 'true') {
        // console.log(res['accessDenied'])
        dataEx.updateObj(true);
      }
    });
  }

  ngOnInit() {
    this.prService.getMainPageProducts().subscribe((res) => {
      this.mainProducts = res.body['adsWithCategories'] as MainPageProducts[];
      // console.log(this.mainProducts.length);
      this.mainProducts.splice(this.mainProducts.length - 1, 1);
      // this.messagingService.requestPermission();
      // this.messagingService.receiveMessage();
    });
  }
  productInfo(obj, product) {
    // this.router.navigateByUrl(`category/${obj.category['name']}`)
    this.router.navigateByUrl(
      `item/${obj.category['name']}/${obj.category.id}/${product.id}`
    );
  }

  addFavorite(obj) {
    this.favService
      .addFavorite(obj * 1)
      .subscribe((res) => this.dataEx.addFavorite(1));
  }
  openCategory(obj) {
    this.router.navigateByUrl(`category/${obj}`);
  }
}
