import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Categories } from 'src/model/categories';
import { City } from 'src/model/city';
import { Country } from 'src/model/country';
import { Curency } from 'src/model/curency';
import { Region } from 'src/model/region';
import { SubCategories } from 'src/model/subCategories';
import { User } from 'src/model/user';
import { VehicleCreate } from 'src/model/vehicleCreate';
import { VehicleEngineSize } from 'src/model/vehicleEngineSize';
import { VehicleMark } from 'src/model/vehicleMark';
import { VehicleMileage } from 'src/model/vehicleMileage';
import { VehicleModel } from 'src/model/vehicleModel';
import { AddAllCategoriesService } from 'src/service/addAllCategories.service';
import { AddProductService } from 'src/service/addProduct.service';
import { AllForVehicleService } from 'src/service/allForVehicle.service';
import { CategoryService } from 'src/service/category.service';
import { LocationService } from 'src/service/location.service';
import { UserService } from 'src/service/user.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {


  categories: Categories[] = [];
  subCategories: SubCategories[] = [];
  currency: Curency[] = []
  user: User;
  vehicle: FormGroup;
  addVehicle: VehicleCreate = new VehicleCreate;
  showVehicleForm: boolean = false;
  selectedCategoryId: number;
  disableSubCategories = true;
  realEstate = false;
  appliance = false;
  clothesAndShoes = false;
  forNewBorns = false;
  jewerlyAndAccessories = false;
  productsAndDrinks = false;
  cigaretteAndAlcohol = false;
  tourism = false;
  work = false;
  services = false;
  acquaintance = false;
  everythingElse = false;
  showOption: boolean;
  showCategoryOption: boolean
  addForm: FormGroup;
  profilePicBase64: string | ArrayBuffer;
  imagesList: any[] = [];
  countries: Country[] = []
  regionList: Region[] = []
  cityList: City[] = []
  vehicleEnginSizeList: VehicleEngineSize[] = [];
  vehicleMilleageList: VehicleMileage[] = [];
  vehicleMarkList: VehicleMark[] = [];
  vehicleModelList: VehicleModel[] = [];
  itemAdded: boolean = false
  yearsArr: number[] = []
  loaderShow: boolean = false;


  @ViewChild('region') clickRegion: any;
  constructor(private categoryService: CategoryService, private formBuilder: FormBuilder,
              private addAllCategories: AddAllCategoriesService, private userService: UserService,
              private addProduct: AddProductService, private LocationService: LocationService,
              private router: Router, private vehicelService: AllForVehicleService
              ) { }

  ngOnInit(): void {
    this.years()
    this.showOption = true;
    this.showCategoryOption = true;
    this.buidForm()
    this.categoryService.getAllCategories().subscribe(
      res =>{
        this.categories = res.body as Categories[]
        this.categories.splice(0,1)
        this.categories.splice(22,1)
      }
    )
    this.addAllCategories.getCurrentList().subscribe(
      res => this.currency = res.body as Curency[]
    )
    this.userService.getUser(localStorage.getItem('%%a$')).subscribe(
      res => {
        this.user = res.body as User
      }
    )
    this.LocationService.getLocation().subscribe(
      res => this.countries = res.body as Country[]
    )
  }

  buidForm(){
    this.addForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      productState: ['', Validators.required],
      space: ['', Validators.required],
      constructionType: ['', Validators.required],
      paymentTime: ['', Validators.required],
      rooms: ['', Validators.required],
      floor: ['', Validators.required],
      clothingSize: ['', Validators.required],
      shoesSize: ['', Validators.required],
      gender: ['', Validators.required],
      forNewBorns: ['', Validators.required],
      isLocal: ['', Validators.required],
      isFrozen: ['', Validators.required],
      isNaturalDrink: ['', Validators.required],
      alcoholVolume: ['', Validators.required],
      departureDay: ['', Validators.required],
      returnDay: ['', Validators.required],
      reservedTickets: ['', Validators.required],
      transportation: ['', Validators.required],
      height: ['', Validators.required],
      weight: ['', Validators.required],
      acquaintanceAim: ['', Validators.required],
      mark: ['', Validators.required],
      model: ['', Validators.required],
      productionYear: ['', Validators.required],
      customsCleared: ['', Validators.required],
      mileage: ['', Validators.required],
      bodyType: ['', Validators.required],
      engineType: ['', Validators.required],
      engineSize: ['', Validators.required],
      driveType: ['', Validators.required],
      transmissionType: ['', Validators.required],
      color: ['', Validators.required],
      wheel: ['', Validators.required],
    });
  }

  // buildVehicleForm()
  // {
  //   this.vehicle = this.formBuilder.group({
  //     userId: ['', Validators.required],
  //     subCategoryID: ['', Validators.required],
  //     currency: ['', Validators.required],
  //     price: ['', Validators.required],
  //     country: ['', Validators.required],
  //     region: ['', Validators.required],
  //     city: ['', Validators.required],
  //     state: ['', Validators.required],
  //     description: ['', Validators.required],
  //     name: ['', Validators.required],
  //     locationLatitude: ['', Validators.required],
  //     locationLongitude: ['', Validators.required],
  //     contact: ['', Validators.required],
  //     aim: ['', Validators.required],
  //     owner: ['', Validators.required],
  //     tags: ['', Validators.required],
  //     imagesList: ['', Validators.required],
  //     mark: ['', Validators.required],
  //     model: ['', Validators.required],
  //     productionYear: ['', Validators.required],
  //     customsCleared: ['', Validators.required],
  //     mileage: ['', Validators.required],
  //     bodyType: ['', Validators.required],
  //     engineType: ['', Validators.required],
  //     engineSize: ['', Validators.required],
  //     driveType: ['', Validators.required],
  //     transmissionType: ['', Validators.required],
  //     color: ['', Validators.required],
  //     wheel: ['', Validators.required],

  //   })
  // }

  addData(){
    this.loaderShow = true;
    let userId = localStorage.getItem('%%a$')
    let newObjectToSend = {
      userID: parseInt(userId),
      subCategoryID: this.addForm.get('subCategoryId').value*1,
      currency: this.addForm.get('currency').value*1,
      price: this.addForm.get('price').value*1,
      salePercent: 0,
      // salePrice: 1,
      country: this.addForm.get('country').value,
      region: this.addForm.get('region').value,
      city: this.addForm.get('city').value,
      state: this.user.state,
      description: this.addForm.get('description').value,
      name: this.addForm.get('name').value,
      locationLatitude: 0,
      locationLongitude: 0,
      contact: this.addForm.get('contact').value,
      owner: this.addForm.get('owner').value == null ? 0: this.addForm.get('owner').value*1,
      comment: 'NO COMMENTS',
      tags: this.addForm.get('tags').value,
      imagesList: this.imagesList
      // imagesList: []
    }
    switch (this.selectedCategoryId) {
      case 2: //Tner
        newObjectToSend['space'] = this.addForm.get('space').value*1;
        newObjectToSend['paymentTime'] = this.addForm.get('paymentTime').value*1;
        newObjectToSend['floor'] = this.addForm.get('floor').value*1;
        newObjectToSend['rooms'] = this.addForm.get('rooms').value*1;
        newObjectToSend['constructionType'] = this.addForm.get('constructionType').value*1;
        this.addProduct.addRealEstate(newObjectToSend).subscribe(
          res => {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
      case 3: //Avtomeqenaner
        newObjectToSend['mark'] = this.addForm.get('mark').value *1;
        newObjectToSend['model'] = this.addForm.get('model').value *1;
        let year = `${this.addForm.get('productionYear').value *1}-01-08T08:54:46.720Z`
        newObjectToSend['productionYear'] = year;
        console.log(newObjectToSend)
        // newObjectToSend['productionYear'] = this.addForm.get('productionYear').value *1;
        newObjectToSend['customsCleared'] = this.addForm.get('customsCleared').value*1 == 1 ? true : false;
        newObjectToSend['mileage'] = this.addForm.get('mileage').value *1;
        newObjectToSend['bodyType'] = this.addForm.get('bodyType').value *1;
        newObjectToSend['engineType'] = this.addForm.get('engineType').value *1;
        newObjectToSend['engineSize'] = this.addForm.get('engineSize').value *1;
        newObjectToSend['driveType'] = this.addForm.get('driveType').value *1;
        newObjectToSend['transmissionType'] = this.addForm.get('transmissionType').value *1;
        newObjectToSend['color'] = this.addForm.get('color').value *1;
        newObjectToSend['wheel'] = this.addForm.get('wheel').value *1;
        this.addProduct.addVehicle(newObjectToSend).subscribe(
          res =>
          {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
      case 4: //Electronics
        newObjectToSend['productState'] = this.addForm.get('productState').value*1;
        this.addProduct.addElectronics(newObjectToSend).subscribe(
          res => {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
      case 5://Appliances
        newObjectToSend['productState'] = this.addForm.get('productState').value*1;
        this.addProduct.addAppilance(newObjectToSend).subscribe(
          res => {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
      case 6://HouseholdGoods
        newObjectToSend['productState'] = this.addForm.get('productState').value*1;
        this.addProduct.addHouseholdGoods(newObjectToSend).subscribe(
          res => {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
      case 7://ClothesAndShoes
        newObjectToSend['productState'] = this.addForm.get('productState').value*1;
        newObjectToSend['clothingSize'] = this.addForm.get('clothingSize').value;
        newObjectToSend['shoesSize'] = this.addForm.get('shoesSize').value*1;
        this.addProduct.addClothesAndShoes(newObjectToSend).subscribe(
          res => {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
      case 8://ForChildren
        newObjectToSend['productState'] = this.addForm.get('productState').value*1;
        newObjectToSend['gender'] = this.addForm.get('gender').value*1 == 1 ? true : false;
        newObjectToSend['forNewBorns'] = this.addForm.get('forNewBorns').value == null ? false: true;
        this.addProduct.addForChildren(newObjectToSend).subscribe(
          res => {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
      case 9://JewerlyAndAccessories
        newObjectToSend['productState'] = this.addForm.get('productState').value*1;
        newObjectToSend['gender'] = this.addForm.get('gender').value*1 == 1 ? true : false;
        this.addProduct.addJewerlyAndAccessories(newObjectToSend).subscribe(
          res => {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
      case 10://Construction
        newObjectToSend['productState'] = this.addForm.get('productState').value*1;
        this.addProduct.addConstruction(newObjectToSend).subscribe(
          res => {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
      case 11://AllForHomeAndGarden
        newObjectToSend['productState'] = this.addForm.get('productState').value*1;
        this.addProduct.addAllForHomeAndGarden(newObjectToSend).subscribe(
          res => {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
        case 12://ProductsAndDrinks
        newObjectToSend['isLocal'] = this.addForm.get('isLocal').value == null ? false: true;
        newObjectToSend['isFrozen'] = this.addForm.get('isFrozen').value == null ? false: true;
        newObjectToSend['isNaturalDrink'] = this.addForm.get('isNaturalDrink').value == null ? false: true;
        this.addProduct.addProductsAndDrinks(newObjectToSend).subscribe(
          res => {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
      case 13://CigaretteAndAlcohol
        newObjectToSend['productState'] = this.addForm.get('productState').value*1;
        newObjectToSend['alcoholVolume'] = this.addForm.get('alcoholVolume').value*1;
        newObjectToSend['isLocal'] = this.addForm.get('isLocal').value == null ? false: true;
        this.addProduct.addCigaretteAndAlcohol(newObjectToSend).subscribe(
          res => {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
      case 14://Furniture
        newObjectToSend['productState'] = this.addForm.get('productState').value*1;
        this.addProduct.addFurniture(newObjectToSend).subscribe(
          res => {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
      case 15://TourismAndRest
        newObjectToSend['departureDay'] = this.addForm.get('departureDay').value;
        newObjectToSend['returnDay'] = this.addForm.get('returnDay').value;
        newObjectToSend['reservedTickets'] = this.addForm.get('reservedTickets').value*1;
        this.addProduct.addTourism(newObjectToSend).subscribe(
          res => {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
      case 16://Sport
        newObjectToSend['productState'] = this.addForm.get('productState').value*1;
        this.addProduct.addSport(newObjectToSend).subscribe(
          res => {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
      case 17://PetsAndPlants
        newObjectToSend['productState'] = this.addForm.get('productState').value*1;
        this.addProduct.addPetsAndPlants(newObjectToSend).subscribe(
          res => {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
      case 18://Culture
        newObjectToSend['productState'] = this.addForm.get('productState').value*1;
        this.addProduct.addCulture(newObjectToSend).subscribe(
          res => {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
      case 19://Work
        newObjectToSend['paymentTime'] = this.addForm.get('paymentTime').value*1;
        this.addProduct.addWork(newObjectToSend).subscribe(
          res => {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
      case 20://Services
        newObjectToSend['paymentTime'] = this.addForm.get('paymentTime').value*1;
        newObjectToSend['transportation'] = this.addForm.get('transportation').value == null ? false: true;
        this.addProduct.addServices(newObjectToSend).subscribe(
          res => {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
      case 21://HealfCare
        newObjectToSend['productState'] = this.addForm.get('productState').value*1;
        this.addProduct.addHealfCare(newObjectToSend).subscribe(
          res => {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
      case 22://Acquaintance
        newObjectToSend['gender'] = this.addForm.get('gender').value == 1 ? true: false;
        // newObjectToSend['subCategoryID'] = this.addForm.get('').value == 1 ? true: false;
        newObjectToSend['height'] = this.addForm.get('height').value*1;
        // console.log(this.addForm.get('height').value*1)
        newObjectToSend['weight'] = this.addForm.get('weight').value*1;
        newObjectToSend['acquaintanceAim'] = this.addForm.get('acquaintanceAim').value*1
        this.addProduct.addAcquaintance(newObjectToSend).subscribe(
          res => {
            this.loaderShow = false;
            this.imagesList = []
            this.itemAdded = true
            this.addForm.reset()
            setTimeout(()=>{
              this.router.navigateByUrl('')
            },1500)
          },
          err => {
            this.loaderShow = false;
            console.log(err)
          }
        )
        break;
      case 23://EverythingElse
        //aranc aim-i
        break;
      case 24://Exchange

        break;

      default:

        break;
    }
    // console.log(this.imagesList)
    console.log(newObjectToSend)
  }
  selectedCountry(val)
  {
    this.addForm.get('country').setValue(parseInt(val))
    this.LocationService.getRegion(parseInt(val)).subscribe(
      res => {
        this.regionList = res.body as Region[]
        // setTimeout(() =>
        // {
        //   console.log(this.clickRegion['nativeElement'].options[0].click())

        // },0)
      }
    )
  }
  selectedRegion(val)
  {
    this.addForm.get('region').setValue(parseInt(val))
    this.LocationService.getCity(parseInt(val)).subscribe(
      res => {
        this.cityList = res.body as City[]
      }
    )
  }
  selectedCity(val)
  {
    this.addForm.get('city').setValue(parseInt(val))
  }
  setCategory(id){
    // console.log(id)
    this.showCategoryOption = false;
    // this.addForm.reset()
    this.addForm.get('name').reset()
    // this.addForm.get('subCategory').reset()
    this.addForm.get('price').reset()
    this.addForm.get('name').reset()
    this.addForm.get('description').reset()
    this.addForm.get('tags').reset()
    id = id*1
    this.selectedCategoryId = id*1
    this.realEstate = false;
    this.appliance = false;
    this.clothesAndShoes = false;
    this.forNewBorns = false;
    this.jewerlyAndAccessories = false;
    this.productsAndDrinks = false;
    this.cigaretteAndAlcohol = false;
    this.tourism = false;
    this.work = false;
    this.services = false;
    this.acquaintance = false;
    this.everythingElse = false;
    this.showVehicleForm = false;
    this.categoryService.getSubCategory(id).subscribe(
      res =>{
        this.subCategories = res.body as SubCategories[]
        this.disableSubCategories = false
        this.showOption = false;
      }
    )
    // this.showVehicleForm = false;
    switch (id) {
      case 2:
        this.realEstate = true;
        break;
      case 3:
        this.showVehicleForm = true;
        this.getAllForVehicle()
        break;
      case 4:
        this.appliance = true;
        break;
      case 5:
        this.appliance = true;

        break;
      case 6:
        this.appliance = true;

        break;
      case 7:
        this.clothesAndShoes = true;

        break;
      case 8:
        this.forNewBorns = true;

        break;
      case 9:
        this.jewerlyAndAccessories = true;

        break;
      case 10:
        this.appliance = true;

        break;
      case 11:
        this.appliance = true;

        break;
      case 12:
        this.productsAndDrinks = true;

        break;
      case 13:
        this.cigaretteAndAlcohol = true;

        break;
      case 14:
        this.appliance = true;

        break;
      case 15:
        this.tourism = true;

        break;
      case 16:
        this.appliance = true;

        break;
      case 17:
        this.appliance = true;

        break;
      case 18:
        this.appliance = true;

        break;
      case 19:
        this.work = true;

        break;
      case 20:
        this.services = true;

        break;
      case 21:
        this.appliance = true;

        break;
      case 22:
        this.acquaintance = true;

        break;
      case 23:
        this.everythingElse = true;

        break;
      case 24:
        //exchange
        break;

      default:
        break;
    }
    console.log(id)

  }
  onUploadProfilePicture(event) {
    const file = event.target.files;
    for(let i = 0; i< file.length; i++){
      const reader = new FileReader();
    reader.onload = () => {
      this.profilePicBase64 = reader.result;
      // <string>this.profilePicBase64.()
      let imageText: string = this.profilePicBase64 as string;
      imageText = imageText.replace(/^data:image\/[a-z]+;base64,/, "")
      // this.imagesList.push(this.profilePicBase64)
      this.imagesList.push(imageText);
      imageText = ""
    };
    reader.readAsDataURL(file[i]);
    }

  }
  getAllForVehicle()
  {
    this.vehicelService.getEngineSize().subscribe(
      res =>
      {
        this.vehicleEnginSizeList = res.body as VehicleEngineSize[]
      }
    )
    this.vehicelService.getMark().subscribe(
      res =>
      {
        this.vehicleMarkList = res.body as VehicleMark[]
      }
    )
    this.vehicelService.getMileage().subscribe(
      res =>
      {
        this.vehicleMilleageList = res.body as VehicleMileage[]
      }
    )
  }

  setModel(id)
  {
    this.vehicelService.getModel(id).subscribe(
      res =>
      {
        this.vehicleModelList = res.body as VehicleModel[]
      }
    )
  }

  years()
  {
    var d = new Date();
    var n = d.getFullYear();
    for(let i = 1990; i <= n; i++){
      this.yearsArr.push(i)
    }
  }
}

