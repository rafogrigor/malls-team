import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Categories } from 'src/model/categories';
import { SearchResponseModel } from 'src/model/searchResponseModel';
import { CategoryService } from 'src/service/category.service';
import { SearchService } from 'src/service/search.service';

@Component({
  selector: 'app-search-items',
  templateUrl: './search-items.component.html',
  styleUrls: ['./search-items.component.css']
})
export class SearchItemsComponent implements OnInit {

  searchText: string;
  searchItems: SearchResponseModel[] = []
  categories: Categories[] = [];

  constructor(private route: ActivatedRoute, private searchService: SearchService, private router: Router, private categoryS: CategoryService) {
    route.queryParams.subscribe(
      res =>
      {
        this.searchText = res['search']
        let body = {
          countryID: 1,
          textAM: this.searchText,
          textEN: this.searchText,
          textRU: this.searchText
        }
        this.searchService.search(body).subscribe(
          res =>
          {
            this.searchItems = res.body as SearchResponseModel[]
            if(this.searchItems)
              this.searchItems.reverse()
          }
        )
      }
    )
   }

  ngOnInit(): void {
    let body = {
      countryID: 1,
      textAM: this.searchText,
      textEN: this.searchText,
      textRU: this.searchText
    }
    this.searchService.search(body).subscribe(
      res =>
      {
        this.searchItems = res.body as SearchResponseModel[]
        if(this.searchItems)
          this.searchItems.reverse()
      }
    )
  }
  productInfo(obj: SearchResponseModel): void
  {
    let categoryName: string;
    this.categoryS.getAllCategories().subscribe(
      res =>
      {
        this.categories = res.body as Categories[];
        categoryName = this.categories.find(c => c.id == parseInt(obj.categoryID)).category
        // console.log(categoryName)
        this.router.navigateByUrl(`item/${categoryName}/${obj.categoryID}/${obj.id}`)
      }
    )
  }

}
