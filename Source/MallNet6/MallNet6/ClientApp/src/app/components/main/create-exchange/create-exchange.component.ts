import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { City } from 'src/model/city';
import { Country } from 'src/model/country';
import { Curency } from 'src/model/curency';
import { GeneralCategoriesField } from 'src/model/generalCategoriesField';
import { NewCreateModel } from 'src/model/newCreateModel';
import { Region } from 'src/model/region';
import { AddAllCategoriesService } from 'src/service/addAllCategories.service';
import { AddProductService } from 'src/service/addProduct.service';
import { GetCurrentLocationService } from 'src/service/getCurrentLocation.service';
import { LocationService } from 'src/service/location.service';

@Component({
  selector: 'app-create-exchange',
  templateUrl: './create-exchange.component.html',
  styleUrls: ['./create-exchange.component.css']
})
export class CreateExchangeComponent implements OnInit {
  countries: Country[] = [];
  regions: Region[] = [];
  cities: City[] = [];
  profilePicBase64: string | ArrayBuffer;
  imagesList: any[] = [];
  imagePreview: any[] = [];
  adCreateForm: FormGroup;
  NewCreateModel: NewCreateModel = new NewCreateModel;
  sendData: GeneralCategoriesField = new GeneralCategoriesField;
  currencyList: Curency[]

  lat: number = 40.173969;
  lng: number = 44.502750;
  showPreview: boolean = false;
  showLoader: boolean = false;
  itemAdded: boolean = false;
  errorMessege: boolean = false;
  contact: string;





  // strings
  regionText: string = this.translate.instant('createAd.region')
  cityText: string = this.translate.instant('createAd.city');
  ownerText: string = this.translate.instant('createAd.owner');
  // currency: string = this.translate.instant('createAd.currency');



  constructor(private translate: TranslateService, private locationService: LocationService,
    private formBuilder: FormBuilder, private currLocation: GetCurrentLocationService,
    private addProduct: AddProductService, private router: Router,
    private currencyService: AddAllCategoriesService) { }

  @ViewChild('fileUpload') inputFile: ElementRef;

  ngOnInit(): void {
    this.buildForm();
    //get country
    this.locationService.getLocation().subscribe(
      res =>
      {
        this.countries = res.body as Country[]
        //get region
        this.locationService.getRegion(1).subscribe(
          res =>
          {
            this.regions = res.body as Region[]
          }
        )
      }
    )
    this.getCurrLocation()
    this.currencyService.getCurrentList().subscribe(
      res =>
      {
        this.currencyList = res.body as Curency[]
        this.currencyList.splice(2,1)
      }
    )
  }


  // Form
  buildForm()
  {
    this.adCreateForm = this.formBuilder.group({
      userId: [''],
      id: [''],
      subCategoryId: [''],
      currency: [''],
      price: [0],
      salePercent: [''],
      country: [1],
      region: [''],
      city: [''],
      description: ['', Validators.required],
      name: ['', Validators.required],
      locationLatitude: [''],
      locationLongitude: [''],
      contact: ['', Validators.required],
      owner: [''],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      isRegional: [''],
      sourceCurrencyID: [3],
      destinationCurrencyID: ['', Validators.required],
      saleSummaRetail: [''],
      buySummaRetail: [''],
      saleSumma: ['', Validators.required],
      buySumma: ['', Validators.required]
    })
    this.adCreateForm.get('sourceCurrencyID').setValue(3)
    this.adCreateForm.get('destinationCurrencyID').setValue(1)
  }

  // Region and City
  getRegion(obj:Region)
  {
    this.regionText = obj.country;
    this.cityText = this.translate.instant('createAd.city')
    // this.adCreateForm.get('region').setValue(obj.id)
    this.NewCreateModel.region = obj.id

    //get city
    this.locationService.getCity(obj.id).subscribe(
      res =>
      {
        this.cities = res.body as City[]
      }
    )
  }
  getCity(obj: City)
  {
    this.cityText = obj.country
    // this.adCreateForm.get('city').setValue(obj.id)
    this.NewCreateModel.city = obj.id
  }

  // Owner
  getOwner(obj)
  {
    this.ownerText = obj == 0 ? this.translate.instant('createAd.ownerArr.individual'): obj == 1 ? this.translate.instant('createAd.ownerArr.agency') : this.translate.instant('createAd.ownerArr.diller')
    // this.adCreateForm.get('owner').setValue(obj)
    this.NewCreateModel.owner = obj
  }

  // Currency
  // getCurrency(val)
  // {
  //   this.currency = val == 0 ? 'AMD' : val == 1 ? 'RUR' : 'USD'
  //   this.NewCreateModel.currency = val*1
  // }

  // Photo
  onUploadProfilePicture(event)
  {
    let file = event.target.files;
    if(file.length > 6)
    {
      alert('Maximum 6 images')
      file = {}
      return
    }
    for(let i = 0; i< file.length; i++){
      if(this.imagesList.length>5)
      {
        alert('Maximum 6 images')
        return
      }
      const reader = new FileReader();
      reader.onload = () => {
      this.profilePicBase64 = reader.result;
      // <string>this.profilePicBase64.()
      let imageText: string = this.profilePicBase64 as string;
      this.imagePreview.push(imageText); //reset Needs
      imageText = imageText.replace(/^data:image\/[a-z]+;base64,/, "")
      // this.imagesList.push(this.profilePicBase64)
      this.imagesList.push(imageText);
      imageText = ""
    };
    reader.readAsDataURL(file[i]);
    }
    this.showPreview = true;
  }
  deletePreview(val){
    let indexPreview = this.imagePreview.findIndex((a) => a == val)
    this.imagePreview.splice(indexPreview, 1)
    let image = val.replace(/^data:image\/[a-z]+;base64,/, "")
    let index = this.imagesList.findIndex((a) => a == image)
    this.imagesList.splice(index, 1)
    this.inputFile.nativeElement.value==""
  }

  // MapLocation
  chooseLocation(event)
  {
    this.lng = event.coords.lng;
    this.lat = event.coords.lat;
  }
  getCurrLocation()
  {
    if(!navigator.geolocation)
    {
      console.log(this.lng, this.lat)
    }
    else
      this.currLocation.getLocation().then(resp =>
        {
          this.lng = resp.lng == undefined ? 44.502750 : resp.lng;
          this.lat = resp.lat == undefined ? 40.173969 : resp.lat;
        })
  }

  createAd()
  {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    this.contact = this.adCreateForm.get('contact').value.toString();
    this.contact = this.contact[0] == '0' ? this.contact.replace('0', '+374') : `+374${this.contact}`
    sessionStorage.setItem('Mobile', '123')
    // this.showLoader = true;
    this.sendData.city = this.adCreateForm.get('city').value*1;
    this.NewCreateModel.contact = this.contact
    this.sendData.country = this.adCreateForm.get('country').value*1;
    this.NewCreateModel.description = this.adCreateForm.get('description').value;
    this.NewCreateModel.locationLatitude = this.lat
    this.NewCreateModel.locationLongitude = this.lng
    this.NewCreateModel.name = this.adCreateForm.get('name').value;
    this.sendData.owner = this.adCreateForm.get('owner').value*1;
    this.NewCreateModel.price = this.adCreateForm.get('price').value*1;
    this.sendData.region = this.adCreateForm.get('region').value*1;
    this.sendData.subCategoryID = this.adCreateForm.get('subCategoryId').value*1;
    this.NewCreateModel.tags = this.adCreateForm.get('tags').value;
    this.NewCreateModel.userID = parseInt(localStorage.getItem('%%a$'))
    this.NewCreateModel.salePrice = this.adCreateForm.get('price').value*1;

    let objToSend = {
      userID: this.NewCreateModel.userID,
      subCategoryID: 185,
      currency: 1,
      price: 0,
      country: 1,
      region: this.NewCreateModel.region,
      city: this.NewCreateModel.city,
      // state: this.NewCreateModel.state,
      state: 0,
      description: this.NewCreateModel.description,
      name: this.NewCreateModel.name,
      locationLatitude: this.NewCreateModel.locationLatitude,
      locationLongitude: this.NewCreateModel.locationLongitude,
      contact: this.NewCreateModel.contact,
      // aim: this.NewCreateModel['aim'],
      owner: this.NewCreateModel.owner,
      tags: this.NewCreateModel.tags,
      isRegional: this.adCreateForm.get('isRegional').value == "" ? false: this.adCreateForm.get('isRegional').value,
      imagesList: this.imagesList,
      sourceCurrencyID: this.adCreateForm.get('sourceCurrencyID').value*1,
      destinationCurrencyID: this.adCreateForm.get('destinationCurrencyID').value*1,
      saleSummaRetail: this.adCreateForm.get('saleSummaRetail').value*1,
      buySummaRetail: this.adCreateForm.get('buySummaRetail').value*1,
      saleSumma: this.adCreateForm.get('saleSumma').value*1,
      buySumma: this.adCreateForm.get('buySumma').value*1
      }

    console.log(objToSend)
    this.addProduct.addExchange(objToSend).subscribe(
      res =>
      {

        this.itemAdded = true;
        setTimeout(() =>
        {
          this.showLoader = false;
          this.router.navigateByUrl(``)

        }, 2500)
      },
      err =>
      {
        this.errorMessege = true;
        setTimeout(() =>
        {
          this.showLoader = false;
          this.errorMessege = false;
        }, 2500)
      }
    )
  }
}

