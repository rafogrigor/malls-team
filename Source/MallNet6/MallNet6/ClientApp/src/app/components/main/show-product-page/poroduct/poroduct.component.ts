import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { filter, switchMap } from 'rxjs/operators';
import { Categories } from 'src/model/categories';
import { City } from 'src/model/city';
import { Country } from 'src/model/country';
import { Curency } from 'src/model/curency';
import { FavoriteRequest } from 'src/model/favoriteRequest';
import { FavoriteResponse } from 'src/model/favoriteResponset';
import { GeneralCategoriesField } from 'src/model/generalCategoriesField';
import { GetProduct } from 'src/model/getProduct';
import { InfoAboutProduct } from 'src/model/infoAboutProduct';
import { Region } from 'src/model/region';
import { AddAllCategoriesService } from 'src/service/addAllCategories.service';
import { CategoryService } from 'src/service/category.service';
import { DataExchangeService } from 'src/service/dataExchange.service';
import { FavoriteService } from 'src/service/favorite.service';
import { GetProductsService } from 'src/service/getProducts.service';
import { LocationService } from 'src/service/location.service';

// enum aim {
//   ForSale = 0,    //Վաճառք
//   ForRent = 1,    //Վարձակալություն
//   Exchange = 2,   //Փոխանակում
//   Buy = 3,        //Առք
//   Search = 4      //Փնտրել
// }

@Component({
  selector: 'app-poroduct',
  templateUrl: './poroduct.component.html',
  styleUrls: ['./poroduct.component.css'],
})
export class PoroductComponent implements OnInit {
  categoryName: string;
  categoryId: number;
  productId: number;
  categories: Categories[];
  selectedCategory: Categories;
  sendFilters = new GetProduct();
  responseItem: InfoAboutProduct;
  curencyList: Curency[];
  currencySymbol: string;
  countryList: Country[];
  regionList: Region[];
  cityList: City[];
  fullRegion = {
    countryRegion: new Country(),
    region: new Region(),
    city: new City(),
  };
  deleteFavorite: boolean = false;
  favoriteList: FavoriteResponse[] = [];
  armLat: number = 40.1772;
  armLng: number = 44.50349;
  lat: number;
  lng: number;
  sourceCurrencyID: string;
  destinationCurrencyID: string;
  showWehicleNot1and2: boolean = true;

  showAim: boolean = false;
  showAqcuance: boolean = false;
  productState: boolean = false;
  showWork: boolean = false;
  showGender: boolean = false;
  showWeightHeight: boolean = false;
  showIsLocal: boolean = false;
  showAlcoholVolume: boolean = false;
  showClothingSizeAndShoesSize: boolean = false;
  showForNewBorns: boolean = false;
  showIsFrozenAndIsNaturalDrink: boolean = false;
  showAllForRealEstate: boolean = false;
  showTransportation: boolean = false;
  showAllForTourismAndRest: boolean = false;
  showAllForExchange: boolean = false;
  showExchangePrice: boolean = true;
  showVehicle: boolean = false;
  logined: boolean = false;
  heartFull: boolean = false;
  placeAnOrderButton: boolean = false;

  aimText: string;
  productStateText: string;
  constructionTypeText: string;

  constructor(
    private route: ActivatedRoute,
    private categoryService: CategoryService,
    private router: Router,
    private getProduct: GetProductsService,
    private currency: AddAllCategoriesService,
    private regionService: LocationService,
    private favServie: FavoriteService,
    private dataEx: DataExchangeService,
    private translate: TranslateService
  ) {
    // route.params.subscribe(
    //   val => this.ngOnInit()
    // )
    route.queryParams.subscribe((res) => {
      if (res['favorite'] == `favorite_item=//-${res['check-=q1$$%Id']}`) {
        this.deleteFavorite = true;
      }
    });
  }

  ngOnInit(): void {
    this.route.paramMap
      .pipe(switchMap((params) => params.getAll('categoryId')))
      .subscribe((data) => {
        // this.categoryName = data;
        this.categoryId = parseInt(data);
        this.categoryService.getAllCategories().subscribe((res) => {
          this.categories = res.body as Categories[];
          this.selectedCategory = this.categories.find(
            (e) => e.id == this.categoryId
          );
          this.readIdFromLink();
          this.getSingleProduct(this.selectedCategory, this.sendFilters.id);
          this.logined = !!localStorage.getItem('%%a$');
          this.checkFavorite();
        });
      });

    // this.getProduct.getSport(this.sendFilters).subscribe(
    //   res =>
    //   {
    //   this.responseItem = res.body[0] as InfoAboutProduct;
    //   this.setLocation()
    //   }
    // )
  }

  //Another Functions
  setLocation() {
    this.regionService.getLocation().subscribe((res) => {
      this.countryList = res.body as Country[];
      this.fullRegion.countryRegion = this.countryList.find(
        (c) => c.id == this.responseItem.country
      );
      if (this.fullRegion.countryRegion != undefined) {
        this.regionService
          .getRegion(this.fullRegion.countryRegion.id)
          .subscribe((res) => {
            this.regionList = res.body as Region[];
            this.fullRegion.region = this.regionList.find(
              (r) => r.id == this.responseItem.region
            );
            this.regionService
              .getCity(this.fullRegion.region.id)
              .subscribe((res) => {
                this.cityList = res.body as City[];
                this.fullRegion.city = this.cityList.find(
                  (c) => c.id == this.responseItem.city
                );
              });
          });
      }
    });
  }

  getCurrencySymbol() {
    this.currency.getCurrentList().subscribe((res) => {
      this.curencyList = res.body as Curency[];
      // console.log(this.curencyList.find(c => c.id == this.responseItem.currency))
      // this.currencySymbol = this.curencyList.find(c => c.id == this.responseItem.currency).currencySymbol
      this.sourceCurrencyID = this.curencyList.find(
        (curency) => curency.id == this.responseItem.sourceCurrencyID
      ).currency;
      this.destinationCurrencyID = this.curencyList.find(
        (curency) => curency.id == this.responseItem.destinationCurrencyID
      ).currency;
    });
  }

  // Get Single Product Function
  getSingleProduct(selectedCategory: Categories, productId: number) {
    switch (selectedCategory.id) {
      case 1: //Sale
        this.getProduct.getSale(this.sendFilters).subscribe(
          (res) => {
            if (res.body != null) {
              this.responseItem = res.body[0] as InfoAboutProduct;
              this.lng = this.responseItem.locationLongitude;
              this.lat = this.responseItem.locationLatitude;
              this.aimText =
                this.responseItem.aim == 0
                  ? this.translate.instant('createAd.forSale')
                  : this.responseItem.aim == 1
                  ? this.translate.instant('createAd.forRent')
                  : this.responseItem.aim == 2
                  ? this.translate.instant('createAd.exchange')
                  : this.responseItem.aim == 3
                  ? this.translate.instant('createAd.buy')
                  : this.translate.instant('createAd.search');

              // this.getCurrencySymbol()
              // this.setLocation()
              this.showAim = false;
              this.productState = false;
              this.showWork = false;
              this.showGender = false;
              this.showIsLocal = false;
              this.showAlcoholVolume = false;
              this.showClothingSizeAndShoesSize = false;
            }
          },
          (err) => console.log(err)
        );
        break;
      case 2: //Tner
        this.getProduct.getRealEstate(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          console.log(
            this.responseItem.locationLatitude,
            this.responseItem.locationLongitude
          );
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');
          this.constructionTypeText =
            this.responseItem.constructionType == 0
              ? this.translate.instant('createAd.stone')
              : this.responseItem.constructionType == 1
              ? this.translate.instant('createAd.panels')
              : this.responseItem.constructionType == 2
              ? this.translate.instant('createAd.monolith')
              : this.responseItem.constructionType == 3
              ? this.translate.instant('createAd.bricks')
              : this.responseItem.constructionType == 4
              ? this.translate.instant('createAd.cassete')
              : this.translate.instant('createAd.wooden');
          // this.getCurrencySymbol()
          // this.setLocation()
          this.showAim = true;
          this.productState = false;
          this.showWork = true;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
          this.showAllForRealEstate = true;
        });
        break;
      case 3: //Avtomeqenaner
        this.getProduct.getVehicle(this.sendFilters).subscribe((res) => {
          console.log(res.body);
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');
          // this.setLocation()
          this.showAim = true;
          this.productState = false;
          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
          this.showAllForRealEstate = false;
          this.showVehicle = true;
          if (
            this.responseItem.subCategoryID > 10 &&
            this.responseItem.subCategoryID <= 30
          )
            this.showWehicleNot1and2 = false;
        });
        break;
      case 4: //Electronics
        this.getProduct.getElectronics(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          // this.getCurrencySymbol()
          // this.setLocation()
          this.showAim = true;
          this.productState = true;
          this.productStateText =
            this.responseItem.productState == 0
              ? this.translate.instant('createAd.new')
              : this.translate.instant('createAd.used');
          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 5: //Appliances
        this.getProduct.getAppliances(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          // this.getCurrencySymbol()
          // this.setLocation()
          this.showAim = true;
          this.productState = false;
          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 6: //HouseholdGoods
        this.getProduct.getHouseholdGood(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          // this.getCurrencySymbol()
          // this.setLocation()
          this.showAim = true;
          this.productState = true;
          this.productStateText =
            this.responseItem.productState == 0
              ? this.translate.instant('createAd.new')
              : this.translate.instant('createAd.used');

          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 7: //ClothesAndShoes
        this.getProduct
          .getClothesAndShoes(this.sendFilters)
          .subscribe((res) => {
            this.responseItem = res.body[0] as InfoAboutProduct;
            this.lng = this.responseItem.locationLongitude;
            this.lat = this.responseItem.locationLatitude;
            this.aimText =
              this.responseItem.aim == 0
                ? this.translate.instant('createAd.forSale')
                : this.responseItem.aim == 1
                ? this.translate.instant('createAd.forRent')
                : this.responseItem.aim == 2
                ? this.translate.instant('createAd.exchange')
                : this.responseItem.aim == 3
                ? this.translate.instant('createAd.buy')
                : this.translate.instant('createAd.search');

            // this.getCurrencySymbol()
            // this.setLocation()
            this.showAim = true;
            this.productState = true;
            this.productStateText =
              this.responseItem.productState == 0
                ? this.translate.instant('createAd.new')
                : this.translate.instant('createAd.used');

            this.showWork = false;
            this.showGender = false;
            this.showWeightHeight = false;
            this.showIsLocal = false;
            this.showAlcoholVolume = false;
            this.showClothingSizeAndShoesSize = true;
          });
        break;
      case 8: //ForChildren
        this.getProduct.getForChildren(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          // this.getCurrencySymbol()
          // this.setLocation()
          this.showAim = true;
          this.productState = true;
          this.productStateText =
            this.responseItem.productState == 0
              ? this.translate.instant('createAd.new')
              : this.translate.instant('createAd.used');

          this.showWork = false;
          this.showGender = true;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
          this.showForNewBorns = true;
        });
        break;
      case 9: //JewerlyAndAccessories
        this.getProduct
          .getJewerlyAndAccessories(this.sendFilters)
          .subscribe((res) => {
            this.responseItem = res.body[0] as InfoAboutProduct;
            this.lng = this.responseItem.locationLongitude;
            this.lat = this.responseItem.locationLatitude;
            this.aimText =
              this.responseItem.aim == 0
                ? this.translate.instant('createAd.forSale')
                : this.responseItem.aim == 1
                ? this.translate.instant('createAd.forRent')
                : this.responseItem.aim == 2
                ? this.translate.instant('createAd.exchange')
                : this.responseItem.aim == 3
                ? this.translate.instant('createAd.buy')
                : this.translate.instant('createAd.search');

            // this.getCurrencySymbol()
            // this.setLocation()
            this.showAim = true;
            this.productState = true;
            this.productStateText =
              this.responseItem.productState == 0
                ? this.translate.instant('createAd.new')
                : this.translate.instant('createAd.used');

            this.showWork = false;
            this.showGender = true;
            this.showWeightHeight = false;
            this.showIsLocal = false;
            this.showAlcoholVolume = false;
            this.showClothingSizeAndShoesSize = false;
          });
        break;
      case 10: //Construction
        this.getProduct.getConstruction(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          // this.getCurrencySymbol()
          // this.setLocation()
          this.showAim = true;
          this.productState = true;
          this.productStateText =
            this.responseItem.productState == 0
              ? this.translate.instant('createAd.new')
              : this.translate.instant('createAd.used');

          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 11: //AllForHomeAndGArden
        this.getProduct
          .getForHomeAndGarden(this.sendFilters)
          .subscribe((res) => {
            this.responseItem = res.body[0] as InfoAboutProduct;
            this.lng = this.responseItem.locationLongitude;
            this.lat = this.responseItem.locationLatitude;
            this.aimText =
              this.responseItem.aim == 0
                ? this.translate.instant('createAd.forSale')
                : this.responseItem.aim == 1
                ? this.translate.instant('createAd.forRent')
                : this.responseItem.aim == 2
                ? this.translate.instant('createAd.exchange')
                : this.responseItem.aim == 3
                ? this.translate.instant('createAd.buy')
                : this.translate.instant('createAd.search');

            // this.getCurrencySymbol()
            // this.setLocation()
            this.showAim = true;
            this.productState = true;
            this.productStateText =
              this.responseItem.productState == 0
                ? this.translate.instant('createAd.new')
                : this.translate.instant('createAd.used');

            this.showWork = false;
            this.showGender = false;
            this.showWeightHeight = false;
            this.showIsLocal = false;
            this.showAlcoholVolume = false;
            this.showClothingSizeAndShoesSize = false;
          });
        break;
      case 12: //ProductsAndDrinks
        this.getProduct
          .getProductsAndDrinks(this.sendFilters)
          .subscribe((res) => {
            this.responseItem = res.body[0] as InfoAboutProduct;
            this.lng = this.responseItem.locationLongitude;
            this.lat = this.responseItem.locationLatitude;
            this.aimText =
              this.responseItem.aim == 0
                ? this.translate.instant('createAd.forSale')
                : this.responseItem.aim == 1
                ? this.translate.instant('createAd.forRent')
                : this.responseItem.aim == 2
                ? this.translate.instant('createAd.exchange')
                : this.responseItem.aim == 3
                ? this.translate.instant('createAd.buy')
                : this.translate.instant('createAd.search');

            // this.getCurrencySymbol()
            // this.setLocation()
            this.showAim = true;
            this.productState = false;
            this.showWork = false;
            this.showGender = false;
            this.showWeightHeight = false;
            this.showIsLocal = true;
            this.showAlcoholVolume = false;
            this.showClothingSizeAndShoesSize = false;
            this.showIsFrozenAndIsNaturalDrink = true;
          });
        break;
      case 13: //CigaretteAndAlcohol
        this.getProduct
          .getCigaretteAndAlcohol(this.sendFilters)
          .subscribe((res) => {
            this.responseItem = res.body[0] as InfoAboutProduct;
            this.lng = this.responseItem.locationLongitude;
            this.lat = this.responseItem.locationLatitude;
            this.aimText =
              this.responseItem.aim == 0
                ? this.translate.instant('createAd.forSale')
                : this.responseItem.aim == 1
                ? this.translate.instant('createAd.forRent')
                : this.responseItem.aim == 2
                ? this.translate.instant('createAd.exchange')
                : this.responseItem.aim == 3
                ? this.translate.instant('createAd.buy')
                : this.translate.instant('createAd.search');

            // this.getCurrencySymbol()
            // this.setLocation()
            this.showAim = true;
            this.productState = true;
            this.productStateText =
              this.responseItem.productState == 0
                ? this.translate.instant('createAd.new')
                : this.translate.instant('createAd.used');

            this.showWork = false;
            this.showGender = false;
            this.showWeightHeight = false;
            this.showIsLocal = true;
            this.showAlcoholVolume = true;
            this.showClothingSizeAndShoesSize = false;
          });
        break;
      case 14: //Furniture
        this.getProduct.getFurniture(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          // this.setLocation()
          this.showAim = true;
          this.productState = true;
          this.productStateText =
            this.responseItem.productState == 0
              ? this.translate.instant('createAd.new')
              : this.translate.instant('createAd.used');

          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 15: //TourismAndRest
        this.getProduct.getTourismAndRest(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');
          this.placeAnOrderButton = true;
          // this.getCurrencySymbol()
          // this.setLocation()
          this.showAim = true;
          this.productState = false;
          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
          this.showAllForTourismAndRest = true;
        });
        break;
      case 16: //Sport
        this.getProduct.getSport(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          // this.getCurrencySymbol()
          // this.setLocation()
          console.log(this.responseItem);
          this.showAim = true;
          this.productState = true;
          this.productStateText =
            this.responseItem.productState == 0
              ? this.translate.instant('createAd.new')
              : this.translate.instant('createAd.used');

          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 17: //PetsAndPlants
        this.getProduct.getPetsAndPlants(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          // this.getCurrencySymbol()
          // this.setLocation()
          this.showAim = true;
          this.productState = true;
          this.productStateText =
            this.responseItem.productState == 0
              ? this.translate.instant('createAd.new')
              : this.translate.instant('createAd.used');

          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 18: //Culture
        this.getProduct.getCulture(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          // this.getCurrencySymbol()
          // this.setLocation()
          this.showAim = true;
          this.productState = true;
          this.productStateText =
            this.responseItem.productState == 0
              ? this.translate.instant('createAd.new')
              : this.translate.instant('createAd.used');

          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 19: //Work
        this.getProduct.getWork(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          // this.getCurrencySymbol()
          // this.setLocation()
          this.showAim = false;
          this.productState = false;
          this.showWork = true;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 20: //Services
        this.getProduct.getServices(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          // this.getCurrencySymbol()
          // this.setLocation()
          this.showAim = false;
          this.productState = false;
          this.showWork = true;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
          this.showTransportation = false;
        });
        break;
      case 21: //HealfCare
        this.getProduct.getHealfCare(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          // this.getCurrencySymbol()
          // this.setLocation()
          this.showAim = true;
          this.productState = true;
          this.productStateText =
            this.responseItem.productState == 0
              ? this.translate.instant('createAd.new')
              : this.translate.instant('createAd.used');

          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 22: //Acquaintance
        this.getProduct.getAcquaintance(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          // this.getCurrencySymbol()
          // this.setLocation()
          this.showAim = false;
          this.productState = false;
          this.showWork = false;
          this.showGender = true;
          this.showWeightHeight = true;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
          this.showAqcuance = true;
        });
        break;
      case 23: //EverythingElse
        this.getProduct.getEverythingElse(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          // this.getCurrencySymbol()
          // this.setLocation()
          this.showAim = false;
          this.productState = false;
          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 24: //Exchange
        this.getProduct.getExchange(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          this.getCurrencySymbol();
          // this.setLocation()
          this.showAim = false;
          this.productState = false;
          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
          this.showAllForExchange = true;
          this.showExchangePrice = false;
        });
        break;
    }
  }

  //GetID
  readIdFromLink() {
    this.route.paramMap
      .pipe(switchMap((params) => params.getAll('id')))
      .subscribe((data) => {
        this.productId = parseInt(data);
      });
    this.sendFilters.id = this.productId;
  }

  //Delete Favorite
  deleteFavoriteItem(obj: InfoAboutProduct) {
    let deleteableFav: FavoriteRequest = {
      adID: obj.id,
      userID: parseInt(localStorage.getItem('%%a$')),
    };
    this.favServie
      .deleteFavorite(deleteableFav)
      .subscribe((res) => console.log(res));
  }

  checkFavorite() {
    if (this.logined) {
      this.favServie
        .getFavoriteList(localStorage.getItem('%%a$'))
        .subscribe((res) => {
          this.favoriteList = res.body['favoriteList'] as FavoriteResponse[];
          if (
            this.favoriteList.find((fav) =>
              fav.ads.find((a) => a.id == this.productId)
            )
          ) {
            this.heartFull = true;
          } else {
            this.heartFull = false;
          }
        });
    }
  }
  addOrDeleteFav(id) {
    if (this.heartFull) {
      let deleteableFav: FavoriteRequest = {
        adID: id,
        userID: parseInt(localStorage.getItem('%%a$')),
      };
      this.favServie.deleteFavorite(deleteableFav).subscribe((res) => {
        this.heartFull = false;
        this.dataEx.addFavorite(-1);
      });
    } else {
      this.favServie.addFavorite(id).subscribe((res) => {
        this.heartFull = true;
        this.dataEx.addFavorite(1);
      });
    }
  }
  showPage() {
    this.router.navigateByUrl(`/user/${this.responseItem.userID}`);
  }
  showImage() {
    console.log();
  }
}
