import { Component, OnInit, HostListener } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Categories } from 'src/model/categories';
import { City } from 'src/model/city';
import { Country } from 'src/model/country';
import { GeneralCategoriesField } from 'src/model/generalCategoriesField';
import { GetProduct } from 'src/model/getProduct';
import { Region } from 'src/model/region';
import { SubCategories } from 'src/model/subCategories';
import { CategoryService } from 'src/service/category.service';
import { GetProductsService } from 'src/service/getProducts.service';
import { LocationService } from 'src/service/location.service';


@Component({
  selector: 'app-show-product-page',
  templateUrl: './show-product-page.component.html',
  styleUrls: ['./show-product-page.component.css']
})
export class ShowProductPageComponent implements OnInit {
  name: string;
  id: number;
  categories: Categories[] = []
  selectedCategory: Categories;
  country: Country[];
  region: Region[];
  city: City[];
  sendFilters: GetProduct = new GetProduct
  sendFilterData: FormGroup;
  mainID: number;
  responseProduct: GeneralCategoriesField[] = []
  addedResponseProduct: GeneralCategoriesField[] = []
  subCategoryID: number;


  showRealEstate: boolean = false;
  showGender: boolean = false;
  showForNewBorns: boolean = false;
  showIsLocal: boolean = false;
  showisFrozen: boolean = false;
  showisNaturalDrink: boolean = false;
  showAlcoholVolume: boolean = false;
  showDepartureDay: boolean = false;
  showReturnDay: boolean = false;
  showReservedTickets: boolean = false;
  showAdPaymentTime: boolean = false;
  showPaymentTime: boolean = false;
  showTransportation: boolean = false;
  showAcquintanceAim: boolean = false;
  showHeight: boolean = false;
  showWeight: boolean = false;
  showAgeFrom: boolean = false;
  showAgeTo: boolean = false;
  showClothingSize: boolean = false;
  showShoesSize: boolean = false;

  //Selects All option
  showAIMall: boolean = true;
  showAdOwner: boolean = true;
  showCurrency: boolean = true;
  showProductState: boolean = true;

  formIsCreated: boolean = false;
  endOfContent: boolean = false;
  loading: boolean = false;
  empty: boolean = false;

  //Subscraption
  private routeSubscription: Subscription;
  private querySubscription: Subscription;


  constructor(private route: ActivatedRoute, private categoryService: CategoryService,
              private getProduct: GetProductsService, private locationService: LocationService,
              private formBuilder: FormBuilder, private router: Router)
  {
    route.params.subscribe(

      val =>
      {
        // this.ngOnInit()
        // window.scrollTo(0,0)
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        this.sendFilters.adCount = 20;
        this.sendFilters.pageNumber = 1
        this.endOfContent = false;
        this.loading = false;
        this.empty = false;
        this.responseProduct = [];
      }

    )
    route.queryParams.subscribe(
      res =>
      {
        // window.scrollTo(0,0)
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        this.sendFilters.adCount = 20;
        this.sendFilters.pageNumber = 1
        this.endOfContent = false;
        this.loading = false;
        this.empty = false;
        this.responseProduct = [];
      }
    )
  }

  @HostListener('document:scroll')
  scrollBottom()
  {
    const scrollable = document.documentElement.scrollHeight - window.innerHeight;
    const scrolled = window.scrollY;
    if(Math.ceil(scrolled) === scrollable)
      {
        if(!this.endOfContent && !this.empty)
        {
          this.loading = true;
          this.sendFilters.pageNumber += 1;
          switch (this.mainID) {
            case 1://Sale
                this.getProduct.getSale(this.sendFilters).subscribe(
                  res =>
                  {
                    this.loading = false;
                    if(res.status == 200){
                      this.addedResponseProduct = res.body as GeneralCategoriesField[];

                        for(let i = 0; i < this.addedResponseProduct.length; i++){
                          this.responseProduct.push(this.addedResponseProduct[i])
                        }
                        this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                    }
                    else this.endOfContent = true;
                  }
                )
              break;
            case 2://Tner
              this.getProduct.getRealEstate(this.sendFilters).subscribe(
                res =>
                {
                  this.loading = false;
                    if(res.status == 200){
                      this.addedResponseProduct = res.body as GeneralCategoriesField[];

                        for(let i = 0; i < this.addedResponseProduct.length; i++){
                          this.responseProduct.push(this.addedResponseProduct[i])
                        }
                        this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                    }
                    else this.endOfContent = true;
                }
              )
              break;
            case 3://Avtomeqenaner
            this.getProduct.getVehicle(this.sendFilters).subscribe(
              res =>
              {
                this.loading = false;
                  if(res.status == 200){
                    this.addedResponseProduct = res.body as GeneralCategoriesField[];

                      for(let i = 0; i < this.addedResponseProduct.length; i++){
                        this.responseProduct.push(this.addedResponseProduct[i])
                      }
                      this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                  }
                  else this.endOfContent = true;
              }
            )
              break;
            case 4://Electronics
                this.getProduct.getElectronics(this.sendFilters).subscribe(
                  res =>
                  {
                    this.loading = false;
                    if(res.status == 200){
                      this.addedResponseProduct = res.body as GeneralCategoriesField[];

                        for(let i = 0; i < this.addedResponseProduct.length; i++){
                          this.responseProduct.push(this.addedResponseProduct[i])
                        }
                        this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                    }
                    else this.endOfContent = true;
                  }
                )
              break;
            case 5://Appliances
                  this.getProduct.getAppliances(this.sendFilters).subscribe(
                    res =>
                    {
                      this.loading = false;
                      if(res.status == 200){
                        this.addedResponseProduct = res.body as GeneralCategoriesField[];

                          for(let i = 0; i < this.addedResponseProduct.length; i++){
                            this.responseProduct.push(this.addedResponseProduct[i])
                          }
                          this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                      }
                      else this.endOfContent = true;
                    }
                  )
              break;
            case 6://HouseholdGoods
                  this.getProduct.getHouseholdGood(this.sendFilters).subscribe(
                    res =>
                    {
                      this.loading = false;
                      if(res.status == 200){
                        this.addedResponseProduct = res.body as GeneralCategoriesField[];

                          for(let i = 0; i < this.addedResponseProduct.length; i++){
                            this.responseProduct.push(this.addedResponseProduct[i])
                          }
                          this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                      }
                      else this.endOfContent = true;
                    }
                  )
              break;
            case 7://ClothesAndShoes
                  this.getProduct.getClothesAndShoes(this.sendFilters).subscribe(
                    res =>
                    {
                      this.loading = false;
                      if(res.status == 200){
                        this.addedResponseProduct = res.body as GeneralCategoriesField[];

                          for(let i = 0; i < this.addedResponseProduct.length; i++){
                            this.responseProduct.push(this.addedResponseProduct[i])
                          }
                          this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                      }
                      else this.endOfContent = true;
                    }
                  )
              break;
            case 8://ForChildren
                  this.getProduct.getForChildren(this.sendFilters).subscribe(
                    res =>
                    {
                      this.loading = false;
                      if(res.status == 200){
                        this.addedResponseProduct = res.body as GeneralCategoriesField[];

                          for(let i = 0; i < this.addedResponseProduct.length; i++){
                            this.responseProduct.push(this.addedResponseProduct[i])
                          }
                          this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                      }
                      else this.endOfContent = true;
                    }
                  )
              break;
            case 9://JewerlyAndAccessories
                  this.getProduct.getJewerlyAndAccessories(this.sendFilters).subscribe(
                    res =>
                    {
                      this.loading = false;
                      if(res.status == 200){
                        this.addedResponseProduct = res.body as GeneralCategoriesField[];

                          for(let i = 0; i < this.addedResponseProduct.length; i++){
                            this.responseProduct.push(this.addedResponseProduct[i])
                          }
                          this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                      }
                      else this.endOfContent = true;
                    }
                  )
              break;
            case 10://Construction
                  this.getProduct.getConstruction(this.sendFilters).subscribe(
                    res =>
                    {
                      this.loading = false;
                      if(res.status == 200){
                        this.addedResponseProduct = res.body as GeneralCategoriesField[];

                          for(let i = 0; i < this.addedResponseProduct.length; i++){
                            this.responseProduct.push(this.addedResponseProduct[i])
                          }
                          this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                      }
                      else this.endOfContent = true;
                    }
                  )
              break;
            case 11://AllForHomeAndGArden
                  this.getProduct.getForHomeAndGarden(this.sendFilters).subscribe(
                    res =>
                    {
                      this.loading = false;
                      if(res.status == 200){
                        this.addedResponseProduct = res.body as GeneralCategoriesField[];

                          for(let i = 0; i < this.addedResponseProduct.length; i++){
                            this.responseProduct.push(this.addedResponseProduct[i])
                          }
                          this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                      }
                      else this.endOfContent = true;
                    }
                  )
              break;
            case 12://ProductsAndDrinks
                  this.getProduct.getProductsAndDrinks(this.sendFilters).subscribe(
                    res =>
                    {
                      this.loading = false;
                      if(res.status == 200){
                        this.addedResponseProduct = res.body as GeneralCategoriesField[];

                          for(let i = 0; i < this.addedResponseProduct.length; i++){
                            this.responseProduct.push(this.addedResponseProduct[i])
                          }
                          this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                      }
                      else this.endOfContent = true;
                    }
                  )
              break;
            case 13://CigaretteAndAlcohol
                  this.getProduct.getCigaretteAndAlcohol(this.sendFilters).subscribe(
                    res =>
                    {
                      this.loading = false;
                      if(res.status == 200){
                        this.addedResponseProduct = res.body as GeneralCategoriesField[];

                          for(let i = 0; i < this.addedResponseProduct.length; i++){
                            this.responseProduct.push(this.addedResponseProduct[i])
                          }
                          this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                      }
                      else this.endOfContent = true;
                    }
                  )
              break;
            case 14://Furniture
                  this.getProduct.getFurniture(this.sendFilters).subscribe(
                    res =>{
                      this.loading = false;
                      if(res.status == 200){
                        this.addedResponseProduct = res.body as GeneralCategoriesField[];

                          for(let i = 0; i < this.addedResponseProduct.length; i++){
                            this.responseProduct.push(this.addedResponseProduct[i])
                          }
                          this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                      }
                      else this.endOfContent = true;
                    }
                  )
              break;
            case 15://TourismAndRest
                  this.getProduct.getTourismAndRest(this.sendFilters).subscribe(
                    res =>
                    {
                      this.loading = false;
                      if(res.status == 200){
                        this.addedResponseProduct = res.body as GeneralCategoriesField[];

                          for(let i = 0; i < this.addedResponseProduct.length; i++){
                            this.responseProduct.push(this.addedResponseProduct[i])
                          }
                          this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                      }
                      else this.endOfContent = true;
                    }
                  )
              break;
            case 16://Sport
                  this.getProduct.getSport(this.sendFilters).subscribe(
                    res =>
                    {
                      this.loading = false;
                      if(res.status == 200){
                        this.addedResponseProduct = res.body as GeneralCategoriesField[];

                          for(let i = 0; i < this.addedResponseProduct.length; i++){
                            this.responseProduct.push(this.addedResponseProduct[i])
                          }
                          this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                      }
                      else this.endOfContent = true;
                    }
                  )
              break;
            case 17://PetsAndPlants
                  this.getProduct.getPetsAndPlants(this.sendFilters).subscribe(
                    res =>
                    {
                      this.loading = false;
                      if(res.status == 200){
                        this.addedResponseProduct = res.body as GeneralCategoriesField[];

                          for(let i = 0; i < this.addedResponseProduct.length; i++){
                            this.responseProduct.push(this.addedResponseProduct[i])
                          }
                          this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                      }
                      else this.endOfContent = true;
                    }
                  )
              break;
            case 18://Culture
                  this.getProduct.getCulture(this.sendFilters).subscribe(
                    res =>
                    {
                      this.loading = false;
                      if(res.status == 200){
                        this.addedResponseProduct = res.body as GeneralCategoriesField[];

                          for(let i = 0; i < this.addedResponseProduct.length; i++){
                            this.responseProduct.push(this.addedResponseProduct[i])
                          }
                          this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                      }
                      else this.endOfContent = true;
                    }
                  )
              break;
            case 19://Work
                  this.getProduct.getWork(this.sendFilters).subscribe(
                    res =>
                    {
                      this.loading = false;
                      if(res.status == 200){
                        this.addedResponseProduct = res.body as GeneralCategoriesField[];

                          for(let i = 0; i < this.addedResponseProduct.length; i++){
                            this.responseProduct.push(this.addedResponseProduct[i])
                          }
                          this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                      }
                      else this.endOfContent = true;
                    }
                  )
              break;
            case 20://Services
                  this.getProduct.getServices(this.sendFilters).subscribe(
                    res =>
                    {
                      this.loading = false;
                      if(res.status == 200){
                        this.addedResponseProduct = res.body as GeneralCategoriesField[];

                          for(let i = 0; i < this.addedResponseProduct.length; i++){
                            this.responseProduct.push(this.addedResponseProduct[i])
                          }
                          this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                      }
                      else this.endOfContent = true;
                    }
                  )
              break;
            case 21://HealfCare
                  this.getProduct.getHealfCare(this.sendFilters).subscribe(
                    res =>
                    {
                      this.loading = false;
                      if(res.status == 200){
                        this.addedResponseProduct = res.body as GeneralCategoriesField[];

                          for(let i = 0; i < this.addedResponseProduct.length; i++){
                            this.responseProduct.push(this.addedResponseProduct[i])
                          }
                          this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                      }
                      else this.endOfContent = true;
                    }
                  )
              break;
            case 22://Acquaintance
                  this.getProduct.getAcquaintance(this.sendFilters).subscribe(
                    res =>
                    {
                      this.loading = false;
                      if(res.status == 200){
                        this.addedResponseProduct = res.body as GeneralCategoriesField[];

                          for(let i = 0; i < this.addedResponseProduct.length; i++){
                            this.responseProduct.push(this.addedResponseProduct[i])
                          }
                          this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                      }
                      else this.endOfContent = true;
                    }
                  )
              break;
            case 23://EverythingElse
                  this.getProduct.getEverythingElse(this.sendFilters).subscribe(
                    res =>
                    {
                      this.loading = false;
                      if(res.status == 200){
                        this.addedResponseProduct = res.body as GeneralCategoriesField[];

                          for(let i = 0; i < this.addedResponseProduct.length; i++){
                            this.responseProduct.push(this.addedResponseProduct[i])
                          }
                          this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                      }
                      else this.endOfContent = true;
                    }
                  )
              break;
            case 24://Exchange
                  this.getProduct.getExchange(this.sendFilters).subscribe(
                    res =>
                    {
                      this.loading = false;
                      if(res.status == 200){
                        this.addedResponseProduct = res.body as GeneralCategoriesField[];

                          for(let i = 0; i < this.addedResponseProduct.length; i++){
                            this.responseProduct.push(this.addedResponseProduct[i])
                          }
                          this.addedResponseProduct.slice(0, this.addedResponseProduct.length)
                      }
                      else this.endOfContent = true;
                    }
                  )
              break;

          }
        }
      }
  }

  ngOnInit(): void {
    // window.scrollTo(0,0)
    this.route.paramMap.pipe(
      switchMap(params => params.getAll('id'))).subscribe(data=>
         {
            this.sendFilters.adCount = 20;
            this.sendFilters.pageNumber = 1
            this.endOfContent = false;
            this.loading = false;
            this.empty = false;
            this.responseProduct = [];
            this.name = data;
            this.id = parseInt(data)
            this.categoryService.getAllCategories().subscribe(
              res =>
              {
                this.categories = res.body as Categories[];
                // this.selectedCategory = this.categories.find(e => e.category == this.name)
                // this.categories.find(e => e.category == this.name)
                this.querySubscription = this.route.queryParams.subscribe(
                  (queryParam: any) =>
                  {
                    this.subCategoryID = queryParam['subId'] * 1;
                    if(this.subCategoryID != undefined || this.subCategoryID != null)
                    {
                      this.sendFilters.subCategoryID = this.subCategoryID;
                      this.selectFiltersAndBuildForm(this.categories.find(e => e.id == this.id).id)
                    }
                    else
                      this.subCategoryID = null
                  }
                );
                this.setSelectedCategory(this.categories.find(e => e.id == this.id));

              }
            )
        }
      );

    this.locationService.getLocation().subscribe(
      res =>
      {
        this.country = res.body as Country[];
      }
    );

  }


  setSelectedCategory(item: Categories)
  {
    this.showHideFilters(item)
    this.selectedCategory = item
    this.selectFiltersAndBuildForm(item.id)
  }

  //setCountry
  selectCountry(obj)
  {
    if(obj == '0')
      return;
    this.locationService.getRegion(obj * 1).subscribe(
      res =>
      {
        this.sendFilterData.get('countryID').setValue(obj*1)
        this.region = res.body as Region[]
      }
    )
  }

  //setRegion
  selectRegion(obj)
  {
    if(obj == '0')
      return
    this.locationService.getCity(obj * 1).subscribe(
      res =>
      {
        this.sendFilterData.get('region').setValue(obj*1)
        this.city = res.body as City[]
      }
    )
  }

  selectCity(obj){
    if (obj == '0')
      this.sendFilterData.get('city').setValue(null)
    else
      this.sendFilterData.get('city').setValue(obj*1)
  }

  //Add Filters
  addFilters()
  {
    this.endOfContent = false;
    this.empty = false;
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    this.sendFilters.pageNumber = 1;
    this.sendFilters.aim = this.sendFilterData.get('aim').value
    this.sendFilters.adOwner = this.sendFilterData.get('adOwner').value
    this.sendFilters.countryID = this.sendFilterData.get('countryID').value
    this.sendFilters.region = this.sendFilterData.get('region').value
    this.sendFilters.city = this.sendFilterData.get('city').value
    this.sendFilters.onlyWithPhotos = this.sendFilterData.get('onlyWithPhotos').value == '' ? false : this.sendFilterData.get('onlyWithPhotos').value
    this.sendFilters.currency = this.sendFilterData.get('currency').value
    this.sendFilters.salePriceFrom = this.sendFilterData.get('salePriceFrom').value == null || this.sendFilterData.get('salePriceFrom').value == '' ? null : this.sendFilterData.get('salePriceFrom').value * 1
    this.sendFilters.salePriceTo = this.sendFilterData.get('salePriceTo').value == null || this.sendFilterData.get('salePriceTo').value == '' ? null : this.sendFilterData.get('salePriceTo').value * 1
    this.sendFilters.productState = this.sendFilterData.get('productState').value

    this.responseProduct = []
    //activateCategoryFilters
    switch (this.mainID) {
      case 1://Sale
        this.getProduct.getSale(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200){
                this.responseProduct = res.body as GeneralCategoriesField[];
                if (this.responseProduct == [])
                  {
                    this.empty = true;
                    console.log('empty')
                  }
              }
              else this.endOfContent = true;
            }
          )
            //pass
        break;
      case 2://Tner
        this.sendFilters.constructionType = this.sendFilterData.get('constructionType').value
        this.sendFilters.paymentTime = this.sendFilterData.get('paymentTime').value == null  || this.sendFilterData.get('paymentTime').value == ''? null  : this.sendFilterData.get('paymentTime').value * 1
        this.sendFilters.rooms = this.sendFilterData.get('rooms').value == null  || this.sendFilterData.get('rooms').value == ''? null  : this.sendFilterData.get('rooms').value * 1
        this.sendFilters.spaceFrom = this.sendFilterData.get('spaceFrom').value == null  || this.sendFilterData.get('spaceFrom').value == ''? null  : this.sendFilterData.get('spaceFrom').value * 1
        this.sendFilters.spaceTo = this.sendFilterData.get('spaceTo').value == null  || this.sendFilterData.get('spaceTo').value == ''? null  : this.sendFilterData.get('spaceTo').value * 1
        this.sendFilters.floorFrom = this.sendFilterData.get('floorFrom').value == null  || this.sendFilterData.get('floorFrom').value == ''? null  : this.sendFilterData.get('floorFrom').value * 1
        this.sendFilters.floorTo = this.sendFilterData.get('floorTo').value == null  || this.sendFilterData.get('floorTo').value == ''? null  : this.sendFilterData.get('floorTo').value * 1
          this.getProduct.getRealEstate(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 3://Avtomeqenaner

        break;
      case 4://Electronics
        this.getProduct.getElectronics(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
            //pass
        break;
      case 5://Appliances
        this.getProduct.getAppliances(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
            //pass
        break;
      case 6://HouseholdGoods
        this.getProduct.getHouseholdGood(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
            //pass
        break;
      case 7://ClothesAndShoes
        this.sendFilters.clothingSize = this.sendFilterData.get('clothingSize').value == null || this.sendFilterData.get('clothingSize').value == '' ? null : this.sendFilterData.get('clothingSize').value
        this.sendFilters.shoesSize = this.sendFilterData.get('shoesSize').value == null || this.sendFilterData.get('shoesSize').value == '' ? null : this.sendFilterData.get('shoesSize').value * 1
          this.getProduct.getClothesAndShoes(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )

        break;
      case 8://ForChildren
        this.sendFilters.gender = this.sendFilterData.get('gender').value == '' ? false : this.sendFilterData.get('gender').value
        this.sendFilters.forNewBorns = this.sendFilterData.get('forNewBorns').value == '' ? false : this.sendFilterData.get('forNewBorns').value
          this.getProduct.getForChildren(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 9://JewerlyAndAccessories
        this.sendFilters.gender = this.sendFilterData.get('gender').value == '' ? false : this.sendFilterData.get('gender').value
          this.getProduct.getJewerlyAndAccessories(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 10://Construction
        this.getProduct.getConstruction(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
          //pass
        break;
      case 11://AllForHomeAndGArden
        this.getProduct.getForHomeAndGarden(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
          //pass
        break;
      case 12://ProductsAndDrinks
        this.sendFilters.isLocal = this.sendFilterData.get('isLocal').value == '' ? false : this.sendFilterData.get('isLocal').value
        this.sendFilters.isFrozen = this.sendFilterData.get('isFrozen').value == '' ? false : this.sendFilterData.get('isFrozen').value
        this.sendFilters.isNaturalDrink = this.sendFilterData.get('isNaturalDrink').value == '' ? false : this.sendFilterData.get('isNaturalDrink').value
          this.getProduct.getProductsAndDrinks(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 13://CigaretteAndAlcohol
        this.sendFilters.isLocal = this.sendFilterData.get('isLocal').value == '' ? false : this.sendFilterData.get('isLocal').value
        this.sendFilters.alcoholVolume = this.sendFilterData.get('alcoholVolume').value == null || this.sendFilterData.get('alcoholVolume').value == ''  ? null : this.sendFilterData.get('alcoholVolume').value * 1
          this.getProduct.getCigaretteAndAlcohol(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 14://Furniture
        this.getProduct.getFurniture(this.sendFilters).subscribe(
          res =>
          {
            if(res.status == 200)
              this.responseProduct = res.body as GeneralCategoriesField[]
            else this.empty = true;
          }
        )
          //pass
        break;
      case 15://TourismAndRest
        this.sendFilters.departureDay = this.sendFilterData.get('departureDay').value
        this.sendFilters.returnDay = this.sendFilterData.get('returnDay').value
        this.sendFilters.reservedTickets = this.sendFilterData.get('reservedTickets').value == null || this.sendFilterData.get('reservedTickets').value == '' ? null : this.sendFilterData.get('reservedTickets').value * 1
          this.getProduct.getTourismAndRest(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 16://Sport
        this.getProduct.getSport(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
          //pass
        break;
      case 17://PetsAndPlants
        this.getProduct.getPetsAndPlants(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
          //pass
        break;
      case 18://Culture
        this.getProduct.getCulture(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
          //pass
        break;
      case 19://Work
        this.sendFilters.adPaymentTime = this.sendFilterData.get('adPaymentTime').value == null || this.sendFilterData.get('adPaymentTime').value == '' ? null : this.sendFilterData.get('adPaymentTime').value * 1
          this.getProduct.getWork(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 20://Services
        this.sendFilters.paymentTime = this.sendFilterData.get('paymentTime').value == null || this.sendFilterData.get('paymentTime').value == '' ? null : this.sendFilterData.get('paymentTime').value * 1
        this.sendFilters.transportation = this.sendFilterData.get('transportation').value == '' ? false : this.sendFilterData.get('transportation').value
          this.getProduct.getServices(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 21://HealfCare
        this.getProduct.getHealfCare(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
          //pass
        break;
      case 22://Acquaintance
        this.sendFilters.acquintanceAim = this.sendFilterData.get('acquintanceAim').value
        this.sendFilters.gender = this.sendFilterData.get('gender').value == '' ? false : this.sendFilterData.get('gender').value
        this.sendFilters.height = this.sendFilterData.get('height').value == null || this.sendFilterData.get('height').value == '' ? null : this.sendFilterData.get('height').value * 1
        this.sendFilters.weight = this.sendFilterData.get('weight').value == null || this.sendFilterData.get('weight').value == '' ? null : this.sendFilterData.get('weight').value * 1
        this.sendFilters.ageFrom = this.sendFilterData.get('ageFrom').value == null || this.sendFilterData.get('ageFrom').value == '' ? null : this.sendFilterData.get('ageFrom').value * 1
        this.sendFilters.ageTo = this.sendFilterData.get('ageTo').value == null || this.sendFilterData.get('ageTo').value == '' ? null : this.sendFilterData.get('ageTo').value * 1
          this.getProduct.getAcquaintance(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 23://EverythingElse
        this.getProduct.getEverythingElse(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )

        break;
      case 24://Exchange
        this.getProduct.getExchange(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )

        break;

    }

  }

  //change filters selects
  changeAim(obj){
    if (obj != 'null')
      this.sendFilterData.get('aim').setValue(obj * 1)
    else
      this.sendFilterData.get('aim').setValue(null)
  }

  selectOwner(obj)
  {
    if (obj != 'null')
      this.sendFilterData.get('adOwner').setValue(obj * 1)
    else
      this.sendFilterData.get('adOwner').setValue(null)
  }

  selectCurrency(obj)
  {
    if (obj != 'null')
      this.sendFilterData.get('currency').setValue(obj * 1)
    else
      this.sendFilterData.get('currency').setValue(null)
  }

  selectProductState(obj)
  {
    if (obj != 'null')
      this.sendFilterData.get('productState').setValue(obj * 1)
    else
      this.sendFilterData.get('productState').setValue(null)
  }

  selectConstructionType(obj)
  {
    if (obj != 'null')
      this.sendFilterData.get('constructionType').setValue(obj * 1)
    else
      this.sendFilterData.get('constructionType').setValue(null)
  }

  selectAcquintanceAim(obj)
  {
    if (obj != 'null')
      this.sendFilterData.get('acquintanceAim').setValue(obj * 1)
    else
      this.sendFilterData.get('acquintanceAim').setValue(null)
  }

  //getProducts
  getAllProducts(selectedCategory: Categories, productId: number){
    switch (selectedCategory.id) {
      case 1://Sale
          this.getProduct.getSale(this.sendFilters).subscribe(
            res =>
            {
              // this.responseItem = res.body[0] as InfoAboutProduct;
            }
          )
        break;
      case 2://Tner
        this.getProduct.getRealEstate(this.sendFilters).subscribe(
          res =>
          {
            // this.responseItem = res.body[0] as InfoAboutProduct;
          }
        )
        break;
      case 3://Avtomeqenaner

        break;
      case 4://Electronics
          this.getProduct.getElectronics(this.sendFilters).subscribe(
            res =>
            {
              // this.responseItem = res.body[0] as InfoAboutProduct;
            }
          )
        break;
      case 5://Appliances
            this.getProduct.getAppliances(this.sendFilters).subscribe(
              res =>
              {
                // this.responseItem = res.body[0] as InfoAboutProduct;
              }
            )
        break;
      case 6://HouseholdGoods
            this.getProduct.getHouseholdGood(this.sendFilters).subscribe(
              res =>
              {
                // this.responseItem = res.body[0] as InfoAboutProduct;
              }
            )
        break;
      case 7://ClothesAndShoes
            this.getProduct.getClothesAndShoes(this.sendFilters).subscribe(
              res =>
              {
                // this.responseItem = res.body[0] as InfoAboutProduct;
              }
            )
        break;
      case 8://ForChildren
            this.getProduct.getForChildren(this.sendFilters).subscribe(
              res =>
              {
                // this.responseItem = res.body[0] as InfoAboutProduct;
              }
            )
        break;
      case 9://JewerlyAndAccessories
            this.getProduct.getJewerlyAndAccessories(this.sendFilters).subscribe(
              res =>
              {
                // this.responseItem = res.body[0] as InfoAboutProduct;
              }
            )
        break;
      case 10://Construction
            this.getProduct.getConstruction(this.sendFilters).subscribe(
              res =>
              {
                // this.responseItem = res.body[0] as InfoAboutProduct;
              }
            )
        break;
      case 11://AllForHomeAndGArden
            this.getProduct.getForHomeAndGarden(this.sendFilters).subscribe(
              res =>
              {
                // this.responseItem = res.body[0] as InfoAboutProduct;
              }
            )
        break;
      case 12://ProductsAndDrinks
            this.getProduct.getProductsAndDrinks(this.sendFilters).subscribe(
              res =>
              {
                // this.responseItem = res.body[0] as InfoAboutProduct;
              }
            )
        break;
      case 13://CigaretteAndAlcohol
            this.getProduct.getCigaretteAndAlcohol(this.sendFilters).subscribe(
              res =>
              {
                // this.responseItem = res.body[0] as InfoAboutProduct;
              }
            )
        break;
      case 14://Furniture
            this.getProduct.getFurniture(this.sendFilters).subscribe(
              res =>{
                // this.responseItem = res.body[0] as InfoAboutProduct;
              }
            )
        break;
      case 15://TourismAndRest
            this.getProduct.getTourismAndRest(this.sendFilters).subscribe(
              res =>
              {
                // this.responseItem = res.body[0] as InfoAboutProduct;
              }
            )
        break;
      case 16://Sport
            this.getProduct.getSport(this.sendFilters).subscribe(
              res =>
              {
                // this.responseItem = res.body[0] as InfoAboutProduct;
              }
            )
        break;
      case 17://PetsAndPlants
            this.getProduct.getPetsAndPlants(this.sendFilters).subscribe(
              res =>
              {
                // this.responseItem = res.body[0] as InfoAboutProduct;
              }
            )
        break;
      case 18://Culture
            this.getProduct.getCulture(this.sendFilters).subscribe(
              res =>
              {
                // this.responseItem = res.body[0] as InfoAboutProduct;
              }
            )
        break;
      case 19://Work
            this.getProduct.getWork(this.sendFilters).subscribe(
              res =>
              {
                // this.responseItem = res.body[0] as InfoAboutProduct;
              }
            )
        break;
      case 20://Services
            this.getProduct.getServices(this.sendFilters).subscribe(
              res =>
              {
                // this.responseItem = res.body[0] as InfoAboutProduct;
              }
            )
        break;
      case 21://HealfCare
            this.getProduct.getHealfCare(this.sendFilters).subscribe(
              res =>
              {
                // this.responseItem = res.body[0] as InfoAboutProduct;
              }
            )
        break;
      case 22://Acquaintance
            this.getProduct.getAcquaintance(this.sendFilters).subscribe(
              res =>
              {
                // this.responseItem = res.body[0] as InfoAboutProduct;
              }
            )
        break;
      case 23://EverythingElse
            this.getProduct.getEverythingElse(this.sendFilters).subscribe(
              res =>
              {
                // this.responseItem = res.body[0] as InfoAboutProduct;
              }
            )
        break;
      case 24://Exchange
            this.getProduct.getExchange(this.sendFilters).subscribe(
              res =>
              {
                // this.responseItem = res.body[0] as InfoAboutProduct;
              }
            )
        break;

    }
  }

  showHideFilters(selectedCategory){


    this.showRealEstate = false;
    this.showGender = false;
    this.showForNewBorns = false;
    this.showIsLocal = false;
    this.showisFrozen = false;
    this.showisNaturalDrink = false;
    this.showAlcoholVolume = false;
    this.showDepartureDay = false;
    this.showReturnDay = false;
    this.showReservedTickets = false;
    this.showAdPaymentTime = false;
    this.showPaymentTime = false;
    this.showTransportation = false;
    this.showAcquintanceAim = false;
    this.showHeight = false;
    this.showWeight = false;
    this.showAgeFrom = false;
    this.showAgeTo = false;
    this.showClothingSize = false;
    this.showShoesSize = false;

    switch (selectedCategory.id) {
      case 1://Sale
            //pass
        break;
      case 2://Tner
          this.showRealEstate = true;
        break;
      case 3://Avtomeqenaner

        break;
      case 4://Electronics
            //pass
        break;
      case 5://Appliances
            //pass
        break;
      case 6://HouseholdGoods
            //pass
        break;
      case 7://ClothesAndShoes
            this.showClothingSize = true;
            this.showShoesSize = true;
        break;
      case 8://ForChildren
          this.showGender = true;
          this.showForNewBorns = true;
        break;
      case 9://JewerlyAndAccessories
          this.showGender = true;
        break;
      case 10://Construction
          //pass
        break;
      case 11://AllForHomeAndGArden
          //pass
        break;
      case 12://ProductsAndDrinks
          this.showIsLocal = true;
          this.showisFrozen = true;
          this.showisNaturalDrink = true;
        break;
      case 13://CigaretteAndAlcohol
          this.showIsLocal = true;
          this.showAlcoholVolume = true;
        break;
      case 14://Furniture
          //pass
        break;
      case 15://TourismAndRest
          this.showDepartureDay = true;
          this.showReturnDay = true;
          this.showReservedTickets = true;
        break;
      case 16://Sport
          //pass
        break;
      case 17://PetsAndPlants
          //pass
        break;
      case 18://Culture
          //pass
        break;
      case 19://Work
          this.showAdPaymentTime = true;
        break;
      case 20://Services
          this.showPaymentTime = true;
          this.showTransportation = true;
        break;
      case 21://HealfCare
          //pass
        break;
      case 22://Acquaintance
          this.showGender = true;
          this.showAcquintanceAim = true;
          this.showHeight = true;
          this.showWeight = true;
          this.showAgeFrom = true;
          this.showAgeTo = true;
        break;
      case 23://EverythingElse

        break;
      case 24://Exchange

        break;

    }
  }

  selectFiltersAndBuildForm(id){
    this.formIsCreated = false;
    switch (id) {
      case 1://Sale
          this.bulidMainForm()
          this.getProduct.getSale(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 2://Tner
          this.buildRealEstateForm()
          this.getProduct.getRealEstate(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 3://Avtomeqenaner
          this.buildVehicleForm()
          this.getProduct.getVehicle(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 4://Electronics
          this.bulidMainForm()
          this.getProduct.getElectronics(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 5://Appliances
          this.bulidMainForm()
          this.getProduct.getAppliances(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 6://HouseholdGoods
          this.bulidMainForm()
          this.getProduct.getHouseholdGood(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 7://ClothesAndShoes
          this.buildClothesAndShoesForm()
          this.getProduct.getClothesAndShoes(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 8://ForChildren
          this.buildForChildrenForm()
          this.getProduct.getForChildren(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 9://JewerlyAndAccessories
          this.buildJewerlyAndAccessoriesForm()
          this.getProduct.getJewerlyAndAccessories(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 10://Construction
          this.bulidMainForm()
          this.getProduct.getConstruction(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 11://AllForHomeAndGArden
          this.bulidMainForm()
          this.getProduct.getForHomeAndGarden(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 12://ProductsAndDrinks
          this.buildProductsAndDrinksForm()
          this.getProduct.getProductsAndDrinks(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 13://CigaretteAndAlcohol
          this.buildCiggaretteAndAlcoholForm()
          this.getProduct.getCigaretteAndAlcohol(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 14://Furniture
          this.bulidMainForm()
          this.getProduct.getFurniture(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 15://TourismAndRest
          this.buildTourismAndRestForm()
          this.getProduct.getTourismAndRest(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 16://Sport
          this.bulidMainForm()
          this.getProduct.getSport(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 17://PetsAndPlants
          this.bulidMainForm()
          this.getProduct.getPetsAndPlants(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 18://Culture
          this.bulidMainForm()
          this.getProduct.getCulture(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 19://Work
          this.buildWorkForm()
          this.getProduct.getWork(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 20://Services
          this.buildServicesForm()
          this.getProduct.getServices(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 21://HealfCare
          this.bulidMainForm()
          this.getProduct.getHealfCare(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 22://Acquaintance
          this.buildAcquaintanceForm()
          this.getProduct.getAcquaintance(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 23://EverythingElse
          this.bulidMainForm()
          this.getProduct.getEverythingElse(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
      case 24://Exchange
          this.bulidMainForm()
          this.getProduct.getExchange(this.sendFilters).subscribe(
            res =>
            {
              if(res.status == 200)
                this.responseProduct = res.body as GeneralCategoriesField[]
              else this.empty = true;
            }
          )
        break;
    }
    this.formIsCreated = true;
    this.mainID = id
  }

  //reactive forms
  bulidMainForm()
  {
    this.sendFilterData = this.formBuilder.group({
      pageNumber: [1],
      adCount: [2],
      countryID: [null],
      region: [null],
      city: [null],
      onlyWithPhotos: [false],
      aim: [null],
      adOwner: [null],
      currency: [null],
      salePriceFrom: [null],
      salePriceTo: [null],
      productState: [null]
    })
  }

  buildAcquaintanceForm()
  {
    this.sendFilterData = this.formBuilder.group({
      countryID: [null],
      region: [null],
      city: [null],
      onlyWithPhotos: [false],
      aim: [null],
      adOwner: [null],
      currency: [null],
      salePriceFrom: [null],
      salePriceTo: [null],
      productState: [null],
      acquintanceAim: [null],
      gender: [null],
      height: [null],
      weight: [null],
      ageFrom: [null],
      ageTo: [null],
    })
  }

  buildCiggaretteAndAlcoholForm()
  {
    this.sendFilterData = this.formBuilder.group({
      countryID: [null],
      region: [null],
      city: [null],
      onlyWithPhotos: [false],
      aim: [null],
      adOwner: [null],
      currency: [null],
      salePriceFrom: [null],
      salePriceTo: [null],
      productState: [null],
      isLocal: [null],
      alcoholVolume: [null]
    })
  }

  buildClothesAndShoesForm()
  {
    this.sendFilterData = this.formBuilder.group({
      countryID: [null],
      region: [null],
      city: [null],
      onlyWithPhotos: [false],
      aim: [null],
      adOwner: [null],
      currency: [null],
      salePriceFrom: [null],
      salePriceTo: [null],
      productState: [null],
      clothingSize: [null],
      shoesSize: [null]
    })
  }

  buildForChildrenForm()
  {
    this.sendFilterData = this.formBuilder.group({
      countryID: [null],
      region: [null],
      city: [null],
      onlyWithPhotos: [false],
      aim: [null],
      adOwner: [null],
      currency: [null],
      salePriceFrom: [null],
      salePriceTo: [null],
      productState: [null],
      gender: [null],
      forNewBorns: [null]
    })
  }

  buildJewerlyAndAccessoriesForm()
  {
    this.sendFilterData = this.formBuilder.group({
      countryID: [null],
      region: [null],
      city: [null],
      onlyWithPhotos: [false],
      aim: [null],
      adOwner: [null],
      currency: [null],
      salePriceFrom: [null],
      salePriceTo: [null],
      productState: [null],
      gender: [null],
    })
  }

  buildProductsAndDrinksForm()
  {
    this.sendFilterData = this.formBuilder.group({
      countryID: [null],
      region: [null],
      city: [null],
      onlyWithPhotos: [false],
      aim: [null],
      adOwner: [null],
      currency: [null],
      salePriceFrom: [null],
      salePriceTo: [null],
      productState: [null],
      isLocal: [null],
      isFrozen: [null],
      isNaturalDrink: [null]
    })
  }

  buildRealEstateForm()
  {
    this.sendFilterData = this.formBuilder.group({
      pageNumber: [1],
      adCount: [10],
      countryID: [null],
      region: [null],
      city: [null],
      onlyWithPhotos: [false],
      aim: [null],
      adOwner: [null],
      currency: [null],
      salePriceFrom: [null],
      salePriceTo: [null],
      productState: [null],
      constructionType: [null],
      paymentTime: [null],
      rooms: [null],
      spaceFrom: [null],
      spaceTo: [null],
      floorFrom: [null],
      floorTo: [null]
    })

  }
  buildVehicleForm()
  {
    this.sendFilterData = this.formBuilder.group({
      pageNumber: [1],
      adCount: [10],
      countryID: [null],
      region: [null],
      city: [null],
      onlyWithPhotos: [false],
      aim: [null],
      adOwner: [null],
      currency: [null],
      salePriceFrom: [null],
      salePriceTo: [null],
      mark: [null],
      model: [null],
      productionYear: [null],
      customsCleared: [null],
      mileage: [null],
      bodyType: [null],
      engineType: [null],
      engineSize: [null],
      driveType: [null],
      transmissionType: [null],
      color: [null],
      wheel: [null]
    })

  }

  buildServicesForm()
  {
    this.sendFilterData = this.formBuilder.group({
      countryID: [null],
      region: [null],
      city: [null],
      onlyWithPhotos: [false],
      aim: [null],
      adOwner: [null],
      currency: [null],
      salePriceFrom: [null],
      salePriceTo: [null],
      productState: [null],
      paymentTime: [null],
      transportation: [null]
    })
  }

  buildTourismAndRestForm()
  {
    this.sendFilterData = this.formBuilder.group({
      countryID: [null],
      region: [null],
      city: [null],
      onlyWithPhotos: [false],
      aim: [null],
      adOwner: [null],
      currency: [null],
      salePriceFrom: [null],
      salePriceTo: [null],
      productState: [null],
      departureDay: [null],
      returnDay: [null],
      reservedTickets: [null],
    })
  }

  buildWorkForm()
  {
    this.sendFilterData = this.formBuilder.group({
      countryID: [null],
      region: [null],
      city: [null],
      onlyWithPhotos: [false],
      aim: [null],
      adOwner: [null],
      currency: [null],
      salePriceFrom: [null],
      salePriceTo: [null],
      productState: [null],
      adPaymentTime: [null]
    })
  }

  //open Single
  productInfo(product){
    // console.log(this.selectedCategory)
    // console.log(product)
    this.router.navigateByUrl(`item/${this.selectedCategory.category}/${this.selectedCategory.id}/${product.id}`)
  }

}
