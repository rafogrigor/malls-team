import {
  Component,
  HostListener,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessagingService } from 'src/service/messaging.service';
import { UserService } from 'src/service/user.service';

@Component({
  selector: 'app-login-register',
  templateUrl: './login-register.component.html',
  styleUrls: ['./login-register.component.css'],
})
export class LoginRegisterComponent implements OnInit {
  registerForm: FormGroup;
  loginForm: FormGroup;
  forgotPasswordGroup: FormGroup;
  resetPassword: FormGroup;
  pass: string;
  showConfirmSide: boolean = false;
  showPasswordReset: boolean = false;
  showLoader: boolean = false;
  showConfirmCodeForgote: boolean = false;
  emailSend: string;
  passwordForLogin: string;
  confirmCodeResponseResetPAssword: number;
  allowPassowrdChange: boolean = false;
  cheked: boolean = false;

  @ViewChild('container') container: any;
  @ViewChild('signIn') signIn: any;
  @ViewChild('rulesCheckBox') rules: any;
  @ViewChild('confirmAcc') confirmAcc: any;
  @Input() mainLogin: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router,
    private messagingService: MessagingService
  ) {}
  ngOnInit(): void {
    this.buidForm();
    this.messagingService.requestPermission();
    this.messagingService.receiveMessage();
  }

  buidForm() {
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      phoneNumber: ['', [Validators.required, Validators.minLength(8)]],
      country: [''],
      rules: [false, Validators.required],
    });
    this.loginForm = this.formBuilder.group({
      userName: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(8)]],
    });
  }

  // Login
  login() {
    this.showLoader = true;
    let loginBody = {
      userName: this.loginForm.get('userName').value,
      password: this.loginForm.get('password').value,
      token:
        localStorage.getItem('fToken') == null ||
        localStorage.getItem('fToken') == 'null'
          ? ''
          : localStorage.getItem('fToken'),
    };
    this.userService.login(loginBody).subscribe(
      (res) => {
        this.showLoader = false;
        // this.showLogOut = true;
        localStorage.setItem('%%a$', res.body['id']);
        if (localStorage.getItem('%%a$')) {
          this.router.navigateByUrl('');
        }
        // this.wrongPasswordOrEmail = false;
        this.loginForm.reset();
      },
      (err) => {
        this.showLoader = false;
        // this.wrongPasswordOrEmail = err.status == 401 ? true : false;
      }
    );
  }

  // Register
  register() {
    this.showLoader = true;
    let body = {
      firstName: this.registerForm.get('firstName').value,
      email: this.registerForm.get('email').value,
      password: this.registerForm.get('password').value,
      phoneNumber: this.registerForm.get('phoneNumber').value.toString(),
      country: this.registerForm.get('country').value * 1,
    };
    this.userService.registerUser(body).subscribe(
      (res) => {
        let mail = res.body['email'];
        this.emailSend = mail;
        this.sendConfEmail(mail);
        this.showConfirmSide = true;
        this.showLoader = false;
        this.passwordForLogin = this.registerForm.get('password').value;
        // this.selectedCountryId = 1
      },
      (err) => {
        console.log(err);
        this.showLoader = false;
      }
    );
  }

  sendConfEmail(sendedEmail: string) {
    this.showLoader = true;
    let data = {
      email: sendedEmail,
    };
    this.userService.sendConfirmEmail(data).subscribe(
      (res) => {
        this.showLoader = false;
        this.pass = res.body['confirmCode'];
        this.registerForm.reset();
        // this.hideRegister = true;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  confirmEmail() {
    this.showLoader = true;
    let pass = this.confirmAcc['nativeElement'].value;
    if (pass !== this.pass) {
      let data = {
        email: this.emailSend,
      };
      this.userService.confirmEmail(data).subscribe(
        (res) => {
          this.showLoader = false;
          // this.signIn['nativeElement'].click()
          // this.showConfirmSide = false;
          this.loginForm.get('userName').setValue(this.emailSend),
            this.loginForm.get('password').setValue(this.passwordForLogin);
          this.login();
        },
        (err) => {
          this.showLoader = false;
        }
      );
    }
    this.registerForm.reset();
  }

  forgotPassword() {
    this.showPasswordReset = true;
    this.forgotPasswordGroup = this.formBuilder.group({
      email: ['', Validators.email],
      forgotConfirmCode: ['', Validators.required],
    });
  }
  sendEmailForReset() {
    this.showLoader = true;
    let sendEmail = this.forgotPasswordGroup.get('email').value;
    this.userService.forgotPassword(sendEmail).subscribe((res) => {
      this.confirmCodeResponseResetPAssword = res.body['confirmCode'];
      this.showConfirmCodeForgote = true;
      this.showLoader = false;
    });
  }
  checkResetStatus() {
    this.showLoader = true;
    if (
      this.confirmCodeResponseResetPAssword ==
      this.forgotPasswordGroup.get('forgotConfirmCode').value * 1
    ) {
      setTimeout(() => {
        this.allowPassowrdChange = true;
        this.showLoader = false;
      }, 700);
      this.resetPassword = this.formBuilder.group({
        newPassword: ['', Validators.required, Validators.minLength(8)],
      });
    }
  }
  changePassword() {
    this.showLoader = true;
    let sendData = {
      email: this.forgotPasswordGroup.get('email').value,
      password: this.resetPassword.get('newPassword').value,
    };
    this.userService.resetPasswordFromLoginPage(sendData).subscribe((res) => {
      console.log(res.body);
      this.allowPassowrdChange = false;
      this.showConfirmCodeForgote = false;
      this.showLoader = false;
      this.showPasswordReset = false;
    });
  }

  // Rules
  clickCheckbox() {
    if (!this.cheked) this.rules['nativeElement'].click();
  }
  toggleEditable(event) {
    if (event.target.checked) {
      this.cheked = true;
    } else this.cheked = false;
  }

  rightPanelActive() {
    this.container['nativeElement'].classList.add('right-panel-active');
  }

  rightPanelDective() {
    this.container['nativeElement'].classList.remove('right-panel-active');
  }
}
