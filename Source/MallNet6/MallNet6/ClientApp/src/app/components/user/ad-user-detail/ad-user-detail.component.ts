import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { MyAds } from 'src/model/myAds';
import { User } from 'src/model/user';
import { UserService } from 'src/service/user.service';
import { UserAdsAndSettingsService } from 'src/service/userAdsAndSettings.service';

@Component({
  selector: 'app-ad-user-detail',
  templateUrl: './ad-user-detail.component.html',
  styleUrls: ['./ad-user-detail.component.css'],
})
export class AdUserDetailComponent implements OnInit {

  userId: string;
  user: User;
  ads: MyAds[] = [];
  adsList: any[] = []
  showRating: boolean = false;
  likeStyle: boolean = false;
  sendLike: boolean = false;
  sendDisLike: boolean = true;
  disLikeStyle: boolean = false;
  ratingValue: number;

  constructor(private activatedRoute: ActivatedRoute, private userService: UserService, private userAds: UserAdsAndSettingsService, private rout: Router) {
               }

  ngOnInit(): void
  {
    this.activatedRoute.paramMap.pipe(
      switchMap(params =>params.getAll('id'))).subscribe(
        data=>
        {
            this.userId = data;
            this.userService.getUserById(this.userId).subscribe(
              res =>
              {
                this.user = res.body as User;
                this.ratingValue = this.user.rating
                if(localStorage.getItem('%%a$') && localStorage.getItem('%%a$') != this.userId.toLocaleString())
                {
                  this.showRating = true;
                  console.log(this.showRating)
                }
                this.userAds.getUserAds(this.userId).subscribe(
                  res =>
                  {
                    this.ads = res.body as MyAds[]
                    this.ads.forEach((e) => {e.ads.forEach((ae) => this.adsList.push(ae) )})
                    this.adsList.map((m) =>
                    {
                      let image = m.image.split(" ")
                      m.image = image[0]
                    })
                  }
                )
              }
            )
        });
  }

  productInfo(obj)
  {
    let category = this.ads.find(a => a.ads.find(ad => ad.id == obj.id)).category
    this.rout.navigateByUrl(`item/${category['name']}/${category['id']}/${obj.id}`)
  }

  disLike(val:boolean)
  {
    this.disLikeStyle = !this.disLikeStyle
    this.likeStyle = false;
    this.ratingValue = val == true ? (this.ratingValue * 10 + 1) / 10 : (this.ratingValue * 10 - 1) / 10
    this.userService.addRating(val, this.userId, localStorage.getItem('%%a$')).subscribe(
      res =>
      {
      },
      err =>
      {
        alert('Alredy rated')
      }
    )
  }

  like(val:boolean)
  {
    this.likeStyle = !this.likeStyle
    this.disLikeStyle = false;
    this.ratingValue = val == true ? (this.ratingValue * 10 + 1) / 10 : (this.ratingValue * 10 - 1) / 10
    this.userService.addRating(val, this.userId, localStorage.getItem('%%a$')).subscribe(
      res =>
      {
      },
      err =>
      {
        alert('Alredy rated')
      }
    )
  }
}
