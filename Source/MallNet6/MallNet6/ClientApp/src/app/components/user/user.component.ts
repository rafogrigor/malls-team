import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/model/user';
import { UserService } from 'src/service/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: User;
  updateForm: FormGroup;
  constructor(private userService: UserService, private formBuilder: FormBuilder,
              private router: Router) { }

  ngOnInit(): void {
    // this.buildForm();
    // this.userService.getUser().subscribe(
    //   res => {
    //     this.user = res.body as User
    //     console.log(res)
    //   }
    // )
  }
  // buildForm(){
  //   this.updateForm = this.formBuilder.group({
  //     lastName: ['', Validators.required],
  //     address: ['', Validators.required],
  //     avatar: ['', Validators.required]
  //   })
  // }
  updateProfile(){

  }

}
