import { Component, OnInit } from '@angular/core';
// import { Translate, TranslateRequest } from '@google-cloud/translate/build/src/v2';
import { Tariff } from 'src/model/tariff';
import { TariffService } from 'src/service/tariff.service';

@Component({
  selector: 'app-tariff',
  templateUrl: './tariff.component.html',
  styleUrls: ['./tariff.component.css']
})
export class TariffComponent implements OnInit {

  tariffs: Tariff[] = []
  // a: TranslateRequest

  constructor(private tariffService: TariffService) { }

  ngOnInit(): void {
    this.tariffService.getTariffs().subscribe(
      res =>
      {
        this.tariffs = res.body as Tariff[]
      }
    )
    // console.log(this.google.getLanguages())
    // this.google.translate

  }

}
