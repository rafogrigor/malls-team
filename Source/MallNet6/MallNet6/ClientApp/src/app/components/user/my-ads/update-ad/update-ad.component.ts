import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { filter, switchMap } from 'rxjs/operators';
import { AllForHomeAndGarden } from 'src/model/allForHomeAndGarden';
import { Categories } from 'src/model/categories';
import { City } from 'src/model/city';
import { Country } from 'src/model/country';
import { Curency } from 'src/model/curency';
import { GeneralCategoriesField } from 'src/model/generalCategoriesField';
import { GetProduct } from 'src/model/getProduct';
import { InfoAboutProduct } from 'src/model/infoAboutProduct';
import { Region } from 'src/model/region';
import { Tariff } from 'src/model/tariff';
import { UpdateModel } from 'src/model/updateModel';
import { User } from 'src/model/user';
import { VehicleBodyType } from 'src/model/vehicleBodyType';
import { VehicleColor } from 'src/model/vehicleColor';
import { VehicleDriverType } from 'src/model/vehicleDriverType';
import { VehicleEngineSize } from 'src/model/vehicleEngineSize';
import { VehicleEngineType } from 'src/model/vehicleEngineType';
import { VehicleMark } from 'src/model/vehicleMark';
import { VehicleModel } from 'src/model/vehicleModel';
import { VehicleTransmissionType } from 'src/model/vehicleTransmissionType';
import { VehicleWheelType } from 'src/model/vehicleWheelType';
import { AddAllCategoriesService } from 'src/service/addAllCategories.service';
import { AllForVehicleService } from 'src/service/allForVehicle.service';
import { CategoryService } from 'src/service/category.service';
import { GetProductsService } from 'src/service/getProducts.service';
import { LocationService } from 'src/service/location.service';
import { TariffService } from 'src/service/tariff.service';
import { UpdateService } from 'src/service/update.service';
import { UserService } from 'src/service/user.service';

@Component({
  selector: 'app-update-ad',
  templateUrl: './update-ad.component.html',
  styleUrls: ['./update-ad.component.css'],
})
export class UpdateAdComponent implements OnInit {
  @ViewChild('closeDeleteModal') deleteModal: ElementRef;

  categoryName: string;
  categoryId: number;
  productId: number;
  categories: Categories[];
  selectedCategory: Categories;
  sendFilters = new GetProduct();
  responseItem: InfoAboutProduct;
  curencyList: Curency[];
  currencySymbol: string;
  countryList: Country[];
  regionList: Region[];
  resItem: InfoAboutProduct;
  cityList: City[];
  fullRegion = {
    countryRegion: new Country(),
    region: new Region(),
    city: new City(),
  };
  priceValue: number;
  sendUpdatedData: UpdateModel = new UpdateModel();
  lat: number;
  lng: number;
  tariffs: Tariff[];
  user: User;
  vehicleESize: number;
  vehicleMark: VehicleMark[];
  vehicleModel: VehicleModel[];
  vehicleEnginSize: VehicleEngineSize[];
  vehicleEngineType: VehicleEngineType[];
  vehicleBodyType: VehicleBodyType[];
  vehicleDriveType: VehicleDriverType[];
  vehicleTransmissionType: VehicleTransmissionType[];
  vehicleColor: VehicleColor[];
  vehicleWheelType: VehicleWheelType[];
  vehicleResponseItem: Promise<any>;
  sourceCurrencyID: string;
  destinationCurrencyID: string;
  currencyList: Curency[];

  showAim: boolean = false;
  productState: boolean = false;
  showWork: boolean = false;
  showGender: boolean = false;
  showWeightHeight: boolean = false;
  showIsLocal: boolean = false;
  showAlcoholVolume: boolean = false;
  showClothingSizeAndShoesSize: boolean = false;
  showForNewBorns: boolean = false;
  showIsFrozenAndIsNaturalDrink: boolean = false;
  showAllForRealEstate: boolean = false;
  showTransportation: boolean = false;
  showAllForTourismAndRest: boolean = false;
  showAllForExchange: boolean = false;
  update: boolean = false;
  maleFemale: boolean = false;
  showLoader: boolean = false;
  showMarkName: boolean = true;
  showESizeName: boolean = true;
  showETypeName: boolean = true;
  showBTypeName: boolean = true;
  showDTypeName: boolean = true;
  showAdError: boolean = false;
  showAdSuccess: boolean = false;
  showVehicleResponse: boolean = false;
  showExchange: boolean = true;

  //////
  // currency: Curency[] = []
  selectedCategoryId: number;
  disableSubCategories = true;
  realEstate = false;
  appliance = false;
  clothesAndShoes = false;
  forNewBorns = false;
  jewerlyAndAccessories = false;
  productsAndDrinks = false;
  cigaretteAndAlcohol = false;
  tourism = false;
  work = false;
  services = false;
  acquaintance = false;
  everythingElse = false;
  showOption: boolean;
  showCategoryOption: boolean;
  addForm: FormGroup;
  adForm: FormGroup;
  profilePicBase64: string | ArrayBuffer;
  imagesList: any[] = [];
  countries: Country[] = [];
  oldImagesLinks: any[] = []
  itemAdded: boolean = false;
  showVehicle: boolean = false;

  // profilePicBase64!: string | ArrayBuffer;
  imagePreview: any[] = []
  showPreview: boolean = false;

  // imagesList: any[] = []

  aimText: string;
  constructionTypeText: string;
  productStateText: string;

  constructor(
    private route: ActivatedRoute,
    private categoryService: CategoryService,
    private router: Router,
    private getProduct: GetProductsService,
    private currencyService: AddAllCategoriesService,
    private regionService: LocationService,
    private formBuilder: FormBuilder,
    private updateService: UpdateService,
    private translate: TranslateService,
    private vehicleService: AllForVehicleService,
    private tariffService: TariffService,
    private userService: UserService
  ) {
    // route.params.subscribe(
    //   val => this.ngOnInit()
    // )
  }

  ngOnInit(): void {
    this.route.paramMap
      .pipe(switchMap((params) => params.getAll('categoryId')))
      .subscribe((data) => {
        // this.categoryName = data;
        this.categoryId = parseInt(data);
        this.buildAdForm();
        this.categoryService.getAllCategories().subscribe((res) => {
          this.categories = res.body as Categories[];
          this.selectedCategory = this.categories.find(
            (e) => e.id == this.categoryId
          );
          this.readIdFromLink();
          this.getSingleProduct(this.selectedCategory, this.sendFilters.id);
          this.buildForm(this.selectedCategory.id);
          // this.setLocation()
        });
      });

    // this.getProduct.getSport(this.sendFilters).subscribe(
    //   res =>
    //   {
    //   this.responseItem = res.body[0] as InfoAboutProduct;
    //   this.setLocation()
    //   }
    // )
    this.userService.getUser(localStorage.getItem('%%a$')).subscribe((res) => {
      this.user = res.body as User;
    });
    this.tariffService.getTariffs().subscribe((res) => {
      this.tariffs = res.body as Tariff[];
      this.tariffs.splice(0, 2);
    });
  }

  //Another Functions
  // setLocation(){
  //   this.regionService.getLocation().subscribe(
  //     res =>{
  //       this.countryList = res as Country[]
  //       this.fullRegion.countryRegion = this.countryList.find(c => c.id == this.responseItem.country)
  //       if (this.fullRegion.countryRegion != undefined){
  //         this.regionService.getRegion(this.fullRegion.countryRegion.id).subscribe(
  //           res => {
  //             this.regionList = res as Region[];
  //             this.fullRegion.region = this.regionList.find(r => r.id == this.responseItem.region)
  //             this.regionService.getCity(this.fullRegion.region.id).subscribe(
  //               res => {
  //                 this.cityList = res as City[]
  //                 this.fullRegion.city = this.cityList.find(c => c.id == this.responseItem.city)
  //               }
  //             )
  //           }
  //         )
  //       }
  //     }
  //   )
  // }

  getCurrencySymbol() {
    this.currencyService.getCurrentList().subscribe((res) => {
      this.curencyList = res.body as Curency[];
      this.sourceCurrencyID = this.curencyList.find(
        (curency) => curency.id == this.responseItem.sourceCurrencyID
      ).currency;
      this.destinationCurrencyID = this.curencyList.find(
        (curency) => curency.id == this.responseItem.destinationCurrencyID
      ).currency;
    });
  }

  // Get Single Product Function
  getSingleProduct(selectedCategory: Categories, productId: number) {
    switch (selectedCategory.id) {
      case 1: //Sale
        this.getProduct.getSale(this.sendFilters).subscribe(
          (res) => {
            this.responseItem = res.body[0] as InfoAboutProduct;
            this.lng = this.responseItem.locationLongitude;
            this.lat = this.responseItem.locationLatitude;
            this.getCurrencySymbol();
            // this.setLocation()
            this.showAim = false;
            this.productState = false;
            this.showWork = false;
            this.showGender = false;
            this.showIsLocal = false;
            this.showAlcoholVolume = false;
            this.showClothingSizeAndShoesSize = false;
          },
          (err) => console.log(err)
        );
        break;
      case 2: //Tner
        this.getProduct.getRealEstate(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          this.constructionTypeText =
            this.responseItem.constructionType == 0
              ? this.translate.instant('createAd.stone')
              : this.responseItem.constructionType == 1
              ? this.translate.instant('createAd.panels')
              : this.responseItem.constructionType == 2
              ? this.translate.instant('createAd.monolith')
              : this.responseItem.constructionType == 3
              ? this.translate.instant('createAd.bricks')
              : this.responseItem.constructionType == 4
              ? this.translate.instant('createAd.cassete')
              : this.translate.instant('createAd.wooden');

          this.getCurrencySymbol();
          // this.setLocation()
          this.showAim = true;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');
          this.productState = false;
          this.showWork = true;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
          this.showAllForRealEstate = true;
        });
        break;
      case 3: //Avtomeqenaner
        this.getProduct.getVehicle(this.sendFilters).subscribe((res) => {
          const vehicleResponseItem = new Promise((reslove, reject) => {
            this.responseItem = res.body[0] as InfoAboutProduct;
            reslove(this.responseItem);
          });
          vehicleResponseItem.then((data) => {
            this.asyncVehicleForm();
            this.lng = this.responseItem.locationLongitude;
            this.lat = this.responseItem.locationLatitude;
            this.showVehicleResponse = true;
            this.addForm.get('mileage').setValue(this.responseItem.mileage);

            this.getCurrencySymbol();
            // this.setLocation()
            this.showAim = true;
            this.aimText =
              this.responseItem.aim == 0
                ? this.translate.instant('createAd.forSale')
                : this.responseItem.aim == 1
                ? this.translate.instant('createAd.forRent')
                : this.responseItem.aim == 2
                ? this.translate.instant('createAd.exchange')
                : this.responseItem.aim == 3
                ? this.translate.instant('createAd.buy')
                : this.translate.instant('createAd.search');
            this.productState = true;
            this.productStateText =
              this.responseItem.productState == 0
                ? this.translate.instant('createAd.new')
                : this.translate.instant('createAd.used');
            this.showWork = false;
            this.showGender = false;
            this.showWeightHeight = false;
            this.showIsLocal = false;
            this.showAlcoholVolume = false;
            this.showClothingSizeAndShoesSize = false;
          });
          // this.showVehicle = true;
        });
        break;
      case 4: //Electronics
        this.getProduct.getElectronics(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          this.getCurrencySymbol();
          // this.setLocation()
          this.showAim = true;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          this.productState = true;
          this.productStateText =
            this.responseItem.productState == 0
              ? this.translate.instant('createAd.new')
              : this.translate.instant('createAd.used');

          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 5: //Appliances
        this.getProduct.getAppliances(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          // this.getCurrencySymbol();
          // this.setLocation()
          this.showAim = true;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          this.productState = false;
          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 6: //HouseholdGoods
        this.getProduct.getHouseholdGood(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          // this.getCurrencySymbol();
          // this.setLocation()
          this.showAim = true;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          this.productState = true;
          this.productStateText =
            this.responseItem.productState == 0
              ? this.translate.instant('createAd.new')
              : this.translate.instant('createAd.used');

          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 7: //ClothesAndShoes
        this.getProduct
          .getClothesAndShoes(this.sendFilters)
          .subscribe((res) => {
            this.responseItem = res.body[0] as InfoAboutProduct;
            this.lng = this.responseItem.locationLongitude;
            this.lat = this.responseItem.locationLatitude;
            // this.getCurrencySymbol();
            // this.setLocation()
            this.showAim = true;
            this.aimText =
              this.responseItem.aim == 0
                ? this.translate.instant('createAd.forSale')
                : this.responseItem.aim == 1
                ? this.translate.instant('createAd.forRent')
                : this.responseItem.aim == 2
                ? this.translate.instant('createAd.exchange')
                : this.responseItem.aim == 3
                ? this.translate.instant('createAd.buy')
                : this.translate.instant('createAd.search');

            this.productState = true;
            this.productStateText =
              this.responseItem.productState == 0
                ? this.translate.instant('createAd.new')
                : this.translate.instant('createAd.used');

            this.showWork = false;
            this.showGender = false;
            this.showWeightHeight = false;
            this.showIsLocal = false;
            this.showAlcoholVolume = false;
            this.showClothingSizeAndShoesSize = true;
          });
        break;
      case 8: //ForChildren
        this.getProduct.getForChildren(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          // this.getCurrencySymbol();
          // this.setLocation()
          this.showAim = true;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          this.productState = true;
          this.productStateText =
            this.responseItem.productState == 0
              ? this.translate.instant('createAd.new')
              : this.translate.instant('createAd.used');

          this.showWork = false;
          this.showGender = true;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
          this.showForNewBorns = true;
        });
        break;
      case 9: //JewerlyAndAccessories
        this.getProduct
          .getJewerlyAndAccessories(this.sendFilters)
          .subscribe((res) => {
            this.responseItem = res.body[0] as InfoAboutProduct;
            this.lng = this.responseItem.locationLongitude;
            this.lat = this.responseItem.locationLatitude;
            // this.getCurrencySymbol();
            // this.setLocation()
            this.showAim = true;
            this.aimText =
              this.responseItem.aim == 0
                ? this.translate.instant('createAd.forSale')
                : this.responseItem.aim == 1
                ? this.translate.instant('createAd.forRent')
                : this.responseItem.aim == 2
                ? this.translate.instant('createAd.exchange')
                : this.responseItem.aim == 3
                ? this.translate.instant('createAd.buy')
                : this.translate.instant('createAd.search');

            this.productState = true;
            this.productStateText =
              this.responseItem.productState == 0
                ? this.translate.instant('createAd.new')
                : this.translate.instant('createAd.used');

            this.showWork = false;
            this.showGender = true;
            this.showWeightHeight = false;
            this.showIsLocal = false;
            this.showAlcoholVolume = false;
            this.showClothingSizeAndShoesSize = false;
          });
        break;
      case 10: //Construction
        this.getProduct.getConstruction(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          // this.getCurrencySymbol();
          // this.setLocation()
          this.showAim = true;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          this.productState = true;
          this.productStateText =
            this.responseItem.productState == 0
              ? this.translate.instant('createAd.new')
              : this.translate.instant('createAd.used');

          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 11: //AllForHomeAndGArden
        this.getProduct
          .getForHomeAndGarden(this.sendFilters)
          .subscribe((res) => {
            this.responseItem = res.body[0] as InfoAboutProduct;
            this.lng = this.responseItem.locationLongitude;
            this.lat = this.responseItem.locationLatitude;
            // this.getCurrencySymbol();
            // this.setLocation()
            this.showAim = true;
            this.aimText =
              this.responseItem.aim == 0
                ? this.translate.instant('createAd.forSale')
                : this.responseItem.aim == 1
                ? this.translate.instant('createAd.forRent')
                : this.responseItem.aim == 2
                ? this.translate.instant('createAd.exchange')
                : this.responseItem.aim == 3
                ? this.translate.instant('createAd.buy')
                : this.translate.instant('createAd.search');

            this.productState = true;
            this.productStateText =
              this.responseItem.productState == 0
                ? this.translate.instant('createAd.new')
                : this.translate.instant('createAd.used');

            this.showWork = false;
            this.showGender = false;
            this.showWeightHeight = false;
            this.showIsLocal = false;
            this.showAlcoholVolume = false;
            this.showClothingSizeAndShoesSize = false;
          });
        break;
      case 12: //ProductsAndDrinks
        this.getProduct
          .getProductsAndDrinks(this.sendFilters)
          .subscribe((res) => {
            this.responseItem = res.body[0] as InfoAboutProduct;
            this.lng = this.responseItem.locationLongitude;
            this.lat = this.responseItem.locationLatitude;
            // this.getCurrencySymbol();
            // this.setLocation()
            this.showAim = true;
            this.aimText =
              this.responseItem.aim == 0
                ? this.translate.instant('createAd.forSale')
                : this.responseItem.aim == 1
                ? this.translate.instant('createAd.forRent')
                : this.responseItem.aim == 2
                ? this.translate.instant('createAd.exchange')
                : this.responseItem.aim == 3
                ? this.translate.instant('createAd.buy')
                : this.translate.instant('createAd.search');

            this.productState = false;
            this.showWork = false;
            this.showGender = false;
            this.showWeightHeight = false;
            this.showIsLocal = true;
            this.showAlcoholVolume = false;
            this.showClothingSizeAndShoesSize = false;
            this.showIsFrozenAndIsNaturalDrink = true;
          });
        break;
      case 13: //CigaretteAndAlcohol
        this.getProduct
          .getCigaretteAndAlcohol(this.sendFilters)
          .subscribe((res) => {
            this.responseItem = res.body[0] as InfoAboutProduct;
            this.lng = this.responseItem.locationLongitude;
            this.lat = this.responseItem.locationLatitude;
            // this.getCurrencySymbol();
            // this.setLocation()
            this.showAim = true;
            this.aimText =
              this.responseItem.aim == 0
                ? this.translate.instant('createAd.forSale')
                : this.responseItem.aim == 1
                ? this.translate.instant('createAd.forRent')
                : this.responseItem.aim == 2
                ? this.translate.instant('createAd.exchange')
                : this.responseItem.aim == 3
                ? this.translate.instant('createAd.buy')
                : this.translate.instant('createAd.search');

            this.productState = true;
            this.productStateText =
              this.responseItem.productState == 0
                ? this.translate.instant('createAd.new')
                : this.translate.instant('createAd.used');

            this.showWork = false;
            this.showGender = false;
            this.showWeightHeight = false;
            this.showIsLocal = true;
            this.showAlcoholVolume = true;
            this.showClothingSizeAndShoesSize = false;
          });
        break;
      case 14: //Furniture
        this.getProduct.getFurniture(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          // this.setLocation()
          this.showAim = true;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          this.productState = true;
          this.productStateText =
            this.responseItem.productState == 0
              ? this.translate.instant('createAd.new')
              : this.translate.instant('createAd.used');

          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 15: //TourismAndRest
        this.getProduct.getTourismAndRest(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          // this.getCurrencySymbol();
          // this.setLocation()
          this.showAim = true;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          this.productState = false;
          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
          this.showAllForTourismAndRest = true;
        });
        break;
      case 16: //Sport
        this.getProduct.getSport(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          // this.getCurrencySymbol();
          // this.setLocation()
          // console.log(this.responseItem)
          this.showAim = true;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          this.productState = true;
          this.productStateText =
            this.responseItem.productState == 0
              ? this.translate.instant('createAd.new')
              : this.translate.instant('createAd.used');

          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 17: //PetsAndPlants
        this.getProduct.getPetsAndPlants(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          // console.log(this.responseItem)
          // this.getCurrencySymbol();
          // this.setLocation()
          this.showAim = true;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          this.productState = true;
          this.productStateText =
            this.responseItem.productState == 0
              ? this.translate.instant('createAd.new')
              : this.translate.instant('createAd.used');

          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 18: //Culture
        this.getProduct.getCulture(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          // this.getCurrencySymbol();
          // this.setLocation()
          this.showAim = true;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          this.productState = true;
          this.productStateText =
            this.responseItem.productState == 0
              ? this.translate.instant('createAd.new')
              : this.translate.instant('createAd.used');

          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 19: //Work
        this.getProduct.getWork(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          // this.getCurrencySymbol();
          // this.setLocation()
          this.showAim = false;
          this.productState = false;
          this.showWork = true;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 20: //Services
        this.getProduct.getServices(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          // this.getCurrencySymbol();
          // this.setLocation()
          this.showAim = false;
          this.productState = false;
          this.showWork = true;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
          this.showTransportation = false;
        });
        break;
      case 21: //HealfCare
        this.getProduct.getHealfCare(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          // this.getCurrencySymbol();
          // this.setLocation()
          this.showAim = true;
          this.aimText =
            this.responseItem.aim == 0
              ? this.translate.instant('createAd.forSale')
              : this.responseItem.aim == 1
              ? this.translate.instant('createAd.forRent')
              : this.responseItem.aim == 2
              ? this.translate.instant('createAd.exchange')
              : this.responseItem.aim == 3
              ? this.translate.instant('createAd.buy')
              : this.translate.instant('createAd.search');

          this.productState = true;
          this.productStateText =
            this.responseItem.productState == 0
              ? this.translate.instant('createAd.new')
              : this.translate.instant('createAd.used');

          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 22: //Acquaintance
        this.getProduct.getAcquaintance(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          // this.getCurrencySymbol();
          // this.setLocation()
          this.showAim = false;
          this.productState = false;
          this.showWork = false;
          this.showGender = true;
          this.showWeightHeight = true;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 23: //EverythingElse
        this.getProduct.getEverythingElse(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          // this.getCurrencySymbol();
          // this.setLocation()
          this.showAim = false;
          this.productState = false;
          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
        });
        break;
      case 24: //Exchange
        this.getProduct.getExchange(this.sendFilters).subscribe((res) => {
          this.responseItem = res.body[0] as InfoAboutProduct;
          this.lng = this.responseItem.locationLongitude;
          this.lat = this.responseItem.locationLatitude;
          this.getCurrencySymbol();
          // this.setLocation()
          this.showAim = false;
          this.productState = false;
          this.showWork = false;
          this.showGender = false;
          this.showWeightHeight = false;
          this.showIsLocal = false;
          this.showAlcoholVolume = false;
          this.showClothingSizeAndShoesSize = false;
          this.showAllForExchange = true;
        });
        break;
    }
  }

  //detect category
  detectCategory(id) {
    this.addForm.get('description').setValue(this.responseItem.description);
    this.addForm.get('tags').setValue(this.responseItem.tags);
    this.addForm.get('contact').setValue(this.responseItem.contact);
    if (id != 24)
      this.addForm.get('price').setValue(this.responseItem['price']);
    this.addForm.get('currency').setValue(this.responseItem.currency);
    this.addForm.get('owner').setValue(this.responseItem.owner);
    this.addForm.get('name').setValue(this.responseItem.name);
    this.addForm.get('comment').setValue(this.responseItem.comment);
    this.addForm.get('id').setValue(this.responseItem.id);
    this.addForm.get('userId').setValue(this.responseItem.userID);
    this.addForm.get('country').setValue(this.responseItem.country);
    this.addForm.get('city').setValue(this.responseItem.city);
    this.addForm.get('region').setValue(this.responseItem.region);
    this.addForm
      .get('locationLatitude')
      .setValue(this.responseItem.locationLatitude);
    this.addForm
      .get('locationLongitude')
      .setValue(this.responseItem.locationLongitude);
    this.addForm.get('subCategoryId').setValue(this.responseItem.subCategoryID);
    this.addForm
      .get('salePercent')
      .setValue(
        this.responseItem.salePercent == null ||
          this.responseItem.salePercent == undefined
          ? 0
          : this.responseItem.salePercent
      );

    // console.log(this.responseItem.userID)
    this.showCategoryOption = false;
    // this.addForm.reset()
    id = id * 1;
    this.selectedCategoryId = id * 1;
    this.realEstate = false;
    this.appliance = false;
    this.clothesAndShoes = false;
    this.forNewBorns = false;
    this.jewerlyAndAccessories = false;
    this.productsAndDrinks = false;
    this.cigaretteAndAlcohol = false;
    this.tourism = false;
    this.work = false;
    this.services = false;
    this.acquaintance = false;
    this.everythingElse = false;
    // this.categoryService.getSubCategory(id).subscribe(
    //   res =>{
    //     this.subCategories = res.body as SubCategories[]
    //     this.disableSubCategories = false
    //     this.showOption = false;
    //   }
    // )
    switch (id) {
      case 2: //RealEstate
        this.realEstate = true;
        this.addForm.get('aim').setValue(this.responseItem.aim);
        this.addForm.get('space').setValue(this.responseItem.space);
        // this.addForm.get('paymentTime').setValue(this.responseItem.paymentTime)
        this.addForm.get('floor').setValue(this.responseItem.floor);
        this.addForm.get('rooms').setValue(this.responseItem.rooms);
        this.addForm
          .get('constructionType')
          .setValue(this.responseItem.constructionType);
        break;
      case 3: //Vehicle
        this.showVehicle = true;
        //avtoner
        break;
      case 4: //Electronics
        this.appliance = true;
        this.addForm
          .get('productState')
          .setValue(this.responseItem.productState);
        this.addForm.get('aim').setValue(this.responseItem.aim);
        break;
      case 5: //Appliances
        this.appliance = true;
        this.addForm
          .get('productState')
          .setValue(this.responseItem.productState);
        this.addForm.get('aim').setValue(this.responseItem.aim);
        break;
      case 6: //HouseholdGoods
        this.appliance = true;
        this.addForm
          .get('productState')
          .setValue(this.responseItem.productState);
        this.addForm.get('aim').setValue(this.responseItem.aim);
        break;
      case 7: //ClothesAndShoes
        this.clothesAndShoes = true;
        this.addForm
          .get('productState')
          .setValue(this.responseItem.productState);
        this.addForm.get('aim').setValue(this.responseItem.aim);
        this.addForm
          .get('clothingSize')
          .setValue(this.responseItem.clothingSize);
        this.addForm.get('shoesSize').setValue(this.responseItem.shoesSize);
        break;
      case 8: //ForChildren
        this.forNewBorns = true;
        this.addForm
          .get('productState')
          .setValue(this.responseItem.productState);
        this.addForm.get('aim').setValue(this.responseItem.aim);
        this.addForm.get('gender').setValue(this.responseItem.gender);
        this.addForm.get('forNewBorns').setValue(this.responseItem.forNewBorns);

        break;
      case 9: //JewerlyAndAccessories
        this.jewerlyAndAccessories = true;
        this.addForm
          .get('productState')
          .setValue(this.responseItem.productState);
        this.addForm.get('aim').setValue(this.responseItem.aim);
        this.addForm.get('gender').setValue(this.responseItem.gender);
        break;
      case 10: //Construction
        this.appliance = true;
        this.addForm
          .get('productState')
          .setValue(this.responseItem.productState);
        this.addForm.get('aim').setValue(this.responseItem.aim);
        break;
      case 11: //AllForHomeAndGarden
        this.appliance = true;
        this.addForm
          .get('productState')
          .setValue(this.responseItem.productState);
        this.addForm.get('aim').setValue(this.responseItem.aim);
        break;
      case 12: //ProductsAndDrinks
        this.productsAndDrinks = true;
        this.addForm.get('aim').setValue(this.responseItem.aim);
        this.addForm.get('isFrozen').setValue(this.responseItem.isFrozen);
        this.addForm.get('isLocal').setValue(this.responseItem.isLocal);
        this.addForm
          .get('isNaturalDrink')
          .setValue(this.responseItem.isNaturalDrink);

        break;
      case 13: //CigaretteAndAlcohol
        this.cigaretteAndAlcohol = true;
        this.addForm
          .get('productState')
          .setValue(this.responseItem.productState);
        this.addForm.get('aim').setValue(this.responseItem.aim);
        this.addForm
          .get('alcoholVolume')
          .setValue(this.responseItem.alcoholVolume);
        this.addForm.get('isLocal').setValue(this.responseItem.isLocal);
        break;
      case 14: //Furniture
        this.appliance = true;
        this.addForm
          .get('productState')
          .setValue(this.responseItem.productState);
        this.addForm.get('aim').setValue(this.responseItem.aim);
        break;
      case 15: //TourismAndRest
        this.tourism = true;
        this.addForm.get('aim').setValue(this.responseItem.aim);
        this.addForm
          .get('departureDay')
          .setValue(this.responseItem.departureDay);
        this.addForm.get('returnDay').setValue(this.responseItem.returnDay);
        this.addForm
          .get('reservedTickets')
          .setValue(this.responseItem.reservedTickets);
        break;
      case 16: //Sporth
        this.appliance = true;
        this.addForm
          .get('productState')
          .setValue(this.responseItem.productState);
        this.addForm.get('aim').setValue(this.responseItem.aim);
        break;
      case 17: //PetsAndPlants
        this.appliance = true;
        this.addForm
          .get('productState')
          .setValue(this.responseItem.productState);
        this.addForm.get('aim').setValue(this.responseItem.aim);
        break;
      case 18: //Culture
        this.appliance = true;
        this.addForm
          .get('productState')
          .setValue(this.responseItem.productState);
        this.addForm.get('aim').setValue(this.responseItem.aim);
        break;
      case 19: //Work
        this.work = true;
        // this.addForm.get('paymentTime').setValue(this.responseItem.paymentTime)
        break;
      case 20: //Services
        this.services = true;
        // this.addForm.get('paymentTime').setValue(this.responseItem.paymentTime)
        this.addForm
          .get('transportation')
          .setValue(this.responseItem.transportation);

        break;
      case 21: //HealfCare
        this.appliance = true;
        this.addForm
          .get('productState')
          .setValue(this.responseItem.productState);
        this.addForm.get('aim').setValue(this.responseItem.aim);
        break;
      case 22: //EverythingElse
        this.acquaintance = true;

        break;
      case 23: //Acquaintance
        this.everythingElse = true;
        this.addForm.get('aim').setValue(this.responseItem.aim);
        this.addForm.get('gender').setValue(this.responseItem.gender);
        this.addForm.get('height').setValue(this.responseItem.height);
        this.addForm.get('weight').setValue(this.responseItem.weight);
        this.addForm
          .get('acquaintanceAim')
          .setValue(this.responseItem.acquaintanceAim);
        break;
      case 24: //Exchange
        this.currencyService.getCurrentList().subscribe((res) => {
          this.currencyList = res.body as Curency[];
          this.currencyList.splice(2, 1);
        });
        this.addForm
          .get('destinationCurrencyID')
          .setValue(this.responseItem.destinationCurrencyID);
        this.addForm
          .get('sourceCurrencyID')
          .setValue(this.responseItem.sourceCurrencyID);
        this.addForm
          .get('saleSummaRetail')
          .setValue(this.responseItem.saleSummaRetail);
        this.addForm
          .get('buySummaRetail')
          .setValue(this.responseItem.buySummaRetail);
        this.addForm.get('saleSumma').setValue(this.responseItem.saleSumma);
        this.addForm.get('buySumma').setValue(this.responseItem.buySumma);
        break;

      default:
        break;
    }
  }

  buildForm(id) {
    switch (id) {
      case 1: //Sale
        this.bulidMainForm();

        break;
      case 2: //Tner
        this.buildRealEstateForm();

        break;
      case 3: //Avtomeqenaner
        // this.buildVehicleForm()
        // this.vehicleService.getMark().subscribe(
        //   res =>
        //   {
        //     const mark = new Promise((reslove, reject) =>
        //     {
        //       this.vehicleMark = res.body as VehicleMark[]
        //       reslove(this.vehicleMark)
        //     })
        //     mark.then(data =>
        //       {
        //         let id = this.vehicleMark.find((mark) => mark.name == this.responseItem.sMark).id
        //         this.addForm.get('mark').setValue(id)
        //         this.vehicleService.getModel(id).subscribe(
        //           res =>
        //           {
        //             const model = new Promise((reslove, reject) =>
        //             {
        //               this.vehicleModel = res.body as VehicleModel[]
        //               reslove(this.vehicleModel)
        //             })
        //             model.then(dataModel =>
        //               {
        //                 let modelId = this.vehicleModel.find(model => model.name == this.responseItem.sModel).id
        //                 this.addForm.get('model').setValue(modelId)
        //               })
        //           }
        //         )

        //       })
        //   }
        // )
        // this.vehicleService.getEngineSize().subscribe(
        //   res =>
        //   {
        //     this.vehicleEnginSize = res.body as VehicleEngineSize[]
        //     let id = this.vehicleEnginSize.find((eSize) => eSize.id == this.responseItem.engineSize).id
        //     this.vehicleESize = this.vehicleEnginSize.find((eSize) => eSize.id == this.responseItem.engineSize).size
        //     this.addForm.get('engineSize').setValue(id)
        //   }
        // )
        // this.vehicleService.getEngineType().subscribe(
        //   res =>
        //   {
        //     this.vehicleEngineType = res.body as VehicleEngineType[]
        //     let id = this.vehicleEngineType.find(eType => eType.engineType == this.responseItem.sEngineType).id
        //     this.addForm.get('engineType').setValue(id)
        //   }
        // )
        // this.vehicleService.getBodyType().subscribe(
        //   res =>
        //   {
        //     this.vehicleBodyType = res.body as VehicleBodyType[]
        //     let id = this.vehicleBodyType.find(bType => bType.name == this.responseItem.sBodyType).id
        //     this.addForm.get('bodyType').setValue(id)
        //   }
        // )
        // this.vehicleService.getDriveType().subscribe(
        //   res =>
        //   {
        //     this.vehicleDriveType = res.body as VehicleDriverType[]
        //     let id = this.vehicleDriveType.find(dType => dType.name == this.responseItem.sDriveType).id
        //     this.addForm.get('driveType').setValue(id)
        //   }
        // )
        // this.vehicleService.getTransmissionType().subscribe(
        //   res =>
        //   {
        //     this.vehicleTransmissionType = res.body as VehicleTransmissionType[]
        //     let id = this.vehicleTransmissionType.find(tType => tType.transmissionType == this.responseItem.sTransmissionType).id
        //     this.addForm.get('transmissionType').setValue(id)
        //   }
        // )
        // this.vehicleService.getColor().subscribe(
        //   res =>
        //   {
        //     this.vehicleColor = res.body as VehicleColor[]
        //     let id = this.vehicleColor.find(color => color.name == this.responseItem.sColor).id
        //     this.addForm.get('color').setValue(id)
        //   }
        // )
        // this.vehicleService.getWheelType().subscribe(
        //   res =>
        //   {
        //     this.vehicleWheelType = res.body as VehicleWheelType[]
        //     let id = this.vehicleWheelType.find(wheel => wheel.wheel == this.responseItem.sWheel).id
        //     this.addForm.get('wheel').setValue(id)
        //   }
        // )

        break;
      case 4: //Electronics
        this.bulidMainForm();

        break;
      case 5: //Appliances
        this.bulidMainForm();

        break;
      case 6: //HouseholdGoods
        this.bulidMainForm();

        break;
      case 7: //ClothesAndShoes
        this.buildClothesAndShoesForm();

        break;
      case 8: //ForChildren
        this.buildForChildrenForm();

        break;
      case 9: //JewerlyAndAccessories
        this.buildJewerlyAndAccessoriesForm();

        break;
      case 10: //Construction
        this.bulidMainForm();

        break;
      case 11: //AllForHomeAndGArden
        this.bulidMainForm();

        break;
      case 12: //ProductsAndDrinks
        this.buildProductsAndDrinksForm();

        break;
      case 13: //CigaretteAndAlcohol
        this.buildCiggaretteAndAlcoholForm();

        break;
      case 14: //Furniture
        this.bulidMainForm();

        break;
      case 15: //TourismAndRest
        this.buildTourismAndRestForm();

        break;
      case 16: //Sport
        this.bulidMainForm();

        break;
      case 17: //PetsAndPlants
        this.bulidMainForm();

        break;
      case 18: //Culture
        this.bulidMainForm();

        break;
      case 19: //Work
        this.buildWorkForm();

        break;
      case 20: //Services
        this.buildServicesForm();

        break;
      case 21: //HealfCare
        this.bulidMainForm();

        break;
      case 22: //Acquaintance
        this.buildAcquaintanceForm();

        break;
      case 23: //EverythingElse
        this.bulidMainForm();

        break;
      case 24: //Exchange
        this.buildExchangeForm();

        break;
    }
  }

  //Build default form
  bulidMainForm() {
    this.addForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      productState: ['', Validators.required],
    });
  }

  buildVehicleForm() {
    this.addForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      mark: ['', Validators.required],
      model: ['', Validators.required],
      productionYear: ['', Validators.required],
      customsCleared: ['', Validators.required],
      mileage: ['', Validators.required],
      bodyType: ['', Validators.required],
      engineSize: ['', Validators.required],
      engineType: ['', Validators.required],
      driveType: ['', Validators.required],
      transmissionType: ['', Validators.required],
      color: ['', Validators.required],
      wheel: ['', Validators.required],
      isRegional: ['', Validators.required],
    });
  }

  buildAcquaintanceForm() {
    this.addForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      constructionType: ['', Validators.required],
      gender: ['', Validators.required],
      height: ['', Validators.required],
      weight: ['', Validators.required],
      acquaintanceAim: ['', Validators.required],
    });
  }

  buildCiggaretteAndAlcoholForm() {
    this.addForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      productState: ['', Validators.required],
      isLocal: ['', Validators.required],
      alcoholVolume: ['', Validators.required],
    });
  }

  buildClothesAndShoesForm() {
    this.addForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      productState: ['', Validators.required],
      clothingSize: ['', Validators.required],
      shoesSize: ['', Validators.required],
    });
  }

  buildForChildrenForm() {
    this.addForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      productState: ['', Validators.required],
      gender: ['', Validators.required],
      forNewBorns: ['', Validators.required],
    });
  }

  buildJewerlyAndAccessoriesForm() {
    this.addForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      productState: ['', Validators.required],
      gender: ['', Validators.required],
    });
  }

  buildProductsAndDrinksForm() {
    this.addForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      isLocal: ['', Validators.required],
      isFrozen: ['', Validators.required],
      isNaturalDrink: ['', Validators.required],
    });
  }

  buildRealEstateForm() {
    this.addForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      productState: ['', Validators.required],
      space: ['', Validators.required],
      constructionType: ['', Validators.required],
      // paymentTime: ['', Validators.required],
      rooms: ['', Validators.required],
      floor: ['', Validators.required],
    });
  }

  buildServicesForm() {
    this.addForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      // paymentTime: ['', Validators.required],
      transportation: ['', Validators.required],
    });
  }

  buildTourismAndRestForm() {
    this.addForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      departureDay: ['', Validators.required],
      returnDay: ['', Validators.required],
      reservedTickets: ['', Validators.required],
    });
  }

  buildExchangeForm() {
    this.addForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      sourceCurrencyID: ['', Validators.required],
      destinationCurrencyID: ['', Validators.required],
      saleSummaRetail: ['', Validators.required],
      buySummaRetail: ['', Validators.required],
      saleSumma: ['', Validators.required],
      buySumma: ['', Validators.required],
    });
  }

  buildWorkForm() {
    this.addForm = this.formBuilder.group({
      userId: ['', Validators.required],
      id: ['', Validators.required],
      subCategoryId: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      salePercent: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      locationLatitude: ['', Validators.required],
      locationLongitude: ['', Validators.required],
      contact: ['', Validators.required],
      owner: ['', Validators.required],
      comment: ['', Validators.required],
      tags: ['', Validators.required],
      imagesList: ['', Validators.required],
      aim: ['', Validators.required],
      productState: ['', Validators.required],
    });
  }

  buildAdForm() {
    this.adForm = this.formBuilder.group({
      tariffID: ['', Validators.required],
    });
  }

  setAd(id) {
    this.tariffService
      .purchaseLoaderAd(
        this.responseItem.id,
        parseInt(localStorage.getItem('%%a$')),
        id * 1
      )
      .subscribe(
        (res) => {
          this.showAdSuccess = true;
        },
        (err) => {
          this.showAdError = true;
        }
      );
  }

  //GetID
  readIdFromLink(): void {
    this.route.paramMap
      .pipe(switchMap((params) => params.getAll('id')))
      .subscribe((data) => {
        this.productId = parseInt(data);
      });
    this.sendFilters.id = this.productId;
  }

  updateAd(obj): void {
    // console.log(obj);
    this.update = true;
  }

  addData(obj) {
    // this.regionService.getLocation().subscribe(
    //   res =>
    //   {
    //     this.countries = res.body as Country[]
    //     let curCountry = this.countries.find(c => c.country == obj['countryName'])
    //     this.fullRegion.countryRegion = curCountry
    //     this.regionService.getRegion(this.fullRegion.countryRegion.id).subscribe(
    //       res =>
    //       {
    //         this.regionList = res.body as Region[]
    //         let curRegion = this.regionList.find(r => r.country == obj['regionName'])
    //         this.fullRegion.region = curRegion
    //         this.regionService.getCity(this.fullRegion.region.id).subscribe(
    //           res =>
    //           {
    //             this.cityList = res.body as City[]
    //             let curCity = this.cityList.find(c => c.country == obj['cityName'])
    //             this.fullRegion.city = curCity;
    //           }
    //         )
    //       }
    //     )
    //   }
    // )
    // console.log(this.fullRegion)
    this.sendUpdatedData.id = obj.value.id;
    this.sendUpdatedData.adType = this.selectedCategory.id;
    this.sendUpdatedData.subCategoryID = obj.value.subCategoryId * 1;
    this.sendUpdatedData.name = obj.value.name;
    this.sendUpdatedData.description = obj.value.description;
    this.sendUpdatedData.locationLatitude = obj.value.locationLatitude * 1;
    this.sendUpdatedData.locationLongitude = obj.value.locationLongitude * 1;
    this.sendUpdatedData.price = obj.value.price * 1;
    this.sendUpdatedData.salePercent = obj.value.salePercent * 1;
    this.sendUpdatedData.contact = obj.value.contact;
    this.sendUpdatedData.country = this.fullRegion.countryRegion.id;
    this.sendUpdatedData.region = this.fullRegion.region.id;
    this.sendUpdatedData.city = this.fullRegion.city.id;
    // this.sendUpdatedData.state = this.responseItem.state
    this.sendUpdatedData.owner = obj.value.owner * 1;
    this.sendUpdatedData.currency = obj.value.currency * 1;
    this.sendUpdatedData.tags = obj.value.tags;
    // this.sendUpdatedData.isRegional = obj.value.isRegional;

    // this.sendUpdatedData.mark = obj.value.mark;
    // this.sendUpdatedData.model = obj.value.model;
    // this.sendUpdatedData.productionYear = obj.value.productionYear;
    // this.sendUpdatedData.customsCleared = obj.value.customsCleared;
    // this.sendUpdatedData.mileage = obj.value.mileage;
    // this.sendUpdatedData.bodyType = obj.value.bodyType;
    // this.sendUpdatedData.engineType = obj.value.engineType;
    // this.sendUpdatedData.engineSize = obj.value.engineSize;
    // this.sendUpdatedData.driveType = obj.value.driveType;
    // this.sendUpdatedData.transmissionType = obj.value.transmissionType;
    // this.sendUpdatedData.color = obj.value.color;
    // this.sendUpdatedData.wheel = obj.value.wheel;
    // this.sendUpdatedData.sourceCurrencyID = obj.value.sourceCurrencyID;
    // this.sendUpdatedData.destinationCurrencyID = obj.value.destinationCurrencyID;
    // this.sendUpdatedData.saleSummaRetail = obj.value.saleSummaRetail;
    // this.sendUpdatedData.buySummaRetail = obj.value.buySummaRetail;
    // this.sendUpdatedData.saleSumma = obj.value.saleSumma;
    // this.sendUpdatedData.buySumma = obj.value.buySumma;

    switch (this.selectedCategory.id) {
      case 2: //RealEstate
        this.sendUpdatedData.aim = obj.value.aim * 1;
        // this.sendUpdatedData.productState = obj.value.productState * 1;
        // this.sendUpdatedData.paymentTime = obj.value.paymentTime * 1;
        this.sendUpdatedData.rooms = obj.value.rooms * 1;
        this.sendUpdatedData.space = obj.value.space * 1;
        this.sendUpdatedData.floor = obj.value.floor * 1;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 3: //Vehicle
        this.sendUpdatedData.mark = this.addForm.get('mark').value * 1;
        this.sendUpdatedData.model = this.addForm.get('model').value * 1;
        this.sendUpdatedData.engineSize =
          this.addForm.get('engineSize').value * 1;
        this.sendUpdatedData.engineType =
          this.addForm.get('engineType').value * 1;
        this.sendUpdatedData.bodyType = this.addForm.get('bodyType').value * 1;
        this.sendUpdatedData.driveType =
          this.addForm.get('driveType').value * 1;
        this.sendUpdatedData.transmissionType =
          this.addForm.get('transmissionType').value * 1;
        this.sendUpdatedData.color = this.addForm.get('color').value * 1;
        this.sendUpdatedData.wheel = this.addForm.get('wheel').value * 1;
        this.sendUpdatedData.mileage = this.addForm.get('mileage').value * 1;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );

        break;
      case 4: //Electronics
        this.sendUpdatedData.aim = obj.value.aim * 1;
        this.sendUpdatedData.productState = obj.value.productState * 1;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 5: //Appliances
        this.sendUpdatedData.aim = obj.value.aim * 1;
        this.sendUpdatedData.productState = obj.value.productState * 1;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 6: //HouseholdGoods
        this.sendUpdatedData.aim = obj.value.aim * 1;
        this.sendUpdatedData.productState = obj.value.productState * 1;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 7: //ClothesAndShoes
        this.sendUpdatedData.aim = obj.value.aim * 1;
        this.sendUpdatedData.productState = obj.value.productState * 1;
        this.sendUpdatedData.clothingSize = obj.value.clothingSize;
        this.sendUpdatedData.shoesSize = obj.value.shoesSize * 1;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 8: //ForChildren
        this.sendUpdatedData.aim = obj.value.aim * 1;
        this.sendUpdatedData.productState = obj.value.productState * 1;
        this.sendUpdatedData.gender = obj.value.gender;
        this.sendUpdatedData.forNewBorns = obj.value.forNewBorns;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 9: //JewerlyAndAccessories
        this.sendUpdatedData.aim = obj.value.aim * 1;
        this.sendUpdatedData.productState = obj.value.productState * 1;
        this.sendUpdatedData.gender = obj.value.gender;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 10: //Construction
        this.sendUpdatedData.aim = obj.value.aim * 1;
        this.sendUpdatedData.productState = obj.value.productState * 1;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 11: //AllForHomeAndGarden
        this.sendUpdatedData.aim = obj.value.aim * 1;
        this.sendUpdatedData.productState = obj.value.productState * 1;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 12: //ProductsAndDrinks
        this.sendUpdatedData.aim = obj.value.aim * 1;
        this.sendUpdatedData.isLocal = obj.value.isLocal;
        this.sendUpdatedData.isFrozen = obj.value.isFrozen;
        this.sendUpdatedData.isNaturalDrink = obj.value.isNaturalDrink;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 13: //CigaretteAndAlcohol
        this.sendUpdatedData.aim = obj.value.aim * 1;
        this.sendUpdatedData.productState = obj.value.productState * 1;
        this.sendUpdatedData.isLocal = obj.value.isLocal;
        this.sendUpdatedData.alcoholVolume = obj.value.alcoholVolume;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 14: //Furniture
        this.sendUpdatedData.aim = obj.value.aim * 1;
        this.sendUpdatedData.productState = obj.value.productState * 1;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 15: //TourismAndRest
        this.sendUpdatedData.aim = obj.value.aim * 1;
        this.sendUpdatedData.departureDay = obj.value.departureDay;
        this.sendUpdatedData.returnDay = obj.value.returnDay;
        this.sendUpdatedData.reservedTickets = obj.value.reservedTickets * 1;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 16: //Sport
        this.sendUpdatedData.aim = obj.value.aim * 1;
        this.sendUpdatedData.productState = obj.value.productState * 1;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 17: //PetsAndPlants
        this.sendUpdatedData.aim = obj.value.aim * 1;
        this.sendUpdatedData.productState = obj.value.productState * 1;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 18: //Culture
        this.sendUpdatedData.aim = obj.value.aim * 1;
        this.sendUpdatedData.productState = obj.value.productState * 1;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 19: //Work
        // this.sendUpdatedData.paymentTime = obj.value.paymentTime * 1;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 20: //Services
        // this.sendUpdatedData.paymentTime = obj.value.paymentTime * 1;
        this.sendUpdatedData.transportation = obj.value.transportation;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 21: //HealfCare
        this.sendUpdatedData.aim = obj.value.aim * 1;
        this.sendUpdatedData.productState = obj.value.productState * 1;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 22: //EverythingElse
        this.sendUpdatedData.productState = obj.value.productState * 1;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 23: //Acquaintance
        this.sendUpdatedData.aim = obj.value.aim * 1;
        this.sendUpdatedData.height = obj.value.height * 1;
        this.sendUpdatedData.weight = obj.value.weight * 1;
        this.sendUpdatedData.gender = obj.value.gender;
        this.sendUpdatedData.acquaintanceAim = obj.value.acquaintanceAim * 1;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
      case 24: //Exchange
        this.sendUpdatedData.sourceCurrencyID = obj.value.sourceCurrencyID;
        this.sendUpdatedData.destinationCurrencyID =
          obj.value.destinationCurrencyID;
        this.sendUpdatedData.saleSummaRetail = obj.value.saleSummaRetail;
        this.sendUpdatedData.buySummaRetail = obj.value.buySummaRetail;
        this.sendUpdatedData.saleSumma = obj.value.saleSumma;
        this.sendUpdatedData.buySumma = obj.value.buySumma;
        this.sendUpdatedData.price = 0;
        this.sendUpdatedData.salePercent = 0;
        this.updateService.updateAd(this.sendUpdatedData).subscribe(
          (res) => {
            this.showLoader = true;
            location.reload();
          },
          (err) => {
            alert('ERROR');
          }
        );
        break;
    }
    console.log(this.sendUpdatedData);
  }

  obenUpdateModal() {
    this.priceValue = this.responseItem.salePercent;
    this.detectCategory(this.selectedCategory.id);
    this.regionService.getLocation().subscribe((res) => {
      this.countries = res.body as Country[];
      let curCountry = this.countries.find(
        (c) => c.country == this.responseItem.countryName
      );
      this.fullRegion.countryRegion = curCountry;
      this.regionService
        .getRegion(this.fullRegion.countryRegion.id)
        .subscribe((res) => {
          this.regionList = res.body as Region[];
          let curRegion = this.regionList.find(
            (r) => r.country == this.responseItem.regionName
          );
          this.fullRegion.region = curRegion;
          this.regionService
            .getCity(this.fullRegion.region.id)
            .subscribe((res) => {
              this.cityList = res.body as City[];
              let curCity = this.cityList.find(
                (c) => c.country == this.responseItem.cityName
              );
              this.fullRegion.city = curCity;
            });
        });
    });
  }
  deleteAd() {
    this.updateService.delete(this.responseItem.id).subscribe((res) => {
      this.deleteModal.nativeElement.click();
      this.router.navigateByUrl(`/profile/my-ads`);
    });
  }

  changedMark(id) {
    this.showMarkName = false;
    this.vehicleService.getModel(id).subscribe((res) => {
      this.vehicleModel = res.body as VehicleModel[];
      let modelId = this.vehicleModel.find(
        (model) => model.name == this.responseItem.sModel
      ).id;
      this.addForm.get('model').setValue(modelId);
    });
  }
  asyncVehicleForm() {
    this.buildVehicleForm();
    this.vehicleService.getMark().subscribe((res) => {
      const mark = new Promise((reslove, reject) => {
        this.vehicleMark = res.body as VehicleMark[];
        reslove(this.vehicleMark);
      });
      mark.then((data) => {
        let id = this.vehicleMark.find(
          (mark) => mark.name == this.responseItem.sMark
        ).id;
        this.addForm.get('mark').setValue(id);
        this.vehicleService.getModel(id).subscribe((res) => {
          const model = new Promise((reslove, reject) => {
            this.vehicleModel = res.body as VehicleModel[];
            reslove(this.vehicleModel);
          });
          model.then((dataModel) => {
            let modelId = this.vehicleModel.find(
              (model) => model.name == this.responseItem.sModel
            ).id;
            this.addForm.get('model').setValue(modelId);
          });
        });
      });
    });
    this.vehicleService.getEngineSize().subscribe((res) => {
      this.vehicleEnginSize = res.body as VehicleEngineSize[];
      let id = this.vehicleEnginSize.find(
        (eSize) => eSize.id == this.responseItem.engineSize
      ).id;
      this.vehicleESize = this.vehicleEnginSize.find(
        (eSize) => eSize.id == this.responseItem.engineSize
      ).size;
      this.addForm.get('engineSize').setValue(id);
    });
    this.vehicleService.getEngineType().subscribe((res) => {
      this.vehicleEngineType = res.body as VehicleEngineType[];
      let id = this.vehicleEngineType.find(
        (eType) => eType.engineType == this.responseItem.sEngineType
      ).id;
      this.addForm.get('engineType').setValue(id);
    });
    this.vehicleService.getBodyType().subscribe((res) => {
      this.vehicleBodyType = res.body as VehicleBodyType[];
      let id = this.vehicleBodyType.find(
        (bType) => bType.name == this.responseItem.sBodyType
      ).id;
      this.addForm.get('bodyType').setValue(id);
    });
    this.vehicleService.getDriveType().subscribe((res) => {
      this.vehicleDriveType = res.body as VehicleDriverType[];
      let id = this.vehicleDriveType.find(
        (dType) => dType.name == this.responseItem.sDriveType
      ).id;
      this.addForm.get('driveType').setValue(id);
    });
    this.vehicleService.getTransmissionType().subscribe((res) => {
      this.vehicleTransmissionType = res.body as VehicleTransmissionType[];
      let id = this.vehicleTransmissionType.find(
        (tType) => tType.transmissionType == this.responseItem.sTransmissionType
      ).id;
      this.addForm.get('transmissionType').setValue(id);
    });
    this.vehicleService.getColor().subscribe((res) => {
      this.vehicleColor = res.body as VehicleColor[];
      let id = this.vehicleColor.find(
        (color) => color.name == this.responseItem.sColor
      ).id;
      this.addForm.get('color').setValue(id);
    });
    this.vehicleService.getWheelType().subscribe((res) => {
      this.vehicleWheelType = res.body as VehicleWheelType[];
      let id = this.vehicleWheelType.find(
        (wheel) => wheel.wheel == this.responseItem.sWheel
      ).id;
      this.addForm.get('wheel').setValue(id);
    });
  }

  changeValue() {
    // console.log(this.priceValue)
  }
  copyImages(img:string[])
  {
    this.oldImagesLinks = [...img]
  }
  deletAdPhoto(img:string)
  {
    let index = this.oldImagesLinks.findIndex(image => image == img)
    this.oldImagesLinks.splice(index, 1)
  }

  onUploadImage(event: any)
  {
    this.imagePreview = []
    this.imagesList = []
    let file = event.target.files
    for(let i = 0; i < file.length; i++)
    {
      const reader = new FileReader()
      reader.onload = () =>
      {
        this.profilePicBase64 = reader.result as string;
        let imageText: string = this.profilePicBase64 as string;
        this.imagePreview.push(imageText); //reset Needs
        imageText = imageText.replace(/^data:image\/[a-z]+;base64,/, '');
        this.imagesList.push(imageText);
      }
      reader.readAsDataURL(file[i]);
    }
    this.showPreview = true;
  }

  deletePreview(val:any)
  {
    let indexPreview = this.imagePreview.findIndex((a) => a == val);
    this.imagePreview.splice(indexPreview, 1);
    let image = val.replace(/^data:image\/[a-z]+;base64,/, '');
    let index = this.imagesList.findIndex((a) => a == image);
    this.imagesList.splice(index, 1);
    // this.inputFile.nativeElement.value == '';
  }


  saveImages()
  {
    let objToSend = {
      adID: this.resItem.id,
      subCategory: this.resItem.subCategoryID,
      newImagesList: this.imagesList,
      oldImagesLinks: this.oldImagesLinks
    }

    this.updateService.updateAd(objToSend).subscribe(
      (res) => {
        this.showLoader = true;
        location.reload();
      },
      (err) => {
        alert('ERROR');
      }
    )
  }
}
