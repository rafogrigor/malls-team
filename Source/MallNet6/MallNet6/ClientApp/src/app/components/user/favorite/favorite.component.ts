import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Categories } from 'src/model/categories';
import { FavoriteResponse } from 'src/model/favoriteResponset';
import { FavoriteService } from 'src/service/favorite.service';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css'],
})
export class FavoriteComponent implements OnInit {
  favoriteList: FavoriteResponse[] = [];
  favoriteListIsEmpty: boolean = false;

  constructor(private favService: FavoriteService, private router: Router) {}

  ngOnInit(): void {
    this.favService
      .getFavoriteList(localStorage.getItem('%%a$'))
      .subscribe((res) => {
        this.favoriteList = res.body['favoriteList'] as FavoriteResponse[];
        if (this.favoriteList.length == 0) this.favoriteListIsEmpty = true;
        this.favoriteList.forEach((value) => {
          value.ads.map((m) => {
            let image = m.image.split(' ');
            m.image = image[0];
          });
        });
      });
  }

  productInfo(category, productId) {
    console.log(category.id);
    console.log(category.name);
    // this.router.navigateByUrl(`item/${this.selectedCategory.category}/${product.id}`)
    this.router.navigate(
      [`item/${category.name}/${category.id}/${productId}`],
      {
        queryParams: {
          favorite: `favorite_item=//-${productId}`,
          'check-=q1$$%Id': productId,
        },
      }
    );
  }
}
