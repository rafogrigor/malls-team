import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/service/user.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  changeGroup: FormGroup
  showMessage: boolean = false;
  constructor(private userService: UserService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.bulidForm();
  }

  bulidForm()
  {
    this.changeGroup = this.formBuilder.group({
      phoneNumber: ['', [Validators.required, Validators.minLength(8)]],
      oldPassword: ['', [Validators.required, Validators.minLength(8)]],
      newPassword: ['', [Validators.required, Validators.minLength(8)]]
    })
  }
  changePassword()
  {
    let phNumber: number = this.changeGroup.get('phoneNumber').value
    let objToSend = {
      phoneNumber: phNumber.toString(),
      oldPassword: this.changeGroup.get('oldPassword').value,
      newPassword: this.changeGroup.get('newPassword').value
    }
    this.userService.changePassword(objToSend).subscribe(
      res =>
      {
        this.showMessage = true;
        setTimeout(() =>
        {
          this.showMessage = false;
          this.router.navigate([''])
        }, 1000)
      }
    )
  }

}
