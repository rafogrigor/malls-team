import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MyAds } from 'src/model/myAds';
import { UserAdsAndSettingsService } from 'src/service/userAdsAndSettings.service';

@Component({
  selector: 'app-my-ads',
  templateUrl: './my-ads.component.html',
  styleUrls: ['./my-ads.component.css'],
})
export class MyAdsComponent implements OnInit {
  myAds: MyAds[] = [];
  constructor(
    private myAdsService: UserAdsAndSettingsService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.myAdsService.getMyAds().subscribe((res) => {
      this.myAds = res.body['adList'] as MyAds[];
      console.log(this.myAds);
      this.myAds.map((myAd) =>
        myAd.ads.find((ad) => {
          let img = ad.image.split(' ');
          ad.image = img[0];
        })
      );
    });
  }

  productInfo(category: any, adId: number): void {
    // this.router.navigateByUrl(`profile/my-ads/update-ad/${categoryName}/${adId}`)
    console.log(category);
    this.router
      .navigate([`update-ad/${category['name']}/${category['id']}/${adId}`], {
        queryParams: {
          a: true,
        },
      })
      .then((r) => {
        console.log(r);
      });
  }
}
