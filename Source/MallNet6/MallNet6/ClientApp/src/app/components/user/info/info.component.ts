import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { City } from 'src/model/city';
import { Country } from 'src/model/country';
import { Region } from 'src/model/region';
import { UpdateUser } from 'src/model/updateUser';
import { User } from 'src/model/user';
import { LocationService } from 'src/service/location.service';
import { UserService } from 'src/service/user.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css'],
})
export class InfoComponent implements OnInit {
  user: User;
  userID = parseInt(localStorage.getItem('%%a$'));
  updateForm: FormGroup;
  country: Country[];
  region: Region[];
  city: City[];
  profilePicBase64: any;
  imageSrc: string = '../../../../assets/images/noimage.png';

  @ViewChild('img') imageUploader: ElementRef;
  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
    private router: Router,
    private locationService: LocationService
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.userService.getUser(this.userID).subscribe((res) => {
      this.user = res.body as User;
      this.updateForm.get('firstName').setValue(this.user.firstName);
      if (this.user.lastName != null) {
        this.updateForm.get('lastName').setValue(this.user.lastName);
      }
      if (this.user.address != null) {
        this.updateForm.get('address').setValue(this.user.address);
      }
      if (this.user.avatar != null) {
        this.updateForm.get('avatar').setValue(this.user.avatar);
        this.imageSrc = this.user.avatar;
      }
      if (this.user.phoneNumber != null) {
        this.updateForm.get('phoneNumber').setValue(this.user.phoneNumber);
      }
      this.getLocation(this.user.country, this.user.region, this.user.city);
    });
  }
  buildForm() {
    this.updateForm = this.formBuilder.group({
      lastName: ['', Validators.required],
      firstName: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      address: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      city: ['', Validators.required],
      avatar: [null],
    });
  }

  openFileInput() {
    this.imageUploader.nativeElement.click();
  }
  getLocation(countryID, regionID, cityID) {
    this.locationService.getLocation().subscribe((res) => {
      this.country = res.body as Country[];
      this.updateForm
        .get('country')
        .setValue(this.country.find((c) => c.id == countryID).id);

      this.locationService.getRegion(countryID).subscribe((res) => {
        this.region = res.body as Region[];
        if (regionID != 0) {
          this.updateForm
            .get('region')
            .setValue(this.region.find((r) => r.id == regionID).id);
        } else this.updateForm.get('region').setValue(this.region[0].id);
      });

      this.locationService
        .getCity(regionID == 0 ? this.country[0].id : regionID)
        .subscribe((res) => {
          this.city = res.body as City[];
          if (cityID != 0) {
            this.updateForm
              .get('city')
              .setValue(this.city.find((c) => c.id == cityID).id);
          } else this.updateForm.get('city').setValue(this.city[0].id);
        });
    });
  }

  changeCountry(obj) {
    this.locationService.getRegion(obj * 1).subscribe((res) => {
      this.region = res.body as Region[];
      this.updateForm.get('region').setValue(this.region[0].id);
      this.locationService.getCity(this.region[0].id).subscribe((res) => {
        this.city = res.body as City[];
        this.updateForm.get('city').setValue(this.city[0].id);
      });
    });
  }
  changeRegion(obj) {
    this.locationService.getCity(obj * 1).subscribe((res) => {
      this.city = res.body as City[];
      this.updateForm.get('city').setValue(this.city[0].id);
    });
  }

  imageChange(e) {
    console.log(e);
  }
  onUploadProfilePicture(event) {
    const file = event.target.files;
    for (let i = 0; i < file.length; i++) {
      if (file[i].type == 'image/png' || file[i].type == 'image/jpeg') {
        const reader = new FileReader();
        reader.onload = () => {
          this.profilePicBase64 = reader.result;
          // <string>this.profilePicBase64.()
          let imageText: string = this.profilePicBase64 as string;
          this.imageSrc = imageText;
          imageText = imageText.replace(/^data:image\/[a-z]+;base64,/, '');
          this.updateForm.get('avatar').setValue(imageText);
          // this.imagesList.push(this.profilePicBase64)
          // this.imagesList.push(imageText);
          // imageText = '';
        };
        reader.readAsDataURL(file[i]);
      } else alert('Select only png, jpg or jpeg');
    }
  }

  updateProfile() {
    let body: UpdateUser = new UpdateUser();
    body.id = this.user.id;
    body.lastName = this.updateForm.get('lastName').value;
    body.firstName = this.updateForm.get('firstName').value;
    body.country = this.updateForm.get('country').value * 1;
    body.city = this.updateForm.get('city').value * 1;
    body.region = this.updateForm.get('region').value * 1;
    body.phoneNumber = this.updateForm.get('phoneNumber').value;
    body.address = this.updateForm.get('address').value;
    body.avatar = this.updateForm.get('avatar').value;
    this.userService.updateRating(body).subscribe((res) => {
      console.log(res);
    });
  }
}
