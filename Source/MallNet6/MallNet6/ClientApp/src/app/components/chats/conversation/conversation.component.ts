import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireDatabase} from '@angular/fire/database';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { FirebaseChatService } from 'src/service/firebaseChat.service';

export interface Chat{
  message: string;
  time: Date;
}

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.css']
})
export class ConversationComponent implements OnInit {

  chatId: string;
  subscribeParam: boolean = false;
  chats: Chat[] = []
  message: FormGroup
  showChat: boolean = false;

  constructor(private activatedRoute: ActivatedRoute, private fireService: FirebaseChatService, private formBuilder: FormBuilder, private fireBase: AngularFireDatabase) {
    this.activatedRoute.params.subscribe(res =>
      {
        if(this.subscribeParam)
          {
            // this.ngOnInit()
          }

      })

   }

  ngOnInit(): void {
    this.buildForm()
    this.subscribeParam = false
    this.activatedRoute.paramMap.pipe(switchMap(params => params.getAll('chatId'))).subscribe(data=>
      {
        this.chatId = data;
        this.subscribeParam = true;
        this.getChat(localStorage.getItem('%%a$'), this.chatId)
      });
  }

  getChat(id, userId)
  {
    this.fireService.getMessege(id, userId).subscribe(
      res =>
      {
        this.chats = res['chat'] as Chat[]
        this.showChat = true
      }
    )
  }


  buildForm(){
    this.message = this.formBuilder.group({
      message:['', Validators.required]
    })
  }

  sendSMS()
  {
    // console.log(this.message.get('message').value)
    let unix_timestamp = 1549312452
    var date = new Date(unix_timestamp * 1000);
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();

    // Will display time in 10:30:23 format
    var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

  //   this.fireService.sendMessage(localStorage.getItem('%%a$'), this.chatId, data)
  //   .then(res =>
  //     {
  //       console.log(res);
  //       this.message.reset();
  //     })
  //     .catch(e => {
  //       console.log(e);
  //     })
  // .set({
  //   message: this.message.get('message').value,
  //   time: new Date()
  // })
  let rootRef = this.fireBase.database.ref('chat_user')
  let rootChatRef = this.fireBase.database.ref('user-chats')
    rootRef.child(localStorage.getItem('%%a$')).child(this.chatId).set({
        message: this.message.get('message').value,
        date: formattedTime
      })

  }
}
