import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { User } from 'src/model/user';
import { FirebaseChatService } from 'src/service/firebaseChat.service';
import { UserService } from 'src/service/user.service';

@Component({
  selector: 'app-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.css']
})
export class ChatsComponent implements OnInit {

  constructor(private chat: FirebaseChatService, private userService: UserService, private firestore: AngularFirestore, private router: Router) { }

  userMessegeList: string[] = []
  users: User[] = []


  ngOnInit(): void {
    this.getDocument(localStorage.getItem('%%a$'))
  }

  async getMessegeUsers()
  {
    try
    {
      // const users = await this.chat.getCollection(localStorage.getItem('%%a$')).toPromise()
      // console.log(users)
      // this.getDocument(users)

    }
    catch(error)
    {
      console.log(error)
    }
  }




 getDocument(users)
  {
    const a = new Promise((reslove, reject) =>
    {
      this.firestore.collection(users)
      .snapshotChanges().pipe(
        map((snaps)=>
        {
          reslove(snaps)
        })
      ).subscribe()
    }
    )
    a.then(data =>
      {
        let doc = data as []
        doc.forEach(el => this.userMessegeList.push(el['payload']['doc']['id']))
      })
      .then((data)=>
      {
          this.userMessegeList.forEach(id =>
            {
              const user = new Promise((resolve, reject)=>
              {
                this.userService.getUser(id).subscribe(
                  res =>
                  {
                    resolve(res)
                  }
                )
              })
              user.then((data)=>
              {
                this.users.push(data['body'])
              })
            })
          // console.log(this.users)
      })

  }


  getUSerMessege(id){
    // this.router.navigateByUrl(`messenger/chat/${id}`)
  }

}
