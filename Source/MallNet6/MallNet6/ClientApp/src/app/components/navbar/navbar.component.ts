import { HttpResponse } from '@angular/common/http';
import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { BestExchange } from 'src/model/bestExchange';
import { Categories } from 'src/model/categories';
import { Country } from 'src/model/country';
import { Curency } from 'src/model/curency';
import { FavoriteResponse } from 'src/model/favoriteResponset';
import { GetProduct } from 'src/model/getProduct';
import { InfoAboutProduct } from 'src/model/infoAboutProduct';
import { SubCategories } from 'src/model/subCategories';
import { AddAllCategoriesService } from 'src/service/addAllCategories.service';
import { CategoryService } from 'src/service/category.service';
import { DataExchangeService } from 'src/service/dataExchange.service';
import { FavoriteService } from 'src/service/favorite.service';
import { GetProductsService } from 'src/service/getProducts.service';
import { LocationService } from 'src/service/location.service';
import { ProductsService } from 'src/service/products.service';
import { SearchService } from 'src/service/search.service';
import { UserService } from 'src/service/user.service';

export interface PassCode {
  confirmCode: number;
}

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  categories: Categories[] = [];
  subCategories: SubCategories[];
  registerForm: FormGroup;
  loginForm: FormGroup;
  pass: PassCode;
  favoriteList: FavoriteResponse[] = [];
  loginObjectBool: Subscription;
  addCount: Subscription;
  currencyList: Curency[] = [];
  selectedCurrency: Curency;
  loginMessage: boolean = false;
  showLogOut = false;
  hideRegister = false;
  disableSubmit = true;
  spin = false;
  excahnge: BestExchange;
  exchangeGetField: GetProduct = new GetProduct();
  exchangeDetailView: InfoAboutProduct;
  redirectedPage: Categories[];
  wrongPasswordOrEmail = false;
  countries: Country[] = [];
  selectedCountryId = 1;
  favCount: number = 0;
  promptValue: string[] = [];
  logined: boolean;
  showNavbar: boolean = false;
  armLat: number = 40.1772;
  armLng: number = 44.50349;
  lat: number;
  lng: number;
  translate = {
    search: ['Որոնել', 'Поиск', 'Search'],
    allCategories: ['Բոլոր բաժինները', 'Все разделы', 'All sections:'],
    logReg: ['Մուտք/Գրանցում', 'Логин/Рег.', 'Login or Register'],
  };
  checkAM: boolean;
  checkRU: boolean;
  checkEN: boolean;

  culture: number;
  @ViewChild('searchInput') searchText: string;
  @ViewChild('loginRegister') openModal: any;
  @ViewChild('closeExchangeModal') closeModalExchange: any;
  @ViewChild('closeLoginModal') closeLoginModal: any;

  constructor(
    private catService: CategoryService,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private route: Router,
    private routSnap: ActivatedRoute,
    private searchService: SearchService,
    private locationService: LocationService,
    private prService: ProductsService,
    private favService: FavoriteService,
    private getProduct: GetProductsService,
    private dateEx: DataExchangeService,
    private currencyListService: AddAllCategoriesService,
    private translateSer: TranslateService
  ) {
    //login admin
    if (!localStorage.getItem('token')) {
      prService.getToken().subscribe((res) => {
        localStorage.setItem('token', res.body['token']);
        setTimeout(() => {
          this.showNavbar = true;
        }, 0);
      });
    } else {
      this.showNavbar = true;
    }
  }

  ngOnInit() {
    if (localStorage.getItem('%%a$')) this.logined = true;
    else this.logined = false;
    this.checkAM = localStorage.getItem('language') == '0' ? true : false;
    this.checkRU = localStorage.getItem('language') == '1' ? true : false;
    this.checkEN = localStorage.getItem('language') == '1' ? true : false;
    this.translateSer.use(localStorage.getItem('language'));

    // this.determineLang();
    this.getCurrencyList();
    this.catService.getAllCategories().subscribe((res) => {
      this.categories = res.body as Categories[];
      // console.log(this.categories.findIndex(e => e.id == 24), this.categories[23])
      this.categories.splice(
        this.categories.findIndex((e) => e.id == 24),
        1
      );
    });
    // console.log(this.categories)
    this.culture = parseInt(localStorage.getItem('language'));
    this.buidForm();
    this.showLogOut = localStorage.getItem('%%a$') ? true : false;
    this.locationService
      .getLocation()
      .subscribe((res) => (this.countries = res.body as Country[]));
    setTimeout(() => {
      this.counter();
    }, 1000);
    this.getExchange();
    this.loginObjectBool = this.dateEx.getObj().subscribe((data) => {
      this.loginMessage = data;
      this.openModal['nativeElement'].click();
    });
    setTimeout(() => {
      this.addCount = this.dateEx.getfavcount().subscribe((data) => {
        this.favCount += data;
      });
    }, 0);
  }

  getSubCategory(id: number) {
    this.catService.getSubCategory(id).subscribe((res) => {
      this.subCategories = res.body as SubCategories[];
    });
  }

  // setLanguage(id:number){
  //   if(id === 0)
  //     localStorage.setItem('language', '0')
  //   else if(id === 1)
  //     localStorage.setItem('language', '1')
  //   else localStorage.setItem('language', '2')
  //   this.culture = id
  //   location.reload()
  // }
  changeLang(lang: string) {
    this.checkAM = lang == '0' ? true : false;
    this.checkRU = lang == '1' ? true : false;
    this.checkEN = lang == '2' ? true : false;
    localStorage.setItem('language', lang);
    this.translateSer.use(lang);
    location.reload();
  }
  // determineLang(){
  //   if(!localStorage.getItem('language')){
  //     localStorage.setItem('language', '0')
  //   }
  // }
  // register(){
  //   let body = {
  //     firstName: this.registerForm.get('firstName').value,
  //     email: this.registerForm.get('email').value,
  //     password: this.registerForm.get('password').value,
  //     phoneNumber: this.registerForm.get('phoneNumber').value.toString(),
  //     country: this.selectedCountryId*1
  //   }
  //   this.userService.registerUser(body).subscribe(
  //     (res => {
  //       let mail = res.body['email'];
  //       this.sendConfEmail(mail)
  //       this.selectedCountryId = 1
  //       }
  //     ),
  //     err => console.log(err)
  //   )
  // }
  buidForm() {
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      phoneNumber: ['', [Validators.required, Validators.minLength(8)]],
      country: [''],
    });
    this.loginForm = this.formBuilder.group({
      userName: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(8)]],
    });
  }
  // sendConfEmail(sendedEmail: string){
  //   let data = {
  //     email: sendedEmail
  //   }
  //   this.userService.sendConfirmEmail(data).subscribe(
  //     res => {
  //       this.pass = res.body["confirmCode"]
  //       this.hideRegister = true;
  //     }
  //   );
  // }
  // confirmEmail(passCode: string){
  //   let pass =parseInt(passCode)
  //   if(pass !== this.pass.confirmCode){
  //     let data ={
  //       email: this.registerForm.get('email').value
  //     }
  //     this.userService.confirmEmail(data).subscribe(
  //       res => {
  //         this.spin = true;

  //         this.registerForm.reset()
  //         this.closeLoginModal['nativeElement'].click()
  //         this.spin = false
  //       }
  //     )
  //   }
  //   this.registerForm.reset()
  // }

  deleteForm() {
    this.registerForm.reset();
    this.loginForm.reset();
  }

  // login(){
  //   let loginBody = {
  //     userName: this.loginForm.get('userName').value,
  //     password: this.loginForm.get('password').value
  //   }
  //   this.userService.login(loginBody).subscribe(
  //     res => {
  //       let element = document.getElementById('loginRegister');
  //       let button = document.createElement("button");
  //       button.setAttribute('data-bs-dismiss','modal');
  //       button.style.width = '1px';
  //       button.style.height = '1px';
  //       button.style.opacity = '0.1';
  //       element.appendChild(button);
  //       element.click();
  //       this.showLogOut = true;
  //       localStorage.setItem('%%a$',res.body["id"]);
  //       this.wrongPasswordOrEmail = false;
  //       this.loginForm.reset();
  //       location.reload()
  //     },
  //     err => {
  //       this.wrongPasswordOrEmail = err.status == 401 ? true : false;
  //     }

  //   )
  // }
  logout() {
    localStorage.removeItem('%%a$');
    this.showLogOut = false;
    location.reload();
  }
  addProduct() {
    this.route.navigateByUrl('add-product');
  }

  searchProduct(e) {
    if (e != null && e != '') {
      this.route.navigate([`search`], {
        queryParams: {
          search: `${e}`,
        },
      });
      // .then(() => location.reload())
    }
  }

  //searchButton

  @HostListener('document:keydown.enter', ['$event'])
  private search(event: Event) {
    this.searchText['nativeElement'].value == ''
      ? false
      : this.searchProduct(this.searchText['nativeElement'].value);
  }

  redirectBySubCategory(categoryName, subCategory) {
    this.route.navigate([`/category/${categoryName}`], {
      queryParams: {
        subcategory: subCategory['type'],
        subId: subCategory['id'],
      },
    });
  }

  //favorite
  favoriteAds() {
    const ID = localStorage.getItem('%%a$');
    if (ID) this.route.navigate([`favorite`]);
  }

  //favCounter
  counter() {
    if (!!localStorage.getItem('%%a$')) {
      this.favService.getFavoriteList(localStorage.getItem('%%a$')).subscribe(
        (res) => {
          this.favoriteList = res.body['favoriteList'] as FavoriteResponse[];
          this.favoriteList.forEach((val, i) => {
            this.favCount += val['ads'].length;
          });
        },
        (err) => {
          console.log('error');
        }
      );
    } else this.favCount = 0;
  }

  //exchange
  getExchange() {
    this.prService.getMainPageProducts().subscribe((res) => {
      this.excahnge = res.body['bestExchange'] as BestExchange;
    });
  }
  getCurrencyList() {
    this.currencyListService.getCurrentList().subscribe((res) => {
      this.currencyList = res.body as Curency[];
      if (localStorage.getItem('currency')) {
        this.selectedCurrency = this.currencyList.find(
          (c) => c.id == parseInt(localStorage.getItem('currency'))
        );
      } else {
        localStorage.setItem('currency', '1');
        this.selectedCurrency = this.currencyList[0];
      }
    });
  }
  changeFav(obj) {
    localStorage.setItem('currency', obj);
  }
  openCategory(obj, id) {
    // console.log(obj)
    this.route.navigateByUrl(`category/${id}`);
  }
  getInfoExchange(userID, adID) {
    this.exchangeGetField.id = adID;
    this.getProduct.getExchange(this.exchangeGetField).subscribe((res) => {
      this.exchangeDetailView = res.body[0] as InfoAboutProduct;
      this.lng = this.exchangeDetailView.locationLongitude;
      this.lat = this.exchangeDetailView.locationLatitude;
      // console.log(this.exchangeDetailView)
    });
  }
  goToExchangeView(obj) {
    console.log(obj);
    this.closeModalExchange['nativeElement'].click();
    this.route.navigateByUrl(`category/24`);
  }
  addExchange() {
    this.route
      .navigateByUrl(`add-exchange`)
      .then(() => this.closeModalExchange['nativeElement'].click());
  }

  //Prompt
  promptSearch(e) {
    this.searchService.prompt(e).subscribe((res) => {
      this.promptValue = res.body['descriptions'] as string[];
      //  console.log(this.promptValue)
    });
  }

  searchPromptValue(val) {
    console.log(val);
    this.route.navigate([`search`], {
      queryParams: {
        search: `${val}`,
      },
    });
  }

  ngOnDestroy() {
    this.loginObjectBool.unsubscribe();
  }
}
