import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient, HttpClientModule } from '@angular/common/http'
import {TranslateModule, TranslateLoader, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader'
import { AgmCoreModule } from '@agm/core'
import { NgbAlertModule, NgbModule, NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFireModule } from "@angular/fire";
import { AngularYandexMapsModule, YaConfig, YA_CONFIG  } from 'angular8-yandex-maps';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';





import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NavbarComponent } from './components/navbar/navbar.component';
import { UserComponent } from './components/user/user.component';
import { AddProductComponent } from './components/main/add-product/add-product.component';
import { ShowProductPageComponent } from './components/main/show-product-page/show-product-page.component';
import { PoroductComponent } from './components/main/show-product-page/poroduct/poroduct.component';
import { CategoryFilterComponent } from './components/main/show-product-page/category-filter/category-filter.component';
import { MyAdsComponent } from './components/user/my-ads/my-ads.component';
import { InfoComponent } from './components/user/info/info.component';
import { SettingsComponent } from './components/user/settings/settings.component';
import { UpdateAdComponent } from './components/user/my-ads/update-ad/update-ad.component';
import { FavoriteComponent } from './components/user/favorite/favorite.component';
import { SearchItemsComponent } from './components/main/show-product-page/search-items/search-items.component';
import { SimilarAdsComponent } from './components/main/similar-ads/similar-ads.component';
import { LoginRegisterComponent } from './components/login-register/login-register.component';
import { CreateNewAdComponent } from './components/main/create-new-ad/create-new-ad.component';
import { AdUserDetailComponent } from './components/user/ad-user-detail/ad-user-detail.component';
import { CreateExchangeComponent } from './components/main/create-exchange/create-exchange.component';
import { TariffComponent } from './components/user/tariff/tariff.component';
import { ChatsComponent } from './components/chats/chats.component';
import { ConversationComponent } from './components/chats/conversation/conversation.component';
import { environment } from "src/environments/environment.prod";
import { BallanceComponent } from './components/user/ballance/ballance.component';



export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/language/', '.json');
}
const mapConfig: YaConfig = {
  apikey: '45e9b930-c9e1-4e13-ac2e-ae9bea524e9c',
  lang: 'en_US',
};


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavbarComponent,
    UserComponent,
    AddProductComponent,
    ShowProductPageComponent,
    PoroductComponent,
    CategoryFilterComponent,
    MyAdsComponent,
    InfoComponent,
    SettingsComponent,
    UpdateAdComponent,
    FavoriteComponent,
    SearchItemsComponent,
    SimilarAdsComponent,
    LoginRegisterComponent,
    CreateNewAdComponent,
    AdUserDetailComponent,
    CreateExchangeComponent,
    TariffComponent,
    ChatsComponent,
    ConversationComponent,
    BallanceComponent
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    TranslateModule.forRoot
    ({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    }),
    AgmCoreModule.forRoot
    ({
      apiKey: "AIzaSyDA1dK2URgq4fvpsZ-JOd6Qk5SyakuM6eA"
    }),
    AngularYandexMapsModule.forRoot(mapConfig),
    NgbModule,
    NgbPaginationModule,
    NgbAlertModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),

  ],
  providers: [
    // {
    //   provide: YA_CONFIG,
    //   useValue: {
    //     apikey: '45e9b930-c9e1-4e13-ac2e-ae9bea524e9c',
    //     lang: 'en_US',
    //   },
    // },
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(translate: TranslateService){

    if (localStorage.getItem('language') && localStorage.getItem('language') == '0'){
      translate.setDefaultLang('0')
    }
    else if (localStorage.getItem('language') && localStorage.getItem('language') == '1')
    {
      translate.setDefaultLang('1')
    }
    else if (localStorage.getItem('language') && localStorage.getItem('language') == '2')
    {
      translate.setDefaultLang('2')
    }
    else
      translate.setDefaultLang('0')
    // localStorage.setItem('language', '0')
    if(localStorage.getItem('language') != '0' && localStorage.getItem('language') != '1' && localStorage.getItem('language') != '2')
    {
      localStorage.setItem('language', '0')
    }
    if(!localStorage.getItem('currency'))
    {
      localStorage.setItem('currency', '1')
    }
  }
 }
