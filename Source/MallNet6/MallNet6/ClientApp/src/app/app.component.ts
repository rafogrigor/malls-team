import {
  Component,
  HostListener,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { Router } from '@angular/router';
import { LoaderAd } from 'src/model/loaderAd';
import { ProductsService } from 'src/service/products.service';
import { TariffService } from 'src/service/tariff.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements AfterViewInit {
  title = 'store';
  load: boolean = false;
  ad: LoaderAd;
  showCloseButton: boolean = false;
  timeLeft: number = 6;
  interval;
  showTimeLeft: boolean = true;

  @ViewChild('showAd') showAd: ElementRef;
  @ViewChild('close') close: ElementRef;

  constructor(
    private prService: ProductsService,
    private tariffService: TariffService,
    private router: Router
  ) {
    this.prService.getToken().subscribe((res) => {
      this.load = true;
      if (!localStorage.getItem('token')) {
        localStorage.setItem('token', res.body['token']);
        setTimeout(() => {
          this.load = true;
        }, 1000);
      } else {
        setTimeout(() => {
          this.load = true;
        }, 1000);
      }
    });
    this.startTimer();
  }

  startTimer() {
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.showTimeLeft = false;
        this.showCloseButton = true;
        clearInterval(this.interval);
      }
    }, 1000);
  }

  ngAfterViewInit() {
    this.tariffService.loaderAd().subscribe((res) => {
      this.ad = res.body as LoaderAd;
      //open modal
      this.showAd.nativeElement.click();
      // ^only

      // document.getElementById('openModal').click
    });
    // this.showAd.nativeE lement.click()
  }
  productInfo(obj: LoaderAd) {
    this.router
      .navigateByUrl(
        `item/${obj.mainCategoryName}/${obj.mainCategoryID}/${obj.adID}`
      )
      .then(() => this.close.nativeElement.click());
  }

  redirect() {
    this.router
      .navigateByUrl('profile/tariff')
      .then(() => this.close.nativeElement.click());
  }
}
