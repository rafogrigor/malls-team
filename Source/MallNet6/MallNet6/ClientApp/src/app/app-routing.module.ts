import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/guards/auth.guard';
import { LoginpageGuard } from 'src/guards/loginpage.guard';
import { LoginRegisterComponent } from './components/login-register/login-register.component';
import { AddProductComponent } from './components/main/add-product/add-product.component';
import { CreateExchangeComponent } from './components/main/create-exchange/create-exchange.component';
import { CreateNewAdComponent } from './components/main/create-new-ad/create-new-ad.component';
import { MainComponent } from './components/main/main.component';
import { CategoryFilterComponent } from './components/main/show-product-page/category-filter/category-filter.component';
import { PoroductComponent } from './components/main/show-product-page/poroduct/poroduct.component';
import { SearchItemsComponent } from './components/main/show-product-page/search-items/search-items.component';
import { ShowProductPageComponent } from './components/main/show-product-page/show-product-page.component';
import { AdUserDetailComponent } from './components/user/ad-user-detail/ad-user-detail.component';
import { ChatsComponent } from './components/chats/chats.component';
import { FavoriteComponent } from './components/user/favorite/favorite.component';
import { InfoComponent } from './components/user/info/info.component';
import { MyAdsComponent } from './components/user/my-ads/my-ads.component';
import { UpdateAdComponent } from './components/user/my-ads/update-ad/update-ad.component';
import { SettingsComponent } from './components/user/settings/settings.component';
import { TariffComponent } from './components/user/tariff/tariff.component';
import { UserComponent } from './components/user/user.component';
import { ConversationComponent } from './components/chats/conversation/conversation.component';
import { BallanceComponent } from './components/user/ballance/ballance.component';

const routes: Routes = [
  {path:'', component: MainComponent, pathMatch: 'full'},
  {path:'category/:id', component: ShowProductPageComponent},
  {path:'item/:category/:categoryId/:id', component: PoroductComponent},
  {path: 'add-product', component: CreateNewAdComponent, canActivate: [AuthGuard]},
  // {path: 'add-product-test', component: AddProductComponent, canActivate: [AuthGuard]},
  {path: 'add-exchange', component: CreateExchangeComponent, canActivate: [AuthGuard]},
  {path: 'profile', redirectTo: 'profile/my-ads'},
  {path: 'profile', component: UserComponent, canActivate: [AuthGuard], children:[
    {path: 'info', component: InfoComponent, canActivateChild: [AuthGuard]},
    {path: 'my-ads', component: MyAdsComponent, canActivateChild: [AuthGuard]},
    {path: 'settings', component: SettingsComponent, canActivateChild: [AuthGuard]},
    {path: 'tariff', component: TariffComponent, canActivateChild: [AuthGuard]},
    {path: 'balance', component: BallanceComponent, canActivateChild: [AuthGuard]},
  ]},
  {path: 'update-ad/:category/:categoryId/:id', canActivate: [AuthGuard], component: UpdateAdComponent},
  {path: 'messenger', component: ChatsComponent, canActivateChild: [AuthGuard], children:[
    {path: 'chat/:chatId', component: ConversationComponent, canActivateChild: [AuthGuard]}
  ]},
  {path: 'search', component: SearchItemsComponent},
  {path: 'favorite', component: FavoriteComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginRegisterComponent, canActivate: [LoginpageGuard]},
  {path: 'user/:id', component: AdUserDetailComponent},
  // {path: '**', redirectTo: ''}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
