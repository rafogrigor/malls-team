importScripts('https://www.gstatic.com/firebasejs/8.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.8.1/firebase-messaging.js');

firebase.initializeApp({
  apiKey: "AIzaSyBadLPrXXJEj4qwFVhPOpn8IJNl3DUnqac",
  authDomain: "store-f34e6.firebaseapp.com",
  databaseURL: "https://store-f34e6-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "store-f34e6",
  storageBucket: "store-f34e6.appspot.com",
  messagingSenderId: "752649366446",
  appId: "1:752649366446:web:df7021d6b64222ada9b63f",
  measurementId: "G-LWZL6MTPK9"
});
const messaging = firebase.messaging();
