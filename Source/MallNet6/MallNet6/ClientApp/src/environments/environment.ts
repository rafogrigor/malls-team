// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {getBaseUrl} from "../main";

export const environment = {
  production: false,
  // baseUrl: 'http://18.193.88.150',
  // baseUrl: 'https://18.192.107.107',
  // baseUrl: 'https://18.192.107.107',
  //baseUrl: 'https://api.malls.team',
  baseUrl: 'https://d4qqr54ehgym.cloudfront.net',
  // baseUrl: 'https://3.70.94.69',
  firebaseConfig: {
    apiKey: "AIzaSyC63HhZLhBZPoTfoHpZfZBKvK-Uk5b8daU",
    authDomain: "mall-armenia.firebaseapp.com",
    projectId: "mall-armenia",
    storageBucket: "mall-armenia.appspot.com",
    messagingSenderId: "51989059050",
    appId: "1:51989059050:web:4ec875cfe56eb3e8d74e93",
    measurementId: "G-MT9YZ65YN0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
