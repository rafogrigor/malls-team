import {getBaseUrl} from "../main";

export const environment = {
  production: true,
  // baseUrl: 'http://18.193.88.150',
  // baseUrl: 'https://18.192.107.107',
  //baseUrl: getBaseUrl(),
  //baseUrl:'http://localhost:5000',
  baseUrl:'https://d4qqr54ehgym.cloudfront.net',
  // baseUrl: 'https://3.70.94.69',
  firebaseConfig: {
    apiKey: "AIzaSyC63HhZLhBZPoTfoHpZfZBKvK-Uk5b8daU",
    authDomain: "mall-armenia.firebaseapp.com",
    projectId: "mall-armenia",
    storageBucket: "mall-armenia.appspot.com",
    messagingSenderId: "51989059050",
    appId: "1:51989059050:web:4ec875cfe56eb3e8d74e93",
    measurementId: "G-MT9YZ65YN0"
  }
};
