using AutoMapper;
using DAL.Models;
using MallNet6.Models;

namespace MallNet6;

public class AutoMapperProfile : Profile
{
    public AutoMapperProfile()
    {
        CreateMap<DAL.Models.AdCreateResponse, Models.AdCreateResponse>();
        //FavoriteAds
        CreateMap<DAL.Models.FavoriteAds, Models.FavoriteAds>();
        //UserAds
        CreateMap<DAL.Models.UserAds, Models.UserAds>();
        //MainPage 
        //Prompt
        CreateMap<GetPromptAPI, GetPromptDal>();
        CreateMap<SearchGetAPI, SearchGetDal>();
        //
        CreateMap<User, UserDal>();
        CreateMap<UserDal, User>();
        CreateMap<UserCreate, UserDal>();
        CreateMap<UserUpdateData, UserDal>();
        CreateMap<UserDal, UserResponse>();
        CreateMap<PreferenceAPI, PreferenceDal>();
        //Categories
        CreateMap<MainCategoryDal, MainCategory>();
        CreateMap<SubCategoryDal, SubCategory>();
        //
        CreateMap<BaseAdDal, BaseAdAPI>();
        //CreateMap<GeneralPageDal, GeneralPageAPI>();
        CreateMap<AcquaintanceDal, AcquaintanceAPI>();
        CreateMap<RealEstateDal, RealEstateAPI>();
        CreateMap<ApplianceDal, ApplianceAPI>();
        CreateMap<HouseholdGoodDal, HouseholdGoodAPI>();
        CreateMap<ClothesAndShoesDal, ClothesAndShoesAPI>();
        CreateMap<ForChildrenDal, ForChildrenAPI>();
        CreateMap<JewerlyAndAccessoriesDal, JewerlyAndAccessoriesAPI>();
        CreateMap<ConstructionDal, ConstructionAPI>();
        CreateMap<AllForHomeAndGardenDal, AllForHomeAndGardenAPI>();
        CreateMap<ProductsAndDrinksDal, ProductsAndDrinksAPI>();
        CreateMap<FurnitureDal, FurnitureAPI>();
        CreateMap<PetsAndPlantsDal, PetsAndPlantsAPI>();
        CreateMap<CultureDal, CultureAPI>();
        CreateMap<WorkDal, WorkAPI>();
        CreateMap<ServicesDal, ServicesAPI>();
        CreateMap<HealfCareDal, HealfCareAPI>();
        CreateMap<SportDal, SportAPI>();
        CreateMap<CigaretteAndAlcoholDal, CigaretteAndAlcoholAPI>();
        CreateMap<SaleDal, SaleAPI>();
        CreateMap<EverythingElseDal, EverythingElseAPI>();
        CreateMap<ElectronicsDal, ElectronicsAPI>();
        CreateMap<TourismAndRestDal, TourismAndRestAPI>();
        CreateMap<ExchangeDal, ExchangeAPI>();
        CreateMap<CurrencyDal, CurrencyAPI>();
        CreateMap<VehicleDal, VehicleAPI>();
        //LoaderAd
        CreateMap<LoaderAdDAL, LoaderAdAPI>();
        CreateMap<CountryDal, CountryAPI>();
        CreateMap<RegionDal, RegionAPI>();
        CreateMap<CityDal, CityAPI>();
        CreateMap<SearchModelDal, SearchModelAPI>();
        //Vehicle parts
        CreateMap<MarkDal, MarkAPI>();
        CreateMap<ModelDal, ModelAPI>();
        CreateMap<MileageDal, MileageAPI>();
        CreateMap<EngineSizeDal, EngineSizeAPI>();
        CreateMap<ColorDal, ColorAPI>();
        CreateMap<BodyTypeDal, BodyTypeAPI>();
        CreateMap<DriveTypeDal, DriveTypeAPI>();
        CreateMap<TransmissionTypeDAL, TransmissionTypeAPI>();
        CreateMap<EngineTypeDAL, EngineTypeAPI>();
        CreateMap<WheelDAL, WheelAPI>();
        //Get
        CreateMap<SportGetAPI, SportGetDAL>();
        CreateMap<WorkGetAPI, WorkGetDAL>();
        CreateMap<SaleGetAPI, SaleGetDAL>();
        CreateMap<EverythingElseGetAPI, EverythingElseGetDAL>();
        CreateMap<PetsAndPlantsGetAPI, PetsAndPlantsGetDAL>();
        CreateMap<CultureGetAPI, CultureGetDAL>();
        CreateMap<HealfCareGetAPI, HealfCareGetDAL>();
        CreateMap<FurnitureGetAPI, FurnitureGetDAL>();
        CreateMap<ForHomeAndGardenGetAPI, ForHomeAndGardenGetDAL>();
        CreateMap<HouseholdGoodGetAPI, HouseholdGoodGetDAL>();
        CreateMap<ConstructionGetAPI, ConstructionGetDAL>();
        CreateMap<ElectronicGetAPI, ElectronicGetDAL>();
        CreateMap<AcquaintanceGetAPI, AcquaintanceGetDAL>();
        CreateMap<ExchangeGetAPI, ExchangeGetDAL>();
        CreateMap<ApplianceGetAPI, ApplianceGetDAL>();
        CreateMap<VehicleGetAPI, VehicleGetDAL>();
        CreateMap<TourismAndRestGetAPI, TourismAndRestGetDAL>();
        CreateMap<ServicesGetAPI, ServicesGetDAL>();
        CreateMap<RealEstateGetAPI, RealEstateGetDAL>();
        CreateMap<ProductsAndDrinksGetAPI, ProductsAndDrinksGetDAL>();
        CreateMap<JewerlyAndAccessoriesGetAPI, JewerlyAndAccessoriesGetDAL>();
        CreateMap<ForChildrenGetAPI, ForChildrenGetDAL>();
        CreateMap<ClothesAndShoesGetAPI, ClothesAndShoesGetDAL>();
        CreateMap<CigaretteAndAlcoholGetAPI, CigaretteAndAlcoholGetDAL>();
        //For Update
        CreateMap<UniversalAPI, UniversalDal>();
        CreateMap<ImagesUpdateAPI, ImagesUpdateDAL>();
        //API to DAL (Create)
        CreateMap<RealEstateAPI, RealEstateDal>();
        CreateMap<ApplianceAPI, ApplianceDal>();
        CreateMap<HouseholdGoodAPI, HouseholdGoodDal>();
        CreateMap<ClothesAndShoesAPI, ClothesAndShoesDal>();
        CreateMap<ForChildrenAPI, ForChildrenDal>();
        CreateMap<JewerlyAndAccessoriesAPI, JewerlyAndAccessoriesDal>();
        CreateMap<ConstructionAPI, ConstructionDal>();
        CreateMap<AllForHomeAndGardenAPI, AllForHomeAndGardenDal>();
        CreateMap<ProductsAndDrinksAPI, ProductsAndDrinksDal>();
        CreateMap<FurnitureAPI, FurnitureDal>();
        CreateMap<PetsAndPlantsAPI, PetsAndPlantsDal>();
        CreateMap<CultureAPI, CultureDal>();
        CreateMap<WorkAPI, WorkDal>();
        CreateMap<ServicesAPI, ServicesDal>();
        CreateMap<HealfCareAPI, HealfCareDal>();
        CreateMap<AcquaintanceAPI, AcquaintanceDal>();
        CreateMap<SportAPI, SportDal>();
        CreateMap<CigaretteAndAlcoholAPI, CigaretteAndAlcoholDal>();
        CreateMap<SaleAPI, SaleDal>();
        CreateMap<EverythingElseAPI, EverythingElseDal>();
        CreateMap<ElectronicsAPI, ElectronicsDal>();
        CreateMap<TourismAndRestAPI, TourismAndRestDal>();
        CreateMap<UserRateAPI, UserRateDal>();
        CreateMap<VehicleAPI, VehicleDal>();
        CreateMap<ExchangeAPI, ExchangeDal>();
        CreateMap<MainPageDal, MainPageAPI>();
        CreateMap<MinimizedAdDal, MinimizedAdAPI>();
        CreateMap<CategoryPartDal, CategoryPartAPI>();
        CreateMap<AdPartDal, AdPartAPI>();
        CreateMap<BestExchangeDal, BestExchangeAPI>();
        CreateMap<DAL.Models.BestExchangeList, Models.BestExchangeList>();
        //For Notifications API Create to UniversalDal
        CreateMap<VehicleAPI, UniversalDal>();
        CreateMap<RealEstateAPI, UniversalDal>();
        CreateMap<ApplianceAPI, UniversalDal>();
        CreateMap<HouseholdGoodAPI, UniversalDal>();
        CreateMap<ClothesAndShoesAPI, UniversalDal>();
        CreateMap<ForChildrenAPI, UniversalDal>();
        CreateMap<JewerlyAndAccessoriesAPI, UniversalDal>();
        CreateMap<ConstructionAPI, UniversalDal>();
        CreateMap<AllForHomeAndGardenAPI, UniversalDal>();
        CreateMap<ProductsAndDrinksAPI, UniversalDal>();
        CreateMap<FurnitureAPI, UniversalDal>();
        CreateMap<PetsAndPlantsAPI, UniversalDal>();
        CreateMap<CultureAPI, UniversalDal>();
        CreateMap<WorkAPI, UniversalDal>();
        CreateMap<ServicesAPI, UniversalDal>();
        CreateMap<HealfCareAPI, UniversalDal>();
        CreateMap<AcquaintanceAPI, UniversalDal>();
        CreateMap<SportAPI, UniversalDal>();
        CreateMap<CigaretteAndAlcoholAPI, UniversalDal>();
        CreateMap<EverythingElseAPI, UniversalDal>();
        CreateMap<ElectronicsAPI, UniversalDal>();
        CreateMap<TourismAndRestAPI, UniversalDal>();
        CreateMap<VehicleAPI, UniversalDal>();
        CreateMap<ExchangeAPI, UniversalDal>();
        //Payments
        CreateMap<TelcellPaymentModelAPI, TelcellPaymentModelDAL>();
        CreateMap<TelcellUserDAL, TelcellUserAPI>();
        //Tariff
        CreateMap<TariffDal, TariffAPI>();
        CreateMap<PurchaseTariffAPI, PurchaseTariffDal>();
        CreateMap<VipDataDal, TariffPaymentAPI>();
        CreateMap<PurchaseLoaderAdAPI, PurchaseLoaderAdDAL>();
        //Email
        CreateMap<EmailConfirmationModelAPI, EmailConfirmationModelDAL>();
        CreateMap<EmailConfirmationModelDAL, EmailConfirmationModelAPI>();
    }
}