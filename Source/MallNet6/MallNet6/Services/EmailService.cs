using System.Net;
using System.Net.Mail;
using MallNet6.Models;
using Microsoft.Rest;

namespace MallNet6.Services;

public class EmailService
{
    public static async Task<int> SendMail(EmailStruct email)
    {
        Random randomizer = new Random();
        int randomNumber = randomizer.Next(100000, 999999);
        using (MailMessage mail = new MailMessage())
        {
            mail.From = new MailAddress("mallsteam7@gmail.com");
            mail.To.Add(email.Email);
            mail.Subject = email.Subject;
            mail.Body = string.Format(email.Body, randomNumber);
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = true;

            using (SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587))
            {
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("mallsteam7@gmail.com", "Mall12345");
                smtp.EnableSsl = true;
                try
                {
                    await smtp.SendMailAsync(mail);
                }
                catch (Exception ex)
                {
                    throw new RestException(ex.Message);
                }

            }
        }
        return randomNumber;
    }
}