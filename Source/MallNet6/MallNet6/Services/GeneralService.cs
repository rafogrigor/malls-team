namespace MallNet6.Services;

public class GeneralService
{
    public async Task<List<string>> GetImageUrlByImageBase64(List<string> imagesBase, string categoryName = "Others")
    {
        List<string> linksList = new();
        try
        {
            foreach (string image in imagesBase)
            {
                string imageName = string.Format("{0}.jpg", Guid.NewGuid());
                string imagePath = Path.Combine(Directory.GetCurrentDirectory(), $@"wwwroot\Images\{categoryName}", imageName);
                byte[] ImageByte = System.Convert.FromBase64String(image);
                if (ImageByte.Length > 0)
                {
                    await File.WriteAllBytesAsync(imagePath, ImageByte);
                    imagePath = string.Format("https://api.malls.team/Images/{0}/{1}", categoryName, imageName);
                    linksList.Add(imagePath);
                }
            }
        }
        catch (Exception)
        {
            return new List<string>();
        }
        return linksList;
    }
    public bool DeleteImagesFromServer(List<string> imagesPaths)
    {
        try
        {
            foreach (string imagePath in imagesPaths)
            {
                if (imagePath.Length > 0)
                {
                    File.Delete(imagePath);
                }
            }
        }
        catch
        {
            return false;
        }
        return true;
    }
}