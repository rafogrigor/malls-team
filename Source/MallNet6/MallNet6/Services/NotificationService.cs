using System.Net;
using System.Text;
using MallNet6.Models;
using Newtonsoft.Json;

namespace MallNet6.Services;

public class NotificationService
    {
        public static async Task SendNotification(List<string> tokens)
        {
            if (tokens == null || tokens.Count.Equals(0))
            {
                return;
            }

            foreach (string token in tokens)
            {
                await Send(new NotificationBody() { Token = token, Body = "TestBody", Title = "TestTitle" });
            }
        }

        public static async Task Send(NotificationBody notification)
        {
            WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
            tRequest.Method = "post";
            //serverKey - Key from Firebase cloud messaging server  
            tRequest.Headers.Add(string.Format("Authorization: key={0}", "AAAADBrKFeo:APA91bFo4PaDpas1p3sJOh8AFo4TteuccZ6Y77f_0DAjqRvWhUnWMpzvM73nCBu4DAcurlRxdbwtrRjEUzz4oB38ZhAJBxMp8EV_02jpLUDtqf1S1GzVcFMavSjnVCCHavY7enOUHCfP"));
            //Sender Id - From firebase project setting  
            tRequest.Headers.Add(string.Format("Sender: id={0}", "51989059050"));
            tRequest.ContentType = "application/json";
            var payload = new
            {
                to = notification.Token,
                //to = "all",
                //"c0YWgtnnSKaDO3n8vPc3M1:APA91bFxaPxCuFvEO51Th3CXQXOv5T7-ZM7wf8E6yHOA3jIjaDoaLPQpWzZLEnldK4IMeAxHgys-tDB8vOu4CYoEOBRoaBTkQDr6zjfD9lTV_RNJuuXrrc39i-37gWJSg_qdYnomY1AJ",
                priority = "high",
                content_available = true,
                notification = new
                {
                    body = notification.Body,
                    title = notification.Title,
                    badge = 1
                },
                data = new
                {
                    key1 = "value1",
                    key2 = "value2"
                }
            };

            string postbody = JsonConvert.SerializeObject(payload).ToString();
            byte[] byteArray = Encoding.UTF8.GetBytes(postbody);
            tRequest.ContentLength = byteArray.Length;
            using Stream dataStream = await tRequest.GetRequestStreamAsync();
            dataStream.Write(byteArray, 0, byteArray.Length);
            using WebResponse tResponse = tRequest.GetResponseAsync().Result;
            using Stream dataStreamResponse = tResponse.GetResponseStream();
            if (dataStreamResponse != null)
            {
                using StreamReader tReader = new(dataStreamResponse);
                string sResponseFromServer = await tReader.ReadToEndAsync();
            }
        }
    }