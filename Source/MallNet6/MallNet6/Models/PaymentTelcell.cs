using System.ComponentModel.DataAnnotations;

namespace MallNet6.Models;

public class TelcellPaymentModelAPI
{
    [Required]
    public int UserID { get; set; }

    [Required]
    public DateTime PaymentDate { get; set; }

    [Required]
    public decimal PaymentAmount { get; set; }

    [Required]
    public long Receipt { get; set; }
}
public class TelcellUserAPI
{
    public int ID { get; set; }
    public string FirstName { get; set; }
}