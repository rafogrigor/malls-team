using System.ComponentModel.DataAnnotations;

namespace MallNet6.Models;

public class EmailStruct
    {
        public string Email { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
    }
    public class EmailConfirmationModelAPI
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public int ConfirmationCode { get; set; }
    }

    public class PreferenceAPI
    {
        [Required]
        public int UserID { get; set; }
        [Required]
        public string Tags { get; set; }
    }

    public class NotificationBody
    {
        [Required]
        public string Body { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Token { get; set; }
    }

    #region Search
    public class SearchGetAPI
    {
        public int CountryID { get; set; }
        public string TextAM { get; set; }
        public string TextEN { get; set; }
        public string TextRU { get; set; }
    }

    public class GetPromptAPI
    {
        public int CountryID { get; set; }
        public string Symbol { get; set; }
    }
    public class Prompts
    {
        public List<Search> Descriptions { get; set; }

        public Prompts()
        {
            Descriptions = new List<Search>();
        }
    }

    public class Search
    {
        public string SearchString { get; set; }
    }
    #endregion

    #region Location
    public class CityAPI
    {
        public int ID { get; set; }
        public string Country { get; set; }
        public int CountryID { get; set; }
    }

    public class RegionAPI
    {
        public int ID { get; set; }
        public string Country { get; set; }
        public int CountryID { get; set; }
    }

    public class CountryAPI
    {
        public int ID { get; set; }
        public string Country { get; set; }
    }

    public class CountryID
    {
        [Required]
        public int ID { get; set; }
    }

    public class RegionID
    {
        [Required]
        public int ID { get; set; }
    }
    #endregion

    #region Category
    public class CategoryId
    {
        public int Value { get; set; }
    }
    public class MainCategory
    {
        public int ID { get; set; }
        public string Category { get; set; }
    }
    public class SubCategory
    {
        public int ID { get; set; }
        public string Type { get; set; }
        public int BaseCategoryID { get; set; }
    }
    #endregion