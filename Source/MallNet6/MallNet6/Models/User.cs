using System.ComponentModel.DataAnnotations;
using DAL.Common;

namespace MallNet6.Models;

#region Create
    public class UserCreate
    {
        [Required]
        [Phone]
        public string PhoneNumber { get; set; }

        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(50)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [MinLength(8)]
        public string Password { get; set; }

        [Required]
        public int Country { get; set; }
    }
    #endregion
    #region Identity
    public class UserApp
    {
        [Required]
        public string UserID { get; set; }
        [Required]
        public string UserSecret { get; set; }
    }
    public class UserIdentity
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        [MinLength(6)]
        public string Password { get; set; }

        public string Token { get; set; }
    }
    #endregion
    #region Get
    public class UserResponse
    {
        public int ID { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public UserState State { get; set; }
        public int Country { get; set; }
        public int Region { get; set; }
        public int City { get; set; }
        public string Address { get; set; }
        public decimal Rating { get; set; }
        public decimal Balance { get; set; }
        public string Avatar { get; set; }
        public bool IsConfirmed { get; set; }
    }
    public class UserInfoGet
    {
        [Required]
        public int UserID { get; set; }
    }
    #endregion
    #region Delete
    public class UserResetPassword
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [MinLength(8)]
        public string Password { get; set; }
        [Required]
        public int ConfirmationCode { get; set; }
    }
    #endregion
    #region Update
    public class UserRateAPI
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public bool IsIncreased { get; set; }
    }
    public class UserUpdateData
    {
        [Required]
        public int ID { get; set; }
        [Phone]
        [Required]
        public string PhoneNumber { get; set; }
        [MinLength(8)]
        public string Password { get; set; }

        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }

        [Required]
        public int Country { get; set; }

        [Required]
        public int Region { get; set; }

        [Required]
        public int City { get; set; }

        [Required]
        [MaxLength(50)]
        public string Address { get; set; }

        public string Avatar { get; set; }
    }

    public class UserUpdatePassword
    {
        [Phone]
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        [MinLength(8)]
        public string OldPassword { get; set; }

        [Required]
        [MinLength(8)]
        public string NewPassword { get; set; }
    }
    #endregion
    #region "Other"
    public class User
    {
        public int ID { get; set; }
        [Required]
        [Phone]
        public string PhoneNumber { get; set; }

        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(50)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public UserState State { get; set; }

        [Required]
        public int Country { get; set; }
        public string SCountry { get; set; }

        [Required]
        public int Region { get; set; }
        public string SRegion { get; set; }

        [Required]
        public int City { get; set; }
        public string SCity { get; set; }

        [Required]
        [MaxLength(50)]
        public string Address { get; set; }

        [Required]
        public decimal Rating { get; set; }

        [Required]
        public decimal Balance { get; set; }

        [Required]
        public string Avatar { get; set; }
        public Result Result { get; set; }
    }

    #endregion