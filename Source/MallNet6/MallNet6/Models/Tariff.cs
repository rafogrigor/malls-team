using System.ComponentModel.DataAnnotations;

namespace MallNet6.Models;

public class TariffAPI
{
    public int ID { get; set; }
    public string Name { get; set; }
    public string Desc { get; set; }
    public decimal Price { get; set; }
    public decimal SalePercent { get; set; }
    public decimal SalePrice { get; set; }
    public int? ViewCount { get; set; }
    public int? AdCount { get; set; }
    public int? Period { get; set; }
}

public class PurchaseTariffAPI
{
    public int UserID { get; set; }
    public int TariffID { get; set; }
}

public class PurchaseLoaderAdAPI
{
    [Required]
    public int AdID { get; set; }
    [Required]
    public int UserID { get; set; }
    [Required]
    public int TariffID { get; set; }
}
public class TariffPaymentAPI
{
    public int ID { get; set; }
    public int UserID { get; set; }
    public int TariffID { get; set; }
    public DateTime PurchaseDate { get; set; }
    public DateTime TariffEndDate { get; set; }
    public bool IsActive { get; set; }
}