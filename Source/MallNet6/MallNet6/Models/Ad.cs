using System.ComponentModel.DataAnnotations;
using DAL.Common;

namespace MallNet6.Models;

#region Create
    public class AcquaintanceAPI : BaseAdAPI
    {
        #region "Main"
        //false male, true female
        [Required]
        public bool Gender { get; set; }
        [Required]
        public AcquaintanceAim AcquaintanceAim { get; set; }
        public int? Height { get; set; }
        public int? Weight { get; set; }
        [Required]
        public int Age { get; set; }
        #endregion
    }
    public class AllForHomeAndGardenAPI : BaseAdAPI
    {
        #region "Main"
        [Required]
        public ProductState ProductState { get; set; }
        #endregion
    }
    public class ApplianceAPI : BaseAdAPI
    {
        #region "Main"
        [Required]
        public ProductState ProductState { get; set; }
        #endregion
    }
    public class CigaretteAndAlcoholAPI : BaseAdAPI
    {
        #region "Main"
        public ProductState? ProductState { get; set; }
        public bool? IsLocal { get; set; }
        public byte? AlcoholVolume { get; set; }
        #endregion
    }
    public class ClothesAndShoesAPI : BaseAdAPI
    {
        #region "Main"
        [Required]
        public ProductState ProductState { get; set; }
        public string ClothingSize { get; set; }
        public sbyte? ShoesSize { get; set; }
        #endregion
    }
    public class ConstructionAPI : BaseAdAPI
    {
        #region "Main"
        [Required]
        public ProductState ProductState { get; set; }
        #endregion
    }
    public class CultureAPI : BaseAdAPI
    {
        #region "Main"
        [Required]
        public ProductState ProductState { get; set; }
        #endregion
    }
    public class ElectronicsAPI : BaseAdAPI
    {
        #region "Main"
        [Required]
        public ProductState ProductState { get; set; }
        #endregion

    }
    public class EverythingElseAPI : BaseAdAPI
    {
    }
    public class ForChildrenAPI : BaseAdAPI
    {
        #region "Main"
        [Required]
        public ProductState ProductState { get; set; }
        public bool? Gender { get; set; }
        public bool? ForNewBorns { get; set; }

        #endregion
    }
    public class FurnitureAPI : BaseAdAPI
    {
        #region "Main"
        [Required]
        public ProductState ProductState { get; set; }
        #endregion
    }
    public class HealfCareAPI : BaseAdAPI
    {
        #region "Main"
        [Required]
        public ProductState ProductState { get; set; }
        #endregion
    }
    public class HouseholdGoodAPI : BaseAdAPI
    {
        #region "Main"
        [Required]
        public ProductState ProductState { get; set; }
        #endregion
    }
    public class JewerlyAndAccessoriesAPI : BaseAdAPI
    {
        #region "Main"
        [Required]
        public ProductState ProductState { get; set; }
        public bool? Gender { get; set; }

        #endregion
    }
    public class PetsAndPlantsAPI : BaseAdAPI
    {
        #region "Main"
        [Required]
        public ProductState ProductState { get; set; }
        #endregion
    }
    public class ProductsAndDrinksAPI : BaseAdAPI
    {
        #region "Main"
        public bool? IsLocal { get; set; }
        public bool? IsFrozen { get; set; }
        public bool? IsNaturalDrink { get; set; }
        #endregion
    }
    public class RealEstateAPI : BaseAdAPI
    {
        #region "Main"
        [Required]
        public short Space { get; set; }
        public ConstructionType? ConstructionType { get; set; }
        public AdPaymentTime? PaymentTime { get; set; }
        public sbyte? Rooms { get; set; }
        public sbyte? Floor { get; set; }
        #endregion
    }
    public class ServicesAPI : BaseAdAPI
    {
        #region "Main"
        public AdPaymentTime? PaymentTime { get; set; }
        //false - passenger, true - cargo
        public bool? Transportation { get; set; }
        #endregion
    }
    public class SportAPI : BaseAdAPI
    {
        #region "Main"
        [Required]
        public ProductState ProductState { get; set; }
        #endregion
    }
    public class SaleAPI : BaseAdAPI
    {

    }
    public class TourismAndRestAPI : BaseAdAPI
    {
        #region "Main"
        public DateTime? DepartureDay { get; set; }
        public DateTime? ReturnDay { get; set; }
        public short? ReservedTickets { get; set; }
        #endregion
    }
    public class VehicleAPI : BaseAdAPI
    {
        public int? Mark { get; set; }
        public string SMark { get; set; }

        public int? Model { get; set; }
        public string SModel { get; set; }

        public DateTime? ProductionYear { get; set; }
        public bool? CustomsCleared { get; set; }
        public int? Mileage { get; set; }
        public int? EngineSize { get; set; }
        public decimal? DEngineSize { get; set; }

        public int? BodyType { get; set; }
        public string SBodyType { get; set; }

        public int? EngineType { get; set; }
        public string SEngineType { get; set; }

        public int? DriveType { get; set; }
        public string SDriveType { get; set; }

        public int? TransmissionType { get; set; }
        public string STransmissionType { get; set; }

        public int? Color { get; set; }
        public string SColor { get; set; }

        public int? Wheel { get; set; }
        public string SWheel { get; set; }
    }
    public class WorkAPI : BaseAdAPI
    {
        #region "Main"
        public AdPaymentTime? PaymentTime { get; set; }
        #endregion
    }

    #endregion
    #region Other
    public class SearchModelAPI
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public string Name { get; set; }
        public int CategoryID { get; set; }
        public string Description { get; set; }
        public AdOwner Owner { get; set; }
        public string Image { get; set; }

    }
    public class AdCreateResponse
    {
        public int ID { get; set; }

        public Result Result { get; set; }
    }

    public class FavoriteAds
    {
        public List<MinimizedAdAPI> FavoriteList { get; set; }
        public Result Result { get; set; }
        public FavoriteAds()
        {
            FavoriteList = new List<MinimizedAdAPI>();
        }
    }

    public class UserAds
    {
        public List<MinimizedAdAPI> AdList { get; set; }
        public Result Result { get; set; }
        public UserAds()
        {
            AdList = new List<MinimizedAdAPI>();
        }
    }

    public class LoaderAdAPI
    {
        public int AdID { get; set; }
        public int UserID { get; set; }
        public int MainCategoryID { get; set; }
        public string MainCategoryName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> Images { get; set; }

        public LoaderAdAPI()
        {
            Result = new Result();
        }

        public Result Result { get; set; }
    }

    #endregion
    #region Get
    public class AdGetBase
    {
        public int? ID { get; set; }
        public uint? PageNumber { get; set; }
        public uint? AdCount { get; set; }
        public int? SubCategoryID { get; set; }
        public int? UserID { get; set; }
        public int? Country { get; set; }
        public int? Region { get; set; }
        public int? City { get; set; }
        public bool OnlyWithPhotos { get; set; } = false;
        public Aim? Aim { get; set; }
        public AdOwner? AdOwner { get; set; }
        public Currency? Currency { get; set; }
        public decimal? SalePriceFrom { get; set; }
        public decimal? SalePriceTo { get; set; }
        public decimal? SalePercentTo { get; set; }
        public decimal? SalePercentFrom { get; set; }
        public ProductState? ProductState { get; set; }
    }
    public class WorkGetAPI : AdGetBase
    {
        public AdPaymentTime? AdPaymentTime { get; set; }
    }
    public class SportGetAPI : AdGetBase
    {

    }
    public class SaleGetAPI : AdGetBase
    {

    }
    public class EverythingElseGetAPI : AdGetBase
    {

    }
    public class PetsAndPlantsGetAPI : AdGetBase
    {

    }
    public class CultureGetAPI : AdGetBase
    {

    }
    public class HealfCareGetAPI : AdGetBase
    {

    }
    public class FurnitureGetAPI : AdGetBase
    {

    }
    public class ForHomeAndGardenGetAPI : AdGetBase
    {

    }
    public class HouseholdGoodGetAPI : AdGetBase
    {

    }
    public class ConstructionGetAPI : AdGetBase
    {

    }
    public class ElectronicGetAPI : AdGetBase
    {

    }
    public class ApplianceGetAPI : AdGetBase
    {

    }
    public class AcquaintanceGetAPI : AdGetBase
    {
        public bool? Gender { get; set; }
        public AcquaintanceAim? AcquintanceAim { get; set; }
        public int? Height { get; set; }
        public int? Weight { get; set; }
        public int? AgeFrom { get; set; }
        public int? AgeTo { get; set; }
    }
    public class RealEstateGetAPI : AdGetBase
    {
        public ConstructionType? ConstructionType { get; set; }
        public byte? PaymentTime { get; set; }
        public byte? Rooms { get; set; }
        public short? SpaceFrom { get; set; }
        public short? SpaceTo { get; set; }
        public byte? FloorFrom { get; set; }
        public byte? FloorTo { get; set; }
    }
    public class VehicleGetAPI : AdGetBase
    {
        public int? Mark { get; set; }
        public int? Model { get; set; }
        public DateTime? ProductionYearFrom { get; set; }
        public DateTime? ProductionYearTo { get; set; }
        public int? MileageFrom { get; set; }
        public int? MileageTo { get; set; }
        public bool? CustomsCleared { get; set; }
        public int? BodyType { get; set; }
        public int? EngineType { get; set; }
        public int? EngineSizeFrom { get; set; }
        public int? EngineSizeTo { get; set; }
        public int? DriveType { get; set; }
        public int? Transmisionype { get; set; }
        public int? Color { get; set; }
        public int? Wheel { get; set; }
    }
    public class ClothesAndShoesGetAPI : AdGetBase
    {
        public string ClothingSize { get; set; }
        public byte? ShoesSize { get; set; }
    }
    public class ForChildrenGetAPI : AdGetBase
    {
        public bool? Gender { get; set; }
        public bool? ForNewBorns { get; set; }
    }
    public class JewerlyAndAccessoriesGetAPI : AdGetBase
    {
        public bool? Gender { get; set; }
    }
    public class ProductsAndDrinksGetAPI : AdGetBase
    {
        public bool? IsLocal { get; set; }
        public bool? IsFrozen { get; set; }
        public bool? IsNaturalDrink { get; set; }
    }
    public class CigaretteAndAlcoholGetAPI : AdGetBase
    {
        public bool? IsLocal { get; set; }
        public byte? AlcoholVolume { get; set; }
    }
    public class TourismAndRestGetAPI : AdGetBase
    {
        public DateTime? DepartureDay { get; set; }
        public DateTime? ReturnDay { get; set; }
        public short? ReservedTickets { get; set; }

    }
    public class ServicesGetAPI : AdGetBase
    {
        public byte? PaymentTime { get; set; }
        public bool? Transportation { get; set; }
    }
    public class ExchangeGetAPI : AdGetBase
    {

    }

    #endregion
    #region Universal
    public class BaseAdAPI
    {
        public int ID { get; set; }
        [Required]
        public int UserID { get; set; }
        [Required]
        public int SubCategoryID { get; set; }
        public int MainCategoryID { get; set; }
        [Required]
        public Currency Currency { get; set; }
        [Required]
        public decimal Price { get; set; }
        public decimal SalePercent { get; set; }
        public decimal SalePrice { get; set; }
        public string CountryName { get; set; }
        public string RegionName { get; set; }
        public string CityName { get; set; }
        [Required]
        public int Country { get; set; }
        [Required]
        public int Region { get; set; }
        [Required]
        public int City { get; set; }
        [Required]
        public AdState State { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public decimal? LocationLatitude { get; set; }

        public decimal? LocationLongitude { get; set; }

        public string Contact { get; set; }

        [Required]
        public Aim Aim { get; set; }

        public AdOwner? Owner { get; set; }
        public decimal View { get; set; }
        public string Tags { get; set; }
        public bool? IsRegional { get; set; }
        public bool IsFavorite { get; set; }
        public bool IsBestPrice { get; set; }
        public List<string> ImagesList { get; set; }

        public BaseAdAPI()
        {
            ImagesList = new List<string>();
        }
    }
    public class UniversalAPI
    {
        [Required]
        public int ID { get; set; }
        [Required]
        public AdType AdType { get; set; }
        public AdState? State { get; set; }
        [Required]
        public int UserID { get; set; }
        public AcquaintanceAim? AcquaintanceAim { get; set; }
        public int? SubCategoryID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? LocationLatitude { get; set; }
        public decimal? LocationLongitude { get; set; }
        public decimal? Price { get; set; }
        public decimal? SalePercent { get; set; }
        public string Contact { get; set; }
        public int? Country { get; set; }
        public int? Region { get; set; }
        public int? City { get; set; }
        public AdOwner? Owner { get; set; }
        public Currency? Currency { get; set; }
        public string Tags { get; set; }
        public bool? IsRegional { get; set; }
        public Aim? Aim { get; set; }
        public ProductState? ProductState { get; set; }
        public bool? IsLocal { get; set; }
        public byte? AlcoholVolume { get; set; }
        public string ClothingSize { get; set; }
        public sbyte? ShoesSize { get; set; }
        public int? Height { get; set; }
        public int? Weight { get; set; }
        public int? Age { get; set; }
        public bool? Gender { get; set; }
        public bool? ForNewBorns { get; set; }
        public bool? IsFrozen { get; set; } = null;
        public bool? IsNaturalDrink { get; set; } = null;
        public ConstructionType? ConstructionType { get; set; }
        public AdPaymentTime? PaymentTime { get; set; }
        public sbyte? Rooms { get; set; }
        public short? Space { get; set; }
        public sbyte? Floor { get; set; }
        //false - passenger, true - cargo
        public bool? Transportation { get; set; }
        public DateTime? DepartureDay { get; set; }
        public DateTime? ReturnDay { get; set; }
        public short? ReservedTickets { get; set; }
        public int? Mark { get; set; }
        public int? Model { get; set; }
        public DateTime? ProductionYear { get; set; }
        public bool? CustomsCleared { get; set; }
        public int? Mileage { get; set; }
        public int? BodyType { get; set; }
        public int? EngineType { get; set; }
        public int? EngineSize { get; set; }
        public int? DriveType { get; set; }
        public int? TransmissionType { get; set; }
        public int? Color { get; set; }
        public int? Wheel { get; set; }
        public short? SourceCurrencyID { get; set; }
        public short? DestinationCurrencyID { get; set; }
        public decimal? SaleSummaRetail { get; set; }
        public decimal? BuySummaRetail { get; set; }
        public decimal? SaleSumma { get; set; }
        public decimal? BuySumma { get; set; }
    }
    #endregion
    #region Update
    public class ImagesUpdateAPI
    {
        [Required]
        public int AdID { get; set; }
        [Required]
        public string SubCategory { get; set; }
        [Required]
        public List<string> NewImagesList { get; set; }
        public List<string> OldImagesLinks { get; set; }
        public ImagesUpdateAPI()
        {
            NewImagesList = new List<string>();
            OldImagesLinks = new List<string>();
        }
    }
    #endregion
    #region Main Page
    public class MainPageAPI
    {
        public BestExchangeAPI BestExchange { get; set; }

        public List<MinimizedAdAPI> AdsWithCategories { get; set; }

        public MainPageAPI()
        {
            AdsWithCategories = new List<MinimizedAdAPI>();
        }
    }
    public class BestExchangeAPI
    {
        public decimal Sale { get; set; }
        public decimal Buy { get; set; }
        public string Symbol { get; set; }
        public int UserID { get; set; }
        public int AdID { get; set; }
        public bool IsBestPrice { get; set; }
    }
    #endregion
    #region Categories
    public class MinimizedAdAPI
    {
        public CategoryPartAPI Category { get; set; }

        public List<AdPartAPI> Ads { get; set; }

        public MinimizedAdAPI()
        {
            Ads = new List<AdPartAPI>();
        }
    }

    public class AdPartAPI
    {
        public int ID { get; set; }
        public AdState State { get; set; }
        public string Name { get; set; }
        public int UserID { get; set; }
        public int CategoryID { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public byte Owner { get; set; }
        public bool IsBestPrice { get; set; }
    }

    public class CategoryPartAPI
    {
        public int ID { get; set; }

        public string Name { get; set; }
    }
    #endregion
    #region Deletion
    //public class DeleteAD
    //{
    //    public int AdId { get; set; }
    //    public UserIdentity User { get; set; }
    //    public DeleteAD()
    //    {
    //        User = new UserIdentity();
    //    }
    //}
    #endregion
    #region Exchanges
    public class ExchangeAPI : BaseAdAPI
    {
        #region "Main"
        [Required]
        public short SourceCurrencyID { get; set; }
        [Required]
        public short DestinationCurrencyID { get; set; }
        [Required]
        public decimal SaleSummaRetail { get; set; }
        [Required]
        public decimal BuySummaRetail { get; set; }
        public decimal? SaleSumma { get; set; }
        public decimal? BuySumma { get; set; }
        #endregion
    }

    public class BestExchangeList
    {
        public List<ExchangeAPI> List { get; set; }
        public Result Result { get; set; }

        public BestExchangeList()
        {
            List = new List<ExchangeAPI>();
        }
    }
    public class CurrencyAPI
    {
        [Required]
        public short ID { get; set; }
        [Required]
        public string Currency { get; set; }
        [Required]
        public string CurrencySymbol { get; set; }
    }
    #endregion
    #region Vehicle
    public class EngineSizeAPI
    {
        public int ID { get; set; }
        public decimal Size { get; set; }
    }
    public class BodyTypeAPI
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class ColorAPI
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class DriveTypeAPI
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class MarkAPI
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class MileageAPI
    {
        public int ID { get; set; }
        public int Mileage { get; set; }
    }
    public class ModelAPI
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int CarTypeID { get; set; }
    }
    public class TransmissionTypeAPI
    {
        public int ID { get; set; }
        public string TransmissionType { get; set; }
    }

    public class EngineTypeAPI
    {
        public int ID { get; set; }
        public string EngineType { get; set; }
    }

    public class WheelAPI
    {
        public int ID { get; set; }
        public string Wheel { get; set; }
    }
    #endregion