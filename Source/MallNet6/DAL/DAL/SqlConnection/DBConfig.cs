﻿namespace DAL.SqlConnection
{
    public class DBConfig
    {
        public string Server { get; set; }
        public string Database { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
