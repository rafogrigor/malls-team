namespace DAL.SqlConnection;

using Microsoft.Extensions.Options;
using System.Data.SqlClient;
public class Manager
{
    private static string ConnectionString = null;
    public readonly DBConfig config;
    public Manager(IOptions<DBConfig> config)
    {
        this.config = config.Value;
    }
    public void InitConnection()
    {
        SqlConnectionStringBuilder bld = new SqlConnectionStringBuilder
        {
            DataSource = config.Server,
            InitialCatalog = config.Database,
            IntegratedSecurity = false,
            UserID = config.Login,
            Password = config.Password,
            //DataSource = @"DESKTOP-3V2QKMD\SQLEXPRESS",
            //InitialCatalog = "AdminBase",
            //IntegratedSecurity = true,
            ConnectTimeout = 30,
            MultipleActiveResultSets = true
        };
        ConnectionString = bld.ConnectionString;
    }


    //Արժե  ջնջել
    public static void CheckConnection()
    {
        using (SqlConnection conn = new SqlConnection(ConnectionString))
        {
            conn.Open();
        }
    }
    public SqlConnection CreateConnection()
    {
        if (string.IsNullOrWhiteSpace(ConnectionString))
        {
            InitConnection();
        }

        var connection = new SqlConnection(ConnectionString);
        connection.Open();
        return connection;
    }
}