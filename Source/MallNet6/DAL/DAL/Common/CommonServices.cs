using System.Data.SqlClient;
using System.Security;
using DAL.Models;

namespace DAL.Common;

public class CommonServices
{
    public static BaseAdDal CreateBasePart(SqlDataReader reader)
        {
            int ord;
            BaseAdDal ad = new BaseAdDal()
            {
                ID = reader.GetInt32(reader.GetOrdinal("ID")),
                UserID = reader.GetInt32(reader.GetOrdinal("UserId")),
                SubCategoryID = reader.GetInt32(reader.GetOrdinal("SubCategoryId")),
                Description = reader.GetString(reader.GetOrdinal("Description")),
                CreationDate = reader.GetDateTime(reader.GetOrdinal("Date")),
                UpdateDate = reader.GetDateTime(reader.GetOrdinal("UpdateDate")),
                LocationLongitude = reader.GetDecimal(reader.GetOrdinal("Location")),
                LocationLatitude = reader.GetDecimal(reader.GetOrdinal("Location")),
                State = (AdState)reader.GetByte(reader.GetOrdinal("State")),
                Contact = reader.GetString(reader.GetOrdinal("Contact")),
                Owner = (AdOwner)reader.GetByte(reader.GetOrdinal("Owner")),
                Country = reader.GetInt32(reader.GetOrdinal("Country")),
                Region = reader.GetInt32(reader.GetOrdinal("Region")),
                City = reader.GetInt32(reader.GetOrdinal("City")),
                Currency = (Currency)reader.GetByte(reader.GetOrdinal("Currency")),
                ImagesList = reader.GetString(reader.GetOrdinal("Images")).Split(' ').ToList()
            };
            ord = reader.GetOrdinal("Price");
            if (!reader.IsDBNull(ord))
            {
                ad.Price = reader.GetDecimal(ord);
            }

            ord = reader.GetOrdinal("Views");
            if (!reader.IsDBNull(ord))
            {
                ad.View = reader.GetInt32(ord);
            }

            return ad;
        }
        #region "Security"
        public static SecureString ConvertToSecureString(string strPassword)
        {
            SecureString secureStr = new SecureString();
            if (strPassword.Length > 0)
            {
                foreach (char c in strPassword.ToCharArray())
                {
                    secureStr.AppendChar(c);
                }
            }
            return secureStr;
        }

        #endregion
}