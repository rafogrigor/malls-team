namespace DAL.Common;

public class Result
{
    public int? Status { get; set; }
    public string Message { get; set; }
}