namespace DAL.Common;

public sealed partial class CommonConstants
{
    #region "Numeric Constants"
        public const int BadID = -1;
        public const int GeoNum = 4326;
        public const decimal RatingIncreaseStep = 0.1M;
        public const int SQLException = 2;
        public const int OtherException = 9;
        public const int Ok = 1;
        public const int UserNotExistsException = 3;
        public const int PurchLoaderAdError = 8;
        public const int NoLoaderAd = 10;
        public const int ViewUpdateErr = 11;
        public const int NoContent = 401;
        public const int AdNotExists = 402;
        public const int InvalidModel = 403;
        public const int NoEnoughMoney = 404;
        public const int SQLCommandCreatingError = 405;
        public const int ImageAddingError = 406;
        #endregion
        #region "String Constants"
        #region "Program"
        public const string ProgramName = "MyListTrade";
        public const string OwnerMail = "mytestforbackend@gmail.com";
        public const string OwnerP = "rafogrigor";
        public const string SQLConnectionError = "SQL connection error";
        public const string OkMessage = "Ok";
        public const string SQLOtherError = "SQL other error";
        public const string UserNotExistsError = "User not exists in DB";
        public const string PurchLoaderAdMessageError = "Loader Ad purchasing time error";
        public const string NoLoaderAdMessage = "Loader ads not exists in database";
        public const string ViewUpdateErrMessage = "Views updateing time error";
        public const string AdNotExistsMessage = "Ad not exists";
        public const string InvalidModelMessage = "Invalid model";
        public const string NoEnoughMoneyMessage = "No enough money";
        public const string SQLCommandCreatingErrorMessage = "SQL command creating error";
        public const string ImageAddingErrorMessage = "Image adding time error";
        #endregion
        #region "User"
        public const string Login = "Login";
        public const string SignUp = "SignUp";
        public const string UserByPhoneNumber = "GetUserByPhoneNumber";
        public const string Authorization = "Authorization";
        public const string Confirm = "Confirm";
        public const string EmailConfirmation = "Email confirmation";
        public const string Email = "Email";
        public const string UserRegError = "User registration error";
        public const string ForgotPassword = "Forgot password";
        public const string UserUpdateError = "User Update Error";
        public const string UserDeleteError = "User delete error";
        public const string SelectBaseAdModelFields = @"Select Ads.[ID],[UserId],[SubCategoryId],[Description],[Name],[Date],[UpdateDate],[Location].Lat As Latitude,[Location].Long As Longitude
                                                        [Price],[State],[Contact],[Views],[Owner],[Country],[Region],[City],[Currency],[Comment]
                                                        [Tags],[Images],[IsBestPrice],[IsRegional]";
        public const string NoContentMessage = "No content : ";
        #endregion

        #endregion
}