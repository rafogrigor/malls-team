namespace DAL.Common;
using System;
 
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("DAL.Common.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ad creating time error: Ad wasn&apos;t created.
        /// </summary>
        public static string AdCreatingError {
            get {
                return ResourceManager.GetString("AdCreatingError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User was deactivated..
        /// </summary>
        public static string DeactivatedUser {
            get {
                return ResourceManager.GetString("DeactivatedUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error Adding Images.
        /// </summary>
        public static string ErrorAddingImages {
            get {
                return ResourceManager.GetString("ErrorAddingImages", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your forgot password code is {0}.
        /// </summary>
        public static string ForgotPasswordMail {
            get {
                return ResourceManager.GetString("ForgotPasswordMail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GetAddWithId.
        /// </summary>
        public static string GetAddWithId {
            get {
                return ResourceManager.GetString("GetAddWithId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GetPaymentWithID.
        /// </summary>
        public static string GetPaymentWithID {
            get {
                return ResourceManager.GetString("GetPaymentWithID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid ad ID.
        /// </summary>
        public static string InvalidAdID {
            get {
                return ResourceManager.GetString("InvalidAdID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid email format..
        /// </summary>
        public static string InvalidEmail {
            get {
                return ResourceManager.GetString("InvalidEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User with phone {0} can&apos;t do this operation.
        /// </summary>
        public static string NotHaveEnoughPermissions {
            get {
                return ResourceManager.GetString("NotHaveEnoughPermissions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An error occured.
        /// </summary>
        public static string OcuredError {
            get {
                return ResourceManager.GetString("OcuredError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Payment creating time error: Payment wasn&apos;t created.
        /// </summary>
        public static string PaymentCreatingError {
            get {
                return ResourceManager.GetString("PaymentCreatingError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Welcome dear user: Your confirmation code is {0}.
        /// </summary>
        public static string SendConfirmMail {
            get {
                return ResourceManager.GetString("SendConfirmMail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User wasn&apos;t confirmed..
        /// </summary>
        public static string UnconfirmedUser {
            get {
                return ResourceManager.GetString("UnconfirmedUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User just confirmed..
        /// </summary>
        public static string UserJustConfirmed {
            get {
                return ResourceManager.GetString("UserJustConfirmed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User wasn&apos;t created..
        /// </summary>
        public static string UserNotCreated {
            get {
                return ResourceManager.GetString("UserNotCreated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User with email {0} not found..
        /// </summary>
        public static string UserNotFound {
            get {
                return ResourceManager.GetString("UserNotFound", resourceCulture);
            }
        }
    }