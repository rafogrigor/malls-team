namespace DAL.Common;

public enum UserState : byte
    {
        User = 0,                   //Սովորական
        VIPUser = 1,                //Վիփ
        Admin = 100,                //Ադմին
        DeactivatedUser = 255,      //Դեակտիվացված
    }
    public enum CultureMode : byte
    {
        AM = 0,                     //Հայերեն
        RU = 1,                     //Ռուսերեն
        EN = 2,                     //Անգլերեն
    }
    public enum AdState : byte
    {
        Normal = 0,                 //Ստեղծված հայտարարություն
        Urgent = 1,                 //Շտապ հայտարարություն
        Vip = 2,                    //Վիպ հայտարարություն
        NotVerified = 100,          //Ստեղծված Ոչ հաստատված հայտարարություն
        Blocked = 255,              //Բլոկավորված հայտարարություն
    }
    public enum AdOwner : byte
    {
        Individual = 0,              //Անհատական
        Agency = 1,                  //Գործակալություն
        Diller = 2                   //Դիլլեր
    }

    public enum AdType : byte
    {
        Sale = 1,                    //Զեղչ             
        RealEstate = 2,              //Անշարժ գույք
        Vehicle = 3,                 //Տրանսպորտ
        Electronics = 4,             //Էլեկտրոնիկա
        Appliances = 5,              //Կենցաղային տեխնիկա
        HouseholdGoods = 6,          //Տնտեսական ապրանքներ
        ClothesAndShoes = 7,         //Հագուստ և կոշիկներ
        ForChildren = 8,            //Երեխաների համար 
        JewerlyAndAccessories = 9,  //Զարդեր և Աքսեսուարներ
        Construction = 10,           //Շինարարություն
        AllForHomeAndGarden = 11,    //Ամեն ինչ տան և այգու համար
        ProductsAndDrinks = 12,      //Մթերք և ըմպելիքներ   
        CigaretteAndAlcohol = 13,    //Ծխախոտ և ալկոհոլ         
        Furniture = 14,              //Կահույք
        TourismAndRest = 15,         //Տուրիզմ և հանգիստ
        Sport = 16,                  //Սպորտ
        PetsAndPlants = 17,          //Կենդանիներ և բույսեր
        Culture = 18,                //Մշակույթ
        Work = 19,                   //Աշխատանք
        Services = 20,               //Ծառայություններ
        HealfCare = 21,              //Առողջապահություն
        EverythingElse = 22,         //Այլ
        Acquaintance = 23,           //Ծանոթություններ
        Exchange = 24,               //Տարադրամ
    }

    public enum Aim : byte
    {
        ForSale = 0,                   //Վաճառք
        ForRent = 1,                   //Վարձակալություն
        Exchange = 2,                  //Փոխանակում
        Buy = 3,                       //Առք
        Search = 4                     //Փնտրել
    }

    public enum Currency : byte
    {
        AMD = 0,                     //Դրամ
        RUR = 1,                     //Ռուբլի
        USD = 2                      //Դոլար  
    }

    public enum ConstructionType : byte
    {
        Stone = 0,                   //Քարե
        Panels = 1,                  //Պանելային
        Monolith = 2,                //Մոնոլիտ
        Bricks = 3,                  //Աղյուսե
        Cassette = 4,                //Կասետային
        Wooden = 5,                  //Փայտե
    }

    public enum ProductState : byte
    {
        New = 0,                     //Նոր
        Used = 1                     //Օգտագործված
    }

    public enum AcquaintanceAim : byte
    {
        Friendship = 0,              //Ընկերություն
        Marriage = 1,                //Ամուսնություն
        Entertainment = 2            //Ժամանց
    }
    public enum AdPaymentTime : byte
    {
        Monthly = 0,                 //Ամսավճար
        Daily = 1                    //Օրավճար
    }