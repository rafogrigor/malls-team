using DAL.Common;
using DAL.Models;

namespace DAL.Interfaces;

public interface ILocation
{
    Task<List<CountryDal>> GetCountries(CultureMode culture, int id = -1);
    Task<List<RegionDal>> GetRegions(CultureMode culture, int countryID);
    Task<List<CityDal>> GetCities(CultureMode culture, int regionID);
}