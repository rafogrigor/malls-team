using DAL.Models;

namespace DAL.Interfaces;

public interface IPayments
{
    Task<int> CreatePayment(TelcellPaymentModelDAL paymentModel);
    Task<bool> CheckPayment(long receipt);
    Task<TelcellUserDAL> GetUserPaymentModelByID(int userID);
}