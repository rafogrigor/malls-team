using DAL.Common;
using DAL.Models;

namespace DAL.Interfaces;

public interface IVehicle
{
    Task<List<MileageDal>> GetMileage();
    Task<List<MarkDal>> GetMarks();
    Task<List<ModelDal>> GetModels(int markID);
    Task<List<EngineSizeDal>> GetEngineSize();
    Task<List<ColorDal>> GetColor(CultureMode currentCulture);
    Task<List<BodyTypeDal>> GetBodyType(CultureMode currentCulture);
    Task<List<TransmissionTypeDAL>> GetTransmissionType(CultureMode currentCulture);
    Task<List<DriveTypeDal>> GetDriveType(CultureMode currentCulture);
    Task<List<EngineTypeDAL>> GetEngineType(CultureMode currentCulture);
    Task<List<WheelDAL>> GetWheelType(CultureMode currentCulture);
}