using DAL.Common;
using DAL.Models;

namespace DAL.Interfaces;

public interface ICategories
{
    Task<List<MainCategoryDal>> GetMainCategoriesAsync(CultureMode culture, int categoryID = -1);
    Task<List<SubCategoryDal>> GetSubCategoriesAsync(CultureMode culture, int baseCategoryID = -1);
}