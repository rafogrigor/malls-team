using DAL.Common;
using DAL.Models;

namespace DAL.Interfaces;

public interface ITariff
{
    Task<List<TariffDal>> GetTariffs(CultureMode currentCulture);
    Task<Result> AddLoaderAd(PurchaseLoaderAdDAL data);
}