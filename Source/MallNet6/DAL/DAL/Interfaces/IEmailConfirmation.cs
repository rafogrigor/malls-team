using DAL.Models;

namespace DAL.Interfaces;

public interface IEmailConfirmation
{
    Task<bool> AddEmailConfirmationCodeToTable(EmailConfirmationModelDAL emailConfirmationModel);
}