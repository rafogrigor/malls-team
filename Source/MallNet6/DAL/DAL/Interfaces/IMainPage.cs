using DAL.Common;
using DAL.Models;

namespace DAL.Interfaces;

public interface IMainPage
{
    Task<MainPageDal> GetMainPage(int countryID, CultureMode culture, Currency favCurrency);
    Task<List<string>> GetPrompts(GetPromptDal symbols);
    Task<List<SearchModelDal>> Search(SearchGetDal symbolsl);
}