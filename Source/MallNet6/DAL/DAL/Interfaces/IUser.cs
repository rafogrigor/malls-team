using DAL.Models;
using Microsoft.AspNetCore.Identity;

namespace DAL.Interfaces;

public interface IUser
{
    Task<IdentityResult> EmailConfirmAsync(string email);
    Task<bool> RateUserAsync(UserRateDal userRateDal);
    Task CheckExistanceAndSaveToken(int userID, string token);
    Task<bool> AddPreferences(PreferenceDal preference);
    Task<SupportMessage> GetSupportMessage(int userID);
}