using DAL.Common;
using DAL.Models;

namespace DAL.Interfaces;

public interface IAds
{
        #region Create Ad
        Task<AdCreateResponse> CreateAd(AcquaintanceDal adModel);
        Task<AdCreateResponse> CreateAd(AllForHomeAndGardenDal adModel);
        Task<AdCreateResponse> CreateAd(ApplianceDal adModel);
        Task<AdCreateResponse> CreateAd(CigaretteAndAlcoholDal adModel);
        Task<AdCreateResponse> CreateAd(ClothesAndShoesDal adModel);
        Task<AdCreateResponse> CreateAd(ConstructionDal adModel);
        Task<AdCreateResponse> CreateAd(CultureDal adModel);
        Task<AdCreateResponse> CreateAd(ElectronicsDal adModel);
        Task<AdCreateResponse> CreateAd(EverythingElseDal adModel);
        Task<AdCreateResponse> CreateAd(ForChildrenDal adModel);
        Task<AdCreateResponse> CreateAd(FurnitureDal adModel);
        Task<AdCreateResponse> CreateAd(HealfCareDal adModel);
        Task<AdCreateResponse> CreateAd(HouseholdGoodDal adModel);
        Task<AdCreateResponse> CreateAd(ExchangeDal adModel);
        Task<AdCreateResponse> CreateAd(JewerlyAndAccessoriesDal adModel);
        Task<AdCreateResponse> CreateAd(PetsAndPlantsDal adModel);
        Task<AdCreateResponse> CreateAd(ProductsAndDrinksDal adModel);
        Task<AdCreateResponse> CreateAd(RealEstateDal adModel);
        Task<AdCreateResponse> CreateAd(ServicesDal adModel);
        Task<AdCreateResponse> CreateAd(TourismAndRestDal adModel);
        Task<AdCreateResponse> CreateAd(VehicleDal adModel);
        Task<AdCreateResponse> CreateAd(WorkDal adModel);
        Task<AdCreateResponse> CreateAd(SportDal adModel);
        #endregion

        #region "GetingData"
        Task<List<CurrencyDal>> GetCurrencyList();

        Task<BestExchangeList> GetBestExchanges(CultureMode culture);
        #endregion

        #region "Other"
        Task<Result> DeleteAd(int adID);
        #endregion

        #region "FavoriteAds"
        Task<Result> DeleteAdAtFavorites(FavoriteAdModel favAd);
        Task<Result> AddAdToFavorites(FavoriteAdModel favAd);
        Task<FavoriteAds> GetUserFavoriteAds(int userID, CultureMode culture);
        Task<UserAds> GetUserAds(int userID, CultureMode culture);
        #endregion

        #region "GettingAds"
        Task<List<WorkDal>> GetWork(WorkGetDAL filter, CultureMode culture);
        Task<List<SportDal>> GetSport(SportGetDAL filter, CultureMode culture);
        Task<List<SaleDal>> GetSale(SaleGetDAL filter, CultureMode culture);
        Task<List<EverythingElseDal>> GetEverythingElse(EverythingElseGetDAL filter, CultureMode culture);
        Task<List<PetsAndPlantsDal>> GetPetsAndPlants(PetsAndPlantsGetDAL filter, CultureMode culture);
        Task<List<CultureDal>> GetCulture(CultureGetDAL filter, CultureMode culture);
        Task<List<HealfCareDal>> GetHealfCare(HealfCareGetDAL filter, CultureMode culture);
        Task<List<FurnitureDal>> GetFurniture(FurnitureGetDAL filter, CultureMode culture);
        Task<List<AllForHomeAndGardenDal>> GetForHomeAndGarden(ForHomeAndGardenGetDAL filter, CultureMode culture);
        Task<List<HouseholdGoodDal>> GetHouseholdGoods(HouseholdGoodGetDAL filter, CultureMode culture);
        Task<List<ConstructionDal>> GetConstructions(ConstructionGetDAL filter, CultureMode culture);
        Task<List<ElectronicsDal>> GetElectronics(ElectronicGetDAL filter, CultureMode culture);
        Task<List<AcquaintanceDal>> GetAcquaintance(AcquaintanceGetDAL filter, CultureMode culture);
        Task<List<ApplianceDal>> GetAppliance(ApplianceGetDAL filter, CultureMode culture);
        Task<List<RealEstateDal>> GetRealEstate(RealEstateGetDAL filter, CultureMode culture);
        Task<List<VehicleDal>> GetVehicle(VehicleGetDAL filter, CultureMode culture);
        Task<List<ClothesAndShoesDal>> GetClothesAndShoes(ClothesAndShoesGetDAL filter, CultureMode culture);
        Task<List<ForChildrenDal>> GetForChildren(ForChildrenGetDAL filter, CultureMode culture);
        Task<List<JewerlyAndAccessoriesDal>> GetJewerlyAndAccessories(JewerlyAndAccessoriesGetDAL filter, CultureMode culture);
        Task<List<ProductsAndDrinksDal>> GetProductsAndDrinks(ProductsAndDrinksGetDAL filter, CultureMode culture);
        Task<List<CigaretteAndAlcoholDal>> GetCigaretteAndAlcohol(CigaretteAndAlcoholGetDAL filter, CultureMode culture);
        Task<List<TourismAndRestDal>> GetTourismAndRest(TourismAndRestGetDAL filter, CultureMode culture);
        Task<List<ServicesDal>> GetServices(ServicesGetDAL filter, CultureMode culture);
        Task<List<ExchangeDal>> GetExchange(ExchangeGetDAL filter, CultureMode culture);
        #endregion

        #region Update
        Task<bool> UpdateAd(UniversalDal ad);
        Task<bool> UpdateAdImages(ImagesUpdateDAL ad);
        #endregion

        #region Notification
        Task<List<string>> GetNotificationTokens(UniversalDal ad);

        Task<string> NotificateUser(int adID);
        #endregion

        #region LoaderAd
        Task<LoaderAdDAL> GetLoaderAd(CultureMode culture);
        #endregion
}