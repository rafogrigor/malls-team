using DAL.Common;

namespace DAL.Models;

public class UserDal
{
    public int ID { get; set; }
    public string PhoneNumber { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public UserState State { get; set; }
    public bool IsConfirmed { get; set; }
    public int Country { get; set; }
    public string SCountry { get; set; }
    public int Region { get; set; }
    public string SRegion { get; set; }
    public int City { get; set; }
    public string SCity { get; set; }
    public string Address { get; set; }
    public decimal Rating { get; set; }
    public decimal Balance { get; set; }
    public string Avatar { get; set; }
    public Result Result { get; set; }
}
public class UserRateDal
{
    public int ID { get; set; }
    public bool IsIncreased { get; set; }
}
public class VipDataDal
{
    public int ID { get; set; }
    public int UserID { get; set; }
    public int TariffID { get; set; }
    public DateTime PurchaseDate { get; set; }
    public DateTime TariffEndDate { get; set; }
    public int FixedViewCount { get; set; }
    public int TargetViewCount { get; set; }
    public int AvailableAdCount { get; set; }
}
public class EmailConfirmationModelDAL
{
    public string Email { get; set; }
    public int ConfirmationCode { get; set; }
}