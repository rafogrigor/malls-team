namespace DAL.Models;

public class TariffDal
{
    public int ID { get; set; }
    public string Name { get; set; }
    public string Desc { get; set; }
    public decimal Price { get; set; }
    public decimal SalePercent { get; set; }
    public decimal SalePrice { get; set; }
    public int? ViewCount { get; set; }
    public int? AdCount { get; set; }
    public int? Period { get; set; }
}
public class PurchaseTariffDal
{
    public int UserID { get; set; }
    public int TariffID { get; set; }
}
public class PurchaseLoaderAdDAL
{
    public int AdID { get; set; }
    public int UserID { get; set; }
    public int TariffID { get; set; }
}