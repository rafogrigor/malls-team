namespace DAL.Models;

public class TelcellPaymentModelDAL
{
    public int UserID { get; set; }
    public DateTime PaymentDate { get; set; }
    public decimal PaymentAmount { get; set; }
    public long Receipt { get; set; }
}
public class TelcellUserDAL
{
    public int ID { get; set; }
    public string FirstName { get; set; }
}