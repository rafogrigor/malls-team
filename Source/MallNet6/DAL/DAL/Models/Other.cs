using DAL.Common;

namespace DAL.Models;

public class PreferenceDal
{
    public int UserID { get; set; }
    public string Tags { get; set; }
}

public class SupportMessage
{
    public List<Message> MessageList { get; set; }
    public Result Result { get; set; }

    public SupportMessage()
    {
        MessageList = new List<Message>();
    }
}

public class Message
{
    public string Text { get; set; }
    public DateTime Date { get; set; }
}

#region Category
public class MainCategoryDal
{
    public int ID { get; set; }
    public string Category { get; set; }
}
public class SubCategoryDal
{
    public int ID { get; set; }
    public string Type { get; set; }
    public int BaseCategoryID { get; set; }

}
#endregion
#region Location
public class CityDal
{
    public int ID { get; set; }
    public string Country { get; set; }
    public int CountryID { get; set; }
}

public class RegionDal
{
    public int ID { get; set; }
    public string Country { get; set; }
    public int CountryID { get; set; }
}

public class CountryDal
{
    public int ID { get; set; }
    public string Country { get; set; }
}
#endregion
#region Search
public class GetPromptDal
{
    public int CountryID { get; set; }
    public string Symbol { get; set; }
}
public class PromptDal
{
    public string Symbol { get; set; }
    public int CountryID { get; set; }
}

public class SearchGetDal
{
    public string TextAM { get; set; }
    public string TextRU { get; set; }
    public string TextEN { get; set; }
    public int CountryID { get; set; }
}
#endregion