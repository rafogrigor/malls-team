using DAL.Common;

namespace DAL.Models;

#region Create
    public class AcquaintanceDal : BaseAdDal
    {
        #region "Main"
        //false male, true female
        public bool? Gender { get; set; }
        public AcquaintanceAim AcquaintanceAim { get; set; }
        public int? Height { get; set; }
        public int? Weight { get; set; }
        public int Age { get; set; }
        #endregion
    }
    public class AllForHomeAndGardenDal : BaseAdDal
    {
        #region "Main"
        public ProductState ProductState { get; set; }
        #endregion
    }
    public class ApplianceDal : BaseAdDal
    {
        #region "Main"
        public ProductState ProductState { get; set; }
        #endregion
    }
    public class CigaretteAndAlcoholDal : BaseAdDal
    {

        #region "Main"
        public ProductState ProductState { get; set; }
        public bool IsLocal { get; set; }
        public byte? AlcoholVolume { get; set; } = null;
        #endregion
    }
    public class ClothesAndShoesDal : BaseAdDal
    {
        #region "Main"
        public ProductState ProductState { get; set; }
        public string ClothingSize { get; set; }
        public sbyte? ShoesSize { get; set; }
        #endregion

    }
    public class ConstructionDal : BaseAdDal
    {
        #region "Main"
        public ProductState ProductState { get; set; }
        #endregion
    }
    public class CultureDal : BaseAdDal
    {
        #region "Main"
        public ProductState ProductState { get; set; }
        #endregion
    }
    public class ElectronicsDal : BaseAdDal
    {
        #region "Main"
        public ProductState ProductState { get; set; }
        #endregion
    }
    public class EverythingElseDal : BaseAdDal
    {
    }
    public class ForChildrenDal : BaseAdDal
    {
        #region "Main"
        public bool? Gender { get; set; }
        public bool? ForNewBorns { get; set; }
        public ProductState ProductState { get; set; }
        #endregion
    }
    public class FurnitureDal : BaseAdDal
    {
        #region "Main"
        public ProductState ProductState { get; set; }
        #endregion
    }
    public class HealfCareDal : BaseAdDal
    {
        #region "Main"
        public ProductState ProductState { get; set; }
        #endregion

    }
    public class HouseholdGoodDal : BaseAdDal
    {
        #region "Main"
        public ProductState ProductState { get; set; }
        #endregion
    }
    public class JewerlyAndAccessoriesDal : BaseAdDal
    {
        #region "Main"
        public bool? Gender { get; set; }
        public ProductState ProductState { get; set; }
        #endregion
    }
    public class PetsAndPlantsDal : BaseAdDal
    {
        #region "Main"
        public ProductState ProductState { get; set; }
        #endregion
    }
    public class ProductsAndDrinksDal : BaseAdDal
    {
        #region "Main"
        public bool? IsLocal { get; set; }
        public bool? IsFrozen { get; set; } = null;
        public bool? IsNaturalDrink { get; set; } = null;
        #endregion
    }
    public class RealEstateDal : BaseAdDal
    {
        #region "Main"
        public ConstructionType? ConstructionType { get; set; }
        public AdPaymentTime? PaymentTime { get; set; }
        public sbyte? Rooms { get; set; }
        public short Space { get; set; }
        public sbyte? Floor { get; set; }
        #endregion
    }
    public class ServicesDal : BaseAdDal
    {
        #region "Main"
        public AdPaymentTime? PaymentTime { get; set; }
        //false - passenger, true - cargo
        public bool? Transportation { get; set; }
        #endregion
    }
    public class SportDal : BaseAdDal
    {
        #region "Main"
        public ProductState ProductState { get; set; }
        #endregion

    }
    public class SaleDal : BaseAdDal
    {

    }
    public class TourismAndRestDal : BaseAdDal
    {
        #region "Main"
        public DateTime? DepartureDay { get; set; }
        public DateTime? ReturnDay { get; set; }
        public short? ReservedTickets { get; set; }

        #endregion

    }
    public class VehicleDal : BaseAdDal
    {
        #region "Main"
        public int? Mark { get; set; }
        public string SMark { get; set; }

        public int? Model { get; set; }
        public string SModel { get; set; }

        public DateTime? ProductionYear { get; set; }
        public bool? CustomsCleared { get; set; }
        public int? Mileage { get; set; }
        public int? EngineSize { get; set; }
        public decimal? DEngineSize { get; set; }

        public int? BodyType { get; set; }
        public string SBodyType { get; set; }

        public int? EngineType { get; set; }
        public string SEngineType { get; set; }

        public int? DriveType { get; set; }
        public string SDriveType { get; set; }

        public int? TransmissionType { get; set; }
        public string STransmissionType { get; set; }

        public int? Color { get; set; }
        public string SColor { get; set; }

        public int? Wheel { get; set; }
        public string SWheel { get; set; }
        #endregion
    }
    public class WorkDal : BaseAdDal
    {
        #region "Main"
        public AdPaymentTime? PaymentTime { get; set; }
        #endregion

    }
    #endregion
    #region Other
    public class NotificateUserDal
    {
        public string Token { get; set; }
        public string PhoneNumber { get; set; }
    }
    public class SearchModelDal
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public string Name { get; set; }
        public int CategoryID { get; set; }
        public string Description { get; set; }
        public AdOwner Owner { get; set; }
        public string Image { get; set; }
    }
    public class AdCreateResponse
    {
        public int ID { get; set; }

        public Result Result { get; set; }
    }
    #region "BasePart"
    public class FavoriteAds
    {
        public List<MinimizedAdDal> FavoriteList { get; set; }
        public Result Result { get; set; }
        public FavoriteAds()
        {
            FavoriteList = new List<MinimizedAdDal>();
        }
    }

    public class UserAds
    {
        public List<MinimizedAdDal> AdList { get; set; }
        public Result Result { get; set; }
        public UserAds()
        {
            AdList = new List<MinimizedAdDal>();
        }
    }
    public class BestExchangeList
    {
        public List<ExchangeDal> List { get; set; }
        public Result Result { get; set; }

        public BestExchangeList()
        {
            List = new List<ExchangeDal>();
        }
    }
    public class LoaderAdDAL
    {
        public int AdID { get; set; }
        public int UserID { get; set; }
        public int MainCategoryID { get; set; }
        public string MainCategoryName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> Images { get; set; }

        public LoaderAdDAL()
        {
            Images = new List<string>();
        }

        public Result Result { get; set; }
    }
    #endregion


    #region Exchanges	
    public class ExchangeDal : BaseAdDal
    {
        #region "Main"
        public short SourceCurrencyID { get; set; }
        public short DestinationCurrencyID { get; set; }
        public decimal SaleSummaRetail { get; set; }
        public decimal BuySummaRetail { get; set; }
        public decimal? SaleSumma { get; set; }
        public decimal? BuySumma { get; set; }
        #endregion
    }
    public class CurrencyDal
    {
        public short ID { get; set; }
        public string Currency { get; set; }
        public string CurrencySymbol { get; set; }
    }
    #endregion




    public class MinimizedAdDal
    {
        public CategoryPartDal Category;

        public List<AdPartDal> Ads;

        public MinimizedAdDal()
        {
            Ads = new List<AdPartDal>();
        }
    }

    public class AdPartDal
    {
        public int ID { get; set; }
        public AdState State { get; set; }
        public string Name { get; set; }
        public int UserID { get; set; }
        public int CategoryID { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public byte Owner { get; set; }
        public bool IsBestPrice { get; set; }
    }

    public class CategoryPartDal
    {
        public int ID { get; set; }

        public string Name { get; set; }
    }
    public class BaseAdDal
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public int SubCategoryID { get; set; }
        public int MainCategoryID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public decimal? LocationLatitude { get; set; }
        public decimal? LocationLongitude { get; set; }
        public decimal Price { get; set; }
        public decimal SalePercent { get; set; }
        public decimal SalePrice { get; set; }
        public string Contact { get; set; }
        public int Country { get; set; }
        public string CountryName { get; set; }
        public int Region { get; set; }
        public string RegionName { get; set; }
        public int City { get; set; }
        public string CityName { get; set; }
        public AdState State { get; set; }
        public Aim? Aim { get; set; }
        public AdOwner? Owner { get; set; }
        public Currency Currency { get; set; }
        public string Tags { get; set; }
        public decimal View { get; set; }
        public AdType AdType { get; set; }
        public List<string> ImagesList { get; set; }
        public bool IsBestPrice { get; set; }
        public bool? IsRegional { get; set; }
        public bool IsFavorite { get; set; }
        public BaseAdDal()
        {
            ImagesList = new List<string>();
        }

    }
    public class FavoriteAdModel
    {
        public int UserID { get; set; }
        public int AdID { get; set; }
        public FavoriteAdModel()
        {
            UserID = CommonConstants.BadID;
            AdID = CommonConstants.BadID;
        }
    }
    #endregion
    #region Get
    public class AdGetBaseDAL
    {
        public int? ID { get; set; }
        public uint? PageNumber { get; set; }
        public uint? AdCount { get; set; }
        public int? SubCategoryID { get; set; }
        public int? UserID { get; set; }
        public int? Country { get; set; }
        public int? Region { get; set; }
        public int? City { get; set; }
        public bool OnlyWithPhotos { get; set; }
        public Aim? Aim { get; set; }
        public AdOwner? AdOwner { get; set; }
        public Currency? Currency { get; set; }
        public decimal? SalePriceFrom { get; set; }
        public decimal? SalePriceTo { get; set; }
        public decimal? SalePercentTo { get; set; }
        public decimal? SalePercentFrom { get; set; }
        public ProductState? ProductState { get; set; }

    }

    public class WorkGetDAL : AdGetBaseDAL
    {
        public AdPaymentTime? AdPaymentTime { get; set; }
    }

    public class SportGetDAL : AdGetBaseDAL
    {

    }
    public class SaleGetDAL : AdGetBaseDAL
    {

    }
    public class EverythingElseGetDAL : AdGetBaseDAL
    {

    }
    public class PetsAndPlantsGetDAL : AdGetBaseDAL
    {

    }
    public class CultureGetDAL : AdGetBaseDAL
    {

    }
    public class HealfCareGetDAL : AdGetBaseDAL
    {

    }
    public class FurnitureGetDAL : AdGetBaseDAL
    {

    }
    public class ForHomeAndGardenGetDAL : AdGetBaseDAL
    {

    }
    public class HouseholdGoodGetDAL : AdGetBaseDAL
    {

    }
    public class ConstructionGetDAL : AdGetBaseDAL
    {

    }
    public class ElectronicGetDAL : AdGetBaseDAL
    {

    }
    public class ApplianceGetDAL : AdGetBaseDAL
    {

    }
    public class AcquaintanceGetDAL : AdGetBaseDAL
    {
        public bool? Gender { get; set; }
        public AcquaintanceAim? AcquintanceAim { get; set; }
        public int? Height { get; set; }
        public int? Weight { get; set; }
        public int? AgeFrom { get; set; }
        public int? AgeTo { get; set; }
    }
    public class RealEstateGetDAL : AdGetBaseDAL
    {
        public ConstructionType? ConstructionType { get; set; }
        public byte? PaymentTime { get; set; }
        public byte? Rooms { get; set; }
        public short? SpaceFrom { get; set; }
        public short? SpaceTo { get; set; }
        public byte? FloorFrom { get; set; }
        public byte? FloorTo { get; set; }
    }
    public class VehicleGetDAL : AdGetBaseDAL
    {
        public int? Mark { get; set; }
        public int? Model { get; set; }
        public DateTime? ProductionYearFrom { get; set; }
        public DateTime? ProductionYearTo { get; set; }
        public int? MileageFrom { get; set; }
        public int? MileageTo { get; set; }
        public bool? CustomsCleared { get; set; }
        public int? BodyType { get; set; }
        public int? EngineType { get; set; }
        public int? EngineSizeFrom { get; set; }
        public int? EngineSizeTo { get; set; }
        public int? DriveType { get; set; }
        public int? TransmissionType { get; set; }
        public int? Color { get; set; }
        public int? Wheel { get; set; }
    }
    public class ClothesAndShoesGetDAL : AdGetBaseDAL
    {
        public string ClothingSize { get; set; }
        public byte? ShoesSize { get; set; }
    }
    public class ForChildrenGetDAL : AdGetBaseDAL
    {
        public bool? Gender { get; set; }
        public bool? ForNewBorns { get; set; }
    }
    public class JewerlyAndAccessoriesGetDAL : AdGetBaseDAL
    {
        public bool? Gender { get; set; }
    }
    public class ProductsAndDrinksGetDAL : AdGetBaseDAL
    {
        public bool? IsLocal { get; set; }
        public bool? IsFrozen { get; set; }
        public bool? IsNaturalDrink { get; set; }
    }
    public class CigaretteAndAlcoholGetDAL : AdGetBaseDAL
    {
        public bool? IsLocal { get; set; }
        public byte? AlcoholVolume { get; set; }
    }
    public class TourismAndRestGetDAL : AdGetBaseDAL
    {
        public DateTime? DepartureDay { get; set; }
        public DateTime? ReturnDay { get; set; }
        public short? ReservedTickets { get; set; }

    }
    public class ServicesGetDAL : AdGetBaseDAL
    {
        public byte? PaymentTime { get; set; }
        public bool? Transportation { get; set; }
    }
    public class ExchangeGetDAL : AdGetBaseDAL
    {

    }
    #endregion
    #region Update
    public class ImagesUpdateDAL
    {
        public int AdID { get; set; }
        public string SubCategory { get; set; }
        public List<string> NewImagesList { get; set; }
        public List<string> OldImagesLinks { get; set; }
        public ImagesUpdateDAL()
        {
            NewImagesList = new List<string>();
            OldImagesLinks = new List<string>();
        }
    }
    #endregion
    #region Main Page
    public class MainPageDal
    {
        public BestExchangeDal BestExchange;

        public List<MinimizedAdDal> AdsWithCategories;

        public MainPageDal()
        {
            AdsWithCategories = new List<MinimizedAdDal>();
        }
    }
    public class BestExchangeDal
    {
        public decimal Sale { get; set; }
        public decimal Buy { get; set; }
        public string Symbol { get; set; }
        public int UserID { get; set; }
        public int AdID { get; set; }
        public bool IsBestPrice { get; set; }
    }
    #endregion
    #region Universal
    public class UniversalDal
    {
        public int ID { get; set; }
        public AdType AdType { get; set; }
        public AdState? State { get; set; }
        public int UserID { get; set; }
        public AcquaintanceAim? AcquaintanceAim { get; set; }
        public int? SubCategoryID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? LocationLatitude { get; set; }
        public decimal? LocationLongitude { get; set; }
        public decimal? Price { get; set; }
        public decimal? SalePercent { get; set; }
        public string Contact { get; set; }
        public int? Country { get; set; }
        public int? Region { get; set; }
        public int? City { get; set; }
        public AdOwner? Owner { get; set; }
        public Currency? Currency { get; set; }
        public string Tags { get; set; }
        public bool? IsRegional { get; set; }
        public Aim? Aim { get; set; }
        public ProductState? ProductState { get; set; }
        public bool? IsLocal { get; set; }
        public byte? AlcoholVolume { get; set; }
        public string ClothingSize { get; set; }
        public sbyte? ShoesSize { get; set; }
        public int? Height { get; set; }
        public int? Weight { get; set; }
        public int? Age { get; set; }
        public bool? Gender { get; set; }
        public bool? ForNewBorns { get; set; }
        public bool? IsFrozen { get; set; } = null;
        public bool? IsNaturalDrink { get; set; } = null;
        public ConstructionType? ConstructionType { get; set; }
        public AdPaymentTime? PaymentTime { get; set; }
        public sbyte? Rooms { get; set; }
        public short? Space { get; set; }
        public sbyte? Floor { get; set; }
        //false - passenger, true - cargo
        public bool? Transportation { get; set; }
        public DateTime? DepartureDay { get; set; }
        public DateTime? ReturnDay { get; set; }
        public short? ReservedTickets { get; set; }
        public int? Mark { get; set; }
        public int? Model { get; set; }
        public DateTime? ProductionYear { get; set; }
        public bool? CustomsCleared { get; set; }
        public int? Mileage { get; set; }
        public int? BodyType { get; set; }
        public int? EngineType { get; set; }
        public int? EngineSize { get; set; }
        public int? DriveType { get; set; }
        public int? TransmissionType { get; set; }
        public int? Color { get; set; }
        public int? Wheel { get; set; }
        public short? SourceCurrencyID { get; set; }
        public short? DestinationCurrencyID { get; set; }
        public decimal? SaleSummaRetail { get; set; }
        public decimal? BuySummaRetail { get; set; }
        public decimal? SaleSumma { get; set; }
        public decimal? BuySumma { get; set; }

    }
    #endregion
    #region Vehicle
    public class EngineSizeDal
    {
        public int ID { get; set; }
        public decimal Size { get; set; }
    }
    public class BodyTypeDal
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class ColorDal
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class DriveTypeDal
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class MarkDal
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class MileageDal
    {
        public int ID { get; set; }
        public int Mileage { get; set; }
    }
    public class ModelDal
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int CarTypeID { get; set; }
    }
    public class TransmissionTypeDAL
    {
        public int ID { get; set; }
        public string TransmissionType { get; set; }
    }

    public class EngineTypeDAL
    {
        public int ID { get; set; }
        public string EngineType { get; set; }
    }

    public class WheelDAL
    {
        public int ID { get; set; }
        public string Wheel { get; set; }
    }
    #endregion