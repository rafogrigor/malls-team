namespace DAL.Extensions;

public static class StringExtensions
{
    public static string RRTrim(this string strSource, int spaceCount = 1)
    {
        string retStr = string.Empty;

        if (!string.IsNullOrWhiteSpace(strSource))
        {
            bool flag = false;
            string[] strArray;
            string strDelimeter;

            strArray = strSource.Split(' ');
            strDelimeter = new string(' ', spaceCount);

            for (int i = 0; i <= strArray.GetUpperBound(0); i++)
            {
                if (!string.IsNullOrWhiteSpace(strArray[i]))
                {
                    if (flag)
                    {
                        retStr += strDelimeter + strArray[i];
                    }
                    else
                    {
                        flag = true;
                        retStr = strArray[i];
                    }
                }
            }
        }
        return retStr;
    }
}