using System.Data;
using DAL.Common;
using DAL.Interfaces;
using DAL.Models;
using DAL.SqlConnection;

namespace DAL.Repositories;
using System.Data.SqlClient;

public class LocationRepository : ILocation
{
    private readonly Manager dbService;

    public LocationRepository(Manager dbService)
    {
        this.dbService = dbService;
    }
    public async Task<List<CityDal>> GetCities(CultureMode culture, int regionID)
    {
        string city = string.Empty;
        List<CityDal> result = null;

        using (SqlConnection conn = this.dbService.CreateConnection())
        using (SqlCommand cmd = conn.CreateCommand())
        {
            cmd.CommandType = CommandType.Text;

            switch (culture)
            {
                case CultureMode.AM:
                    city = "CityAM";
                    break;
                case CultureMode.RU:
                    city = "CityRU";
                    break;
                case CultureMode.EN:
                    city = "CityEN";
                    break;
            }

            cmd.CommandText = $@"select ID,{city},RegionID
                                        from City
                                    where 1 = 1 {Environment.NewLine}";

            if (regionID != -1)
            {
                cmd.CommandText += $" and RegionID = @ID {Environment.NewLine}";
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = regionID;
            }

            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
            {
                if (reader.HasRows)
                {
                    result = new List<CityDal>();
                    int ordID = reader.GetOrdinal("ID");
                    int ordCity = reader.GetOrdinal(city);
                    int ordRegionID = reader.GetOrdinal("RegionID");
                    while (reader.Read())
                    {
                        result.Add(new CityDal()
                        {
                            ID = reader.GetInt32(ordID),
                            Country = reader.GetString(ordCity),
                            CountryID = reader.GetInt32(ordRegionID),
                        });
                    }
                }
                return result;
            }
        }
    }

    public async Task<List<CountryDal>> GetCountries(CultureMode culture, int id = -1)
    {
        string country = string.Empty;
        List<CountryDal> result = null;

        using (SqlConnection conn = this.dbService.CreateConnection())
        using (SqlCommand cmd = conn.CreateCommand())
        {
            cmd.CommandType = CommandType.Text;

            switch (culture)
            {
                case CultureMode.AM:
                    country = "CountryAM";
                    break;
                case CultureMode.RU:
                    country = "CountryRU";
                    break;
                case CultureMode.EN:
                    country = "CountryEN";
                    break;
            }

            cmd.CommandText = $@"select ID,{country}
                                        from Country
                                    where 1 = 1 {Environment.NewLine}";

            if (id != -1)
            {
                cmd.CommandText += $" and ID = @ID {Environment.NewLine}";
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;
            }

            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
            {
                if (reader.HasRows)
                {
                    result = new List<CountryDal>();
                    int ordID = reader.GetOrdinal("ID");
                    int ordCountry = reader.GetOrdinal(country);
                    while (reader.Read())
                    {
                        result.Add(new CountryDal() { ID = reader.GetInt32(ordID), Country = reader.GetString(ordCountry) });
                    }
                }
                return result;
            }
        }
    }

    public async Task<List<RegionDal>> GetRegions(CultureMode culture, int countryID)
    {
        string region = string.Empty;
        List<RegionDal> result = null;

        using (SqlConnection conn = this.dbService.CreateConnection())
        using (SqlCommand cmd = conn.CreateCommand())
        {
            cmd.CommandType = CommandType.Text;

            switch (culture)
            {
                case CultureMode.AM:
                    region = "RegionAM";
                    break;
                case CultureMode.RU:
                    region = "RegionRU";
                    break;
                case CultureMode.EN:
                    region = "RegionEN";
                    break;
            }

            cmd.CommandText = $@"select ID,{region},CountryID
                                        from Region
                                    where 1 = 1 {Environment.NewLine}";

            if (countryID != -1)
            {
                cmd.CommandText += $" and CountryID = @ID {Environment.NewLine}";
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = countryID;
            }

            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
            {
                if (reader.HasRows)
                {
                    result = new List<RegionDal>();
                    int ordID = reader.GetOrdinal("ID");
                    int ordRegion = reader.GetOrdinal(region);
                    int ordCountryID = reader.GetOrdinal("CountryID");
                    while (reader.Read())
                    {
                        result.Add(new RegionDal()
                        {
                            ID = reader.GetInt32(ordID),
                            Country = reader.GetString(ordRegion),
                            CountryID = reader.GetInt32(ordCountryID),
                        });
                    }
                }
                return result;
            }
        }
    }
}