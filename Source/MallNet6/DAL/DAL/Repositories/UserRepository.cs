using System.Data;
using DAL.Common;
using DAL.Extensions;
using DAL.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Identity;

namespace DAL.Repositories;

using DAL.SqlConnection;
using System.Data.SqlClient;

public class UserRepository : IUserPasswordStore<UserDal>, IUserStore<UserDal>, IUser, IEmailConfirmation
{
    private readonly Manager dbService;

    public UserRepository(Manager dbService)
    {
        this.dbService = dbService;
    }
    public async Task<IdentityResult> DeleteAsync(UserDal user, CancellationToken cancellationToken)
    {
        using (SqlConnection con = dbService.CreateConnection())
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = con;
            cmd.CommandText = "DeleteUserData";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@PhoneNumber", SqlDbType.VarChar).Value = user.PhoneNumber;

            try
            {
                if (await cmd.ExecuteNonQueryAsync() == 0)
                {
                    throw new Exception($"User with PhoneNumber {user.PhoneNumber} was deactivated or not exists in DB");
                }
            }
            catch (Exception ex)
            {
                return IdentityResult.Failed(new IdentityError() { Description = ex.Message });
            }
            return IdentityResult.Success;
        }
    }
    public async Task<UserDal> FindByIdAsync(string userID, CancellationToken cancellationToken)
    {
        UserDal userData = new UserDal();

        using (SqlConnection con = dbService.CreateConnection())
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.CommandText = "GetUserDataById";
            cmd.Parameters.Add("@ID", SqlDbType.Int).Value = int.TryParse(userID, out int result) ? result : -1;
            try
            {
                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    if (!reader.HasRows)
                    {
                        throw new Exception($"User with ID {userID} not exists in DB");
                    }

                    if (reader.Read())
                    {
                        int ordPhoneNumber = reader.GetOrdinal("PhoneNumber");
                        int ordFirstName = reader.GetOrdinal("FirstName");
                        int ordLastName = reader.GetOrdinal("LastName");
                        int ordEmail = reader.GetOrdinal("Email");
                        int ordState = reader.GetOrdinal("State");
                        int ordIsConfirmed = reader.GetOrdinal("IsConfirmed");
                        int ordPassword = reader.GetOrdinal("PasswordHash");
                        int ordCountry = reader.GetOrdinal("Country");
                        int ordRegion = reader.GetOrdinal("Region");
                        int ordCity = reader.GetOrdinal("City");
                        int ordAddress = reader.GetOrdinal("Address");
                        int ordRating = reader.GetOrdinal("Rating");
                        int ordBalance = reader.GetOrdinal("Balance");
                        int ordAvatar = reader.GetOrdinal("Avatar");

                        userData.PhoneNumber = reader.GetString(ordPhoneNumber);
                        userData.FirstName = reader.GetString(ordFirstName);
                        userData.Email = reader.GetString(ordEmail);
                        userData.Password = reader.GetString(ordPassword);
                        userData.Rating = reader.GetDecimal(ordRating);
                        userData.Balance = reader.GetDecimal(ordBalance);
                        userData.State = (UserState)reader.GetByte(ordState);
                        userData.IsConfirmed = reader.GetBoolean(ordIsConfirmed);

                        if (!reader.IsDBNull(ordLastName))
                        {
                            userData.LastName = reader.GetString(ordLastName);
                        }

                        if (!reader.IsDBNull(ordCountry))
                        {
                            userData.Country = reader.GetInt32(ordCountry);
                        }

                        if (!reader.IsDBNull(ordRegion))
                        {
                            userData.Region = reader.GetInt32(ordRegion);
                        }

                        if (!reader.IsDBNull(ordCity))
                        {
                            userData.City = reader.GetInt32(ordCity);
                        }

                        if (!reader.IsDBNull(ordAddress))
                        {
                            userData.Address = reader.GetString(ordAddress);
                        }

                        if (!reader.IsDBNull(ordAvatar))
                        {
                            userData.Avatar = reader.GetString(ordAvatar);
                        }
                    }
                    return userData;
                }
            }
            catch
            {
                return null;
            }
        }
    }
    public async Task<UserDal> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
    {
        UserDal userData = new UserDal();

        using (SqlConnection con = dbService.CreateConnection())
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetUserData";
            cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = normalizedUserName;
            try
            {
                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    if (!reader.HasRows)
                    {
                        throw new Exception($"User with PhoneNumber {normalizedUserName} not exists in DB");
                    }

                    if (reader.Read())
                    {
                        int ordUserID = reader.GetOrdinal("ID");
                        int ordPhoneNumber = reader.GetOrdinal("PhoneNumber");
                        int ordFirstName = reader.GetOrdinal("FirstName");
                        int ordLastName = reader.GetOrdinal("LastName");
                        int ordEmail = reader.GetOrdinal("Email");
                        int ordState = reader.GetOrdinal("State");
                        int ordIsConfirmed = reader.GetOrdinal("IsConfirmed");
                        int ordPassword = reader.GetOrdinal("PasswordHash");
                        int ordCountry = reader.GetOrdinal("Country");
                        int ordRegion = reader.GetOrdinal("Region");
                        int ordCity = reader.GetOrdinal("City");
                        int ordAddress = reader.GetOrdinal("Address");
                        int ordRating = reader.GetOrdinal("Rating");
                        int ordBalance = reader.GetOrdinal("Balance");
                        int ordAvatar = reader.GetOrdinal("Avatar");

                        userData.ID = reader.GetInt32(ordUserID);
                        userData.PhoneNumber = reader.GetString(ordPhoneNumber);
                        userData.FirstName = reader.GetString(ordFirstName);
                        userData.Email = reader.GetString(ordEmail);
                        userData.Password = reader.GetString(ordPassword);
                        userData.Rating = reader.GetDecimal(ordRating);
                        userData.Balance = reader.GetDecimal(ordBalance);
                        userData.State = (UserState)reader.GetByte(ordState);
                        userData.IsConfirmed = reader.GetBoolean(ordIsConfirmed);

                        if (!reader.IsDBNull(ordLastName))
                        {
                            userData.LastName = reader.GetString(ordLastName);
                        }

                        if (!reader.IsDBNull(ordCountry))
                        {
                            userData.Country = reader.GetInt32(ordCountry);
                        }

                        if (!reader.IsDBNull(ordRegion))
                        {
                            userData.Region = reader.GetInt32(ordRegion);
                        }

                        if (!reader.IsDBNull(ordCity))
                        {
                            userData.City = reader.GetInt32(ordCity);
                        }

                        if (!reader.IsDBNull(ordAddress))
                        {
                            userData.Address = reader.GetString(ordAddress);
                        }

                        if (!reader.IsDBNull(ordAvatar))
                        {
                            userData.Avatar = reader.GetString(ordAvatar);
                        }
                    }
                    return userData;
                }
            }
            catch
            {
                return null;
            }
        }
    }
    public async Task<bool> RateUserAsync(UserRateDal userRateDal)
    {
        using (SqlConnection con = dbService.CreateConnection())
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $@"UPDATE dbo.UserData
                                     SET  Rating = Rating + @IncreaseStep
                                     Where ID = @UserID";
            cmd.Parameters.Add("@IncreaseStep", SqlDbType.Decimal).Value = userRateDal.IsIncreased ? Common.CommonConstants.RatingIncreaseStep : -Common.CommonConstants.RatingIncreaseStep;
            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = userRateDal.ID;

            if (await cmd.ExecuteNonQueryAsync() == 1)
            {
                return true;
            }

            return false;
        }
    }
    public Task<string> GetNormalizedUserNameAsync(UserDal user, CancellationToken cancellationToken)
    {
        return Task.FromResult(user.PhoneNumber);
    }
    public Task<string> GetPasswordHashAsync(UserDal user, CancellationToken cancellationToken)
    {
        return Task.FromResult(user.Password);
    }
    public Task<string> GetUserIdAsync(UserDal user, CancellationToken cancellationToken)
    {
        return Task.FromResult(user.ID.ToString());
    }
    public Task<string> GetUserNameAsync(UserDal user, CancellationToken cancellationToken)
    {
        return Task.FromResult(user.PhoneNumber);
    }
    public Task<bool> HasPasswordAsync(UserDal user, CancellationToken cancellationToken)
    {
        return Task.FromResult(!string.IsNullOrWhiteSpace(user.Password));
    }
    public Task SetNormalizedUserNameAsync(UserDal user, string normalizedName, CancellationToken cancellationToken)
    {
        user.PhoneNumber = normalizedName;
        return Task.CompletedTask;
    }

    public Task SetPasswordHashAsync(UserDal user, string passwordHash, CancellationToken cancellationToken)
    {
        user.Password = passwordHash;
        return Task.CompletedTask;
    }

    public Task SetUserNameAsync(UserDal user, string userName, CancellationToken cancellationToken)
    {
        user.PhoneNumber = userName;
        return Task.CompletedTask;
    }

    public async Task<IdentityResult> EmailConfirmAsync(string email)
    {
        using (SqlConnection con = dbService.CreateConnection())
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "EmailConfirmation";
            cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = email;

            try
            {
                if (await cmd.ExecuteNonQueryAsync() == 0)
                {
                    throw new Exception($"User with Email {email} not exists in DB");
                }
            }
            catch (Exception ex)
            {
                return IdentityResult.Failed(new IdentityError() { Description = ex.Message });
            }
            return IdentityResult.Success;
        }
    }

    public async Task<IdentityResult> RestorePassword(UserDal user)
    {
        using (SqlConnection con = dbService.CreateConnection())
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $@"UPDATE dbo.UserData
                                        SET  State = State {Environment.NewLine}";

            if (!string.IsNullOrWhiteSpace(user.Password))
            {
                cmd.CommandText += $", [PasswordHash] = @Password {Environment.NewLine}";
                cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = user.Password;
            }

            cmd.CommandText += $"WHERE [Email] = @Email {Environment.NewLine}";
            cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = user.Email;

            try
            {
                if (await cmd.ExecuteNonQueryAsync() == 0)
                {
                    throw new Exception($"User with Email {user.Email} not exists in DB");
                }
            }
            catch (Exception ex)
            {
                return IdentityResult.Failed(new IdentityError() { Description = ex.Message });
            }
            return IdentityResult.Success;
        }
    }

    public async Task<IdentityResult> UpdateAsync(UserDal user, CancellationToken cancellationToken)
    {
        using (SqlConnection con = dbService.CreateConnection())
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $@"UPDATE dbo.UserData
                                        SET  State = State {Environment.NewLine}";

            if (user.Password == null)
            {
                if (!string.IsNullOrWhiteSpace(user.FirstName))
                {
                    cmd.CommandText += $",[FirstName] = @FirstName {Environment.NewLine}";

                    cmd.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = user.FirstName;
                }

                if (!string.IsNullOrWhiteSpace(user.LastName))
                {
                    cmd.CommandText += $",[LastName] = @LastName {Environment.NewLine}";

                    cmd.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = user.LastName;
                }

                if (user.Country != 0)
                {
                    cmd.CommandText += $",[Country] = @Country {Environment.NewLine}";

                    cmd.Parameters.Add("@Country", SqlDbType.Int).Value = user.Country;
                }

                if (user.Region != 0)
                {
                    cmd.CommandText += $",[Region] = @Region {Environment.NewLine}";

                    cmd.Parameters.Add("@Region", SqlDbType.Int).Value = user.Region;
                }

                if (user.City != 0)
                {
                    cmd.CommandText += $",[City] = @City {Environment.NewLine}";

                    cmd.Parameters.Add("@City", SqlDbType.Int).Value = user.City;
                }

                if (!string.IsNullOrWhiteSpace(user.Address))
                {
                    cmd.CommandText += $",[Address] = @Address {Environment.NewLine}";

                    cmd.Parameters.Add("@Address", SqlDbType.NVarChar).Value = user.Address;
                }

                if (!string.IsNullOrWhiteSpace(user.Avatar))
                {
                    cmd.CommandText += $",[Avatar] = @Avatar {Environment.NewLine}";

                    cmd.Parameters.Add("@Avatar", SqlDbType.NVarChar).Value = user.Avatar;
                }
            }
            else
            {
                cmd.CommandText += $",[PasswordHash] = @Password {Environment.NewLine}";

                cmd.Parameters.Add("@Password", SqlDbType.NVarChar).Value = user.Password;
            }

            cmd.CommandText += $"WHERE [PhoneNumber] = @PhoneNumber And [State] <> 255 {Environment.NewLine}";
            cmd.Parameters.Add("@PhoneNumber", SqlDbType.VarChar).Value = user.PhoneNumber;

            try
            {
                if (await cmd.ExecuteNonQueryAsync() == 0)
                {
                    throw new Exception($"User with PhoneNumber {user.PhoneNumber} not exists in DB");
                }
            }
            catch (Exception ex)
            {
                return IdentityResult.Failed(new IdentityError() { Description = ex.Message });
            }
            return IdentityResult.Success;
        }
    }

    public void Dispose()
    {

    }

    public async Task<IdentityResult> CreateAsync(UserDal user, CancellationToken cancellationToken)
    {
        using (SqlConnection con = dbService.CreateConnection())
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = con;
            cmd.CommandText = "CreateUser";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@PhoneNumber", SqlDbType.VarChar).Value = user.PhoneNumber;
            cmd.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = user.FirstName;
            cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = user.Email;
            cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = user.Password;
            cmd.Parameters.Add("@Country", SqlDbType.Int).Value = user.Country;
            try
            {
                if (await cmd.ExecuteNonQueryAsync() == 0)
                {
                    throw new Exception($"Can't create user with Email {user.Email} or PhoneNumber {user.PhoneNumber}");
                }
            }
            catch (Exception ex)
            {
                return IdentityResult.Failed(new IdentityError() { Description = ex.Message });
            }
            return IdentityResult.Success;
        }
    }

    public async Task CheckExistanceAndSaveToken(int userID, string token)
    {
        using (SqlConnection con = dbService.CreateConnection())
        {
            using (SqlCommand cmd = con.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "CheckAndSaveToken";
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = userID;
                cmd.Parameters.Add("@Token", SqlDbType.NVarChar).Value = token;
                await cmd.ExecuteNonQueryAsync();
            }
        }

    }

    #region Preferences
    public async Task<bool> AddPreferences(PreferenceDal preference)
    {
        using (SqlConnection con = dbService.CreateConnection())
        {
            using (SqlCommand cmd = con.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = $@"INSERT INTO Preferences(UserID,Tags)
                                         VALUES(@UserID,@Tags) {Environment.NewLine}";
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = preference.UserID;
                cmd.Parameters.Add("@Tags", SqlDbType.NVarChar).Value = preference.Tags.RRTrim();
                if (await cmd.ExecuteNonQueryAsync() == 1)
                {
                    return true;
                }
                return false;
            }
        }
    }

    public async Task<UserDal> GetUserByIDCultured(int userID, CultureMode culture)
    {
        UserDal userData = new UserDal();

        using (SqlConnection con = dbService.CreateConnection())
        using (SqlCommand cmd = con.CreateCommand())
        {
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $@"SELECT UserData.ID,[PhoneNumber],[FirstName],[LastName],[Email],[PasswordHash],[State],[IsConfirmed],
			                        [Country],[Region],[City],[Address],[Rating],[Balance],[Avatar]";
            switch (culture)
            {
                case CultureMode.RU:
                    cmd.CommandText += ",IsNull([CountryRU],'') AS SCountry,IsNull([RegionRU],'') AS SRegion,IsNull([CityRU],'') AS SCity ";
                    break;
                case CultureMode.EN:
                    cmd.CommandText += ",IsNull([CountryEN],'') AS SCountry,IsNull([RegionEN],'') AS SRegion,IsNull([CityEN],'') AS SCity ";
                    break;
                case CultureMode.AM:
                default:
                    cmd.CommandText += ",IsNull([CountryAM],'') AS SCountry,IsNull([RegionAM],'') AS SRegion,IsNull([CityAM],'') AS SCity ";
                    break;
            }

            cmd.CommandText += @" From UserData left join Country ON UserData.Country = Country.ID
                                     left join Region on UserData.Region = Region.ID left join City On UserData.City = City.ID
                                    Where UserData.[ID] = @ID";
            cmd.Parameters.Add("@ID", SqlDbType.Int).Value = userID;

            try
            {
                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    if (!reader.HasRows)
                    {
                        Result result = new Result() { Status = CommonConstants.UserNotExistsException, Message = $"{CommonConstants.UserNotExistsError}" };
                        return new UserDal() { Result = result };
                    }

                    if (reader.Read())
                    {
                        int ordID = reader.GetOrdinal("ID");
                        int ordPhoneNumber = reader.GetOrdinal("PhoneNumber");
                        int ordFirstName = reader.GetOrdinal("FirstName");
                        int ordLastName = reader.GetOrdinal("LastName");
                        int ordEmail = reader.GetOrdinal("Email");
                        int ordState = reader.GetOrdinal("State");
                        int ordIsConfirmed = reader.GetOrdinal("IsConfirmed");
                        int ordPassword = reader.GetOrdinal("PasswordHash");
                        int ordCountry = reader.GetOrdinal("Country");
                        int ordSCountry = reader.GetOrdinal("SCountry");
                        int ordRegion = reader.GetOrdinal("Region");
                        int ordSRegion = reader.GetOrdinal("SRegion");
                        int ordCity = reader.GetOrdinal("City");
                        int ordSCity = reader.GetOrdinal("SCity");
                        int ordAddress = reader.GetOrdinal("Address");
                        int ordRating = reader.GetOrdinal("Rating");
                        int ordBalance = reader.GetOrdinal("Balance");
                        int ordAvatar = reader.GetOrdinal("Avatar");

                        userData.ID = reader.GetInt32(ordID);
                        userData.PhoneNumber = reader.GetString(ordPhoneNumber);
                        userData.FirstName = reader.GetString(ordFirstName);
                        userData.Email = reader.GetString(ordEmail);
                        userData.Password = reader.GetString(ordPassword);
                        userData.Rating = reader.GetDecimal(ordRating);
                        userData.Balance = reader.GetDecimal(ordBalance);
                        userData.State = (UserState)reader.GetByte(ordState);
                        userData.IsConfirmed = reader.GetBoolean(ordIsConfirmed);
                        userData.SCountry = reader.GetString(ordSCountry);
                        userData.SCity = reader.GetString(ordSCity);
                        userData.SRegion = reader.GetString(ordSRegion);


                        if (!reader.IsDBNull(ordLastName))
                        {
                            userData.LastName = reader.GetString(ordLastName);
                        }

                        if (!reader.IsDBNull(ordCountry))
                        {
                            userData.Country = reader.GetInt32(ordCountry);
                        }

                        if (!reader.IsDBNull(ordRegion))
                        {
                            userData.Region = reader.GetInt32(ordRegion);
                        }

                        if (!reader.IsDBNull(ordCity))
                        {
                            userData.City = reader.GetInt32(ordCity);
                        }

                        if (!reader.IsDBNull(ordAddress))
                        {
                            userData.Address = reader.GetString(ordAddress);
                        }

                        if (!reader.IsDBNull(ordAvatar))
                        {
                            userData.Avatar = reader.GetString(ordAvatar);
                        }
                    }

                    userData.Result = new Result() { Status = CommonConstants.Ok, Message = CommonConstants.OkMessage };
                    return userData;
                }
            }
            catch (SqlException ex)
            {
                Result result = new Result() { Status = CommonConstants.OtherException, Message = $"{CommonConstants.SQLOtherError} ({ex.Message})" };
                return new UserDal() { Result = result };
            }
        }
    }
    #endregion

    public async Task<SupportMessage> GetSupportMessage(int userID)
    {
        using (SqlConnection con = dbService.CreateConnection())
        using (SqlCommand cmd = con.CreateCommand())
        {
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $@"SELECT Text,Date 
                                     FROM SupportMessages
                                     WHERE UserID = @UserID ";

            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = userID;

            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
            {
                if (!reader.HasRows)
                {
                    return new SupportMessage()
                    {
                        Result = new Result()
                        {
                            Status = CommonConstants.NoContent,
                            Message = CommonConstants.NoContentMessage
                        }
                    };
                }

                int ordText = reader.GetOrdinal("Text");
                int ordDate = reader.GetOrdinal("Date");
                SupportMessage msgs = new SupportMessage();

                while (await reader.ReadAsync())
                {
                    msgs.MessageList.Add(new Message()
                    {
                        Text = reader.GetString(ordText),
                        Date = reader.GetDateTime(ordDate)
                    });
                }

                msgs.Result = new Result()
                {
                    Status = CommonConstants.Ok,
                    Message = CommonConstants.OkMessage
                };

                return msgs;
            }
        }
    }

    #region Email Confiramtion
    public async Task<bool> AddEmailConfirmationCodeToTable(EmailConfirmationModelDAL emailConfirmationModel)
    {
        using (SqlConnection con = dbService.CreateConnection())
        using (SqlCommand cmd = con.CreateCommand())
        {
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $@" UPDATE T SET
                                      [ConfirmationCode] = @ConfirmationCode,
                                      [ConfirmationDate] = @ConfirmationDate
                FROM [UserEmailConfirmation] AS T
                WHERE [Email] = @Email
                INSERT INTO [UserEmailConfirmation](
                [Email],
                [ConfirmationCode],
                [ConfirmationDate])
                SELECT
                    [Email] = @Email,
                    [ConfirmationCode] = @ConfirmationCode,
                    [ConfirmationDate] = @ConfirmationDate
                WHERE NOT EXISTS(SELECT 'not yet loaded' FROM [UserEmailConfirmation] AS T WHERE T.Email = @Email){Environment.NewLine}";
            cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = emailConfirmationModel.Email;
            cmd.Parameters.Add("@ConfirmationCode", SqlDbType.Int).Value = emailConfirmationModel.ConfirmationCode;
            cmd.Parameters.Add("@ConfirmationDate", SqlDbType.DateTime2).Value = DateTime.UtcNow;
            try
            {
                if (await cmd.ExecuteNonQueryAsync() != 0)
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

    }
    public async Task<bool> CheckEmailConfirmationCode(EmailConfirmationModelDAL emailConfirmationModel)
    {
        using (SqlConnection con = dbService.CreateConnection())
        {
            using (SqlCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = $@" Select T.ConfirmationCode
                                      FROM [UserEmailConfirmation] AS T
                WHERE [Email] = @Email{Environment.NewLine}";
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = emailConfirmationModel.Email;
                try
                {
                    int confirmationCodeFromTable;
                    using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        if (!reader.HasRows)
                        {
                            return false;
                        }
                        int ordConfirmationCode = reader.GetOrdinal("ConfirmationCode");

                        if (!await reader.ReadAsync())
                        {
                            return false;

                        }
                        confirmationCodeFromTable = reader.GetInt32(ordConfirmationCode);
                        if (confirmationCodeFromTable != emailConfirmationModel.ConfirmationCode)
                        {
                            return false;
                        }
                    }
                    cmd.CommandText = $@" Delete
                                                  FROM [UserEmailConfirmation]
                                                  WHERE [Email] = @Email{Environment.NewLine}";

                    if ((await cmd.ExecuteNonQueryAsync()) != 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
    #endregion
}