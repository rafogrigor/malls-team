using System.Data;
using DAL.Common;
using DAL.Interfaces;
using DAL.Models;
using DAL.SqlConnection;

namespace DAL.Repositories;
using System.Data.SqlClient;

public class PaymentsRepository : IPayments
{
    private readonly Manager dbService;

    public PaymentsRepository(Manager dbService)
    {
        this.dbService = dbService;
    }
    public async Task<int> CreatePayment(TelcellPaymentModelDAL paymentModel)
    {
        using (SqlConnection con = this.dbService.CreateConnection())
        {

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = $" Begin Transaction {Environment.NewLine} ";
                cmd.CommandText += $" Insert into PaymentsHistory([UserID], [PaymentDate], [PaymentAmount], [Receipt] ) {Environment.NewLine}";
                cmd.CommandText += $" Values(@UserID, @PaymentDate, @PaymentAmount, @Receipt) {Environment.NewLine}";
                cmd.CommandText += $" SET @CreatedPaymentID = SCOPE_IDENTITY() {Environment.NewLine}";
                cmd.CommandText += $" UPDATE UserData { Environment.NewLine}";
                cmd.CommandText += $" SET [Balance] = [Balance] + @PaymentAmount { Environment.NewLine}";
                cmd.CommandText += $" Where [ID] = @UserID {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = paymentModel.UserID;
                cmd.Parameters.Add("@PaymentDate", SqlDbType.DateTime2).Value = paymentModel.PaymentDate;
                cmd.Parameters.Add("@PaymentAmount", SqlDbType.Money).Value = paymentModel.PaymentAmount;
                cmd.Parameters.Add("@Receipt", SqlDbType.BigInt).Value = paymentModel.Receipt;
                cmd.Parameters.Add("@CreatedPaymentID", SqlDbType.Int).Direction = ParameterDirection.Output;
                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch (Exception)
                {
                    return CommonConstants.BadID;
                }
                int result = int.TryParse(cmd.Parameters["@CreatedPaymentID"].Value.ToString(), out result) ? result : CommonConstants.BadID;
                return result;
            }
        }
    }
    public async Task<TelcellUserDAL> GetUserPaymentModelByID(int userID)
    {
        TelcellUserDAL userData = new TelcellUserDAL();

        using (SqlConnection con = this.dbService.CreateConnection())
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;

            cmd.CommandText = $@" Select [FirstName] { Environment.NewLine} ";
            cmd.CommandText += $" From UserData { Environment.NewLine} ";
            cmd.CommandText += $" Where [ID] = @ID { Environment.NewLine} ";

            cmd.Parameters.Add("@ID", SqlDbType.Int).Value = userID;
            try
            {
                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    if (!reader.HasRows)
                    {
                        throw new Exception($"User with ID {userID} not exists in DB");
                    }

                    if (reader.Read())
                    {

                        int ordFirstName = reader.GetOrdinal("FirstName");
                        userData.FirstName = reader.GetString(ordFirstName);
                        userData.ID = userID;
                    }
                    return userData;
                }
            }
            catch
            {
                return null;
            }
        }
    }
    public async Task<bool> CheckPayment(long receipt)
    {
        using (SqlConnection con = this.dbService.CreateConnection())
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;

            cmd.CommandText = $@"Select [ID] { Environment.NewLine}";
            cmd.CommandText += $" From PaymentsHistory { Environment.NewLine}";
            cmd.CommandText += $" Where [Receipt] = @Receipt { Environment.NewLine}";
            cmd.Parameters.Add("@Receipt", SqlDbType.Int).Value = receipt;
            try
            {
                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    if (!reader.HasRows)
                    {
                        return false;
                    }

                    if (!reader.Read())
                    {
                        return false;
                    }
                    int paymentID = reader.GetInt32(reader.GetOrdinal("ID"));
                    if (paymentID >= 0)
                    {
                        return true;

                    }
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}