using System.Data;
using DAL.Common;
using DAL.Interfaces;
using DAL.Models;
using DAL.SqlConnection;

namespace DAL.Repositories;

public class VehicleRepository : IVehicle
{
    private readonly Manager dbService;

    public VehicleRepository(Manager dbService)
    {
        this.dbService = dbService;
    }
    public async Task<List<BodyTypeDal>> GetBodyType(CultureMode currentCulture)
    {
        List<BodyTypeDal> bodyTypes = null;

        using (System.Data.SqlClient.SqlConnection con = this.dbService.CreateConnection())
        {
            using (System.Data.SqlClient.SqlCommand cmd = con.CreateCommand())
            {
                string culture;
                switch (currentCulture)
                {
                    case CultureMode.RU:
                        culture = "BodyTypeRU";
                        break;
                    case CultureMode.EN:
                        culture = "BodyTypeEN";
                        break;
                    case CultureMode.AM:
                    default:
                        culture = "BodyTypeAM";
                        break;
                }
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = $@"select ID,{culture}
                                         from BodyType {Environment.NewLine}";
                try
                {
                    using (System.Data.SqlClient.SqlDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        int ordID = 0;
                        int ordBodyType = 0;

                        if (reader.HasRows)
                        {
                            bodyTypes = new List<BodyTypeDal>();
                            ordID = reader.GetOrdinal("ID");
                            ordBodyType = reader.GetOrdinal($"{culture}");
                        }

                        while (reader.Read())
                        {
                            bodyTypes.Add(new BodyTypeDal()
                            {
                                ID = reader.GetInt32(ordID),
                                Name = reader.GetString(ordBodyType)
                            }); ;
                        }

                        return bodyTypes;
                    }
                }
                catch (Exception)
                {
                    return new List<BodyTypeDal>();
                }
            }
        }
    }

    public async Task<List<ColorDal>> GetColor(CultureMode currentCulture)
    {
        List<ColorDal> colors = null;

        using (System.Data.SqlClient.SqlConnection con = this.dbService.CreateConnection())
        {
            using (System.Data.SqlClient.SqlCommand cmd = con.CreateCommand())
            {
                string culture;
                switch (currentCulture)
                {
                    case CultureMode.RU:
                        culture = "ColorRU";
                        break;
                    case CultureMode.EN:
                        culture = "ColorEN";
                        break;
                    case CultureMode.AM:
                    default:
                        culture = "ColorAM";
                        break;
                }
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = $@"select ID,{culture}
                                         from Color {Environment.NewLine}";
                try
                {
                    using (System.Data.SqlClient.SqlDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        int ordID = 0;
                        int ordColors = 0;

                        if (reader.HasRows)
                        {
                            colors = new List<ColorDal>();
                            ordID = reader.GetOrdinal("ID");
                            ordColors = reader.GetOrdinal($"{culture}");
                        }

                        while (reader.Read())
                        {
                            colors.Add(new ColorDal()
                            {
                                ID = reader.GetInt32(ordID),
                                Name = reader.GetString(ordColors)
                            }); ;
                        }

                        return colors;
                    }
                }
                catch (Exception)
                {
                    return new List<ColorDal>();
                }
            }
        }
    }

    public async Task<List<DriveTypeDal>> GetDriveType(CultureMode currentCulture)
    {
        List<DriveTypeDal> driveTypes = null;

        using (System.Data.SqlClient.SqlConnection con = this.dbService.CreateConnection())
        {
            using (System.Data.SqlClient.SqlCommand cmd = con.CreateCommand())
            {
                string culture;
                switch (currentCulture)
                {
                    case CultureMode.RU:
                        culture = "DriveTypeRU";
                        break;
                    case CultureMode.EN:
                        culture = "DriveTypeEN";
                        break;
                    case CultureMode.AM:
                    default:
                        culture = "DriveTypeAM";
                        break;
                }
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = $@"select ID,{culture}
                                         from DriveType {Environment.NewLine}";
                try
                {
                    using (System.Data.SqlClient.SqlDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        int ordID = 0;
                        int ordDriveTypes = 0;

                        if (reader.HasRows)
                        {
                            driveTypes = new List<DriveTypeDal>();
                            ordID = reader.GetOrdinal("ID");
                            ordDriveTypes = reader.GetOrdinal($"{culture}");
                        }

                        while (reader.Read())
                        {
                            driveTypes.Add(new DriveTypeDal()
                            {
                                ID = reader.GetInt32(ordID),
                                Name = reader.GetString(ordDriveTypes)
                            }); ;
                        }

                        return driveTypes;
                    }
                }
                catch (Exception)
                {
                    return new List<DriveTypeDal>();
                }
            }
        }
    }

    public async Task<List<EngineSizeDal>> GetEngineSize()
    {
        List<EngineSizeDal> sizes = null;

        using (System.Data.SqlClient.SqlConnection con = this.dbService.CreateConnection())
        {
            using (System.Data.SqlClient.SqlCommand cmd = con.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetEngineSize";

                using (System.Data.SqlClient.SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordSize = 0;

                    if (reader.HasRows)
                    {
                        sizes = new List<EngineSizeDal>();
                        ordID = reader.GetOrdinal("ID");
                        ordSize = reader.GetOrdinal("Size");
                    }

                    while (reader.Read())
                    {
                        sizes.Add(new EngineSizeDal()
                        {
                            ID = reader.GetInt32(ordID),
                            Size = reader.GetDecimal(ordSize)
                        }); ;
                    }

                    return sizes;
                }
            }
        }
    }

    public async Task<List<EngineTypeDAL>> GetEngineType(CultureMode currentCulture)
    {
        List<EngineTypeDAL> engineTypes = new List<EngineTypeDAL>();

        using (System.Data.SqlClient.SqlConnection con = this.dbService.CreateConnection())
        {
            using (System.Data.SqlClient.SqlCommand cmd = con.CreateCommand())
            {
                string culture;
                switch (currentCulture)
                {
                    case CultureMode.RU:
                        culture = "EngineTypeRU";
                        break;
                    case CultureMode.EN:
                        culture = "EngineTypeEN";
                        break;
                    case CultureMode.AM:
                    default:
                        culture = "EngineTypeAM";
                        break;
                }
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = $@"select ID,{culture}
                                         from EngineType {Environment.NewLine}";
                try
                {
                    using (System.Data.SqlClient.SqlDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        int ordID = 0;
                        int ordEngineType = 0;

                        if (reader.HasRows)
                        {
                            ordID = reader.GetOrdinal("ID");
                            ordEngineType = reader.GetOrdinal($"{culture}");
                        }

                        while (reader.Read())
                        {
                            engineTypes.Add(new EngineTypeDAL()
                            {
                                ID = reader.GetInt32(ordID),
                                EngineType = reader.GetString(ordEngineType)
                            }); ;
                        }

                        return engineTypes;
                    }
                }
                catch
                {
                    return new List<EngineTypeDAL>();
                }
            }
        }
    }

    public async Task<List<MarkDal>> GetMarks()
    {
        List<MarkDal> marks = null;

        using (System.Data.SqlClient.SqlConnection con = this.dbService.CreateConnection())
        {
            using (System.Data.SqlClient.SqlCommand cmd = con.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetMark";

                using (System.Data.SqlClient.SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordName = 0;

                    if (reader.HasRows)
                    {
                        marks = new List<MarkDal>();
                        ordID = reader.GetOrdinal("ID");
                        ordName = reader.GetOrdinal("Name");
                    }

                    while (reader.Read())
                    {
                        marks.Add(new MarkDal()
                        {
                            ID = reader.GetInt32(ordID),
                            Name = reader.GetString(ordName)
                        }); ;
                    }

                    return marks;
                }
            }
        }
    }

    public async Task<List<MileageDal>> GetMileage()
    {
        List<MileageDal> mileages = null;

        using (System.Data.SqlClient.SqlConnection con = this.dbService.CreateConnection())
        {
            using (System.Data.SqlClient.SqlCommand cmd = con.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetMileage";

                using (System.Data.SqlClient.SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int orMile = 0;

                    if (reader.HasRows)
                    {
                        mileages = new List<MileageDal>();
                        ordID = reader.GetOrdinal("ID");
                        orMile = reader.GetOrdinal("Mile");
                    }

                    while (reader.Read())
                    {
                        mileages.Add(new MileageDal()
                        {
                            ID = reader.GetInt32(ordID),
                            Mileage = reader.GetInt32(orMile)
                        }); ;
                    }

                    return mileages;
                }
            }
        }
    }

    public async Task<List<ModelDal>> GetModels(int markID = -1)
    {
        List<ModelDal> models = null;

        using (System.Data.SqlClient.SqlConnection con = this.dbService.CreateConnection())
        {
            using (System.Data.SqlClient.SqlCommand cmd = con.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = $@"SELECT [ID],[Name],[CarTypeID]
	                                     FROM Model 
                                         WHERE 1 = 1 {Environment.NewLine}";

                if (markID != -1)
                {
                    cmd.CommandText += $" and CarTypeID = @MarkID {Environment.NewLine}";
                    cmd.Parameters.Add("@MarkID", SqlDbType.Int).Value = markID;
                }

                using (System.Data.SqlClient.SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordName = 0;
                    int ordCarTypeID = 0;

                    if (reader.HasRows)
                    {
                        models = new List<ModelDal>();
                        ordID = reader.GetOrdinal("ID");
                        ordName = reader.GetOrdinal("Name");
                        ordCarTypeID = reader.GetOrdinal("CarTypeID");
                    }

                    while (reader.Read())
                    {
                        models.Add(new ModelDal()
                        {
                            ID = reader.GetInt32(ordID),
                            Name = reader.GetString(ordName),
                            CarTypeID = reader.GetInt32(ordCarTypeID)
                        }); ;
                    }

                    return models;
                }
            }
        }
    }

    public async Task<List<TransmissionTypeDAL>> GetTransmissionType(CultureMode currentCulture)
    {
        List<TransmissionTypeDAL> transmissionTypes = new List<TransmissionTypeDAL>();

        using (System.Data.SqlClient.SqlConnection con = this.dbService.CreateConnection())
        {
            using (System.Data.SqlClient.SqlCommand cmd = con.CreateCommand())
            {
                string culture;
                switch (currentCulture)
                {
                    case CultureMode.RU:
                        culture = "TransmissionRU";
                        break;
                    case CultureMode.EN:
                        culture = "TransmissionEN";
                        break;
                    case CultureMode.AM:
                    default:
                        culture = "TransmissionAM";
                        break;
                }
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = $@"select ID,{culture}
                                         from TransmissionType {Environment.NewLine}";
                try
                {
                    using (System.Data.SqlClient.SqlDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        int ordID = 0;
                        int ordTransmissionType = 0;

                        if (reader.HasRows)
                        {
                            transmissionTypes = new List<TransmissionTypeDAL>();
                            ordID = reader.GetOrdinal("ID");
                            ordTransmissionType = reader.GetOrdinal($"{culture}");
                        }

                        while (reader.Read())
                        {
                            transmissionTypes.Add(new TransmissionTypeDAL()
                            {
                                ID = reader.GetInt32(ordID),
                                TransmissionType = reader.GetString(ordTransmissionType)
                            }); ;
                        }

                        return transmissionTypes;
                    }
                }
                catch (Exception)
                {
                    return new List<TransmissionTypeDAL>();
                }
            }
        }
    }

    public async Task<List<WheelDAL>> GetWheelType(CultureMode currentCulture)
    {
        List<WheelDAL> wheelTypes = new List<WheelDAL>();


        using (System.Data.SqlClient.SqlConnection con = this.dbService.CreateConnection())
        {
            using (System.Data.SqlClient.SqlCommand cmd = con.CreateCommand())
            {
                string culture;
                switch (currentCulture)
                {
                    case CultureMode.RU:
                        culture = "WheelRU";
                        break;
                    case CultureMode.EN:
                        culture = "WheelEN";
                        break;
                    case CultureMode.AM:
                    default:
                        culture = "WheelAM";
                        break;
                }
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = $@"select ID,{culture}
                                         from Wheel {Environment.NewLine}";
                try
                {
                    using (System.Data.SqlClient.SqlDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        int ordID = 0;
                        int ordWheel = 0;

                        if (reader.HasRows)
                        {
                            wheelTypes = new List<WheelDAL>();
                            ordID = reader.GetOrdinal("ID");
                            ordWheel = reader.GetOrdinal($"{culture}");
                        }

                        while (reader.Read())
                        {
                            wheelTypes.Add(new WheelDAL()
                            {
                                ID = reader.GetInt32(ordID),
                                Wheel = reader.GetString(ordWheel)
                            }); ;
                        }

                        return wheelTypes;
                    }
                }
                catch
                {
                    return new List<WheelDAL>();
                }
            }
        }

    }
}