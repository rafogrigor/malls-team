using System.Data;
using DAL.Common;
using DAL.Interfaces;
using DAL.Models;
using DAL.SqlConnection;

namespace DAL.Repositories;
using System.Data.SqlClient;

public class TariffRepository : ITariff
{
    private readonly Manager dbService;

    public TariffRepository(Manager dbService)
    {
        this.dbService = dbService;
    }
    public async Task<List<TariffDal>> GetTariffs(CultureMode currentCulture)
    {
        string nameWithCulture;
        string descWithCulture;

        switch (currentCulture)
        {
            case CultureMode.AM:
                nameWithCulture = "NameAM";
                descWithCulture = "DescAM";
                break;
            case CultureMode.RU:
                nameWithCulture = "NameRU";
                descWithCulture = "DescRU";
                break;
            case CultureMode.EN:
                nameWithCulture = "NameEN";
                descWithCulture = "DescEN";
                break;
            default:
                throw new InvalidOperationException("Invalid culture");
        }

        List<TariffDal> tariffs = new List<TariffDal>();

        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = con.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = $"select ID,Price,SalePercent,SalePrice,ViewCount,AdCount,Period,{nameWithCulture},{descWithCulture} from Tariffs";

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = -1;
                    int ordName = -1;
                    int ordDesc = -1;
                    int ordPrice = -1;
                    int ordSalePercent = -1;
                    int ordSalePrice = -1;
                    int ordViewCount = -1;
                    int ordAdCount = -1;
                    int ordPeriod = -1;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordName = reader.GetOrdinal(nameWithCulture);
                        ordDesc = reader.GetOrdinal(descWithCulture);
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordViewCount = reader.GetOrdinal("ViewCount");
                        ordAdCount = reader.GetOrdinal("AdCount");
                        ordPeriod = reader.GetOrdinal("Period");
                    }

                    while (reader.Read())
                    {
                        TariffDal tariff = new TariffDal()
                        {
                            ID = reader.GetInt32(ordID),
                            Name = reader.GetString(ordName),
                            Desc = reader.GetString(ordDesc),
                            Price = reader.GetDecimal(ordPrice),
                            SalePercent = reader.GetDecimal(ordSalePercent),
                            SalePrice = reader.GetDecimal(ordSalePrice)
                        };

                        if (!reader.IsDBNull(ordViewCount))
                        {
                            tariff.ViewCount = reader.GetInt32(ordViewCount);
                        }

                        if (!reader.IsDBNull(ordAdCount))
                        {
                            tariff.AdCount = reader.GetInt32(ordAdCount);
                        }

                        if (!reader.IsDBNull(ordPeriod))
                        {
                            tariff.Period = reader.GetInt32(ordPeriod);
                        }

                        tariffs.Add(tariff);
                    }

                    return tariffs;
                }
            }
        }
    }

    public async Task<Result> AddLoaderAd(PurchaseLoaderAdDAL data)
    {
        using (SqlConnection con = this.dbService.CreateConnection())
        using (SqlCommand cmd = con.CreateCommand())
        {
            cmd.Connection.Open();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $@"DECLARE @FixedViewCount INT,
									@TargetViewCount INT,
									@PaymentAmount MONEY

									SELECT @FixedViewCount = a.[Views],
									@TargetViewCount = @FixedViewCount + t.ViewCount,
									@PaymentAmount = t.SalePrice
									FROM UserData u
									LEFT JOIN Ads a on a.UserID = u.ID AND a.ID = @AdID
									JOIN Tariffs t on u.ID = @UserID and t.ID = @TariffID AND NOT t.ViewCount IS NULL
									LEFT JOIN LoaderAds l on l.AdID = @AdID and a.[Views] < l.TargetViewCount
									WHERE NOT a.ID IS NULL AND u.Balance > t.SalePrice 
                                    AND l.ID IS NULL

									IF (NOT @FixedViewCount IS NULL)
									BEGIN
										BEGIN TRANSACTION
										DECLARE @PurchDate SMALLDATETIME
										SET @PurchDate = GETDATE()

										INSERT INTO LoaderAds(AdID,UserID,TariffID,FixedViewCount,TargetViewCount,PurchaseDate)
										VALUES(@AdID,@UserID,@TariffID,@FixedViewCount,@TargetViewCount,@PurchDate)

										INSERT INTO TariffPayments(AdID,UserID,TariffID,PaymentDate,PaymentAmount)
										VALUES(@AdID,@UserID,@TariffID,@PurchDate,@PaymentAmount)

										UPDATE UserData
										SET Balance = Balance - @PaymentAmount
										WHERE ID = @UserID
										COMMIT;
									END";

            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = data.UserID;
            cmd.Parameters.Add("@AdID", SqlDbType.Int).Value = data.AdID;
            cmd.Parameters.Add("@TariffID", SqlDbType.Int).Value = data.TariffID;

            if (await cmd.ExecuteNonQueryAsync() == 3)
            {
                return new Result() { Status = CommonConstants.Ok, Message = CommonConstants.OkMessage };
            }

            return new Result() { Status = CommonConstants.PurchLoaderAdError, Message = CommonConstants.PurchLoaderAdMessageError };
        }
    }
}