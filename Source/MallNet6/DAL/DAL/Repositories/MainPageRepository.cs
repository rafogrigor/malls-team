using System.Data;
using DAL.Common;
using DAL.Interfaces;
using DAL.Models;
using DAL.SqlConnection;

namespace DAL.Repositories;
using System.Data.SqlClient;

public class MainPageRepository : IMainPage
{
    private readonly Manager dbService;

    public MainPageRepository(Manager dbService)
    {
        this.dbService = dbService;
    }
    public async Task<MainPageDal> GetMainPage(int countryID, CultureMode culture, Currency favCurrency)
    {
        MainPageDal mainPage = new MainPageDal();
        List<AdPartDal> adList = null;
        AdPartDal adPart;
        CategoryPartDal categoryPart;
        int lastCategory = -1;
        int temp;

        using (SqlConnection con = this.dbService.CreateConnection())
        using (SqlCommand cmd = con.CreateCommand())
        {
            cmd.Connection = con;
            cmd.CommandText = "MainPage";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@CountryID", SqlDbType.Int).Value = countryID;
            cmd.Parameters.Add("@Culture", SqlDbType.Int).Value = (int)culture;
            cmd.Parameters.Add("@FavoriteCurrency", SqlDbType.TinyInt).Value = (byte)favCurrency;
            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
            {
                int ordID = 0;
                int ordState = 0;
                int ordUserID = 0;
                int ordDescription = 0;
                int ordName = 0;
                int ordOwner = 0;
                int ordImages = 0;
                int ordCategoryID = 0;
                int ordCategoryName = 0;
                int ordIsBestPrice = 0;

                if (reader.HasRows)
                {
                    ordCategoryID = reader.GetOrdinal("CategoryID");
                    ordCategoryName = reader.GetOrdinal("CategoryName");
                    ordID = reader.GetOrdinal("ID");
                    ordState = reader.GetOrdinal("State");
                    ordUserID = reader.GetOrdinal("UserId");
                    ordDescription = reader.GetOrdinal("Description");
                    ordName = reader.GetOrdinal("Name");
                    ordOwner = reader.GetOrdinal("Owner");
                    ordImages = reader.GetOrdinal("Images");
                    ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                }

                while (reader.Read())
                {
                    temp = reader.GetInt32(ordCategoryID);

                    if (lastCategory != temp)
                    {
                        categoryPart = new CategoryPartDal()
                        {
                            ID = temp,
                            Name = reader.GetString(ordCategoryName)
                        };

                        lastCategory = temp;
                        adList = new List<AdPartDal>();

                        mainPage.AdsWithCategories.Add(new MinimizedAdDal()
                        {
                            Ads = adList,
                            Category = categoryPart
                        });
                    }

                    adPart = new AdPartDal()
                    {
                        ID = reader.GetInt32(ordID),
                        State = (AdState)reader.GetByte(ordState),
                        UserID = reader.GetInt32(ordUserID),
                        CategoryID = lastCategory,
                        IsBestPrice = reader.GetBoolean(ordIsBestPrice)
                    };

                    if (reader.IsDBNull(ordName))
                    {
                        adPart.Name = string.Empty;
                    }
                    else
                    {
                        adPart.Name = reader.GetString(ordName);
                    }

                    if (reader.IsDBNull(ordDescription))
                    {
                        adPart.Description = string.Empty;
                    }
                    else
                    {
                        adPart.Description = reader.GetString(ordDescription);
                    }

                    if (!reader.IsDBNull(ordOwner))
                    {
                        adPart.Owner = reader.GetByte(ordOwner);
                    }

                    if (!reader.IsDBNull(ordImages))
                    {
                        adPart.Image = reader.GetString(ordImages).Split(' ').FirstOrDefault();
                    }

                    adList.Add(adPart);
                }

                if (await reader.NextResultAsync())
                {
                    int ordSale = 0;
                    int ordBuy = 0;
                    int ordSymbol = 0;
                    int ordAdID = 0;

                    if (reader.HasRows)
                    {
                        ordSale = reader.GetOrdinal("SaleSummaRetail");
                        ordBuy = reader.GetOrdinal("BuySummaRetail");
                        ordSymbol = reader.GetOrdinal("CurrencySymbol");
                        ordUserID = reader.GetOrdinal("UserID");
                        ordAdID = reader.GetOrdinal("AdID");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                    }

                    while (reader.Read())
                    {
                        mainPage.BestExchange = new BestExchangeDal()
                        {
                            Sale = reader.GetDecimal(ordSale),
                            Buy = reader.GetDecimal(ordBuy),
                            Symbol = reader.GetString(ordSymbol),
                            UserID = reader.GetInt32(ordUserID),
                            AdID = reader.GetInt32(ordAdID),
                            IsBestPrice = reader.GetBoolean(ordIsBestPrice)
                        };
                    }
                }

                return mainPage;
            }
        }
    }

    public async Task<List<string>> GetPrompts(GetPromptDal searchPromptBody)
    {
        List<string> sameDescriptions = new List<string>();
        using (SqlConnection con = this.dbService.CreateConnection())
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;
            cmd.CommandText = "GetPrompts";
            cmd.Parameters.Add("@Symbol", SqlDbType.NVarChar).Value = searchPromptBody.Symbol;
            cmd.Parameters.Add("@CountryID", SqlDbType.Int).Value = searchPromptBody.CountryID;
            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
            {
                if (reader.HasRows)
                {
                    int ordDescription = reader.GetOrdinal("Name");
                    while (reader.Read())
                    {
                        string desc = reader.GetString(ordDescription);
                        if (!sameDescriptions.Contains(desc))
                        {
                            sameDescriptions.Add(desc);
                        }
                    }
                }

                return sameDescriptions;
            }

        }
    }

    public async Task<List<SearchModelDal>> Search(SearchGetDal searchBody)
    {
        List<SearchModelDal> result = new List<SearchModelDal>();
        using (SqlConnection con = this.dbService.CreateConnection())
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $@"SELECT main.ID as CategoryID,a.ID,UserId,[Description],
														[Name],[Owner],[Images]
									FROM Ads a 
										JOIN SubCategories AS sub ON sub.ID = a.SubCategoryId
										JOIN MainCategories main on main.ID = sub.BaseCategoryID
									WHERE ((a.IsRegional = 1 AND a.Country = @CountryID) 
															OR a.IsRegional = 0) {Environment.NewLine}";

            if (!string.IsNullOrWhiteSpace(searchBody.TextAM + searchBody.TextRU + searchBody.TextEN))
            {
                cmd.CommandText += " and (1 <> 1 ";
                if (!string.IsNullOrWhiteSpace(searchBody.TextAM))
                {
                    cmd.CommandText += $@" OR (UPPER(a.[Description])  LIKE UPPER(@TextAM) + '%' OR UPPER(a.[Description])  LIKE '%' + UPPER(@TextAM) + '%'   OR
											   UPPER(a.[Name])	     LIKE UPPER(@TextAM) + '%' OR UPPER(a.[Name])	  LIKE '%' + UPPER(@TextAM) + '%'   OR
											   UPPER(a.[Tags])		 LIKE UPPER(@TextAM) + '%' OR UPPER(a.[Tags])		  LIKE '%' + UPPER(@TextAM) + '%') {Environment.NewLine}";
                    cmd.Parameters.Add("@TextAM", SqlDbType.NVarChar).Value = searchBody.TextAM;
                }

                if (!string.IsNullOrWhiteSpace(searchBody.TextEN))
                {
                    cmd.CommandText += $@"OR (UPPER(a.[Description])  LIKE UPPER(@TextEN) + '%' OR UPPER(a.[Description])  LIKE '%' + UPPER(@TextEN) + '%'   OR
										       UPPER(a.[Name])		 LIKE UPPER(@TextEN) + '%' OR UPPER(a.[Name])	  LIKE '%' + UPPER(@TextEN) + '%'   OR
										       UPPER(a.[Tags])		 LIKE UPPER(@TextEN) + '%' OR UPPER(a.[Tags])		  LIKE '%' + UPPER(@TextEN) + '%') ";
                    cmd.Parameters.Add("@TextEN", SqlDbType.VarChar).Value = searchBody.TextEN;
                }

                if (!string.IsNullOrWhiteSpace(searchBody.TextRU))
                {
                    cmd.CommandText += $@" OR (UPPER(a.[Description])  LIKE UPPER(@TextRU) + '%' OR UPPER(a.[Description])  LIKE '%' + UPPER(@TextRU) + '%'   OR
												UPPER(a.[Name])         LIKE UPPER(@TextRU) + '%' OR UPPER(a.[Name])     LIKE '%' + UPPER(@TextRU) + '%'   OR
												UPPER(a.[Tags])         LIKE UPPER(@TextRU) + '%' OR UPPER(a.[Tags])     LIKE '%' + UPPER(@TextRU) + '%') {Environment.NewLine}";
                    cmd.Parameters.Add("@TextRU", SqlDbType.NVarChar).Value = searchBody.TextRU;
                }

                cmd.CommandText += ")";
            }
            else
            {
                return null;
            }

            cmd.Parameters.Add("@CountryID", SqlDbType.Int).Value = searchBody.CountryID;

            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
            {
                if (!reader.HasRows)
                {
                    return null;
                }

                int ordID = reader.GetOrdinal("ID");
                int ordCategoryID = reader.GetOrdinal("CategoryID");
                int ordUserId = reader.GetOrdinal("UserId");
                int ordDescription = reader.GetOrdinal("Description");
                int ordName = reader.GetOrdinal("Name");
                int ordOwner = reader.GetOrdinal("Owner");
                int ordImage = reader.GetOrdinal("Images");

                while (reader.Read())
                {
                    SearchModelDal search = new SearchModelDal()
                    {
                        ID = reader.GetInt32(ordID),
                        CategoryID = reader.GetInt32(ordCategoryID),
                        UserID = reader.GetInt32(ordUserId),
                        Name = reader.GetString(ordName),
                        Description = reader.GetString(ordDescription),
                        Owner = (AdOwner)reader.GetByte(ordOwner),
                        Image = reader.GetString(ordImage).Split(' ').FirstOrDefault()
                    };

                    result.Add(search);
                }

                return result;
            }
        }
    }
}