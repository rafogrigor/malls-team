using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using DAL.Common;
using DAL.Interfaces;
using DAL.Models;
using DAL.SqlConnection;

namespace DAL.Repositories;

using System.Data.SqlClient;
public class CategoriesRepository : ICategories
{
    private readonly Manager dbService;

    public CategoriesRepository(Manager dbService)
    {
        this.dbService = dbService;
    }
    public async Task<List<MainCategoryDal>> GetMainCategoriesAsync(CultureMode culture, int categoryID = -1)
    {
        using (SqlConnection con = this.dbService.CreateConnection())
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT [ID]";

            switch (culture)
            {
                case CultureMode.RU:
                    cmd.CommandText += ",[CategoryRU]";
                    break;
                case CultureMode.EN:
                    cmd.CommandText += ",[CategoryEN]";
                    break;
                case CultureMode.AM:
                default:
                    cmd.CommandText += ",[CategoryAM]";
                    break;
            }

            cmd.CommandText += Environment.NewLine;
            cmd.CommandText += $" FROM [dbo].[MainCategories] {Environment.NewLine}";
            if (categoryID != -1)
            {
                cmd.CommandText += $"Where [ID] = {categoryID} {Environment.NewLine}";
            }
            try
            {
                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    List<MainCategoryDal> result = new List<MainCategoryDal>();
                    if (reader.HasRows)
                    {
                        int ordID = reader.GetOrdinal("ID");
                        int ordCategory;

                        switch (culture)
                        {
                            case CultureMode.RU:
                                ordCategory = reader.GetOrdinal("CategoryRU");
                                break;
                            case CultureMode.EN:
                                ordCategory = reader.GetOrdinal("CategoryEN");
                                break;
                            case CultureMode.AM:
                            default:
                                ordCategory = reader.GetOrdinal("CategoryAM");
                                break;
                        }
                        while (reader.Read())
                        {
                            result.Add
                                       (
                                       new MainCategoryDal
                                       {
                                           ID = reader.GetInt32(ordID),
                                           Category = reader.GetString(ordCategory)
                                       }
                                       );
                        }
                    }
                    return result;
                }

            }
            catch
            {
                return new List<MainCategoryDal>();
            }
        }
    }

    public async Task<List<SubCategoryDal>> GetSubCategoriesAsync(CultureMode culture, int baseCategoryID = -1)
    {
        using (SqlConnection con = this.dbService.CreateConnection())
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT [ID],[BaseCategoryID]";
            switch (culture)
            {
                case CultureMode.RU:
                    cmd.CommandText += ",[TypeRU]";
                    break;
                case CultureMode.EN:
                    cmd.CommandText += ",[TypeEN]";
                    break;
                case CultureMode.AM:
                default:
                    cmd.CommandText += ",[TypeAM]";
                    break;
            }
            cmd.CommandText += $"{Environment.NewLine} FROM [dbo].[SubCategories] {Environment.NewLine}";
            if (baseCategoryID != -1)
            {
                cmd.CommandText += $" WHERE [BaseCategoryId] = @BaseCategoryId {Environment.NewLine}";
                cmd.Parameters.Add("@BaseCategoryId", SqlDbType.Int).Value = baseCategoryID;
            }
            try
            {
                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    List<SubCategoryDal> result = new List<SubCategoryDal>();
                    if (reader.HasRows)
                    {
                        int ordID = reader.GetOrdinal("ID");
                        int ordBaseCatID = reader.GetOrdinal("BaseCategoryId");
                        int ordType;

                        switch (culture)
                        {
                            case CultureMode.RU:
                                ordType = reader.GetOrdinal("TypeRU");
                                break;
                            case CultureMode.EN:
                                ordType = reader.GetOrdinal("TypeEN");
                                break;
                            case CultureMode.AM:
                            default:
                                ordType = reader.GetOrdinal("TypeAM");
                                break;
                        }
                        while (reader.Read())
                        {
                            result.Add
                                       (
                                       new SubCategoryDal
                                       {
                                           ID = reader.GetInt32(ordID),
                                           Type = reader.GetString(ordType),
                                           BaseCategoryID = reader.GetInt32(ordBaseCatID)
                                       }
                                       );
                        }
                    }
                    return result;
                }
            }
            catch
            {
                return new List<SubCategoryDal>();
            }
        }
    }
}