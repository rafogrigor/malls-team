using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using DAL.Common;
using DAL.Extensions;
using DAL.Interfaces;
using DAL.Models;
using DAL.SqlConnection;

namespace DAL.Repositories;
using System.Data.SqlClient;

public class AdsRepository : IAds
{
    private readonly Manager dbService;

    public AdsRepository(Manager dbService)
    {
        this.dbService = dbService;
    }
    #region Create Ad
    /// <summary>
    /// Creates a BaseSQLCommand Aim All Categories
    /// </summary>
    /// <param name="baseModel"></param>
    /// <returns>SQLCommand</returns>
    public SqlCommand CreateBasePart(BaseAdDal baseModel)
    {
        string columnNames = "[UserId],[SubCategoryId],[Date],[UpdateDate],[Price],[State],[Country],[Region],[City],[TableName],[Currency]";
        string columnValues = $"@UserId,@SubCategoryId,@Date,@UpdateDate,@Price,@State,@Country,@Region,@City,@TableName,@Currency";

        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = baseModel.UserID;
            cmd.Parameters.Add("@SubCategoryId", SqlDbType.Int).Value = baseModel.SubCategoryID;
            cmd.Parameters.Add("@Date", SqlDbType.SmallDateTime).Value = DateTime.UtcNow;
            cmd.Parameters.Add("@UpdateDate", SqlDbType.SmallDateTime).Value = DateTime.UtcNow;
            cmd.Parameters.Add("@Price", SqlDbType.Money).Value = baseModel.Price;
            cmd.Parameters.Add("@State", SqlDbType.TinyInt).Value = baseModel.State;
            cmd.Parameters.Add("@Country", SqlDbType.Int).Value = baseModel.Country;
            cmd.Parameters.Add("@Region", SqlDbType.Int).Value = baseModel.Region;
            cmd.Parameters.Add("@City", SqlDbType.Int).Value = baseModel.City;
            cmd.Parameters.Add("@TableName", SqlDbType.VarChar).Value = baseModel.AdType;
            cmd.Parameters.Add("@Currency", SqlDbType.TinyInt).Value = baseModel.Currency;
            cmd.Parameters.Add("@CreatedAdID", SqlDbType.Int).Direction = ParameterDirection.Output;

            if (baseModel.IsRegional.HasValue)
            {
                columnNames += ",[IsRegional]";
                columnValues += ",@IsRegional";
                cmd.Parameters.Add("@IsRegional", SqlDbType.Bit).Value = baseModel.IsRegional.Value;
            }

            if (!string.IsNullOrWhiteSpace(baseModel.Description))
            {
                columnNames += ",[Description]";
                columnValues += ",@Description";
                cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = baseModel.Description;
            }

            if (!string.IsNullOrWhiteSpace(baseModel.Name))
            {
                columnNames += ",[Name]";
                columnValues += ",@Name";
                cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = baseModel.Name;
            }

            if (baseModel.LocationLatitude.HasValue)
            {
                columnNames += ",[Location]";
                columnValues += $",geography::Point(@Latitude, @Longitude, {CommonConstants.GeoNum})";
                cmd.Parameters.Add("@Latitude", SqlDbType.Decimal).Value = baseModel.LocationLatitude.Value;
                cmd.Parameters.Add("@Longitude", SqlDbType.Decimal).Value = baseModel.LocationLongitude.Value;
            }

            if (!string.IsNullOrWhiteSpace(baseModel.Contact))
            {
                columnNames += ",[Contact]";
                columnValues += ",@Contact";
                cmd.Parameters.Add("@Contact", SqlDbType.VarChar).Value = baseModel.Contact;
            }

            if (baseModel.Owner.HasValue)
            {
                columnNames += ",[Owner]";
                columnValues += ",@Owner";
                cmd.Parameters.Add("@Owner", SqlDbType.TinyInt).Value = baseModel.Owner;
            }

            if (baseModel.Aim.HasValue)
            {
                columnNames += ",[Aim]";
                columnValues += ",@Aim";
                cmd.Parameters.Add("@Aim", SqlDbType.TinyInt).Value = baseModel.Aim;
            }

            if (!string.IsNullOrWhiteSpace(baseModel.Tags))
            {
                columnNames += ",[Tags]";
                columnValues += ",@Tags";
                cmd.Parameters.Add("@Tags", SqlDbType.NVarChar).Value = baseModel.Tags;
            }

            if (baseModel.ImagesList.Count != 0)
            {
                columnNames += ",[Images]";
                columnValues += ",@Images";
                cmd.Parameters.Add("@Images", SqlDbType.NVarChar).Value = string.Join(" ", baseModel.ImagesList);
            }

            cmd.CommandText += $@"BEGIN TRANSACTION
									  DECLARE @TariffID INT,
											  @Amount MONEY,
											  @Balance MONEY				
	
								      INSERT INTO Ads({columnNames})
                                      VALUES({columnValues}) 
									
									  SET @CreatedAdID = SCOPE_IDENTITY()									

									  IF(@State IN (1,2))
									  BEGIN										
										  SELECT @TariffID = t.ID,@Amount = t.SalePrice, @Balance = u.Balance 
										  FROM Tariffs t join UserData u on t.ID = @State and u.ID = @UserID
									
										  IF(@Balance >= @Amount)
										  BEGIN
											  INSERT INTO TariffPayments([AdID],[UserID],[TariffID],[PaymentDate],[PaymentAmount])
											  VALUES(@CreatedAdID,@UserID,@TariffID,GETDATE(),@Amount)

											  UPDATE UserData
											  SET Balance = Balance - @Amount
											  WHERE ID = @UserID
										  END
									  END {Environment.NewLine}";

            return cmd;
        }
    }

    #endregion

    public async Task<AdCreateResponse> CreateAd(AcquaintanceDal adModel)
    {
        string colNames;
        string values;

        using (SqlConnection con = this.dbService.CreateConnection())
        {

            adModel.AdType = AdType.Acquaintance;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;

                colNames = "AdID,AcquaintanceAim,Age";
                values = $"@CreatedAdID,@Gender,@AcquaintanceAim,@Age";
                cmd.Parameters.Add("@Gender", SqlDbType.Bit).Value = adModel.Gender;
                cmd.Parameters.Add("@AcquaintanceAim", SqlDbType.TinyInt).Value = adModel.AcquaintanceAim;
                cmd.Parameters.Add("@Age", SqlDbType.Int).Value = adModel.Age;

                if (adModel.Gender.HasValue)
                {
                    colNames += ",Gender";
                    values += ",@Gender";
                    cmd.Parameters.Add("@Gender", SqlDbType.Int).Value = adModel.Gender.Value;
                }

                if (adModel.Height.HasValue)
                {
                    colNames += ",Height";
                    values += ",@Height";
                    cmd.Parameters.Add("@Height", SqlDbType.Int).Value = adModel.Height.Value;
                }

                if (adModel.Weight.HasValue)
                {
                    colNames += ",Weight";
                    values += ",@Weight";
                    cmd.Parameters.Add("@Weight", SqlDbType.Int).Value = adModel.Weight.Value;
                }

                cmd.CommandText += $"INSERT INTO Acquaintance({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";
                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;

                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }
    public async Task<AdCreateResponse> CreateAd(AllForHomeAndGardenDal adModel)
    {
        string colNames;
        string values;
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            adModel.AdType = AdType.AllForHomeAndGarden;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }
                cmd.Connection = con;
                colNames = "AdID,ProductState";
                values = $"@CreatedAdID,@ProductState";
                cmd.Parameters.Add("@ProductState", SqlDbType.TinyInt).Value = adModel.ProductState;

                cmd.CommandText += $"INSERT INTO AllForHomeAndGarden({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;

                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }

    public async Task<AdCreateResponse> CreateAd(ApplianceDal adModel)
    {
        string colNames;
        string values;
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            adModel.AdType = AdType.Appliances;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;
                colNames = "AdID,ProductState";
                values = $"@CreatedAdID,@ProductState";
                cmd.Parameters.Add("@ProductState", SqlDbType.TinyInt).Value = adModel.ProductState;

                cmd.CommandText += $"INSERT INTO Appliances({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;
                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }
    public async Task<AdCreateResponse> CreateAd(CigaretteAndAlcoholDal adModel)
    {
        string colNames;
        string values;
        using (SqlConnection con = this.dbService.CreateConnection())
        {

            adModel.AdType = AdType.CigaretteAndAlcohol;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;

                colNames = "AdID,ProductState,IsLocal";
                values = $"@CreatedAdID,@ProductState,@IsLocal";
                cmd.Parameters.Add("@ProductState", SqlDbType.TinyInt).Value = adModel.ProductState;
                cmd.Parameters.Add("@IsLocal", SqlDbType.Bit).Value = adModel.IsLocal;

                if (adModel.AlcoholVolume.HasValue)
                {
                    colNames += ",AlcoholVolume";
                    values += ",@AlcoholVolume";
                    cmd.Parameters.Add("@AlcoholVolume", SqlDbType.TinyInt).Value = adModel.AlcoholVolume.Value;
                }

                cmd.CommandText += $"INSERT INTO CigaretteAndAlcohol({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT";

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;

                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }

    public async Task<AdCreateResponse> CreateAd(ClothesAndShoesDal adModel)
    {
        string colNames;
        string values;
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            adModel.AdType = AdType.ClothesAndShoes;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con; colNames = "AdID,ProductState";
                values = $"@CreatedAdID,@ProductState";
                cmd.Parameters.Add("@ProductState", SqlDbType.TinyInt).Value = adModel.ProductState;

                if (!string.IsNullOrWhiteSpace(adModel.ClothingSize))
                {
                    colNames += ",ClothingSize";
                    values += ",@ClothingSize";
                    cmd.Parameters.Add("@ClothingSize", SqlDbType.VarChar).Value = adModel.ClothingSize;
                }

                if (adModel.ShoesSize.HasValue)
                {
                    colNames += ",ShoesSize";
                    values += ",@ShoesSize";
                    cmd.Parameters.Add("@ShoesSize", SqlDbType.TinyInt).Value = adModel.ShoesSize;
                }

                cmd.CommandText += $"INSERT INTO ClothesAndShoes({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;

                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }
    public async Task<AdCreateResponse> CreateAd(ConstructionDal adModel)
    {
        string colNames;
        string values;
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            adModel.AdType = AdType.Construction;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;

                colNames = "AdID,ProductState";
                values = $"@CreatedAdID,@ProductState";
                cmd.Parameters.Add("@ProductState", SqlDbType.TinyInt).Value = adModel.ProductState;

                cmd.CommandText += $"INSERT INTO Construction({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;

                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }
    public async Task<AdCreateResponse> CreateAd(CultureDal adModel)
    {
        string colNames;
        string values;
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            adModel.AdType = AdType.Culture;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;
                colNames = "AdID,ProductState";
                values = $"@CreatedAdID,@ProductState";
                cmd.Parameters.Add("@ProductState", SqlDbType.TinyInt).Value = adModel.ProductState;

                cmd.CommandText += $"INSERT INTO Culture({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";
                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;

                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }
    public async Task<AdCreateResponse> CreateAd(ElectronicsDal adModel)
    {
        string colNames;
        string values;
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            adModel.AdType = AdType.Electronics;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;
                colNames = "AdID";
                values = $"@CreatedAdID";


                cmd.CommandText += $"INSERT INTO Electronics({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;

                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }
    public async Task<AdCreateResponse> CreateAd(EverythingElseDal adModel)
    {
        string colNames;
        string values;
        using (SqlConnection con = this.dbService.CreateConnection())
        {

            adModel.AdType = AdType.EverythingElse;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;

                colNames = "AdID";
                values = $"@CreatedAdID";

                cmd.CommandText += $"INSERT INTO EverythingElse({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";
                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;

                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }
    public async Task<AdCreateResponse> CreateAd(ForChildrenDal adModel)
    {
        string colNames;
        string values;
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            adModel.AdType = AdType.ForChildren;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;

                colNames = "AdID,ForNewBorns,ProductState";
                values = $"@CreatedAdID,@ForNewBorns,@ProductState";

                cmd.Parameters.Add("@ForNewBorns", SqlDbType.Bit).Value = adModel.ForNewBorns;
                cmd.Parameters.Add("@ProductState", SqlDbType.TinyInt).Value = adModel.ProductState;

                if (adModel.Gender.HasValue)
                {
                    colNames += ",Gender";
                    values += ",@Gender";
                    cmd.Parameters.Add("@Gender", SqlDbType.Bit).Value = adModel.Gender;
                }

                cmd.CommandText += $"INSERT INTO ForChildren({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;

                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }
    public async Task<AdCreateResponse> CreateAd(FurnitureDal adModel)
    {
        string colNames;
        string values;
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            adModel.AdType = AdType.Furniture;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;

                colNames = "AdID,ProductState";
                values = $"@CreatedAdID,@ProductState";
                cmd.Parameters.Add("@ProductState", SqlDbType.TinyInt).Value = adModel.ProductState;

                cmd.CommandText += $"INSERT INTO Furniture({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";
                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;

                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }
    public async Task<AdCreateResponse> CreateAd(HealfCareDal adModel)
    {
        string colNames;
        string values;
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            adModel.AdType = AdType.HealfCare;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;

                colNames = "AdID,ProductState";
                values = $"@CreatedAdID,@ProductState";
                cmd.Parameters.Add("@ProductState", SqlDbType.TinyInt).Value = adModel.ProductState;

                cmd.CommandText += $"INSERT INTO HealfCare({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;

                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }
    public async Task<AdCreateResponse> CreateAd(HouseholdGoodDal adModel)
    {
        string colNames;
        string values;
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            adModel.AdType = AdType.HouseholdGoods;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;
                colNames = "AdID,ProductState";
                values = $"@CreatedAdID,@ProductState";
                cmd.Parameters.Add("@ProductState", SqlDbType.TinyInt).Value = adModel.ProductState;

                cmd.CommandText += $"INSERT INTO HouseholdGoods({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;

                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }
    public async Task<AdCreateResponse> CreateAd(JewerlyAndAccessoriesDal adModel)
    {
        string colNames;
        string values;
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            adModel.AdType = AdType.JewerlyAndAccessories;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;
                colNames = "AdID,ProductState";
                values = $"@CreatedAdID,@ProductState";
                cmd.Parameters.Add("@ProductState", SqlDbType.TinyInt).Value = adModel.ProductState;

                if (adModel.Gender.HasValue)
                {
                    colNames += ",Gender";
                    values += ",@Gender";
                    cmd.Parameters.Add("@Gender", SqlDbType.Bit).Value = adModel.Gender;
                }

                cmd.CommandText += $"INSERT INTO JewerlyAndAccessories({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;

                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }
    public async Task<AdCreateResponse> CreateAd(PetsAndPlantsDal adModel)
    {
        string colNames;
        string values;
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            adModel.AdType = AdType.PetsAndPlants;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;
                colNames = "AdID,ProductState";
                values = $"@CreatedAdID,@ProductState";
                cmd.Parameters.Add("@ProductState", SqlDbType.TinyInt).Value = adModel.ProductState;

                cmd.CommandText += $"INSERT INTO PetsAndPlants({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;

                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }
    public async Task<AdCreateResponse> CreateAd(ProductsAndDrinksDal adModel)
    {
        string colNames;
        string values;
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            adModel.AdType = AdType.ProductsAndDrinks;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;
                colNames = "AdID,IsLocal";
                values = $"@CreatedAdID,@IsLocal";
                cmd.Parameters.Add("@IsLocal", SqlDbType.Bit).Value = adModel.IsLocal;

                if (adModel.IsFrozen.HasValue)
                {
                    colNames += ",IsFrozen";
                    values += ",@IsFrozen";
                    cmd.Parameters.Add("@IsFrozen", SqlDbType.Bit).Value = adModel.IsFrozen;
                }

                if (adModel.IsNaturalDrink.HasValue)
                {
                    colNames += ",IsNaturalDrink";
                    values += ",@IsNaturalDrink";
                    cmd.Parameters.Add("@IsNaturalDrink", SqlDbType.Bit).Value = adModel.IsNaturalDrink;
                }
                cmd.CommandText += $"INSERT INTO [ProductsAndDrinks]({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;

                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }
    public async Task<AdCreateResponse> CreateAd(RealEstateDal adModel)
    {
        string colNames;
        string values;
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            adModel.AdType = AdType.RealEstate;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;
                colNames = "AdID,Space";
                values = $"@CreatedAdID,@Space";
                cmd.Parameters.Add("@Space", SqlDbType.SmallInt).Value = adModel.Space;
                if (adModel.Rooms.HasValue)
                {
                    colNames += ",Rooms";
                    values += ",@Rooms";
                    cmd.Parameters.Add("@Rooms", SqlDbType.TinyInt).Value = adModel.Rooms;
                }
                if (adModel.Floor.HasValue)
                {
                    colNames += ",Floor";
                    values += ",@Floor";
                    cmd.Parameters.Add("@Floor", SqlDbType.TinyInt).Value = adModel.Floor;
                }
                if (adModel.ConstructionType.HasValue)
                {
                    colNames += ",ConstructionType";
                    values += ",@ConstructionType";
                    cmd.Parameters.Add("@ConstructionType", SqlDbType.TinyInt).Value = adModel.ConstructionType;
                }
                if (adModel.PaymentTime.HasValue)
                {
                    colNames += ",PaymentTime";
                    values += ",@PaymentTime";
                    cmd.Parameters.Add("@PaymentTime", SqlDbType.TinyInt).Value = adModel.PaymentTime;
                }
                cmd.CommandText += $"INSERT INTO RealEstate({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;
                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }

    public async Task<AdCreateResponse> CreateAd(ServicesDal adModel)
    {
        string colNames;
        string values;
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            adModel.AdType = AdType.Services;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;
                colNames = "AdID";
                values = $"@CreatedAdID";
                if (adModel.Transportation.HasValue)
                {
                    colNames += ",Transportation";
                    values += ",@Transportation";
                    cmd.Parameters.Add("@Transportation", SqlDbType.Bit).Value = adModel.Transportation.Value;
                }
                if (adModel.PaymentTime.HasValue)
                {
                    colNames += ",PaymentTime";
                    values += ",@PaymentTime";
                    cmd.Parameters.Add("@PaymentTime", SqlDbType.TinyInt).Value = adModel.PaymentTime;
                }
                cmd.CommandText += $"INSERT INTO Services({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch (Exception)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;

                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }
    public async Task<AdCreateResponse> CreateAd(TourismAndRestDal adModel)
    {
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            adModel.AdType = AdType.TourismAndRest;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;
                cmd.Parameters.Add("@DepartureDay", SqlDbType.DateTime2).Value = adModel.DepartureDay;
                cmd.Parameters.Add("@ReturnDay", SqlDbType.DateTime2).Value = adModel.ReturnDay;
                cmd.Parameters.Add("@ReservedTickets", SqlDbType.SmallInt).Value = adModel.ReservedTickets;

                cmd.CommandText += $@"INSERT INTO TourismAndRest([AdID],[DepartureDay],[ReturnDay],[ReservedTickets])
                                          VALUES(@CreatedAdID,@DepartureDay,@ReturnDay,@ReservedTickets)
                                          COMMIT; {Environment.NewLine}";

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;

                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }
    public async Task<AdCreateResponse> CreateAd(VehicleDal adModel)
    {
        string colNames;
        string values;

        using (SqlConnection con = this.dbService.CreateConnection())
        {
            adModel.AdType = AdType.Vehicle;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;

                colNames = "AdID";
                values = $"@CreatedAdID";

                if (adModel.Mark.HasValue)
                {
                    colNames += ",Mark";
                    values += ",@Mark";
                    cmd.Parameters.Add("@Mark", SqlDbType.Int).Value = adModel.Mark.Value;
                }

                if (adModel.Model.HasValue)
                {
                    colNames += ",Model";
                    values += ",@Model";
                    cmd.Parameters.Add("@Model", SqlDbType.Int).Value = adModel.Model.Value;
                }

                if (adModel.ProductionYear.HasValue)
                {
                    colNames += ",ProductionYear";
                    values += ",@ProductionYear";
                    cmd.Parameters.Add("@ProductionYear", SqlDbType.SmallDateTime).Value = adModel.ProductionYear.Value;
                }

                if (adModel.CustomsCleared.HasValue)
                {
                    colNames += ",CustomsCleared";
                    values += ",@CustomsCleared";
                    cmd.Parameters.Add("@CustomsCleared", SqlDbType.Bit).Value = adModel.CustomsCleared.Value;
                }

                if (adModel.Mileage.HasValue)
                {
                    colNames += ",Mileage";
                    values += ",@Mileage";
                    cmd.Parameters.Add("@Mileage", SqlDbType.Int).Value = adModel.Mileage.Value;
                }

                if (adModel.BodyType.HasValue)
                {
                    colNames += ",BodyType";
                    values += ",@BodyType";
                    cmd.Parameters.Add("@BodyType", SqlDbType.Int).Value = adModel.BodyType.Value;
                }

                if (adModel.EngineType.HasValue)
                {
                    colNames += ",EngineType";
                    values += ",@EngineType";
                    cmd.Parameters.Add("@EngineType", SqlDbType.Int).Value = adModel.EngineType.Value;
                }

                if (adModel.EngineSize.HasValue)
                {
                    colNames += ",EngineSize";
                    values += ",@EngineSize";
                    cmd.Parameters.Add("@EngineSize", SqlDbType.Int).Value = adModel.EngineSize.Value;
                }

                if (adModel.DriveType.HasValue)
                {
                    colNames += ",DriveType";
                    values += ",@DriveType";
                    cmd.Parameters.Add("@DriveType", SqlDbType.Int).Value = adModel.DriveType.Value;
                }

                if (adModel.TransmissionType.HasValue)
                {
                    colNames += ",TransmissionType";
                    values += ",@TransmissionType";
                    cmd.Parameters.Add("@TransmissionType", SqlDbType.Int).Value = adModel.TransmissionType.Value;
                }

                if (adModel.Color.HasValue)
                {
                    colNames += ",Color";
                    values += ",@Color";
                    cmd.Parameters.Add("@Color", SqlDbType.Int).Value = adModel.Color.Value;
                }

                if (adModel.Wheel.HasValue)
                {
                    colNames += ",Wheel";
                    values += ",@Wheel";
                    cmd.Parameters.Add("@Wheel", SqlDbType.Int).Value = adModel.Wheel.Value;
                }

                cmd.CommandText += $"INSERT INTO Vehicle({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";
                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;

                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }
    public async Task<AdCreateResponse> CreateAd(WorkDal adModel)
    {
        string colNames;
        string values;
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            adModel.AdType = AdType.Work;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;
                colNames = "AdID";
                values = $"@CreatedAdID";


                if (adModel.PaymentTime.HasValue)
                {
                    colNames += ",PaymentTime";
                    values += ",@PaymentTime";
                    cmd.Parameters.Add("@PaymentTime", SqlDbType.TinyInt).Value = adModel.PaymentTime;
                }

                cmd.CommandText += $"INSERT INTO Work({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;

                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }

    public async Task<AdCreateResponse> CreateAd(SportDal adModel)
    {
        string colNames;
        string values;
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            adModel.AdType = AdType.Sport;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;
                colNames = "AdID,ProductState";
                values = $"@CreatedAdID,@ProductState";
                cmd.Parameters.Add("@ProductState", SqlDbType.TinyInt).Value = adModel.ProductState;

                cmd.CommandText += $"INSERT INTO Sport({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;
                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }
    public async Task<AdCreateResponse> CreateAd(ExchangeDal adModel)
    {
        string colNames;
        string values;
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            adModel.AdType = AdType.Exchange;

            PurchaseTariffDal PurchModel = new PurchaseTariffDal() { UserID = adModel.UserID, TariffID = (int)adModel.State };

            if ((adModel.State == AdState.Urgent || adModel.State == AdState.Vip) && !(await CheckBalance(con, PurchModel)))
            {
                return new AdCreateResponse()
                {
                    ID = -1,

                    Result = new Result()
                    {
                        Status = CommonConstants.NoEnoughMoney,
                        Message = CommonConstants.NoEnoughMoneyMessage
                    }
                };
            }

            using (SqlCommand cmd = CreateBasePart(adModel))
            {
                if (cmd == null)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLCommandCreatingError,
                            Message = CommonConstants.SQLCommandCreatingErrorMessage
                        }
                    };
                }

                cmd.Connection = con;

                colNames = "[AdID],[SourceCurrencyID],[DestinationCurrencyID],[SaleSummaRetail],[BuySummaRetail]";
                values = $"@CreatedAdID,@SourceCurrencyID,@DestinationCurrencyID,@SaleSummaRetail,@BuySummaRetail";
                cmd.Parameters.Add("@SourceCurrencyID", SqlDbType.SmallInt).Value = adModel.SourceCurrencyID;
                cmd.Parameters.Add("@DestinationCurrencyID", SqlDbType.SmallInt).Value = adModel.DestinationCurrencyID;
                cmd.Parameters.Add("@SaleSummaRetail", SqlDbType.Money).Value = adModel.SaleSummaRetail;
                cmd.Parameters.Add("@BuySummaRetail", SqlDbType.Money).Value = adModel.BuySummaRetail;

                if (adModel.SaleSumma.HasValue && adModel.BuySumma.HasValue)
                {
                    colNames += ",[SaleSumma],[BuySumma]";
                    values += ",@SaleSumma,@BuySumma";
                    cmd.Parameters.Add("@SaleSumma", SqlDbType.Money).Value = adModel.SaleSumma;
                    cmd.Parameters.Add("@BuySumma", SqlDbType.Money).Value = adModel.BuySumma;
                }
                cmd.CommandText += $"INSERT INTO Exchange({colNames}) {Environment.NewLine}";
                cmd.CommandText += $"VALUES({values}) {Environment.NewLine}";
                cmd.CommandText += " COMMIT;";
                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch (Exception)
                {
                    return new AdCreateResponse()
                    {
                        ID = -1,

                        Result = new Result()
                        {
                            Status = CommonConstants.SQLException,
                            Message = CommonConstants.SQLOtherError
                        }
                    };
                }

                int result = int.TryParse(cmd.Parameters["@CreatedAdId"].Value.ToString(), out result) ? result : CommonConstants.BadID;

                return new AdCreateResponse()
                {
                    ID = result,

                    Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    }
                };
            }
        }
    }

    #region GetingData
    public async Task<FavoriteAds> GetUserFavoriteAds(int userID, CultureMode culture)
    {
        using (SqlConnection con = this.dbService.CreateConnection())
        using (SqlCommand cmd = con.CreateCommand())
        {
            cmd.Connection = con;
            cmd.CommandText = $@"SELECT main.ID as CategoryID,a.ID,a.UserId,[Description],
											[Name],[Owner],[Images],a.[State],a.[UpdateDate],
												CASE
													WHEN @Culture = 0 THEN main.CategoryAM
													WHEN @Culture = 1 THEN main.CategoryRU
													WHEN @Culture = 2 THEN main.CategoryEN
												END as CategoryName
									FROM Ads a JOIN FavoriteAds f ON a.ID = f.AdID AND f.UserID = @UserID
									JOIN SubCategories AS sub ON sub.ID = a.SubCategoryID
															JOIN MainCategories main ON main.ID = sub.BaseCategoryID";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("@Culture", SqlDbType.Int).Value = (int)culture;
            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = userID;
            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
            {

                if (!reader.HasRows)
                {
                    return new FavoriteAds()
                    {
                        Result = new Result()
                        {
                            Status = CommonConstants.NoContent,
                            Message = CommonConstants.NoContentMessage
                        }
                    };
                }

                int ordCategoryID = reader.GetOrdinal("CategoryID");
                int ordCategoryName = reader.GetOrdinal("CategoryName");
                int ordID = reader.GetOrdinal("ID");
                int ordUserID = reader.GetOrdinal("UserId");
                int ordDescription = reader.GetOrdinal("Description");
                int ordName = reader.GetOrdinal("Name");
                int ordOwner = reader.GetOrdinal("Owner");
                int ordImages = reader.GetOrdinal("Images");
                //Result
                FavoriteAds list = new FavoriteAds();
                int lastCategory = -1;
                List<AdPartDal> adList = null;
                AdPartDal adPart;
                CategoryPartDal categoryPart;

                while (reader.Read())
                {
                    int temp = reader.GetInt32(ordCategoryID);

                    if (lastCategory != temp)
                    {
                        categoryPart = new CategoryPartDal()
                        {
                            ID = temp,
                            Name = reader.GetString(ordCategoryName)
                        };

                        lastCategory = temp;
                        adList = new List<AdPartDal>();

                        list.FavoriteList.Add(new MinimizedAdDal()
                        {
                            Ads = adList,
                            Category = categoryPart
                        });
                    }

                    adPart = new AdPartDal()
                    {
                        ID = reader.GetInt32(ordID),
                        UserID = reader.GetInt32(ordUserID),
                        CategoryID = lastCategory
                    };

                    if (reader.IsDBNull(ordName))
                    {
                        adPart.Name = string.Empty;
                    }
                    else
                    {
                        adPart.Name = reader.GetString(ordName);
                    }

                    if (reader.IsDBNull(ordDescription))
                    {
                        adPart.Description = string.Empty;
                    }
                    else
                    {
                        adPart.Description = reader.GetString(ordDescription);
                    }

                    if (!reader.IsDBNull(ordOwner))
                    {
                        adPart.Owner = reader.GetByte(ordOwner);
                    }

                    if (reader.IsDBNull(ordImages))
                    {
                        adPart.Image = string.Empty;
                    }
                    else
                    {
                        adPart.Image = reader.GetString(ordImages).Split(' ').FirstOrDefault();
                    }

                    adList.Add(adPart);
                }

                list.Result = new Result()
                {
                    Status = CommonConstants.Ok,
                    Message = CommonConstants.OkMessage
                };
                list.FavoriteList.Reverse();

                return list;
            }
        }
    }

    public async Task<UserAds> GetUserAds(int userID, CultureMode culture)
    {
        using (SqlConnection con = this.dbService.CreateConnection())
        using (SqlCommand cmd = con.CreateCommand())
        {
            cmd.Connection = con;
            cmd.CommandText = $@"SELECT main.ID as CategoryID,a.ID,a.UserId,[Description],
											[Name],[Owner],[Images],a.[State],a.[UpdateDate],
												CASE
													WHEN @Culture = 0 THEN main.CategoryAM
													WHEN @Culture = 1 THEN main.CategoryRU
													WHEN @Culture = 2 THEN main.CategoryEN
												END as CategoryName
									FROM Ads a 
									JOIN SubCategories AS sub ON sub.ID = a.SubCategoryID
															JOIN MainCategories main ON main.ID = sub.BaseCategoryID
									WHERE a.UserID = @UserID
                                    ORDER BY UpdateDate DESC";

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("@Culture", SqlDbType.Int).Value = (int)culture;
            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = userID;
            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
            {
                if (!reader.HasRows)
                {
                    return new UserAds()
                    {
                        Result = new Result()
                        {
                            Status = CommonConstants.NoContent,
                            Message = CommonConstants.NoContentMessage
                        }
                    };
                }

                int ordCategoryID = reader.GetOrdinal("CategoryID");
                int ordCategoryName = reader.GetOrdinal("CategoryName");
                int ordID = reader.GetOrdinal("ID");
                int ordUserID = reader.GetOrdinal("UserId");
                int ordDescription = reader.GetOrdinal("Description");
                int ordName = reader.GetOrdinal("Name");
                int ordOwner = reader.GetOrdinal("Owner");
                int ordImages = reader.GetOrdinal("Images");
                //Result
                UserAds list = new UserAds();
                int lastCategory = -1;
                List<AdPartDal> adList = null;
                AdPartDal adPart;
                CategoryPartDal categoryPart;

                while (reader.Read())
                {
                    int temp = reader.GetInt32(ordCategoryID);

                    if (lastCategory != temp)
                    {
                        categoryPart = new CategoryPartDal()
                        {
                            ID = temp,
                            Name = reader.GetString(ordCategoryName)
                        };

                        lastCategory = temp;
                        adList = new List<AdPartDal>();

                        list.AdList.Add(new MinimizedAdDal()
                        {
                            Ads = adList,
                            Category = categoryPart
                        });
                    }

                    adPart = new AdPartDal()
                    {
                        ID = reader.GetInt32(ordID),
                        UserID = reader.GetInt32(ordUserID),
                        CategoryID = lastCategory
                    };

                    if (reader.IsDBNull(ordName))
                    {
                        adPart.Name = string.Empty;
                    }
                    else
                    {
                        adPart.Name = reader.GetString(ordName);
                    }

                    if (reader.IsDBNull(ordDescription))
                    {
                        adPart.Description = string.Empty;
                    }
                    else
                    {
                        adPart.Description = reader.GetString(ordDescription);
                    }

                    if (!reader.IsDBNull(ordOwner))
                    {
                        adPart.Owner = reader.GetByte(ordOwner);
                    }

                    if (reader.IsDBNull(ordImages))
                    {
                        adPart.Image = string.Empty;
                    }
                    else
                    {
                        adPart.Image = reader.GetString(ordImages).Split(' ').FirstOrDefault();
                    }

                    adList.Add(adPart);
                }

                list.Result = new Result()
                {
                    Status = CommonConstants.Ok,
                    Message = CommonConstants.OkMessage
                };

                return list;
            }
        }
    }
    #endregion

    #region DeleteAd
    public async Task<Result> DeleteAd(int adID)
    {
        using (SqlConnection con = this.dbService.CreateConnection())
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = con;
            cmd.CommandText = $@"DELETE FROM Ads
                                     WHERE ID = @ID {Environment.NewLine}";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("@ID", SqlDbType.Int).Value = adID;
            if (await cmd.ExecuteNonQueryAsync() > 0)
            {
                return new Result()
                {
                    Status = CommonConstants.Ok,
                    Message = CommonConstants.OkMessage
                };
            }

            return new Result()
            {
                Status = CommonConstants.AdNotExists,
                Message = CommonConstants.AdNotExistsMessage
            };
        }
    }

    #endregion

    #region FavoriteAds 

    public async Task<Result> DeleteAdAtFavorites(FavoriteAdModel favAd)
    {
        using (SqlConnection con = this.dbService.CreateConnection())
        using (SqlCommand cmd = con.CreateCommand())
        {
            cmd.Connection = con;
            cmd.CommandText = $@"DELETE FROM FavoriteAds
									 WHERE UserID = @UserID AND AdID = @AdID {Environment.NewLine}";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = favAd.UserID;
            cmd.Parameters.Add("@AdID", SqlDbType.Int).Value = favAd.AdID;

            if ((await cmd.ExecuteNonQueryAsync()) > 0)
            {
                return new Result()
                {
                    Status = CommonConstants.Ok,
                    Message = CommonConstants.OkMessage
                };
            }

            return new Result()
            {
                Status = CommonConstants.AdNotExists,
                Message = CommonConstants.AdNotExistsMessage
            };
        }
    }

    public async Task<Result> AddAdToFavorites(FavoriteAdModel favAd)
    {
        using (SqlConnection con = this.dbService.CreateConnection())
        using (SqlCommand cmd = con.CreateCommand())
        {
            cmd.Connection = con;
            cmd.CommandText = $@"INSERT INTO FavoriteAds(UserID,AdID)
									 VALUES(@UserID,@AdID) {Environment.NewLine}";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = favAd.UserID;
            cmd.Parameters.Add("@AdID", SqlDbType.Int).Value = favAd.AdID;

            if ((await cmd.ExecuteNonQueryAsync()) > 0)
            {
                return new Result()
                {
                    Status = CommonConstants.Ok,
                    Message = CommonConstants.OkMessage
                };
            }

            return new Result()
            {
                Status = CommonConstants.AdNotExists,
                Message = CommonConstants.AdNotExistsMessage
            };
        }
    }
    #endregion

    #region GetAds
    public async Task<List<WorkDal>> GetWork(WorkGetDAL filter, CultureMode culture)
    {
        List<WorkDal> WorkList = new List<WorkDal>();
        string specificFields = ",[PaymentTime]";
        using (SqlConnection con = this.dbService.CreateConnection())
        using (SqlCommand cmd = CreateSQLCommandForGettingAds("Work", specificFields, filter, culture))
        {
            if (cmd == null)
            {
                return WorkList;
            }

            cmd.Connection = con;
            string conditions = "";
            if (filter.AdPaymentTime.HasValue)
            {
                conditions += " And  [PaymentTime] = @AdPaymentTime";
                cmd.Parameters.Add("@AdPaymentTime", SqlDbType.TinyInt).Value = filter.AdPaymentTime.Value;
            }

            cmd.CommandText += $@"{conditions}
                                      {GetUpdateQuery(filter.ID)}";

            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
            {
                int ordID = 0;
                int ordUserId = 0;
                int ordSubCategoryId = 0;
                int ordMainCategoryId = 0;
                int ordDescription = 0;
                int ordName = 0;
                int ordDate = 0;
                int ordUpdateDate = 0;
                int ordLatitude = 0;
                int ordLongitude = 0;
                int ordPrice = 0;
                int ordSalePercent = 0;
                int ordSalePrice = 0;
                int ordState = 0;
                int ordContact = 0;
                int ordViews = 0;
                int ordOwner = 0;
                int ordCountry = 0;
                int ordRegion = 0;
                int ordCity = 0;
                int ordCurrency = 0;
                int ordTags = 0;
                int ordImages = 0;
                int ordIsBestPrice = 0;
                int ordIsRegional = 0;
                int ordIsFav = 0;
                int ordPaymentTime = 0;

                if (reader.HasRows)
                {
                    ordID = reader.GetOrdinal("ID");
                    ordUserId = reader.GetOrdinal("UserId");
                    ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                    ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                    ordDescription = reader.GetOrdinal("Description");
                    ordName = reader.GetOrdinal("Name");
                    ordDate = reader.GetOrdinal("Date");
                    ordUpdateDate = reader.GetOrdinal("UpdateDate");
                    ordLatitude = reader.GetOrdinal("Latitude");
                    ordLongitude = reader.GetOrdinal("Longitude");
                    ordPrice = reader.GetOrdinal("Price");
                    ordSalePercent = reader.GetOrdinal("SalePercent");
                    ordSalePrice = reader.GetOrdinal("SalePrice");
                    ordState = reader.GetOrdinal("State");
                    ordContact = reader.GetOrdinal("Contact");
                    ordViews = reader.GetOrdinal("Views");
                    ordOwner = reader.GetOrdinal("Owner");
                    ordCountry = reader.GetOrdinal("Country");
                    ordRegion = reader.GetOrdinal("Region");
                    ordCity = reader.GetOrdinal("City");
                    ordCurrency = reader.GetOrdinal("Currency");
                    ordTags = reader.GetOrdinal("Tags");
                    ordImages = reader.GetOrdinal("Images");
                    ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                    ordIsRegional = reader.GetOrdinal("IsRegional");
                    ordIsFav = reader.GetOrdinal("IsFav");
                    ordPaymentTime = reader.GetOrdinal("PaymentTime");

                    while (await reader.ReadAsync())
                    {
                        WorkDal obj = new WorkDal()
                        {
                            ID = reader.GetInt32(ordID),
                            UserID = reader.GetInt32(ordUserId),
                            SubCategoryID = reader.GetInt32(ordSubCategoryId),
                            MainCategoryID = reader.GetInt32(ordMainCategoryId),
                            Description = reader.GetString(ordDescription),
                            Name = reader.GetString(ordName),
                            CreationDate = reader.GetDateTime(ordDate),
                            UpdateDate = reader.GetDateTime(ordUpdateDate),
                            Price = reader.GetDecimal(ordPrice),
                            SalePercent = reader.GetDecimal(ordSalePercent),
                            SalePrice = reader.GetDecimal(ordSalePrice),
                            State = (AdState)reader.GetByte(ordState),
                            Contact = reader.GetString(ordContact),
                            View = Math.Round(reader.GetDecimal(ordViews)),
                            CountryName = reader.GetString(ordCountry),
                            RegionName = reader.GetString(ordRegion),
                            CityName = reader.GetString(ordCity),
                            Currency = (Currency)reader.GetByte(ordCurrency),
                            Tags = reader.GetString(ordTags),
                            ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                            IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                            IsRegional = reader.GetBoolean(ordIsRegional),
                            IsFavorite = reader.GetBoolean(ordIsFav),
                        };

                        if (!reader.IsDBNull(ordLatitude))
                        {
                            obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                        }

                        if (!reader.IsDBNull(ordLongitude))
                        {
                            obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                        }

                        if (!reader.IsDBNull(ordOwner))
                        {
                            obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                        }

                        if (!reader.IsDBNull(ordPaymentTime))
                        {
                            obj.PaymentTime = (AdPaymentTime)reader.GetByte(ordPaymentTime);
                        }

                        WorkList.Add(obj);
                    }
                }

                return WorkList;
            }
        }
    }

    public async Task<List<SportDal>> GetSport(SportGetDAL filter, CultureMode culture)
    {
        List<SportDal> sportList = new List<SportDal>();
        string specificFields = ",ProductState";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("Sport", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return sportList;
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordAim = 0;
                    int ordProductState = 0;
                    int ordIsFav = 0;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        ordAim = reader.GetOrdinal("Aim");
                        ordProductState = reader.GetOrdinal("ProductState");

                        while (await reader.ReadAsync())
                        {
                            SportDal obj = new SportDal()
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav),
                                Aim = (Aim)reader.GetByte(ordAim),
                                ProductState = (ProductState)reader.GetByte(ordProductState)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            sportList.Add(obj);
                        }
                    }

                    return sportList;
                }
            }
        }
    }

    public async Task<List<SaleDal>> GetSale(SaleGetDAL filter, CultureMode culture)
    {
        List<SaleDal> saleList = new List<SaleDal>();
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = con.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;

                string cultureDependedColumns;
                switch (culture)
                {
                    case CultureMode.AM:
                        cultureDependedColumns = ",[TypeAM],c.[CountryAM] as Country,r.[RegionAM] as Region,city.[CityAM] as City";
                        break;
                    case CultureMode.RU:
                        cultureDependedColumns = ",[TypeRU],c.[CountryRU] as Country,r.[RegionRU] as Region,city.[CityRU] as City";
                        break;
                    case CultureMode.EN:
                        cultureDependedColumns = ",[TypeEN],c.[CountryEN] as Country,r.[RegionEN] as Region,city.[CityEN] as City";
                        break;
                    default:
                        cultureDependedColumns = string.Empty;
                        break;
                }

                cmd.CommandText = $@"SELECT * FROM
                                        (SELECT TOP(@AdCount) c.ID as CountryID,r.ID as RegionID,city.ID as CityID,ROW_NUMBER() OVER(ORDER BY a.ID) as rowNumber,a.[ID],a.[UserId],[SubCategoryId]{cultureDependedColumns},a.[Description],a.[Name],
                                            a.[Date],a.[UpdateDate],CAST(a.[Location].Lat AS DECIMAL(15,12)) AS Latitude,CAST(a.[Location].Long AS DECIMAL(15,12)) AS Longitude,
                                            a.[Price],a.[SalePercent],a.[SalePrice],a.[State],a.[Contact],a.[Views],a.[Owner],a.[Currency],
                                            a.[Tags],a.[Images],a.[Aim],a.[IsBestPrice],a.[IsRegional],ISNULL(FAD.UserID,-1) AS IsFav,m.[ID] as MainCategoryID
                                        FROM [Ads] a with (nolock) 
                                            JOIN [SubCategories] ON a.SubCategoryId = [SubCategories].ID 
											JOIN [MainCategories] m ON m.ID = [SubCategories].BaseCategoryId
                                            JOIN [Country] c ON c.ID = a.Country
                                            JOIN [Region] r ON r.ID = Region
                                            JOIN [City] city ON city.ID = City 
                                            LEFT JOIN [FavoriteAds] FAD ON FAD.UserID = @UserID {Environment.NewLine}
										WHERE a.SalePercent > 0{Environment.NewLine}";

                cmd.Parameters.Add("@AdCount", SqlDbType.Int).Value = filter.AdCount.Value * filter.PageNumber.Value;
                cmd.CommandText += $@" ) AS Result 
                                   WHERE 1 = 1 {Environment.NewLine}";

                string conditions = string.Empty;
                if (!filter.ID.HasValue)
                {
                    if (filter.PageNumber.HasValue)
                    {
                        cmd.CommandText += $" AND (rowNumber > @BeginRowNum AND rowNumber <= @EndRowNum) {Environment.NewLine}";
                        cmd.Parameters.Add("@BeginRowNum", SqlDbType.Int).Value = filter.AdCount * (filter.PageNumber - 1);
                        cmd.Parameters.Add("@EndRowNum", SqlDbType.Int).Value = filter.AdCount * filter.PageNumber;
                    }

                    if (filter.SubCategoryID.HasValue)
                    {
                        cmd.CommandText += $" AND ( SubCategoryId = @SubCategoryID ) {Environment.NewLine}";
                        cmd.Parameters.Add("@SubCategoryID", SqlDbType.Int).Value = filter.SubCategoryID;
                    }

                    if (filter.Country.HasValue)
                    {
                        conditions += $@" AND CountryID = @CountryID ";
                        cmd.Parameters.Add("@CountryID", SqlDbType.Int).Value = filter.Country;
                    }

                    if (filter.Region.HasValue)
                    {
                        conditions += " AND RegionID = @Region";
                        cmd.Parameters.Add("@Region", SqlDbType.Int).Value = filter.Region;
                    }

                    if (filter.City.HasValue)
                    {
                        conditions += " AND CityID = @City";
                        cmd.Parameters.Add("@City", SqlDbType.Int).Value = filter.City;
                    }

                    if (filter.OnlyWithPhotos)
                    {
                        conditions += " AND [Images] <> ''";
                    }

                    if (filter.Aim.HasValue)
                    {
                        conditions += " AND [Aim] = @Aim";
                        cmd.Parameters.Add("@Aim", SqlDbType.TinyInt).Value = filter.Aim;
                    }

                    if (filter.AdOwner.HasValue)
                    {
                        conditions += " AND [Owner] = @AdOwner";
                        cmd.Parameters.Add("@AdOwner", SqlDbType.TinyInt).Value = filter.AdOwner;
                    }

                    if (filter.Currency.HasValue)
                    {
                        conditions += " AND [Currency] = @Currency";
                        cmd.Parameters.Add("@Currency", SqlDbType.TinyInt).Value = filter.Currency;
                    }

                    if (filter.SalePriceFrom.HasValue)
                    {
                        conditions += " AND [SalePrice] >= @SalePriceFrom";
                        cmd.Parameters.Add("@SalePriceFrom", SqlDbType.Money).Value = filter.SalePriceFrom;
                    }

                    if (filter.SalePriceTo.HasValue)
                    {
                        conditions += " AND [SalePrice] <= @SalePriceTo";
                        cmd.Parameters.Add("@SalePriceTo", SqlDbType.Money).Value = filter.SalePriceTo;
                    }

                    if (filter.SalePercentFrom.HasValue)
                    {
                        conditions += " AND [SalePercent] >= @SalePercentFrom";
                        cmd.Parameters.Add("@SalePercentFrom", SqlDbType.Money).Value = filter.SalePercentFrom;
                    }

                    if (filter.SalePercentTo.HasValue)
                    {
                        conditions += " AND [SalePercent] <= @SalePercentTo";
                        cmd.Parameters.Add("@SalePercentTo", SqlDbType.Money).Value = filter.SalePercentTo;
                    }

                    if (filter.ProductState.HasValue)
                    {
                        conditions += " AND [ProductState] = @ProductState";
                        cmd.Parameters.Add("@ProductState", SqlDbType.TinyInt).Value = filter.ProductState;
                    }
                }

                cmd.CommandText += $@"{conditions}
                                          {GetUpdateQuery(filter.ID)}";

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordMainCategoryID = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordMainCategoryID = reader.GetOrdinal("MainCategoryID");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        while (await reader.ReadAsync())
                        {
                            SaleDal obj = new SaleDal()
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            saleList.Add(obj);
                        }
                    }

                    return saleList;
                }
            }
        }
    }

    public async Task<List<EverythingElseDal>> GetEverythingElse(EverythingElseGetDAL filter, CultureMode culture)
    {
        List<EverythingElseDal> elseList = new List<EverythingElseDal>();
        string specificFields = "";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("EverythingElse", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return elseList;
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;
                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        while (await reader.ReadAsync())
                        {
                            EverythingElseDal obj = new EverythingElseDal()
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            elseList.Add(obj);
                        }
                    }

                    return elseList;
                }
            }
        }
    }

    public async Task<List<PetsAndPlantsDal>> GetPetsAndPlants(PetsAndPlantsGetDAL filter, CultureMode culture)
    {
        List<PetsAndPlantsDal> petsAndPlantsList = new List<PetsAndPlantsDal>();
        string specificFields = ",ProductState";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("PetsAndPlants", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return petsAndPlantsList;
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;
                    int ordAim = 0;
                    int ordProductState = 0;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        ordAim = reader.GetOrdinal("Aim");
                        ordProductState = reader.GetOrdinal("ProductState");

                        while (await reader.ReadAsync())
                        {
                            PetsAndPlantsDal obj = new PetsAndPlantsDal()
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav),
                                Aim = (Aim)reader.GetByte(ordAim),
                                ProductState = (ProductState)reader.GetByte(ordProductState)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            petsAndPlantsList.Add(obj);
                        }
                    }

                    return petsAndPlantsList;
                }
            }
        }
    }

    public async Task<List<CultureDal>> GetCulture(CultureGetDAL filter, CultureMode culture)
    {
        List<CultureDal> cultureList = new List<CultureDal>();
        string specificFields = ",ProductState";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("Culture", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return cultureList;
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;
                    int ordAim = 0;
                    int ordProductState = 0;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        ordAim = reader.GetOrdinal("Aim");
                        ordProductState = reader.GetOrdinal("ProductState");
                        while (await reader.ReadAsync())
                        {
                            CultureDal obj = new CultureDal()
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav),
                                Aim = (Aim)reader.GetByte(ordAim),
                                ProductState = (ProductState)reader.GetByte(ordProductState)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            cultureList.Add(obj);
                        }
                    }

                    return cultureList;
                }
            }
        }
    }

    public async Task<List<HealfCareDal>> GetHealfCare(HealfCareGetDAL filter, CultureMode culture)
    {
        List<HealfCareDal> healfcareList = new List<HealfCareDal>();
        string specificFields = ",ProductState";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("HealfCare", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return healfcareList;
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;
                    int ordAim = 0;
                    int ordProductState = 0;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        ordAim = reader.GetOrdinal("Aim");
                        ordProductState = reader.GetOrdinal("ProductState");
                        while (await reader.ReadAsync())
                        {
                            HealfCareDal obj = new HealfCareDal()
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav),
                                Aim = (Aim)reader.GetByte(ordAim),
                                ProductState = (ProductState)reader.GetByte(ordProductState)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            healfcareList.Add(obj);
                        }
                    }

                    return healfcareList;
                }
            }
        }
    }

    public async Task<List<FurnitureDal>> GetFurniture(FurnitureGetDAL filter, CultureMode culture)
    {
        List<FurnitureDal> furnitureList = new List<FurnitureDal>();
        string specificFields = ",ProductState";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("Furniture", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return furnitureList;
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;
                    int ordAim = 0;
                    int ordProductState = 0;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        ordAim = reader.GetOrdinal("Aim");
                        ordProductState = reader.GetOrdinal("ProductState");
                        while (await reader.ReadAsync())
                        {
                            FurnitureDal obj = new FurnitureDal()
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav),
                                Aim = (Aim)reader.GetByte(ordAim),
                                ProductState = (ProductState)reader.GetByte(ordProductState)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            furnitureList.Add(obj);
                        }
                    }

                    return furnitureList;
                }
            }
        }
    }

    public async Task<List<AllForHomeAndGardenDal>> GetForHomeAndGarden(ForHomeAndGardenGetDAL filter, CultureMode culture)
    {
        List<AllForHomeAndGardenDal> forHomeAndGarden = new List<AllForHomeAndGardenDal>();
        string specificFields = ",ProductState";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("AllForHomeAndGarden", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return forHomeAndGarden;
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;
                    int ordAim = 0;
                    int ordProductState = 0;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        ordAim = reader.GetOrdinal("Aim");
                        ordProductState = reader.GetOrdinal("ProductState");
                        while (await reader.ReadAsync())
                        {
                            AllForHomeAndGardenDal obj = new AllForHomeAndGardenDal()
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav),
                                Aim = (Aim)reader.GetByte(ordAim),
                                ProductState = (ProductState)reader.GetByte(ordProductState)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            forHomeAndGarden.Add(obj);
                        }
                    }

                    return forHomeAndGarden;
                }
            }
        }
    }

    public async Task<List<HouseholdGoodDal>> GetHouseholdGoods(HouseholdGoodGetDAL filter, CultureMode culture)
    {
        List<HouseholdGoodDal> householdGoodsList = new List<HouseholdGoodDal>();
        string specificFields = ",ProductState";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("HouseholdGoods", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return householdGoodsList;
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;
                    int ordAim = 0;
                    int ordProductState = 0;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        ordAim = reader.GetOrdinal("Aim");
                        ordProductState = reader.GetOrdinal("ProductState");
                        while (await reader.ReadAsync())
                        {
                            HouseholdGoodDal obj = new HouseholdGoodDal()
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav),
                                Aim = (Aim)reader.GetByte(ordAim),
                                ProductState = (ProductState)reader.GetByte(ordProductState)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            householdGoodsList.Add(obj);
                        }
                    }

                    return householdGoodsList;
                }
            }
        }
    }

    public async Task<List<ConstructionDal>> GetConstructions(ConstructionGetDAL filter, CultureMode culture)
    {
        List<ConstructionDal> constructionList = new List<ConstructionDal>();
        string specificFields = ",ProductState";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("Construction", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return constructionList;
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;
                    int ordAim = 0;
                    int ordProductState = 0;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        ordAim = reader.GetOrdinal("Aim");
                        ordProductState = reader.GetOrdinal("ProductState");
                        while (await reader.ReadAsync())
                        {
                            ConstructionDal obj = new ConstructionDal()
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav),
                                Aim = (Aim)reader.GetByte(ordAim),
                                ProductState = (ProductState)reader.GetByte(ordProductState)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            constructionList.Add(obj);
                        }
                    }

                    return constructionList;
                }
            }
        }
    }

    public async Task<List<ElectronicsDal>> GetElectronics(ElectronicGetDAL filter, CultureMode culture)
    {
        List<ElectronicsDal> electronicList = new List<ElectronicsDal>();
        string specificFields = ",ProductState";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("Electronics", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return electronicList;
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;
                    int ordAim = 0;
                    int ordProductState = 0;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        ordAim = reader.GetOrdinal("Aim");
                        ordProductState = reader.GetOrdinal("ProductState");
                        while (await reader.ReadAsync())
                        {
                            ElectronicsDal obj = new ElectronicsDal()
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav),
                                Aim = (Aim)reader.GetByte(ordAim),
                                ProductState = (ProductState)reader.GetByte(ordProductState)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            electronicList.Add(obj);
                        }
                    }

                    return electronicList;
                }
            }
        }
    }

    public async Task<List<AcquaintanceDal>> GetAcquaintance(AcquaintanceGetDAL filter, CultureMode culture)
    {
        List<AcquaintanceDal> acquaintanceList = new List<AcquaintanceDal>();
        string specificFields = ",Gender,AcquaintanceAim,Height,Weight,Age";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("Acquaintance", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return acquaintanceList;
                }

                if (filter.Gender.HasValue)
                {
                    cmd.CommandText += $" and Gender = @Gender {Environment.NewLine}";
                    cmd.Parameters.Add("@Gender", SqlDbType.Bit).Value = filter.Gender.Value;
                }

                if (filter.AcquintanceAim.HasValue)
                {
                    cmd.CommandText += $" and AcquintanceAim = @AcquintanceAim {Environment.NewLine}";
                    cmd.Parameters.Add("@AcquintanceAim", SqlDbType.Bit).Value = filter.AcquintanceAim.Value;
                }

                if (filter.Height.HasValue)
                {
                    cmd.CommandText += $" and Height <= @Height {Environment.NewLine}";
                    cmd.Parameters.Add("@Height", SqlDbType.Int).Value = filter.Height.Value;
                }

                if (filter.Weight.HasValue)
                {
                    cmd.CommandText += $" and Weight <= @Weight {Environment.NewLine}";
                    cmd.Parameters.Add("@Weight", SqlDbType.Int).Value = filter.Weight.Value;
                }

                if (filter.AgeFrom.HasValue)
                {
                    cmd.CommandText += $" and Age >= @AgeFrom {Environment.NewLine}";
                    cmd.Parameters.Add("@AgeFrom", SqlDbType.Int).Value = filter.AgeFrom.Value;
                }

                if (filter.AgeTo.HasValue)
                {
                    cmd.CommandText += $" and Age <= @AgeTo {Environment.NewLine}";
                    cmd.Parameters.Add("@AgeTo", SqlDbType.Int).Value = filter.AgeTo.Value;
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;
                    int ordAcquintanceAim = 0;
                    int ordGender = 0;
                    int ordHeight = 0;
                    int ordWeight = 0;
                    int ordAge = 0;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        ordAcquintanceAim = reader.GetOrdinal("AcquaintanceAim");
                        ordGender = reader.GetOrdinal("Gender");
                        ordHeight = reader.GetOrdinal("Height");
                        ordWeight = reader.GetOrdinal("Weight");
                        ordAge = reader.GetOrdinal("Age");

                        while (await reader.ReadAsync())
                        {
                            AcquaintanceDal obj = new AcquaintanceDal
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav),
                                Gender = reader.GetBoolean(ordGender),
                                Age = reader.GetInt32(ordAge)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            if (!reader.IsDBNull(ordAcquintanceAim))
                            {
                                obj.AcquaintanceAim = (AcquaintanceAim)reader.GetByte(ordAcquintanceAim);
                            }

                            if (!reader.IsDBNull(ordHeight))
                            {
                                obj.Height = reader.GetInt32(ordHeight);
                            }

                            if (!reader.IsDBNull(ordWeight))
                            {
                                obj.Weight = reader.GetInt32(ordWeight);
                            }

                            acquaintanceList.Add(obj);
                        }
                    }

                    return acquaintanceList;
                }
            }
        }
    }

    public async Task<List<ApplianceDal>> GetAppliance(ApplianceGetDAL filter, CultureMode culture)
    {
        List<ApplianceDal> applianceList = new List<ApplianceDal>();
        string specificFields = ",ProductState";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("Appliances", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return applianceList;
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;
                    int ordAim = 0;
                    int ordProductState = 0;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        ordAim = reader.GetOrdinal("Aim");
                        ordProductState = reader.GetOrdinal("ProductState");

                        while (await reader.ReadAsync())
                        {
                            ApplianceDal obj = new ApplianceDal()
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav),
                                Aim = (Aim)reader.GetByte(ordAim),
                                ProductState = (ProductState)reader.GetByte(ordProductState)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            applianceList.Add(obj);
                        }
                    }

                    return applianceList;
                }
            }
        }
    }

    public async Task<List<RealEstateDal>> GetRealEstate(RealEstateGetDAL filter, CultureMode culture)
    {
        List<RealEstateDal> realEstateList = new List<RealEstateDal>();
        string specificFields = ",ConstructionType,PaymentTime,Rooms,Space,Floor";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("RealEstate", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return realEstateList;
                }

                if (filter.ConstructionType.HasValue)
                {
                    cmd.CommandText += $" and ConstructionType = @ConstructionType {Environment.NewLine}";
                    cmd.Parameters.Add("@ConstructionType", SqlDbType.Bit).Value = filter.ConstructionType.Value;
                }

                if (filter.PaymentTime.HasValue)
                {
                    cmd.CommandText += $" and PaymentTime = @PaymentTime {Environment.NewLine}";
                    cmd.Parameters.Add("@PaymentTime", SqlDbType.TinyInt).Value = filter.PaymentTime.Value;
                }

                if (filter.Rooms.HasValue)
                {
                    cmd.CommandText += $" and Rooms = @Rooms {Environment.NewLine}";
                    cmd.Parameters.Add("@Rooms", SqlDbType.TinyInt).Value = filter.Rooms.Value;
                }

                if (filter.SpaceFrom.HasValue)
                {
                    cmd.CommandText += $" and Space >= @SpaceFrom {Environment.NewLine}";
                    cmd.Parameters.Add("@SpaceFrom", SqlDbType.SmallInt).Value = filter.SpaceFrom.Value;
                }

                if (filter.SpaceTo.HasValue)
                {
                    cmd.CommandText += $" and Space <= @SpaceTo {Environment.NewLine}";
                    cmd.Parameters.Add("@SpaceTo", SqlDbType.SmallInt).Value = filter.SpaceTo.Value;
                }

                if (filter.FloorFrom.HasValue)
                {
                    cmd.CommandText += $" and Floor >= @FloorFrom {Environment.NewLine}";
                    cmd.Parameters.Add("@FloorFrom", SqlDbType.TinyInt).Value = filter.FloorFrom.Value;
                }

                if (filter.FloorTo.HasValue)
                {
                    cmd.CommandText += $" and Floor <= @FloorTo {Environment.NewLine}";
                    cmd.Parameters.Add("@FloorTo", SqlDbType.TinyInt).Value = filter.FloorTo.Value;
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;
                    int ordAim = 0;
                    int ordConstructionType = 0;
                    int ordPaymentTime = 0;
                    int ordRooms = 0;
                    int ordSpace = 0;
                    int ordFloor = 0;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        ordAim = reader.GetOrdinal("Aim");
                        ordConstructionType = reader.GetOrdinal("ConstructionType");
                        ordPaymentTime = reader.GetOrdinal("PaymentTime");
                        ordRooms = reader.GetOrdinal("Rooms");
                        ordSpace = reader.GetOrdinal("Space");
                        ordFloor = reader.GetOrdinal("Floor");
                        while (await reader.ReadAsync())
                        {
                            RealEstateDal obj = new RealEstateDal
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav),
                                Aim = (Aim)reader.GetByte(ordAim),
                                Space = reader.GetInt16(ordSpace)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            if (!reader.IsDBNull(ordConstructionType))
                            {
                                obj.ConstructionType = (ConstructionType)reader.GetByte(ordConstructionType);
                            }

                            if (!reader.IsDBNull(ordPaymentTime))
                            {
                                obj.PaymentTime = (AdPaymentTime)reader.GetByte(ordPaymentTime);
                            }

                            if (!reader.IsDBNull(ordRooms))
                            {
                                obj.Rooms = (sbyte)reader.GetByte(ordRooms);
                            }

                            if (!reader.IsDBNull(ordFloor))
                            {
                                obj.Floor = (sbyte)reader.GetByte(ordFloor);
                            }

                            realEstateList.Add(obj);
                        }
                    }

                    return realEstateList;
                }
            }
        }
    }

    public async Task<List<VehicleDal>> GetVehicle(VehicleGetDAL filter, CultureMode culture)
    {
        List<VehicleDal> vehicleList = new List<VehicleDal>();
        string specificFields = @",ProductionYear,Mileage,CustomsCleared,es.ID as EngineSizeID,es.Size  as EngineSize,vm.ID as MarkID,vm.Name as SMark,vmod.ID as ModelID,vmod.Name as SModel,
                                        bt.ID as BodyTypeID,et.ID as EngineTypeID,dt.ID as DriveTypeID,tt.ID as TransmissionTypeID,clr.ID as ColorID,wt.ID as WheelID ";

        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("Vehicle", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return vehicleList;
                }

                if (filter.Mark.HasValue)
                {
                    cmd.CommandText += $" and Mark = @Mark {Environment.NewLine}";
                    cmd.Parameters.Add("@Mark", SqlDbType.Int).Value = filter.Mark.Value;
                }

                if (filter.Model.HasValue)
                {
                    cmd.CommandText += $" and Model = @Model {Environment.NewLine}";
                    cmd.Parameters.Add("@Model", SqlDbType.Int).Value = filter.Model.Value;
                }

                if (filter.ProductionYearFrom.HasValue)
                {
                    cmd.CommandText += $" and ProductionYear >= @ProductionYearFrom {Environment.NewLine}";
                    cmd.Parameters.Add("@ProductionYearFrom", SqlDbType.Date).Value = filter.ProductionYearFrom.Value;
                }

                if (filter.ProductionYearTo.HasValue)
                {
                    cmd.CommandText += $" and ProductionYear <= @ProductionYearTo {Environment.NewLine}";
                    cmd.Parameters.Add("@ProductionYearTo", SqlDbType.Date).Value = filter.ProductionYearTo.Value;
                }

                if (filter.MileageFrom.HasValue)
                {
                    cmd.CommandText += $" and Mileage >= @MileageFrom {Environment.NewLine}";
                    cmd.Parameters.Add("@MileageFrom", SqlDbType.Int).Value = filter.MileageFrom.Value;
                }

                if (filter.MileageTo.HasValue)
                {
                    cmd.CommandText += $" and Mileage <= @MileageTo {Environment.NewLine}";
                    cmd.Parameters.Add("@MileageTo", SqlDbType.Int).Value = filter.MileageTo.Value;
                }

                if (filter.CustomsCleared.HasValue)
                {
                    cmd.CommandText += $" and CustomsCleared = 1 {Environment.NewLine}";
                }

                if (filter.BodyType.HasValue)
                {
                    cmd.CommandText += $" and BodyType = @BodyType {Environment.NewLine}";
                    cmd.Parameters.Add("@BodyType", SqlDbType.Int).Value = filter.BodyType.Value;
                }

                if (filter.EngineType.HasValue)
                {
                    cmd.CommandText += $" and EngineType = @EngineType {Environment.NewLine}";
                    cmd.Parameters.Add("@EngineType", SqlDbType.Int).Value = filter.EngineType.Value;
                }

                if (filter.EngineSizeFrom.HasValue)
                {
                    cmd.CommandText += $" and EngineSize >= @EngineSizeFrom {Environment.NewLine}";
                    cmd.Parameters.Add("@EngineSizeFrom", SqlDbType.Int).Value = filter.EngineSizeFrom.Value;
                }

                if (filter.EngineSizeTo.HasValue)
                {
                    cmd.CommandText += $" and EngineSize <= @EngineSizeTo {Environment.NewLine}";
                    cmd.Parameters.Add("@EngineSizeTo", SqlDbType.Int).Value = filter.EngineSizeTo.Value;
                }

                if (filter.DriveType.HasValue)
                {
                    cmd.CommandText += $" and DriveType = @DriveType {Environment.NewLine}";
                    cmd.Parameters.Add("@DriveType", SqlDbType.Int).Value = filter.DriveType.Value;
                }

                if (filter.TransmissionType.HasValue)
                {
                    cmd.CommandText += $" and TransmissionType = @TransmissionType {Environment.NewLine}";
                    cmd.Parameters.Add("@TransmissionType", SqlDbType.Int).Value = filter.TransmissionType.Value;
                }

                if (filter.Color.HasValue)
                {
                    cmd.CommandText += $" and Color = @Color {Environment.NewLine}";
                    cmd.Parameters.Add("@Color", SqlDbType.Int).Value = filter.Color.Value;
                }

                if (filter.Wheel.HasValue)
                {
                    cmd.CommandText += $" and Wheel = @Wheel {Environment.NewLine}";
                    cmd.Parameters.Add("@Wheel", SqlDbType.Int).Value = filter.Wheel.Value;
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                try
                {
                    using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        int ordID = 0;
                        int ordUserId = 0;
                        int ordSubCategoryId = 0;
                        int ordMainCategoryId = 0;
                        int ordDescription = 0;
                        int ordName = 0;
                        int ordDate = 0;
                        int ordUpdateDate = 0;
                        int ordLatitude = 0;
                        int ordLongitude = 0;
                        int ordPrice = 0;
                        int ordSalePercent = 0;
                        int ordSalePrice = 0;
                        int ordState = 0;
                        int ordContact = 0;
                        int ordViews = 0;
                        int ordOwner = 0;
                        int ordCountry = 0;
                        int ordRegion = 0;
                        int ordCity = 0;
                        int ordCurrency = 0;
                        int ordTags = 0;
                        int ordImages = 0;
                        int ordIsBestPrice = 0;
                        int ordIsRegional = 0;
                        int ordIsFav = 0;
                        int ordAim = 0;
                        int ordMarkID = 0;
                        int ordModelID = 0;
                        int ordMark = 0;
                        int ordModel = 0;
                        int ordProductionYear = 0;
                        int ordMileage = 0;
                        int ordCustomsCleared = 0;
                        int ordBodyType = 0;
                        int ordBodyTypeID = 0;
                        int ordEngineType = 0;
                        int ordEngineTypeID = 0;
                        int ordEngineSize = 0;
                        int ordEngineSizeID = 0;
                        int ordDriveType = 0;
                        int ordDriveTypeID = 0;
                        int ordTransmissionType = 0;
                        int ordTransmissionTypeID = 0;
                        int ordColor = 0;
                        int ordColorID = 0;
                        int ordWheel = 0;
                        int ordWheelID = 0;

                        if (reader.HasRows)
                        {
                            ordID = reader.GetOrdinal("ID");
                            ordUserId = reader.GetOrdinal("UserId");
                            ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                            ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                            ordDescription = reader.GetOrdinal("Description");
                            ordName = reader.GetOrdinal("Name");
                            ordDate = reader.GetOrdinal("Date");
                            ordUpdateDate = reader.GetOrdinal("UpdateDate");
                            ordLatitude = reader.GetOrdinal("Latitude");
                            ordLongitude = reader.GetOrdinal("Longitude");
                            ordPrice = reader.GetOrdinal("Price");
                            ordSalePercent = reader.GetOrdinal("SalePercent");
                            ordSalePrice = reader.GetOrdinal("SalePrice");
                            ordState = reader.GetOrdinal("State");
                            ordContact = reader.GetOrdinal("Contact");
                            ordViews = reader.GetOrdinal("Views");
                            ordOwner = reader.GetOrdinal("Owner");
                            ordCountry = reader.GetOrdinal("Country");
                            ordRegion = reader.GetOrdinal("Region");
                            ordCity = reader.GetOrdinal("City");
                            ordCurrency = reader.GetOrdinal("Currency");
                            ordTags = reader.GetOrdinal("Tags");
                            ordImages = reader.GetOrdinal("Images");
                            ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                            ordIsRegional = reader.GetOrdinal("IsRegional");
                            ordIsFav = reader.GetOrdinal("IsFav");
                            ordAim = reader.GetOrdinal("Aim");
                            ordMark = reader.GetOrdinal("SMark");
                            ordModel = reader.GetOrdinal("SModel");
                            ordMarkID = reader.GetOrdinal("MarkID");
                            ordModelID = reader.GetOrdinal("ModelID");
                            ordProductionYear = reader.GetOrdinal("ProductionYear");
                            ordMileage = reader.GetOrdinal("Mileage");
                            ordCustomsCleared = reader.GetOrdinal("CustomsCleared");
                            ordBodyType = reader.GetOrdinal("SBodyType");
                            ordBodyTypeID = reader.GetOrdinal("BodyTypeID");
                            ordEngineType = reader.GetOrdinal("SEngineType");
                            ordEngineTypeID = reader.GetOrdinal("EngineTypeID");
                            ordEngineSize = reader.GetOrdinal("EngineSize");
                            ordEngineSizeID = reader.GetOrdinal("EngineSizeID");
                            ordDriveType = reader.GetOrdinal("SDriveType");
                            ordDriveTypeID = reader.GetOrdinal("DriveTypeID");
                            ordTransmissionType = reader.GetOrdinal("STransmission");
                            ordTransmissionTypeID = reader.GetOrdinal("TransmissionTypeID");
                            ordColor = reader.GetOrdinal("SColor");
                            ordColorID = reader.GetOrdinal("ColorID");
                            ordWheel = reader.GetOrdinal("SWheel");
                            ordWheelID = reader.GetOrdinal("WheelID");
                            while (await reader.ReadAsync())
                            {
                                VehicleDal obj = new VehicleDal()
                                {
                                    ID = reader.GetInt32(ordID),
                                    UserID = reader.GetInt32(ordUserId),
                                    SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                    MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                    Description = reader.GetString(ordDescription),
                                    Name = reader.GetString(ordName),
                                    CreationDate = reader.GetDateTime(ordDate),
                                    UpdateDate = reader.GetDateTime(ordUpdateDate),
                                    Price = reader.GetDecimal(ordPrice),
                                    SalePercent = reader.GetDecimal(ordSalePercent),
                                    SalePrice = reader.GetDecimal(ordSalePrice),
                                    State = (AdState)reader.GetByte(ordState),
                                    Contact = reader.GetString(ordContact),
                                    View = Math.Round(reader.GetDecimal(ordViews)),
                                    CountryName = reader.GetString(ordCountry),
                                    RegionName = reader.GetString(ordRegion),
                                    CityName = reader.GetString(ordCity),
                                    Currency = (Currency)reader.GetByte(ordCurrency),
                                    Tags = reader.GetString(ordTags),
                                    ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                    IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                    IsRegional = reader.GetBoolean(ordIsRegional),
                                    IsFavorite = reader.GetBoolean(ordIsFav),
                                    Aim = (Aim)reader.GetByte(ordAim)
                                };

                                if (!reader.IsDBNull(ordLatitude))
                                {
                                    obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                                }

                                if (!reader.IsDBNull(ordLongitude))
                                {
                                    obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                                }

                                if (!reader.IsDBNull(ordOwner))
                                {
                                    obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                                }

                                if (!reader.IsDBNull(ordMark))
                                {
                                    obj.SMark = reader.GetString(ordMark);
                                    obj.Mark = reader.GetInt32(ordMarkID);
                                }

                                if (!reader.IsDBNull(ordModel))
                                {
                                    obj.SModel = reader.GetString(ordModel);
                                    obj.Model = reader.GetInt32(ordModelID);
                                }

                                if (!reader.IsDBNull(ordProductionYear))
                                {
                                    obj.ProductionYear = reader.GetDateTime(ordProductionYear);
                                }

                                if (!reader.IsDBNull(ordMileage))
                                {
                                    obj.Mileage = reader.GetInt32(ordMileage);
                                }

                                if (!reader.IsDBNull(ordCustomsCleared))
                                {
                                    obj.CustomsCleared = reader.GetBoolean(ordCustomsCleared);
                                }

                                if (!reader.IsDBNull(ordBodyType))
                                {
                                    obj.SBodyType = reader.GetString(ordBodyType);
                                    obj.BodyType = reader.GetInt32(ordBodyTypeID);
                                }

                                if (!reader.IsDBNull(ordEngineType))
                                {
                                    obj.SEngineType = reader.GetString(ordEngineType);
                                    obj.EngineType = reader.GetInt32(ordEngineTypeID);
                                }

                                if (!reader.IsDBNull(ordEngineSize))
                                {
                                    obj.DEngineSize = reader.GetDecimal(ordEngineSize);
                                    obj.EngineSize = reader.GetInt32(ordEngineSizeID);
                                }

                                if (!reader.IsDBNull(ordDriveType))
                                {
                                    obj.SDriveType = reader.GetString(ordDriveType);
                                    obj.DriveType = reader.GetInt32(ordDriveTypeID);
                                }

                                if (!reader.IsDBNull(ordTransmissionType))
                                {
                                    obj.STransmissionType = reader.GetString(ordTransmissionType);
                                    obj.TransmissionType = reader.GetInt32(ordTransmissionTypeID);
                                }

                                if (!reader.IsDBNull(ordColor))
                                {
                                    obj.SColor = reader.GetString(ordColor);
                                    obj.Color = reader.GetInt32(ordColorID);
                                }

                                if (!reader.IsDBNull(ordWheel))
                                {
                                    obj.SWheel = reader.GetString(ordWheel);
                                    obj.Wheel = reader.GetInt32(ordWheelID);
                                }

                                vehicleList.Add(obj);
                            }
                        }

                        return vehicleList;
                    }
                }
                catch (Exception)
                {
                    return new List<VehicleDal>();
                }
            }
        }
    }

    public async Task<List<ClothesAndShoesDal>> GetClothesAndShoes(ClothesAndShoesGetDAL filter, CultureMode culture)
    {
        List<ClothesAndShoesDal> clothesAndShoesList = new List<ClothesAndShoesDal>();
        string specificFields = ",ProductState,ClothingSize,ShoesSize";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("ClothesAndShoes", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return clothesAndShoesList;
                }

                if (!string.IsNullOrWhiteSpace(filter.ClothingSize))
                {
                    cmd.CommandText += $" and ClothingSize = @ClothingSize {Environment.NewLine}";
                    cmd.Parameters.Add("@ClothingSize", SqlDbType.VarChar).Value = filter.ClothingSize;
                }

                if (filter.ShoesSize.HasValue)
                {
                    cmd.CommandText += $" and ShoesSize = @ShoesSize {Environment.NewLine}";
                    cmd.Parameters.Add("@ShoesSize", SqlDbType.TinyInt).Value = filter.ShoesSize.Value;
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;
                    int ordAim = 0;
                    int ordProductState = 0;
                    int ordClothingSize = 0;
                    int ordShoesSize = 0;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        ordAim = reader.GetOrdinal("Aim");
                        ordProductState = reader.GetOrdinal("ProductState");
                        ordClothingSize = reader.GetOrdinal("ClothingSize");
                        ordShoesSize = reader.GetOrdinal("ShoesSize");
                        while (await reader.ReadAsync())
                        {
                            ClothesAndShoesDal obj = new ClothesAndShoesDal()
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav),
                                Aim = (Aim)reader.GetByte(ordAim),
                                ProductState = (ProductState)reader.GetByte(ordProductState)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            if (!reader.IsDBNull(ordClothingSize))
                            {
                                obj.ClothingSize = reader.GetString(ordClothingSize);
                            }

                            if (!reader.IsDBNull(ordShoesSize))
                            {
                                obj.ShoesSize = (sbyte)reader.GetByte(ordShoesSize);
                            }

                            clothesAndShoesList.Add(obj);
                        }
                    }

                    return clothesAndShoesList;
                }
            }
        }
    }

    public async Task<List<ForChildrenDal>> GetForChildren(ForChildrenGetDAL filter, CultureMode culture)
    {
        List<ForChildrenDal> forChildrenList = new List<ForChildrenDal>();
        string specificFields = ",ProductState,Gender,ForNewBorns";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("ForChildren", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return forChildrenList;
                }

                if (filter.Gender.HasValue)
                {
                    cmd.CommandText += $" and Gender = @Gender {Environment.NewLine}";
                    cmd.Parameters.Add("@Gender", SqlDbType.Bit).Value = filter.Gender.Value;
                }

                if (filter.ForNewBorns.HasValue)
                {
                    cmd.CommandText += $" and ForNewBorns = 1 {Environment.NewLine}";
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;
                    int ordAim = 0;
                    int ordProductState = 0;
                    int ordGender = 0;
                    int ordForNewborns = 0;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        ordAim = reader.GetOrdinal("Aim");
                        ordProductState = reader.GetOrdinal("ProductState");
                        ordGender = reader.GetOrdinal("Gender");
                        ordForNewborns = reader.GetOrdinal("ForNewborns");
                        while (await reader.ReadAsync())
                        {
                            ForChildrenDal obj = new ForChildrenDal()
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav),
                                Aim = (Aim)reader.GetByte(ordAim),
                                ProductState = (ProductState)reader.GetByte(ordProductState)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            if (!reader.IsDBNull(ordGender))
                            {
                                obj.Gender = reader.GetBoolean(ordGender);
                            }

                            if (!reader.IsDBNull(ordForNewborns))
                            {
                                obj.ForNewBorns = reader.GetBoolean(ordForNewborns);
                            }

                            forChildrenList.Add(obj);
                        }
                    }

                    return forChildrenList;
                }
            }
        }
    }

    public async Task<List<JewerlyAndAccessoriesDal>> GetJewerlyAndAccessories(JewerlyAndAccessoriesGetDAL filter, CultureMode culture)
    {
        List<JewerlyAndAccessoriesDal> jewerlyList = new List<JewerlyAndAccessoriesDal>();
        string specificFields = ",ProductState,Gender";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("JewerlyAndAccessories", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return jewerlyList;
                }

                if (filter.Gender.HasValue)
                {
                    cmd.CommandText += $" and Gender = @Gender {Environment.NewLine}";
                    cmd.Parameters.Add("@Gender", SqlDbType.Bit).Value = filter.Gender.Value;
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;
                    int ordAim = 0;
                    int ordProductState = 0;
                    int ordGender = 0;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        ordAim = reader.GetOrdinal("Aim");
                        ordProductState = reader.GetOrdinal("ProductState");
                        ordGender = reader.GetOrdinal("Gender");
                        while (await reader.ReadAsync())
                        {
                            JewerlyAndAccessoriesDal obj = new JewerlyAndAccessoriesDal()
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav),
                                Aim = (Aim)reader.GetByte(ordAim),
                                ProductState = (ProductState)reader.GetByte(ordProductState)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            if (!reader.IsDBNull(ordGender))
                            {
                                obj.Gender = reader.GetBoolean(ordGender);
                            }

                            jewerlyList.Add(obj);
                        }
                    }

                    return jewerlyList;
                }
            }
        }
    }

    public async Task<List<ProductsAndDrinksDal>> GetProductsAndDrinks(ProductsAndDrinksGetDAL filter, CultureMode culture)
    {
        List<ProductsAndDrinksDal> productsAndDrinksList = new List<ProductsAndDrinksDal>();
        string specificFields = ",IsLocal,IsFrozen,IsNaturalDrink";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("ProductsAndDrinks", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return productsAndDrinksList;
                }

                if (filter.IsLocal.HasValue)
                {
                    cmd.CommandText += $" and IsLocal = @IsLocal {Environment.NewLine}";
                    cmd.Parameters.Add("@IsLocal", SqlDbType.Bit).Value = filter.IsLocal.Value;
                }

                if (filter.IsFrozen.HasValue)
                {
                    cmd.CommandText += $" and IsFrozen = @IsFrozen {Environment.NewLine}";
                    cmd.Parameters.Add("@IsFrozen", SqlDbType.Bit).Value = filter.IsFrozen.Value;
                }

                if (filter.IsNaturalDrink.HasValue)
                {
                    cmd.CommandText += $" and IsNaturalDrink = @IsNaturalDrink {Environment.NewLine}";
                    cmd.Parameters.Add("@IsNaturalDrink", SqlDbType.Bit).Value = filter.IsNaturalDrink.Value;
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;
                    int ordAim = 0;
                    int ordIsLocal = 0;
                    int ordIsFrozen = 0;
                    int ordIsNaturalDrink = 0;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        ordAim = reader.GetOrdinal("Aim");
                        ordIsLocal = reader.GetOrdinal("IsLocal");
                        ordIsFrozen = reader.GetOrdinal("IsFrozen");
                        ordIsNaturalDrink = reader.GetOrdinal("IsNaturalDrink");
                        while (await reader.ReadAsync())
                        {
                            ProductsAndDrinksDal obj = new ProductsAndDrinksDal()
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav),
                                Aim = (Aim)reader.GetByte(ordAim)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.IsLocal = reader.GetBoolean(ordIsLocal);
                            }

                            if (!reader.IsDBNull(ordIsFrozen))
                            {
                                obj.IsFrozen = reader.GetBoolean(ordIsFrozen);
                            }

                            if (!reader.IsDBNull(ordIsNaturalDrink))
                            {
                                obj.IsNaturalDrink = reader.GetBoolean(ordIsNaturalDrink);
                            }

                            productsAndDrinksList.Add(obj);
                        }
                    }

                    return productsAndDrinksList;
                }
            }
        }
    }

    public async Task<List<CigaretteAndAlcoholDal>> GetCigaretteAndAlcohol(CigaretteAndAlcoholGetDAL filter, CultureMode culture)
    {
        List<CigaretteAndAlcoholDal> cigaretteAndAlcoholList = new List<CigaretteAndAlcoholDal>();
        string specificFields = ",ProductState,IsLocal,AlcoholVolume";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("CigaretteAndAlcohol", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return cigaretteAndAlcoholList;
                }

                if (filter.IsLocal.HasValue)
                {
                    cmd.CommandText += $" and IsLocal = @IsLocal {Environment.NewLine}";
                    cmd.Parameters.Add("@IsLocal", SqlDbType.Bit).Value = filter.IsLocal.Value;
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;
                    int ordAim = 0;
                    int ordProductState = 0;
                    int ordIsLocal = 0;
                    int ordAlcoholVolume = 0;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        ordAim = reader.GetOrdinal("Aim");
                        ordProductState = reader.GetOrdinal("ProductState");
                        ordIsLocal = reader.GetOrdinal("IsLocal");
                        ordAlcoholVolume = reader.GetOrdinal("AlcoholVolume");

                        while (await reader.ReadAsync())
                        {
                            CigaretteAndAlcoholDal obj = new CigaretteAndAlcoholDal()
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav),
                                Aim = (Aim)reader.GetByte(ordAim)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            if (!reader.IsDBNull(ordProductState))
                            {
                                obj.ProductState = (ProductState)reader.GetByte(ordProductState);
                            }

                            if (!reader.IsDBNull(ordIsLocal))
                            {
                                obj.IsLocal = reader.GetBoolean(ordIsLocal);
                            }

                            if (!reader.IsDBNull(ordAlcoholVolume))
                            {
                                obj.AlcoholVolume = reader.GetByte(ordAlcoholVolume);
                            }

                            cigaretteAndAlcoholList.Add(obj);
                        }
                    }

                    return cigaretteAndAlcoholList;
                }
            }
        }
    }

    public async Task<List<TourismAndRestDal>> GetTourismAndRest(TourismAndRestGetDAL filter, CultureMode culture)
    {
        List<TourismAndRestDal> tourismAndRestList = new List<TourismAndRestDal>();
        string specificFields = ",DepartureDay,ReturnDay,ReservedTickets";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("TourismAndRest", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return tourismAndRestList;
                }

                if (filter.DepartureDay.HasValue)
                {
                    cmd.CommandText += $" and DepartureDay = @DepartureDay {Environment.NewLine}";
                    cmd.Parameters.Add("@DepartureDay", SqlDbType.DateTime).Value = filter.DepartureDay.Value;
                }

                if (filter.ReturnDay.HasValue)
                {
                    cmd.CommandText += $" and ReturnDay = @ReturnDay {Environment.NewLine}";
                    cmd.Parameters.Add("@ReturnDay", SqlDbType.DateTime).Value = filter.ReturnDay.Value;
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;
                    int ordAim = 0;
                    int ordDepartureDay = 0;
                    int ordReturnDay = 0;
                    int ordReservedTickets = 0;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        ordAim = reader.GetOrdinal("Aim");
                        ordDepartureDay = reader.GetOrdinal("DepartureDay");
                        ordReturnDay = reader.GetOrdinal("ReturnDay");
                        ordReservedTickets = reader.GetOrdinal("ReservedTickets");
                        while (await reader.ReadAsync())
                        {
                            TourismAndRestDal obj = new TourismAndRestDal()
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav),
                                Aim = (Aim)reader.GetByte(ordAim)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            if (!reader.IsDBNull(ordDepartureDay))
                            {
                                obj.DepartureDay = reader.GetDateTime(ordDepartureDay);
                            }

                            if (!reader.IsDBNull(ordReturnDay))
                            {
                                obj.ReturnDay = reader.GetDateTime(ordReturnDay);
                            }

                            if (!reader.IsDBNull(ordReservedTickets))
                            {
                                obj.ReservedTickets = reader.GetInt16(ordReservedTickets);
                            }

                            tourismAndRestList.Add(obj);
                        }
                    }

                    return tourismAndRestList;
                }
            }
        }
    }

    public async Task<List<ServicesDal>> GetServices(ServicesGetDAL filter, CultureMode culture)
    {
        List<ServicesDal> servicesList = new List<ServicesDal>();
        string specificFields = ",PaymentTime,Transportation";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("Services", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return servicesList;
                }

                if (filter.PaymentTime.HasValue)
                {
                    cmd.CommandText += $" and PaymentTime = @PaymentTime {Environment.NewLine}";
                    cmd.Parameters.Add("@PaymentTime", SqlDbType.TinyInt).Value = filter.PaymentTime.Value;
                }

                if (filter.Transportation.HasValue)
                {
                    cmd.CommandText += $" and Transportation = @Transportation {Environment.NewLine}";
                    cmd.Parameters.Add("@Transportation", SqlDbType.Bit).Value = filter.Transportation.Value;
                }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;
                    int ordPaymentTime = 0;
                    int ordTransportation = 0;

                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        ordPaymentTime = reader.GetOrdinal("PaymentTime");
                        ordTransportation = reader.GetOrdinal("Transportation");
                        while (await reader.ReadAsync())
                        {
                            ServicesDal obj = new ServicesDal()
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav),
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            if (!reader.IsDBNull(ordPaymentTime))
                            {
                                obj.PaymentTime = (AdPaymentTime)reader.GetByte(ordPaymentTime);
                            }

                            if (!reader.IsDBNull(ordTransportation))
                            {
                                obj.Transportation = reader.GetBoolean(ordTransportation);
                            }

                            servicesList.Add(obj);
                        }
                    }

                    return servicesList;
                }
            }
        }
    }

    public async Task<List<ExchangeDal>> GetExchange(ExchangeGetDAL filter, CultureMode culture)
    {
        List<ExchangeDal> exchnageList = new List<ExchangeDal>();
        string specificFields = ", [SourceCurrencyID],[DestinationCurrencyID],[SaleSummaRetail]" +
            "                    , [BuySummaRetail],[SaleSumma],[BuySumma] ";
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = CreateSQLCommandForGettingAds("Exchange", specificFields, filter, culture))
            {
                if (cmd == null)
                {
                    return exchnageList;
                }
                //TODO : Raf Sargsyan
                //  if (filter.IsLocal.HasValue)
                //  {
                //      cmd.CommandText += $" and IsLocal = @IsLocal {Environment.NewLine}";
                //      cmd.Parameters.Add("@IsLocal", SqlDbType.Bit).Value = filter.IsLocal.Value;
                //  }

                cmd.Connection = con;
                cmd.CommandText += GetUpdateQuery(filter.ID);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordID = 0;
                    int ordUserId = 0;
                    int ordSubCategoryId = 0;
                    int ordMainCategoryId = 0;
                    int ordDescription = 0;
                    int ordName = 0;
                    int ordDate = 0;
                    int ordUpdateDate = 0;
                    int ordLatitude = 0;
                    int ordLongitude = 0;
                    int ordPrice = 0;
                    int ordSalePercent = 0;
                    int ordSalePrice = 0;
                    int ordState = 0;
                    int ordContact = 0;
                    int ordViews = 0;
                    int ordOwner = 0;
                    int ordCountry = 0;
                    int ordRegion = 0;
                    int ordCity = 0;
                    int ordCurrency = 0;
                    int ordTags = 0;
                    int ordImages = 0;
                    int ordIsBestPrice = 0;
                    int ordIsRegional = 0;
                    int ordIsFav = 0;
                    int ordSourceCurrencyID = 0;
                    int ordDestinationCurrencyID = 0;
                    int ordSaleSummaRetail = 0;
                    int ordBuySummaRetail = 0;
                    int ordSaleSumma = 0;
                    int ordBuySumma = 0;


                    if (reader.HasRows)
                    {
                        ordID = reader.GetOrdinal("ID");
                        ordUserId = reader.GetOrdinal("UserId");
                        ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                        ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                        ordDescription = reader.GetOrdinal("Description");
                        ordName = reader.GetOrdinal("Name");
                        ordDate = reader.GetOrdinal("Date");
                        ordUpdateDate = reader.GetOrdinal("UpdateDate");
                        ordLatitude = reader.GetOrdinal("Latitude");
                        ordLongitude = reader.GetOrdinal("Longitude");
                        ordPrice = reader.GetOrdinal("Price");
                        ordSalePercent = reader.GetOrdinal("SalePercent");
                        ordSalePrice = reader.GetOrdinal("SalePrice");
                        ordState = reader.GetOrdinal("State");
                        ordContact = reader.GetOrdinal("Contact");
                        ordViews = reader.GetOrdinal("Views");
                        ordOwner = reader.GetOrdinal("Owner");
                        ordCountry = reader.GetOrdinal("Country");
                        ordRegion = reader.GetOrdinal("Region");
                        ordCity = reader.GetOrdinal("City");
                        ordCurrency = reader.GetOrdinal("Currency");
                        ordTags = reader.GetOrdinal("Tags");
                        ordImages = reader.GetOrdinal("Images");
                        ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                        ordIsRegional = reader.GetOrdinal("IsRegional");
                        ordIsFav = reader.GetOrdinal("IsFav");
                        ordSourceCurrencyID = reader.GetOrdinal("SourceCurrencyID");
                        ordDestinationCurrencyID = reader.GetOrdinal("DestinationCurrencyID");
                        ordSaleSummaRetail = reader.GetOrdinal("SaleSummaRetail");
                        ordBuySummaRetail = reader.GetOrdinal("BuySummaRetail");
                        ordSaleSumma = reader.GetOrdinal("SaleSumma");
                        ordBuySumma = reader.GetOrdinal("BuySumma");
                        while (await reader.ReadAsync())
                        {
                            ExchangeDal obj = new ExchangeDal()
                            {
                                ID = reader.GetInt32(ordID),
                                UserID = reader.GetInt32(ordUserId),
                                SubCategoryID = reader.GetInt32(ordSubCategoryId),
                                MainCategoryID = reader.GetInt32(ordMainCategoryId),
                                Description = reader.GetString(ordDescription),
                                Name = reader.GetString(ordName),
                                CreationDate = reader.GetDateTime(ordDate),
                                UpdateDate = reader.GetDateTime(ordUpdateDate),
                                Price = reader.GetDecimal(ordPrice),
                                SalePercent = reader.GetDecimal(ordSalePercent),
                                SalePrice = reader.GetDecimal(ordSalePrice),
                                State = (AdState)reader.GetByte(ordState),
                                Contact = reader.GetString(ordContact),
                                View = Math.Round(reader.GetDecimal(ordViews)),
                                CountryName = reader.GetString(ordCountry),
                                RegionName = reader.GetString(ordRegion),
                                CityName = reader.GetString(ordCity),
                                Currency = (Currency)reader.GetByte(ordCurrency),
                                Tags = reader.GetString(ordTags),
                                ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                                IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                                IsRegional = reader.GetBoolean(ordIsRegional),
                                IsFavorite = reader.GetBoolean(ordIsFav),
                                SourceCurrencyID = reader.GetInt16(ordSourceCurrencyID),
                                DestinationCurrencyID = reader.GetInt16(ordDestinationCurrencyID),
                                SaleSummaRetail = reader.GetDecimal(ordSaleSummaRetail),
                                BuySummaRetail = reader.GetDecimal(ordBuySummaRetail)
                            };

                            if (!reader.IsDBNull(ordLatitude))
                            {
                                obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                            }

                            if (!reader.IsDBNull(ordLongitude))
                            {
                                obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                            }

                            if (!reader.IsDBNull(ordOwner))
                            {
                                obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                            }

                            if (!reader.IsDBNull(ordSaleSumma))
                            {
                                obj.SaleSumma = reader.GetDecimal(ordSaleSumma);
                            }

                            if (!reader.IsDBNull(ordBuySumma))
                            {
                                obj.BuySumma = reader.GetDecimal(ordBuySumma);
                            }

                            exchnageList.Add(obj);
                        }
                    }

                    return exchnageList;
                }
            }
        }
    }

    public async Task<BestExchangeList> GetBestExchanges(CultureMode culture)
    {
        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = con.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;

                string cultureDependedColumns = string.Empty;
                switch (culture)
                {
                    case CultureMode.AM:
                        cultureDependedColumns = ",[TypeAM],c.[CountryAM] as Country,r.[RegionAM] as Region,city.[CityAM] as City";
                        break;
                    case CultureMode.RU:
                        cultureDependedColumns = ",[TypeRU],c.[CountryRU] as Country,r.[RegionRU] as Region,city.[CityRU] as City";
                        break;
                    case CultureMode.EN:
                        cultureDependedColumns = ",[TypeEN],c.[CountryEN] as Country,r.[RegionEN] as Region,city.[CityEN] as City";
                        break;
                }

                cmd.CommandText = $@"SELECT c.ID as CountryID,r.ID as RegionID,city.ID as CityID,a.[ID],a.[UserId],[SubCategoryId],m.ID as MainCategoryId,a.[Description],a.[Name],
                                            a.[Date],a.[UpdateDate],CAST(a.[Location].Lat AS DECIMAL(15, 12)) AS Latitude, CAST(a.[Location].Long AS DECIMAL(15, 12)) AS Longitude,
                                               a.[Price],a.[SalePercent],a.[SalePrice],a.[State],a.[Contact],a.[Views],a.[Owner],a.[Currency],
                                            a.[Tags],a.[Images],a.[Aim],a.[IsBestPrice],a.[IsRegional],ISNULL(FAD.UserID,-1) AS IsFav,[SourceCurrencyID],[DestinationCurrencyID],[SaleSummaRetail],
                                            [BuySummaRetail],[SaleSumma],[BuySumma]{cultureDependedColumns}
                                        FROM Exchange with(nolock) JOIN[Ads] a with(nolock) ON a.ID = Exchange.AdID
                                                         JOIN[SubCategories] s ON a.SubCategoryId = s.ID
                                                         JOIN[MainCategories] m ON s.BaseCategoryID = m.ID
                                                         JOIN[Country] c ON c.ID = a.Country
                                                         JOIN[Region] r ON r.ID = Region
                                                         JOIN[City] city ON city.ID = City
                                                         LEFT JOIN [FavoriteAds] FAD ON FAD.UserID = @UserID {Environment.NewLine}
                                        WHERE a.IsBestPrice = 1";

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    if (!reader.HasRows)
                    {
                        return new BestExchangeList()
                        {
                            Result = new Result()
                            {
                                Status = CommonConstants.NoContent,
                                Message = CommonConstants.NoContentMessage
                            }
                        };
                    }

                    int ordID = reader.GetOrdinal("ID");
                    int ordUserId = reader.GetOrdinal("UserId");
                    int ordSubCategoryId = reader.GetOrdinal("SubCategoryId");
                    int ordMainCategoryId = reader.GetOrdinal("MainCategoryId");
                    int ordDescription = reader.GetOrdinal("Description");
                    int ordName = reader.GetOrdinal("Name");
                    int ordDate = reader.GetOrdinal("Date");
                    int ordUpdateDate = reader.GetOrdinal("UpdateDate");
                    int ordLatitude = reader.GetOrdinal("Latitude");
                    int ordLongitude = reader.GetOrdinal("Longitude");
                    int ordPrice = reader.GetOrdinal("Price");
                    int ordSalePercent = reader.GetOrdinal("SalePercent");
                    int ordSalePrice = reader.GetOrdinal("SalePrice");
                    int ordState = reader.GetOrdinal("State");
                    int ordContact = reader.GetOrdinal("Contact");
                    int ordViews = reader.GetOrdinal("Views");
                    int ordOwner = reader.GetOrdinal("Owner");
                    int ordCountry = reader.GetOrdinal("Country");
                    int ordRegion = reader.GetOrdinal("Region");
                    int ordCity = reader.GetOrdinal("City");
                    int ordCurrency = reader.GetOrdinal("Currency");
                    int ordTags = reader.GetOrdinal("Tags");
                    int ordImages = reader.GetOrdinal("Images");
                    int ordIsBestPrice = reader.GetOrdinal("IsBestPrice");
                    int ordIsRegional = reader.GetOrdinal("IsRegional");
                    int ordIsFav = reader.GetOrdinal("IsFav");
                    int ordSourceCurrencyID = reader.GetOrdinal("SourceCurrencyID");
                    int ordDestinationCurrencyID = reader.GetOrdinal("DestinationCurrencyID");
                    int ordSaleSummaRetail = reader.GetOrdinal("SaleSummaRetail");
                    int ordBuySummaRetail = reader.GetOrdinal("BuySummaRetail");
                    int ordSaleSumma = reader.GetOrdinal("SaleSumma");
                    int ordBuySumma = reader.GetOrdinal("BuySumma");
                    BestExchangeList bestExchanges = new BestExchangeList();

                    while (await reader.ReadAsync())
                    {
                        ExchangeDal obj = new ExchangeDal()
                        {
                            ID = reader.GetInt32(ordID),
                            UserID = reader.GetInt32(ordUserId),
                            SubCategoryID = reader.GetInt32(ordSubCategoryId),
                            MainCategoryID = reader.GetInt32(ordMainCategoryId),
                            Description = reader.GetString(ordDescription),
                            Name = reader.GetString(ordName),
                            CreationDate = reader.GetDateTime(ordDate),
                            UpdateDate = reader.GetDateTime(ordUpdateDate),
                            Price = reader.GetDecimal(ordPrice),
                            SalePercent = reader.GetDecimal(ordSalePercent),
                            SalePrice = reader.GetDecimal(ordSalePrice),
                            State = (AdState)reader.GetByte(ordState),
                            Contact = reader.GetString(ordContact),
                            View = Math.Round(reader.GetDecimal(ordViews)),
                            CountryName = reader.GetString(ordCountry),
                            RegionName = reader.GetString(ordRegion),
                            CityName = reader.GetString(ordCity),
                            Currency = (Currency)reader.GetByte(ordCurrency),
                            Tags = reader.GetString(ordTags),
                            ImagesList = reader.GetString(ordImages).Split(' ').ToList(),
                            IsBestPrice = reader.GetBoolean(ordIsBestPrice),
                            IsRegional = reader.GetBoolean(ordIsRegional),
                            IsFavorite = reader.GetBoolean(ordIsFav),
                            SourceCurrencyID = reader.GetInt16(ordSourceCurrencyID),
                            DestinationCurrencyID = reader.GetInt16(ordDestinationCurrencyID),
                            SaleSummaRetail = reader.GetDecimal(ordSaleSummaRetail),
                            BuySummaRetail = reader.GetDecimal(ordBuySummaRetail),
                        };

                        if (!reader.IsDBNull(ordLatitude))
                        {
                            obj.LocationLatitude = reader.GetDecimal(ordLatitude);
                        }

                        if (!reader.IsDBNull(ordLongitude))
                        {
                            obj.LocationLongitude = reader.GetDecimal(ordLongitude);
                        }

                        if (!reader.IsDBNull(ordOwner))
                        {
                            obj.Owner = (AdOwner)reader.GetByte(ordOwner);
                        }

                        if (!reader.IsDBNull(ordSaleSumma))
                        {
                            obj.SaleSumma = reader.GetDecimal(ordSaleSumma);
                        }

                        if (!reader.IsDBNull(ordBuySumma))
                        {
                            obj.BuySumma = reader.GetDecimal(ordBuySumma);
                        }

                        bestExchanges.List.Add(obj);
                    }

                    bestExchanges.Result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    };

                    return bestExchanges;
                }
            }
        }
    }

    private SqlCommand CreateSQLCommandForGettingAds(string tableName, string specificFields, AdGetBaseDAL baseFilters, CultureMode culture)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandType = CommandType.Text;

            string cultureDependedColumns;
            switch (culture)
            {
                case CultureMode.AM:
                    cultureDependedColumns = ",[TypeAM],c.[CountryAM] as Country,r.[RegionAM] as Region,city.[CityAM] as City";
                    break;
                case CultureMode.RU:
                    cultureDependedColumns = ",[TypeRU],c.[CountryRU] as Country,r.[RegionRU] as Region,city.[CityRU] as City";
                    break;
                case CultureMode.EN:
                    cultureDependedColumns = ",[TypeEN],c.[CountryEN] as Country,r.[RegionEN] as Region,city.[CityEN] as City";
                    break;
                default:
                    cultureDependedColumns = string.Empty;
                    break;
            }


            if (tableName.Equals("Vehicle"))
            {
                switch (culture)
                {
                    case CultureMode.AM:
                        cultureDependedColumns += ",BodyTypeAM as SBodyType,EngineTypeAM as SEngineType,DriveTypeAM as SDriveType,TransmissionAM as STransmission,ColorAM as SColor,WheelAM as SWheel";
                        break;
                    case CultureMode.RU:
                        cultureDependedColumns += ",BodyTypeRU as SBodyType,EngineTypeRU as SEngineType,DriveTypeRU as SDriveType,TransmissionRU as STransmission,ColorRU as SColor,WheelRU as SWheel";
                        break;
                    case CultureMode.EN:
                        cultureDependedColumns += ",BodyTypeEN as SBodyType,EngineTypeEN as SEngineType,DriveTypeEN as SDriveType,TransmissionEN as STransmission,ColorEN as SColor,WheelEN as SWheel";
                        break;
                }
            }

            cmd.CommandText = $@"SELECT * FROM
                                        (SELECT TOP(@AdCount) c.ID as CountryID,r.ID as RegionID,city.ID as CityID,ROW_NUMBER() OVER(ORDER BY State DESC,UpdateDate DESC,a.ID) as rowNumber,a.[ID],a.[UserId],[SubCategoryId],m.ID as MainCategoryId{cultureDependedColumns},a.[Description],a.[Name],
                                            a.[Date],a.[UpdateDate],CAST(a.[Location].Lat AS DECIMAL(15,12)) AS Latitude,CAST(a.[Location].Long AS DECIMAL(15,12)) AS Longitude,
                                            a.[Price],a.[SalePercent],a.[SalePrice],a.[State],a.[Contact],a.[Views],a.[Owner],a.[Currency],
                                            a.[Tags],a.[Images],a.[Aim],a.[IsBestPrice],a.[IsRegional],
                                            CAST(CASE FAD.AdID 
                                                WHEN NULL THEN 0
                                                ELSE 1
                                            END AS BIT) AS IsFav {specificFields}
                                        FROM {tableName} with (nolock) JOIN [Ads] a with (nolock) ON a.ID = {tableName}.AdID 
                                                         JOIN [SubCategories] s ON a.SubCategoryId = s.ID 
                                                         JOIN [MainCategories] m ON s.BaseCategoryID = m.ID
                                                         JOIN [Country] c ON c.ID = a.Country
                                                         JOIN [Region] r ON r.ID = Region
                                                         JOIN [City] city ON city.ID = City
                                                         LEFT JOIN [FavoriteAds] FAD ON FAD.UserID = @UserID {Environment.NewLine}";

            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = baseFilters.UserID.HasValue ? baseFilters.UserID.Value : -1;

            if (tableName.Equals("Vehicle"))
            {
                cmd.CommandText += $@"left JOIN Mark vm on vm.ID = Mark 
                                          left JOIN Model vmod on vmod.ID = Model
                                          left JOIN BodyType bt on bt.ID = BodyType 
                                          left JOIN EngineType et on et.ID = EngineType 
                                          left JOIN DriveType dt on dt.ID = DriveType 
                                          left JOIN TransmissionType tt on tt.ID = TransmissionType 
                                          left JOIN Color clr on clr.ID = Color
                                          left JOIN Wheel wt on wt.ID = Wheel 
                                          left JOIN EngineSize es on es.ID = EngineSize{Environment.NewLine}";

            }

            if (baseFilters.ID.HasValue)
            {
                cmd.Parameters.Add("@AdCount", SqlDbType.Int).Value = 1;
                cmd.Parameters.Add("@AdID", SqlDbType.Int).Value = baseFilters.ID.Value;
                cmd.CommandText += $" WHERE a.ID = @AdID {Environment.NewLine}";
            }
            else
            {
                cmd.Parameters.Add("@AdCount", SqlDbType.Int).Value = baseFilters.AdCount.Value * baseFilters.PageNumber.Value;
            }

            cmd.CommandText += $@" ) AS Result 
                                   WHERE 1 = 1 {Environment.NewLine}";

            if (baseFilters.ID.HasValue)
            {
                return cmd;
            }

            string conditions = string.Empty;
            if (baseFilters.PageNumber.HasValue)
            {
                cmd.CommandText += $" AND (rowNumber > @BeginRowNum AND rowNumber <= @EndRowNum) {Environment.NewLine}";
                cmd.Parameters.Add("@BeginRowNum", SqlDbType.Int).Value = baseFilters.AdCount * (baseFilters.PageNumber - 1);
                cmd.Parameters.Add("@EndRowNum", SqlDbType.Int).Value = baseFilters.AdCount * baseFilters.PageNumber;
            }

            if (baseFilters.SubCategoryID.HasValue)
            {
                cmd.CommandText += $" AND ( SubCategoryId = @SubCategoryID ) {Environment.NewLine}";
                cmd.Parameters.Add("@SubCategoryID", SqlDbType.Int).Value = baseFilters.SubCategoryID;
            }

            if (baseFilters.Country.HasValue)
            {
                conditions += $@" AND CountryID = @CountryID ";
                cmd.Parameters.Add("@CountryID", SqlDbType.Int).Value = baseFilters.Country;
            }

            if (baseFilters.Region.HasValue)
            {
                conditions += " AND RegionID = @Region";
                cmd.Parameters.Add("@Region", SqlDbType.Int).Value = baseFilters.Region;
            }

            if (baseFilters.City.HasValue)
            {
                conditions += " AND CityID = @City";
                cmd.Parameters.Add("@City", SqlDbType.Int).Value = baseFilters.City;
            }

            if (baseFilters.OnlyWithPhotos)
            {
                conditions += " AND [Images] <> ''";
            }

            if (baseFilters.Aim.HasValue)
            {
                conditions += " AND [Aim] = @Aim";
                cmd.Parameters.Add("@Aim", SqlDbType.TinyInt).Value = baseFilters.Aim;
            }

            if (baseFilters.AdOwner.HasValue)
            {
                conditions += " AND [Owner] = @AdOwner";
                cmd.Parameters.Add("@AdOwner", SqlDbType.TinyInt).Value = baseFilters.AdOwner;
            }

            if (baseFilters.Currency.HasValue)
            {
                conditions += " AND [Currency] = @Currency";
                cmd.Parameters.Add("@Currency", SqlDbType.TinyInt).Value = baseFilters.Currency;
            }

            if (baseFilters.SalePriceFrom.HasValue)
            {
                conditions += " AND [SalePrice] >= @SalePriceFrom";
                cmd.Parameters.Add("@SalePriceFrom", SqlDbType.Money).Value = baseFilters.SalePriceFrom;
            }

            if (baseFilters.SalePriceTo.HasValue)
            {
                conditions += " AND [SalePrice] <= @SalePriceTo";
                cmd.Parameters.Add("@SalePriceTo", SqlDbType.Money).Value = baseFilters.SalePriceTo;
            }

            if (baseFilters.SalePercentFrom.HasValue)
            {
                conditions += " AND [SalePercent] >= @SalePercentFrom";
                cmd.Parameters.Add("@SalePercentFrom", SqlDbType.Money).Value = baseFilters.SalePercentFrom;
            }

            if (baseFilters.SalePercentTo.HasValue)
            {
                conditions += " AND [SalePercent] <= @SalePercentTo";
                cmd.Parameters.Add("@SalePercentTo", SqlDbType.Money).Value = baseFilters.SalePercentTo;
            }

            if (baseFilters.ProductState.HasValue)
            {
                conditions += " AND [ProductState] = @ProductState";
                cmd.Parameters.Add("@ProductState", SqlDbType.TinyInt).Value = baseFilters.ProductState;
            }

            cmd.CommandText += conditions;
            return cmd;
        }
    }

    private string GetUpdateQuery(int? id)
    {
        if (!id.HasValue)
        {
            return string.Empty;
        }

        return $@"UPDATE Ads
						SET [Views] = [Views] + 1.3 
					WHERE ID = @AdID {Environment.NewLine}";
    }

    public async Task<List<CurrencyDal>> GetCurrencyList()
    {
        List<CurrencyDal> CurrencyList = new List<CurrencyDal>();
        using (SqlConnection con = this.dbService.CreateConnection())
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = con;
            cmd.CommandText = "CurrencyGet";
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    if (!reader.HasRows)
                    {
                        return null;
                    }

                    int ordID = reader.GetOrdinal("ID");
                    int ordCurrency = reader.GetOrdinal("Currency");
                    int ordCurrencySymbol = reader.GetOrdinal("CurrencySymbol");

                    while (reader.Read())
                    {
                        CurrencyDal currency = new CurrencyDal()
                        {
                            ID = reader.GetInt16(ordID),
                            Currency = reader.GetString(ordCurrency),
                            CurrencySymbol = reader.GetString(ordCurrencySymbol)
                        };


                        CurrencyList.Add(currency);
                    }

                    return CurrencyList;
                }
            }
            catch (Exception)
            {
                return new List<CurrencyDal>();
            }
        }
    }
    #endregion

    #region Update

    public async Task<bool> UpdateAd(UniversalDal ad)
    {
        bool specFiledChanged = false;
        string specFieldsConds = string.Empty;

        using (SqlConnection conn = this.dbService.CreateConnection())
        {
            if ((ad.State == AdState.Urgent || ad.State == AdState.Vip) && !(await CheckBalance(conn, new PurchaseTariffDal() { UserID = ad.UserID, TariffID = (int)ad.State })))
            {
                return false;
            }

            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = $@"BEGIN TRANSACTION
                                        DECLARE @TariffID INT,
											  @Amount MONEY,
											  @Balance MONEY  {Environment.NewLine}";
                if (ad.State.HasValue)
                {
                    cmd.CommandText += $@"
                                        IF(@State IN (1,2))
									      BEGIN										
										      SELECT @TariffID = t.ID,@Amount = t.SalePrice, @Balance = u.Balance 
										      FROM Tariffs t join UserData u on t.ID = @State and u.ID = @UserID
									
										      IF(@Balance >= @Amount)
										      BEGIN
											      INSERT INTO TariffPayments([AdID],[UserID],[TariffID],[PaymentDate],[PaymentAmount])
											      VALUES(@ID,@UserID,@TariffID,GETDATE(),@Amount)

											      UPDATE UserData
											      SET Balance = Balance - @Amount
											      WHERE ID = @UserID
										      END
									      END 
                                        UPDATE Ads 
										 SET State = @State {Environment.NewLine}";
                    cmd.Parameters.Add("@State", SqlDbType.TinyInt).Value = ad.State;
                    cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = ad.UserID;
                }
                else
                {
                    cmd.CommandText += $@"UPDATE Ads 
										 SET UserID = UserID {Environment.NewLine}";
                }
                if (ad.Aim.HasValue)
                {
                    cmd.CommandText += $",Aim = @Aim {Environment.NewLine}";
                    cmd.Parameters.Add("@Aim", SqlDbType.TinyInt).Value = ad.Aim.Value;
                }

                if (ad.SubCategoryID.HasValue)
                {
                    cmd.CommandText += $",SubCategoryId = @SubCategoryID {Environment.NewLine}";
                    cmd.Parameters.Add("@SubCategoryID", SqlDbType.Int).Value = ad.SubCategoryID.Value;
                }

                if (ad.Description != null)
                {
                    cmd.CommandText += $",Description = @Description {Environment.NewLine}";
                    cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = ad.Description;
                }

                if (!string.IsNullOrWhiteSpace(ad.Name))
                {
                    cmd.CommandText += $",Name = @Name {Environment.NewLine}";
                    cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = ad.Name;
                }

                if (ad.Price.HasValue)
                {
                    cmd.CommandText += $",Price = @Price {Environment.NewLine}";
                    cmd.Parameters.Add("@Price", SqlDbType.Money).Value = ad.Price.Value;
                }
                if (ad.SalePercent.HasValue)
                {
                    cmd.CommandText += $",SalePercent = @SalePercent {Environment.NewLine}";
                    cmd.Parameters.Add("@SalePercent", SqlDbType.Decimal).Value = ad.SalePercent.Value;
                }

                if (ad.Contact != null)
                {
                    cmd.CommandText += $",Contact = @Contact {Environment.NewLine}";
                    cmd.Parameters.Add("@Contact", SqlDbType.VarChar).Value = ad.Contact;
                }

                if (ad.Owner.HasValue)
                {
                    cmd.CommandText += $",Owner = @Owner {Environment.NewLine}";
                    cmd.Parameters.Add("@Owner", SqlDbType.TinyInt).Value = ad.Owner.Value;
                }

                if (ad.Country.HasValue)
                {
                    cmd.CommandText += $",Country = @Country {Environment.NewLine}";
                    cmd.Parameters.Add("@Country", SqlDbType.Int).Value = ad.Country.Value;
                }

                if (ad.Region.HasValue)
                {
                    cmd.CommandText += $",Region = @Region {Environment.NewLine}";
                    cmd.Parameters.Add("@Region", SqlDbType.Int).Value = ad.Region.Value;
                }

                if (ad.City.HasValue)
                {
                    cmd.CommandText += $",City = @City {Environment.NewLine}";
                    cmd.Parameters.Add("@City", SqlDbType.Int).Value = ad.City.Value;
                }

                if (ad.LocationLatitude.HasValue && ad.LocationLongitude.HasValue)
                {
                    cmd.CommandText += $",[Location] = GEOGRAPHY::Point(@LocationLatitude,@LocationLongitude,{CommonConstants.GeoNum}) {Environment.NewLine}";
                    cmd.Parameters.Add("@LocationLatitude", SqlDbType.Decimal).Value = ad.LocationLatitude.Value;
                    cmd.Parameters.Add("@LocationLongitude", SqlDbType.Decimal).Value = ad.LocationLongitude.Value;
                }

                if (ad.Currency.HasValue)
                {
                    cmd.CommandText += $",Currency = @Currency {Environment.NewLine}";
                    cmd.Parameters.Add("@Currency", SqlDbType.TinyInt).Value = ad.Currency.Value;
                }

                if (ad.Tags != null)
                {
                    cmd.CommandText += $",Tags = @Tags {Environment.NewLine}";
                    cmd.Parameters.Add("@Tags", SqlDbType.NVarChar).Value = ad.Tags;
                }

                if (ad.IsRegional.HasValue)
                {
                    cmd.CommandText += $",IsRegional = @IsRegional {Environment.NewLine}";
                    cmd.Parameters.Add("@IsRegional", SqlDbType.Bit).Value = ad.IsRegional.Value;
                }

                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = ad.ID;
                cmd.Parameters.Add("@DateNow", SqlDbType.SmallDateTime).Value = DateTime.Now;
                cmd.CommandText += $@",UpdateDate = @DateNow
										  WHERE ID = @ID {Environment.NewLine}";


                if (ad.ProductState.HasValue)
                {
                    specFieldsConds += $",ProductState = @ProductState {Environment.NewLine}";
                    cmd.Parameters.Add("@ProductState", SqlDbType.TinyInt).Value = ad.ProductState.Value;
                    specFiledChanged = true;
                }

                if (ad.IsLocal.HasValue)
                {
                    specFieldsConds += $",IsLocal = @IsLocal {Environment.NewLine}";
                    cmd.Parameters.Add("@IsLocal", SqlDbType.Bit).Value = ad.IsLocal.Value;
                    specFiledChanged = true;
                }

                if (ad.AlcoholVolume.HasValue)
                {
                    specFieldsConds += $",AlcoholVolume = @AlcoholVolume {Environment.NewLine}";
                    cmd.Parameters.Add("@AlcoholVolume", SqlDbType.TinyInt).Value = ad.AlcoholVolume.Value;
                    specFiledChanged = true;
                }

                if (ad.ClothingSize != null)
                {
                    specFieldsConds += $",ClothingSize = @ClothingSize {Environment.NewLine}";
                    cmd.Parameters.Add("@ClothingSize", SqlDbType.VarChar).Value = ad.ClothingSize;
                    specFiledChanged = true;
                }

                if (ad.ShoesSize.HasValue)
                {
                    specFieldsConds += $",ShoesSize = @ShoesSize {Environment.NewLine}";
                    cmd.Parameters.Add("@ShoesSize", SqlDbType.TinyInt).Value = ad.ShoesSize.Value;
                    specFiledChanged = true;
                }

                if (ad.Height.HasValue)
                {
                    specFieldsConds += $",Height = @Height {Environment.NewLine}";
                    cmd.Parameters.Add("@Height", SqlDbType.Int).Value = ad.Height.Value;
                    specFiledChanged = true;
                }

                if (ad.Weight.HasValue)
                {
                    specFieldsConds += $",Weight = @Weight {Environment.NewLine}";
                    cmd.Parameters.Add("@Weight", SqlDbType.Int).Value = ad.Weight.Value;
                    specFiledChanged = true;
                }
                if (ad.Age.HasValue)
                {
                    specFieldsConds += $",Age = @Age {Environment.NewLine}";
                    cmd.Parameters.Add("@Age", SqlDbType.Int).Value = ad.Age.Value;
                    specFiledChanged = true;
                }
                if (ad.SourceCurrencyID.HasValue)
                {
                    specFieldsConds += $",SourceCurrencyID = @SourceCurrencyID {Environment.NewLine}";
                    cmd.Parameters.Add("@SourceCurrencyID", SqlDbType.SmallInt).Value = ad.SourceCurrencyID.Value;
                    specFiledChanged = true;
                }

                if (ad.DestinationCurrencyID.HasValue)
                {
                    specFieldsConds += $",DestinationCurrencyID = @DestinationCurrencyID {Environment.NewLine}";
                    cmd.Parameters.Add("@DestinationCurrencyID", SqlDbType.SmallInt).Value = ad.DestinationCurrencyID.Value;
                    specFiledChanged = true;
                }

                if (ad.SaleSummaRetail.HasValue)
                {
                    specFieldsConds += $",SaleSummaRetail = @SaleSummaRetail {Environment.NewLine}";
                    cmd.Parameters.Add("@SaleSummaRetail", SqlDbType.Money).Value = ad.SaleSummaRetail.Value;
                    specFiledChanged = true;
                }

                if (ad.BuySummaRetail.HasValue)
                {
                    specFieldsConds += $",BuySummaRetail = @BuySummaRetail {Environment.NewLine}";
                    cmd.Parameters.Add("@BuySummaRetail", SqlDbType.Money).Value = ad.BuySummaRetail.Value;
                    specFiledChanged = true;
                }

                if (ad.SaleSumma.HasValue)
                {
                    specFieldsConds += $",SaleSumma = @SaleSumma {Environment.NewLine}";
                    cmd.Parameters.Add("@SaleSumma", SqlDbType.Money).Value = ad.SaleSumma.Value;
                    specFiledChanged = true;
                }

                if (ad.BuySumma.HasValue)
                {
                    specFieldsConds += $",BuySumma = @BuySumma {Environment.NewLine}";
                    cmd.Parameters.Add("@BuySumma", SqlDbType.Money).Value = ad.BuySumma.Value;
                    specFiledChanged = true;
                }

                if (ad.Gender.HasValue)
                {
                    specFieldsConds += $",Gender = @Gender {Environment.NewLine}";
                    cmd.Parameters.Add("@Gender", SqlDbType.Bit).Value = ad.Gender.Value;
                    specFiledChanged = true;
                }

                if (ad.AcquaintanceAim.HasValue)
                {
                    specFieldsConds += $",AcquintanceAim = @AcquintanceAim {Environment.NewLine}";
                    cmd.Parameters.Add("@AcquintanceAim", SqlDbType.TinyInt).Value = ad.AcquaintanceAim.Value;
                    specFiledChanged = true;
                }

                if (ad.ForNewBorns.HasValue)
                {
                    specFieldsConds += $",ForNewBorns = @ForNewBorns {Environment.NewLine}";
                    cmd.Parameters.Add("@ForNewBorns", SqlDbType.Bit).Value = ad.ForNewBorns.Value;
                    specFiledChanged = true;
                }

                if (ad.IsFrozen.HasValue)
                {
                    specFieldsConds += $",IsFrozen = @IsFrozen {Environment.NewLine}";
                    cmd.Parameters.Add("@IsFrozen", SqlDbType.Bit).Value = ad.IsFrozen.Value;
                    specFiledChanged = true;
                }

                if (ad.IsNaturalDrink.HasValue)
                {
                    specFieldsConds += $",IsNaturalDrink = @IsNaturalDrink {Environment.NewLine}";
                    cmd.Parameters.Add("@IsNaturalDrink", SqlDbType.Bit).Value = ad.IsNaturalDrink.Value;
                    specFiledChanged = true;
                }

                if (ad.ConstructionType.HasValue)
                {
                    specFieldsConds += $",ConstructionType = @ConstructionType {Environment.NewLine}";
                    cmd.Parameters.Add("@ConstructionType", SqlDbType.TinyInt).Value = ad.ConstructionType.Value;
                    specFiledChanged = true;
                }

                if (ad.PaymentTime.HasValue)
                {
                    specFieldsConds += $",PaymentTime = @PaymentTime {Environment.NewLine}";
                    cmd.Parameters.Add("@PaymentTime", SqlDbType.TinyInt).Value = ad.PaymentTime.Value;
                    specFiledChanged = true;
                }

                if (ad.Rooms.HasValue)
                {
                    specFieldsConds += $",Rooms = @Rooms {Environment.NewLine}";
                    cmd.Parameters.Add("@Rooms", SqlDbType.TinyInt).Value = ad.Rooms.Value;
                    specFiledChanged = true;
                }

                if (ad.Space.HasValue)
                {
                    specFieldsConds += $",Space = @Space {Environment.NewLine}";
                    cmd.Parameters.Add("@Space", SqlDbType.SmallInt).Value = ad.Space.Value;
                    specFiledChanged = true;
                }

                if (ad.Floor.HasValue)
                {
                    specFieldsConds += $",Floor = @Floor {Environment.NewLine}";
                    cmd.Parameters.Add("@Floor", SqlDbType.TinyInt).Value = ad.Floor.Value;
                    specFiledChanged = true;
                }

                if (ad.Transportation.HasValue)
                {
                    specFieldsConds += $",Transportation = @Transportation {Environment.NewLine}";
                    cmd.Parameters.Add("@Transportation", SqlDbType.Bit).Value = ad.Transportation.Value;
                    specFiledChanged = true;
                }

                if (ad.DepartureDay.HasValue)
                {
                    specFieldsConds += $",DepartureDay = @DepartureDay {Environment.NewLine}";
                    cmd.Parameters.Add("@DepartureDay", SqlDbType.DateTime2).Value = ad.DepartureDay.Value;
                    specFiledChanged = true;
                }

                if (ad.ReturnDay.HasValue)
                {
                    specFieldsConds += $",ReturnDay = @ReturnDay {Environment.NewLine}";
                    cmd.Parameters.Add("@ReturnDay", SqlDbType.DateTime2).Value = ad.ReturnDay.Value;
                    specFiledChanged = true;
                }

                if (ad.ReservedTickets.HasValue)
                {
                    specFieldsConds += $",ReservedTickets = @ReservedTickets {Environment.NewLine}";
                    cmd.Parameters.Add("@ReservedTickets", SqlDbType.SmallInt).Value = ad.ReservedTickets.Value;
                    specFiledChanged = true;
                }

                if (ad.Mark.HasValue)
                {
                    specFieldsConds += $",Mark = @Mark {Environment.NewLine}";
                    cmd.Parameters.Add("@Mark", SqlDbType.Int).Value = ad.Mark.Value;
                    specFiledChanged = true;
                }

                if (ad.Model.HasValue)
                {
                    specFieldsConds += $",Model = @Model {Environment.NewLine}";
                    cmd.Parameters.Add("@Model", SqlDbType.Int).Value = ad.Model.Value;
                    specFiledChanged = true;
                }

                if (ad.Mileage.HasValue)
                {
                    specFieldsConds += $",Mileage = @Mileage {Environment.NewLine}";
                    cmd.Parameters.Add("@Mileage", SqlDbType.Int).Value = ad.Mileage.Value;
                    specFiledChanged = true;
                }

                if (ad.CustomsCleared.HasValue)
                {
                    specFieldsConds += $",CustomsCleared = @CustomsCleared {Environment.NewLine}";
                    cmd.Parameters.Add("@CustomsCleared", SqlDbType.Bit).Value = ad.CustomsCleared.Value;
                    specFiledChanged = true;
                }

                if (ad.BodyType.HasValue)
                {
                    specFieldsConds += $",BodyType = @BodyType {Environment.NewLine}";
                    cmd.Parameters.Add("@BodyType", SqlDbType.Int).Value = ad.BodyType.Value;
                    specFiledChanged = true;
                }

                if (ad.EngineType.HasValue)
                {
                    specFieldsConds += $",EngineType = @EngineType {Environment.NewLine}";
                    cmd.Parameters.Add("@EngineType", SqlDbType.Int).Value = ad.EngineType.Value;
                    specFiledChanged = true;
                }

                if (ad.EngineSize.HasValue)
                {
                    specFieldsConds += $",EngineSize = @EngineSize {Environment.NewLine}";
                    cmd.Parameters.Add("@EngineSize", SqlDbType.Int).Value = ad.EngineSize.Value;
                    specFiledChanged = true;
                }

                if (ad.DriveType.HasValue)
                {
                    specFieldsConds += $",DriveType = @DriveType {Environment.NewLine}";
                    cmd.Parameters.Add("@DriveType", SqlDbType.Int).Value = ad.DriveType.Value;
                    specFiledChanged = true;
                }

                if (ad.TransmissionType.HasValue)
                {
                    specFieldsConds += $",TransmissionType = @TransmissionType {Environment.NewLine}";
                    cmd.Parameters.Add("@TransmissionType", SqlDbType.Int).Value = ad.TransmissionType.Value;
                    specFiledChanged = true;
                }

                if (ad.Color.HasValue)
                {
                    specFieldsConds += $",Color = @Color {Environment.NewLine}";
                    cmd.Parameters.Add("@Color", SqlDbType.Int).Value = ad.Color.Value;
                    specFiledChanged = true;
                }

                if (ad.Wheel.HasValue)
                {
                    specFieldsConds += $",Wheel = @Wheel {Environment.NewLine}";
                    cmd.Parameters.Add("@Wheel", SqlDbType.Int).Value = ad.Wheel.Value;
                    specFiledChanged = true;
                }

                if (specFiledChanged)
                {
                    cmd.CommandText += $@" UPDATE {ad.AdType}
											   SET {specFieldsConds.Substring(1)} 
											   WHERE AdID = @ID {Environment.NewLine}";
                }

                cmd.CommandText += $" COMMIT; {Environment.NewLine}";
                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch (Exception ex)
                {
                    throw new Exception($"Invalid field name : {ex.Message}");
                }

                return true;
            }
        }
    }

    public async Task<bool> UpdateAdImages(ImagesUpdateDAL ad)
    {
        if (ad.NewImagesList.Count == 0)
        {
            return false;
        }
        using (SqlConnection conn = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;


                cmd.CommandText = $@"BEGIN TRANSACTION 
                                        Update Ads  
                                        Set Images = @Images
                                        Where ID=@AdID 
                                        COMMIT; {Environment.NewLine} ";
                cmd.Parameters.Add("@Images", SqlDbType.NVarChar).Value = string.Join(" ", ad.NewImagesList);
                cmd.Parameters.Add("@AdID", SqlDbType.Int).Value = ad.AdID;
                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch (Exception)
                {
                    return false;
                }

                return true;
            }
        }
    }
    #endregion

    #region Notification

    public async Task<List<string>> GetNotificationTokens(UniversalDal ad)
    {
        if (ad.Aim.Value != Aim.Buy && ad.Aim.Value != Aim.ForSale)
        {
            return null;
        }

        using (SqlConnection con = this.dbService.CreateConnection())
        {
            using (SqlCommand cmd = con.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;

                if (ad.State.Equals(AdState.Vip))
                {
                    cmd.CommandText = $@"SELECT Token
											 FROM PushNotifications {Environment.NewLine}";
                }
                else
                {
                    string usersFromPreferencesQuery = $@"SELECT UserID 
                                                            FROM Preferences P
                                                            WHERE (1 <> 1) {Environment.NewLine}";

                    if (!string.IsNullOrWhiteSpace(ad.Tags))
                    {
                        string[] tagsArr = ad.Tags.RRTrim().Split(' ');
                        for (int i = 0; i < tagsArr.Length; i++)
                        {
                            usersFromPreferencesQuery += $" OR CHARINDEX('{tagsArr[i]}',P.Tags) > -1{Environment.NewLine}";
                        }
                    }

                    string usersFromAdsQuery = $@"SELECT UserID
                                                    FROM Ads A {Environment.NewLine}";


                    if (ad.AdType.Equals(AdType.Vehicle))
                    {
                        usersFromAdsQuery += $@" JOIN Vehicle V on A.ID = V.AdID 
                                                 WHERE (1 = 1) {Environment.NewLine}";

                        if (ad.Mark.HasValue)
                        {
                            usersFromAdsQuery += $@" AND V.Mark = @Mark {Environment.NewLine}";
                            cmd.Parameters.Add("@Mark", SqlDbType.Int).Value = ad.Mark.Value;
                        }

                        if (ad.Model.HasValue)
                        {
                            usersFromAdsQuery += $@" AND V.Model = @Model {Environment.NewLine}";
                            cmd.Parameters.Add("@Model", SqlDbType.Int).Value = ad.Mark.Value;
                        }

                        if (ad.ProductionYear.HasValue)
                        {
                            usersFromAdsQuery += $@" AND V.ProductionYear = @ProductionYear {Environment.NewLine}";
                            cmd.Parameters.Add("@ProductionYear", SqlDbType.Date).Value = ad.ProductionYear.Value;
                        }

                        if (ad.BodyType.HasValue)
                        {
                            usersFromAdsQuery += $@" AND V.BodyType = @BodyType {Environment.NewLine}";
                            cmd.Parameters.Add("@BodyType", SqlDbType.Int).Value = ad.BodyType.Value;
                        }

                        if (ad.EngineType.HasValue)
                        {
                            usersFromAdsQuery += $@" AND V.EngineType = @EngineType {Environment.NewLine}";
                            cmd.Parameters.Add("@EngineType", SqlDbType.Int).Value = ad.EngineType.Value;
                        }

                        if (ad.EngineSize.HasValue)
                        {
                            usersFromAdsQuery += $@" AND V.EngineSize = @EngineSize {Environment.NewLine}";
                            cmd.Parameters.Add("@EngineSize", SqlDbType.Int).Value = ad.EngineSize.Value;
                        }

                        if (ad.DriveType.HasValue)
                        {
                            usersFromAdsQuery += $@" AND V.DriveType = @DriveType {Environment.NewLine}";
                            cmd.Parameters.Add("@DriveType", SqlDbType.Int).Value = ad.DriveType.Value;
                        }

                        if (ad.TransmissionType.HasValue)
                        {
                            usersFromAdsQuery += $@" AND V.TransmissionType = @TransmissionType {Environment.NewLine}";
                            cmd.Parameters.Add("@TransmissionType", SqlDbType.Int).Value = ad.TransmissionType.Value;
                        }

                        if (ad.Color.HasValue)
                        {
                            usersFromAdsQuery += $@" AND V.Color = @Color {Environment.NewLine}";
                            cmd.Parameters.Add("@Color", SqlDbType.Int).Value = ad.Color.Value;
                        }

                        if (ad.Wheel.HasValue)
                        {
                            usersFromAdsQuery += $@" AND V.Wheel = @Wheel {Environment.NewLine}";
                            cmd.Parameters.Add("@Wheel", SqlDbType.Int).Value = ad.Wheel.Value;
                        }
                    }
                    else if (ad.AdType.Equals(AdType.RealEstate))
                    {
                        usersFromAdsQuery += $@" JOIN RealEstate R on A.ID = R.AdID 
                                                 WHERE (1 = 1) {Environment.NewLine}";

                        if (ad.Space.HasValue)
                        {
                            usersFromAdsQuery += $@" AND R.Space >= @Space {Environment.NewLine}";
                            cmd.Parameters.Add("@Space", SqlDbType.SmallInt).Value = ad.Space.Value;
                        }

                        if (ad.ConstructionType.HasValue)
                        {
                            usersFromAdsQuery += $@" AND R.ConstructionType = @ConstructionType {Environment.NewLine}";
                            cmd.Parameters.Add("@ConstructionType", SqlDbType.TinyInt).Value = ad.ConstructionType.Value;
                        }

                        if (ad.PaymentTime.HasValue)
                        {
                            usersFromAdsQuery += $@" AND R.PaymentTime = @PaymentTime {Environment.NewLine}";
                            cmd.Parameters.Add("@PaymentTime", SqlDbType.TinyInt).Value = ad.PaymentTime.Value;
                        }

                        if (ad.Rooms.HasValue)
                        {
                            usersFromAdsQuery += $@" AND R.Rooms >= @Rooms {Environment.NewLine}";
                            cmd.Parameters.Add("@Rooms", SqlDbType.TinyInt).Value = ad.Rooms.Value;
                        }

                        if (ad.Country.HasValue)
                        {
                            usersFromAdsQuery += $@" AND A.Country = @Country {Environment.NewLine}";
                            cmd.Parameters.Add("@Country", SqlDbType.Int).Value = ad.Country.Value;
                        }

                        if (ad.Region.HasValue)
                        {
                            usersFromAdsQuery += $@" AND A.Region = @Region {Environment.NewLine}";
                            cmd.Parameters.Add("@Region", SqlDbType.Int).Value = ad.Region.Value;
                        }

                        if (ad.City.HasValue)
                        {
                            usersFromAdsQuery += $@" AND A.City = @City {Environment.NewLine}";
                            cmd.Parameters.Add("@City", SqlDbType.Int).Value = ad.City.Value;
                        }
                    }
                    else
                    {
                        usersFromAdsQuery += $" WHERE (1 = 1) {Environment.NewLine}";
                    }

                    usersFromAdsQuery += $@" AND A.Aim = @Aim
                                             AND A.SubCategoryId = @SubCategoryId
                                             AND A.Currency = @Currency
                                             AND (A.SalePrice <= @MaxSalePrice AND A.SalePrice >= @MinSalePrice) {Environment.NewLine}";

                    cmd.Parameters.Add("@Aim", SqlDbType.TinyInt).Value = ad.Aim == Aim.Buy ? Aim.ForSale : Aim.Buy;
                    cmd.Parameters.Add("@SubCategoryId", SqlDbType.Int).Value = ad.SubCategoryID;
                    cmd.Parameters.Add("@Currency", SqlDbType.TinyInt).Value = ad.Currency;
                    cmd.Parameters.Add("@MinSalePrice", SqlDbType.Decimal).Value = ad.Price * (decimal)0.9;
                    cmd.Parameters.Add("@MaxSalePrice", SqlDbType.Decimal).Value = ad.Price * (decimal)1.1;
                    cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = ad.UserID;

                    cmd.CommandText = $@"SELECT DISTINCT P.Token 
                                         FROM 
                                            (
                                                ({usersFromPreferencesQuery})
                                                    UNION ALL
                                                ({usersFromAdsQuery})
                                            ) RES1
                                        JOIN PushNotifications P on RES1.UserID = P.UserID
										WHERE P.UserID <> @UserID {Environment.NewLine}";
                }

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    int ordToken = -1;
                    List<string> userTokens = null;

                    if (reader.HasRows)
                    {
                        userTokens = new List<string>();
                        ordToken = reader.GetOrdinal("Token");
                    }

                    while (reader.Read())
                    {
                        userTokens.Add(reader.GetString(ordToken));
                    }

                    return userTokens;
                }
            }
        }
    }
    public async Task<string> NotificateUser(int adID)
    {
        using (SqlConnection con = this.dbService.CreateConnection())
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = con;
            cmd.CommandText = $@"select top 1 Token 
                                    from PushNotifications p join Ads a on p.UserID = a.UserID
                                    where a.ID = @ID and a.State = 2{Environment.NewLine}";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("@ID", SqlDbType.Int).Value = adID;
            string token = (await cmd.ExecuteScalarAsync())?.ToString();

            return token;
        }
    }
    #endregion

    #region LoaderAd

    public async Task<LoaderAdDAL> GetLoaderAd(CultureMode culture)
    {
        List<LoaderAdDAL> adsList = new List<LoaderAdDAL>();

        using (SqlConnection con = this.dbService.CreateConnection())
        using (SqlCommand cmd = con.CreateCommand())
        {
            string cultureDependedColumns = string.Empty;

            switch (culture)
            {
                case CultureMode.AM:
                    cultureDependedColumns = "CategoryAM";
                    break;
                case CultureMode.RU:
                    cultureDependedColumns = "CategoryRU";
                    break;
                case CultureMode.EN:
                    cultureDependedColumns = "CategoryEN";
                    break;
            }
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $@"SELECT  TOP(5)
										m.{cultureDependedColumns},
										a.ID,
										a.UserID,
										m.ID as MainCategoryID,
										a.[Name],
										a.[Description],
										a.[Images]
									FROM LoaderAds l
									JOIN Ads a on l.AdID = a.ID
									JOIN SubCategories s on a.SubCategoryId = s.ID
									JOIN MainCategories m on m.ID = s.BaseCategoryID
									WHERE a.[Views] <= l.TargetViewCount
									ORDER BY PurchaseDate ASC";



            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
            {
                int ordAdID = 0;
                int ordUserID = 0;
                int ordMainCategoryID = 0;
                int ordMainCategoryName = 0;
                int ordName = 0;
                int ordDescription = 0;
                int ordImages = 0;

                if (reader.HasRows)
                {
                    ordAdID = reader.GetOrdinal("ID");
                    ordUserID = reader.GetOrdinal("UserID");
                    ordMainCategoryID = reader.GetOrdinal("MainCategoryID");
                    ordMainCategoryName = reader.GetOrdinal(cultureDependedColumns);
                    ordName = reader.GetOrdinal("Name");
                    ordDescription = reader.GetOrdinal("Description");
                    ordImages = reader.GetOrdinal("Images");
                }

                while (await reader.ReadAsync())
                {
                    LoaderAdDAL obj = new LoaderAdDAL()
                    {
                        AdID = reader.GetInt32(ordAdID),
                        UserID = reader.GetInt32(ordUserID),
                        MainCategoryID = reader.GetInt32(ordMainCategoryID),
                        MainCategoryName = reader.GetString(ordMainCategoryName),
                        Name = reader.GetString(ordName),
                        Description = reader.GetString(ordDescription),
                        Images = reader.GetString(ordImages).Split(' ').ToList()
                    };

                    adsList.Add(obj);
                }
            }

            if (adsList.Count > 0)
            {
                Random rnd = new Random();
                int rndNumber = rnd.Next(0, adsList.Count);
                cmd.Parameters.Add("@AdID", SqlDbType.Int).Value = adsList[rndNumber].AdID;
                cmd.CommandText = GetUpdateQuery(adsList[rndNumber].AdID);

                if (await cmd.ExecuteNonQueryAsync() == 1)
                {
                    Result result = new Result()
                    {
                        Status = CommonConstants.Ok,
                        Message = CommonConstants.OkMessage
                    };

                    adsList[rndNumber].Result = result;
                    return adsList[rndNumber];
                }
                else
                {
                    return new LoaderAdDAL()
                    {
                        Result = new Result()
                        {
                            Status = CommonConstants.ViewUpdateErr,
                            Message = CommonConstants.ViewUpdateErrMessage
                        }
                    };
                }
            }
            else
            {
                return new LoaderAdDAL()
                {
                    Result = new Result()
                    {
                        Status = CommonConstants.NoLoaderAd,
                        Message = CommonConstants.NoLoaderAdMessage
                    }
                };
            }
        }
    }

    #endregion

    #region Other
    public async Task<bool> CheckBalance(SqlConnection con, PurchaseTariffDal purchBody)
    {
        using (SqlCommand cmd = con.CreateCommand())
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "CheckUserBalance";
            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = purchBody.UserID;
            cmd.Parameters.Add("@TariffID", SqlDbType.Int).Value = purchBody.TariffID;

            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
            {
                if (reader.HasRows)
                {
                    return true;
                }
                return false;
            }
        }
    }

    #endregion
}